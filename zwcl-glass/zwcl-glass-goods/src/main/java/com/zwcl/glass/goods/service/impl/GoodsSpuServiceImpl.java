package com.zwcl.glass.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.common.core.exception.BusinessException;
import com.zwcl.glass.goods.dto.TagSpuDto;
import com.zwcl.glass.goods.entity.GoodsImages;
import com.zwcl.glass.goods.entity.GoodsSpu;
import com.zwcl.glass.goods.mapper.GoodsSpuMapper;
import com.zwcl.glass.goods.service.GoodsBaseService;
import com.zwcl.glass.goods.service.GoodsSpuService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import com.zwcl.glass.goods.vo.GoodsDetailVo;
import com.zwcl.glass.tools.api.feign.ConclusionTagMapClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
@Service
public class GoodsSpuServiceImpl extends GoodsBaseService<GoodsSpuMapper, GoodsSpu> implements GoodsSpuService {


    @Autowired
    private GoodsSpuMapper goodsSpuMapper;

    @Autowired
    private ConclusionTagMapClient conclusionTagMapClient;
    /**
     * 由于没有自定义的id生成器，key采用#cateId的方式会报错
     * @param cateId
     * @return
     */
    @Override
    @Cacheable(value = ZWCL_GLASS_GOODS_SPU_CACHE, key = "#a0")
    public List<GoodsSpu> getSpusById(Integer cateId){
        return this.list(new LambdaQueryWrapper<GoodsSpu>().eq(GoodsSpu::getCateId, cateId));
    }

    @Override
    @Cacheable(value = ZWCL_CONCLUSION_TAG_CACHE, key = "#a0")
    public Map<String,String> getConclusionAndTagMap(String paperCode){
        ApiResult<Map<String,String>> feignResult= conclusionTagMapClient.getConclusionAndTagMap(paperCode);
        if(feignResult.getCode()!=200){
            throw new BusinessException("调用glass-getConclusionAndTagMap失败");
        }
        Map<String,String> conclusionTagMap = feignResult.getData();
        return conclusionTagMap;
    }
    /**
     * 关键字搜索，直接从库中检索
     * @return
     */
    @Override
    public Page<GoodsSpu> getSpusByTag(Page<GoodsSpu> page, TagSpuDto dto) {
        //先搜索出第一个，其他的属性在后续搜索
        return goodsSpuMapper.getSpusByTag(page,dto);
    }

    /**
     * 可能查询效率堪忧，因为有多个标签，并且只能用Like
     * @param tags
     * @return
     */
    @Override
    public List<GoodsSpu> listSpuByTag(String[] tags) {
        return null;
    }

}
