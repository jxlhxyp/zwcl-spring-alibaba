package com.zwcl.glass.goods.vo;

import com.zwcl.glass.goods.entity.GoodsImages;
import com.zwcl.glass.goods.entity.GoodsProperty;
import com.zwcl.glass.goods.entity.GoodsSku;
import com.zwcl.glass.goods.entity.GoodsSpu;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * 商品详细输出
 */
@Data
public class GoodsDetailVo {
    //商品概览
    GoodsSpu goodsSpu;

    //商品有的规格属性
    Map<Integer,List<GoodsPropertyVo>> propertyMap;

    //图片
    List<GoodsImagesVo> imagesList;

    //相关规格属性的价格
    List<GoodsSku> skuList;
}
