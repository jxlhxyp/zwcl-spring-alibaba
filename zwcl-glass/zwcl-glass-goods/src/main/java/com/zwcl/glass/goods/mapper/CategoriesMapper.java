package com.zwcl.glass.goods.mapper;

import com.zwcl.glass.goods.entity.Categories;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
public interface CategoriesMapper extends BaseMapper<Categories> {

}
