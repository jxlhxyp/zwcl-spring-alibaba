package com.zwcl.glass.goods.controller;


import com.zwcl.glass.goods.entity.Brand;
import com.zwcl.glass.goods.entity.Categories;
import com.zwcl.glass.goods.service.BrandService;
import com.zwcl.glass.goods.service.CategoriesService;
import com.zwcl.glass.goods.service.GoodsBaseService;
import com.zwcl.glass.goods.service.GoodsCommonService;
import com.zwcl.glass.goods.service.impl.BrandServiceImpl;
import com.zwcl.glass.goods.service.impl.CategoriesServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
@RestController
@RequestMapping("/categories")
public class CategoriesController {

    @Autowired
    private GoodsCommonService goodsCommonService;

    @Autowired
    private CategoriesServiceImpl categoriesService;

    /**
     * 列表
     */
    @GetMapping("/list")
    public List<Categories> getBrandList(Boolean isAll)  {
        List<Categories> list = goodsCommonService.getCategoriesList(isAll);
        return list;
    }


    @GetMapping("/refreshCache")
    public void refreshCache(){
        categoriesService.deleteCache(GoodsBaseService.ZWCL_GLASS_GOODS_CATEGORIES_CACHE+"::all");
        categoriesService.getCategoriesList();
    }

}

