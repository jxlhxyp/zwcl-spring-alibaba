package com.zwcl.glass.goods.service;

import com.zwcl.common.core.utils.SpringUtils;
import com.zwcl.glass.tools.api.enums.TestCodeEnum;
import org.springframework.stereotype.Component;

@Component
public class GoodsRecommendFactory {

    /**
     * 根据平台enum获取具体实现
     *
     * @param testCodeEnum
     * @return
     */
    public GoodsRecommend getService(TestCodeEnum testCodeEnum) {
        return (GoodsRecommend) SpringUtils.getBean(testCodeEnum.getServiceName());
    }
}
