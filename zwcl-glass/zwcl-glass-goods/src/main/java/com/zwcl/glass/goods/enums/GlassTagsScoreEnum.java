package com.zwcl.glass.goods.enums;

import com.zwcl.common.core.exception.BusinessException;
import com.zwcl.common.core.utils.JsonUtils;
import com.zwcl.common.core.utils.StringUtilsEx;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.IntStream;

/**
 * 镜片推导权重
 */
public enum GlassTagsScoreEnum {

    REFRACTION("Refraction", "折射率",50,true),

    FUNCTION("Function", "功能类", 7,false),

    //已废弃
    ADDITIONAL("Additional", "功能类", 7,false),

    MATERIALS("Materials", "材质类", 5,false),

    PRICE("HidePrice", "性价比", 3,false),


    ;
    /**
     * value
     */
    private String value;

    /**
     * desc
     */
    private String desc;

    /**
     * service实体
     */
    private Integer weight;

    /**
     * 是否是主标签
     */
    private Boolean isMain;


    private static final Map<String, String> map;

    private static final Map<String, Integer> weightMap;

    static {
        GlassTagsScoreEnum[] enums = GlassTagsScoreEnum.values();
        int size = enums.length;
        map = IntStream.range(0, size).collect(LinkedHashMap::new, (map, desc) -> {
            map.put(enums[desc].getValue(), enums[desc].getDesc());
        }, Map::putAll);

        weightMap = IntStream.range(0, size).collect(LinkedHashMap::new, (map, weightType) -> {
            map.put(enums[weightType].getValue(), enums[weightType].getWeight());
        }, Map::putAll);
    }


    /**
     * @param value          value值
     * @param desc           desc值
     * @param weight    serviceName值
     */
    GlassTagsScoreEnum(String value, String desc, Integer weight, Boolean isMain) {
        this.value = value;
        this.desc = desc;
        this.weight = weight;
        this.isMain = isMain;
    }

    /**
     * 返回value
     *
     * @return value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * 返回desc
     *
     * @return desc 字符串
     */
    public String getDesc() {
        return this.desc;
    }

    /**
     * 返回serviceName
     *
     * @return
     */
    public Integer getWeight() {
        return this.weight;
    }

    public Boolean getIsMain() { return this.isMain;}

    /**
     * 根据value获取desc
     *
     * @param value value
     * @return 处理结果
     */
    public static String getDesc(Integer value) {
        return getMap().get(value) == null ? StringUtilsEx.EMPTY : getMap().get(value);
    }

    /**
     * 根据value获取权重
     *
     * @param value value
     * @return 处理结果
     */
    public static Integer getWeight(Integer value) {
        return weightMap.get(value) == null ? 0 : weightMap.get(value);
    }

    public static GlassTagsScoreEnum of(String value) {
        GlassTagsScoreEnum[] values = GlassTagsScoreEnum.values();
        for (GlassTagsScoreEnum anEnum : values) {
            if (anEnum.getValue().equals(value)) {
                return anEnum;
            }
        }
        throw new BusinessException(value+"不存在的镜片标签类型");
    }

    /**
     * 获取map
     *
     * @return 返回map
     */
    public static Map<String, String> getMap() {
        return map;
    }

    /**
     * 获取json
     *
     * @return 处理结果
     * @throws BusinessException 自定义异常
     */
    public static String getJson() throws BusinessException {
        return JsonUtils.objectToJson(getMap());
    }
}
