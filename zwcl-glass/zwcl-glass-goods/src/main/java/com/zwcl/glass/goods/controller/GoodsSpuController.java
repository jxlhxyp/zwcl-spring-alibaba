package com.zwcl.glass.goods.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.common.core.utils.BeanUtils;
import com.zwcl.glass.goods.dto.CateSpuDto;
import com.zwcl.glass.goods.dto.TagSpuDto;
import com.zwcl.glass.goods.entity.*;
import com.zwcl.glass.goods.service.GoodsBaseService;
import com.zwcl.glass.goods.service.GoodsCommonService;
import com.zwcl.glass.goods.service.GoodsSpuService;
import com.zwcl.glass.goods.service.impl.GoodsSkuServiceImpl;
import com.zwcl.glass.goods.service.impl.GoodsSpuServiceImpl;
import com.zwcl.glass.goods.vo.GoodsDetailVo;
import com.zwcl.glass.goods.vo.GoodsImagesVo;
import com.zwcl.glass.goods.vo.GoodsPropertyVo;
import com.zwcl.glass.goods.vo.GoodsSummaryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
@RestController
@RequestMapping("/goodsSpu")
public class GoodsSpuController {

    @Autowired
    private GoodsCommonService goodsCommonService;

    @Autowired
    private GoodsSpuService goodsSpuService;

    @Autowired
    private GoodsSpuServiceImpl spuService;

    @GetMapping("/refreshCache")
    public void refreshCache(Integer cateId){
        spuService.deleteCache(GoodsBaseService.ZWCL_GLASS_GOODS_SPU_CACHE+"::"+cateId.toString());
        spuService.getSpusById(cateId);
    }

    //根据类别获取商品分页列表
    @PostMapping("/spuByCate")
    public Page<GoodsSummaryVo> getSpuPage(@RequestBody CateSpuDto dto)  {
        List<GoodsSpu> list = goodsCommonService.getGoodsSpuList(dto.getCateId(),false);
        if(null!=list && list.size()>0) {
            //分页，合并图片
            Page<GoodsSpu> curPage = BeanUtils.getCurPage(dto.getCurrent(), dto.getSize(), list);
            Page<GoodsSummaryVo> page = BeanUtils.convertPageResult(curPage, GoodsSummaryVo.class);
            for (GoodsSummaryVo item : page.getRecords()) {
                GoodsImagesVo masterImg = goodsCommonService.getMasterSpuImage(item.getId());
                if (null != masterImg) {
                    item.setMasterImgUrl(masterImg.getLink());
                }
            }
            return page;
        }
        return new Page<GoodsSummaryVo>();
    }

    //根据tags搜索商品分页列表
    @PostMapping("/spuByTag")
    public Page<GoodsSummaryVo> getSpuPage(@RequestBody TagSpuDto dto)  {
        Page<GoodsSpu> page = new Page<>(dto.getCurrent(), dto.getSize());
        Page<GoodsSpu> pageResult = goodsSpuService.getSpusByTag(page, dto);
        if(null!= pageResult && pageResult.getRecords().size()>0) {
            //分页，合并图片
            Page<GoodsSummaryVo> pageVo = BeanUtils.convertPageResult(pageResult, GoodsSummaryVo.class);
            for (GoodsSummaryVo item : pageVo.getRecords()) {
                GoodsImagesVo masterImg = goodsCommonService.getMasterSpuImage(item.getId());
                if(null!=masterImg) {
                    item.setMasterImgUrl(masterImg.getLink());
                }
            }
            return pageVo;
        }
        return new Page<GoodsSummaryVo>();
    }

    //根据商品id获取商品详情，主要是规格
    //TODO:如果是下单，需要进行价格校验
    @GetMapping("/detail")
    public GoodsDetailVo getSpuById(Integer id){
        GoodsDetailVo detailVo=new GoodsDetailVo();
        GoodsSpu goodsSpu = goodsSpuService.getById(id);
        List<GoodsImagesVo> imagesList=goodsCommonService.getSpuImageList(id,false);
        List<GoodsPropertyVo> propertyList =BeanUtils.convertList(goodsCommonService.getGoodsProperty(id,false), GoodsPropertyVo.class);
        if(null!=propertyList && propertyList.size()>0) {
            //转化属性
            Map<Integer, PropertyName> propertyNameMap = goodsCommonService.getPropNameMap();
            Map<Integer, PropertyValue> propertyValueMap = goodsCommonService.getPropValueMap();
            for (GoodsPropertyVo item : propertyList) {
                item.setPropName(propertyNameMap.get(item.getPropNameId()).getTitle());
                item.setPropValue(propertyValueMap.get(item.getPropValueId()).getValue());
            }
            Map<Integer, List<GoodsPropertyVo>> properVoMap = propertyList.stream().collect(Collectors.groupingBy(GoodsPropertyVo::getPropNameId));
            detailVo.setPropertyMap(properVoMap);
        }
        //设置值
        detailVo.setGoodsSpu(goodsSpu);
        detailVo.setImagesList(imagesList);
        return detailVo;
    }

}

