package com.zwcl.glass.goods.service;

import com.zwcl.glass.goods.entity.GoodsImages;
import com.zwcl.common.mybatis.service.BaseService;
import com.zwcl.glass.goods.vo.GoodsImagesVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
public interface GoodsImagesService extends BaseService<GoodsImages> {

    List<GoodsImagesVo> getImagesBySpuId(Integer spuId);
}
