package com.zwcl.glass.goods.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("glass_goods_spu")
public class GoodsSpu extends BaseEntity<GoodsSpu> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品编号，唯一
     */
    private String spuNo;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 品牌ID
     */
    private Integer brandId;

    /**
     * 分类ID
     */
    private Integer cateId;

    /**
     * 最低售价
     */
    private BigDecimal lowPrice;

    /**
     * 最高售价
     */
    private BigDecimal hightPrice;

    /**
     * 商品标签
     */
    private String tags;

    /**
     * 商品内容
     */
    private String content;

    /**
     * 商品描述
     */
    private String summary;

    /**
     * 上架状态: 1是0是
     */
    private Integer isSale;

    /**
     * 推荐指数
     */
    private Integer recommendRate;

    /**
     * 商品的打分，仅在推荐处使用
     */
    @TableField(exist = false)
    private Integer score;


    /**
     * 去掉sku那一级后，增加的到此处的参数
     */
    private String outerSkuId;

    private String outerSkuUrl;

    private BigDecimal jdPrice;

    private BigDecimal tbPrice;

    private Integer totalSaleNum;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
