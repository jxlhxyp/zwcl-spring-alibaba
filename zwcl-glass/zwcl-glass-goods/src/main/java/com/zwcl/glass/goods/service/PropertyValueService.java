package com.zwcl.glass.goods.service;

import com.zwcl.glass.goods.entity.PropertyName;
import com.zwcl.glass.goods.entity.PropertyValue;
import com.zwcl.common.mybatis.service.BaseService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
public interface PropertyValueService extends BaseService<PropertyValue> {
    List<PropertyValue> getPropertyValueList();

    PropertyValue getPropertyValueByName(Integer nameId,String value);
}
