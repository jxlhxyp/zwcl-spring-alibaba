package com.zwcl.glass.goods.service;

import com.zwcl.glass.goods.entity.Brand;
import com.zwcl.glass.goods.entity.Categories;
import com.zwcl.common.mybatis.service.BaseService;

import java.util.List;

/**
 * <p>
 *  服务类，要加缓存
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
public interface CategoriesService extends BaseService<Categories> {
    List<Categories> getCategoriesList();

    Categories getCategoriesByName(String name);
}
