package com.zwcl.glass.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zwcl.glass.goods.entity.Categories;
import com.zwcl.glass.goods.entity.PropertyName;
import com.zwcl.glass.goods.mapper.PropertyNameMapper;
import com.zwcl.glass.goods.service.GoodsBaseService;
import com.zwcl.glass.goods.service.PropertyNameService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
@Service
public class PropertyNameServiceImpl extends GoodsBaseService<PropertyNameMapper, PropertyName> implements PropertyNameService {

    @Override
    @Cacheable(value = ZWCL_GLASS_GOODS_PROPNAME_CACHE, key = "'all'")
    public List<PropertyName> getPropertyNameList() {
        return this.list(new LambdaQueryWrapper<PropertyName>().orderByAsc(PropertyName::getSort));
    }

    @Override
    public PropertyName getPropertyNameByTitle(Integer cateId, String title) {
        return this.getOne(new LambdaQueryWrapper<PropertyName>().eq(PropertyName::getTitle,title).eq(PropertyName::getCateId,cateId));
    }

}
