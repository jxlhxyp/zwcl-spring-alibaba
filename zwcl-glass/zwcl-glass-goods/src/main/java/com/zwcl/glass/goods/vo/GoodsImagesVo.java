package com.zwcl.glass.goods.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 存缓存中，需要继承序列化接口
 */
@Data
public class GoodsImagesVo implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;

    /**
     * 商品ID
     */
    private Integer spuId;

    /**
     * 图片URL地址
     */
    private String link;

    /**
     * 图片位置
     */
    private Integer position;

    /**
     * 是否主图: 1是,0否
     */
    private Boolean isMaster;

    private Boolean delFlag;
}
