package com.zwcl.glass.goods.service;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

public abstract class GoodsBaseService<M extends BaseMapper<T>, T> extends BaseServiceImpl<M, T> {
    @Autowired
    private RedisTemplate redisTemplate;

    public static final String ZWCL_GLASS_GOODS_BRAND_CACHE = "Zwcl:Glass:Goods:Brand";

    public static final String ZWCL_GLASS_GOODS_CATEGORIES_CACHE = "Zwcl:Glass:Goods:Categories";

    public static final String ZWCL_GLASS_GOODS_IMAGES_CACHE = "Zwcl:Glass:Goods:Images";

    public static final String ZWCL_GLASS_GOODS_PROPERTY_CACHE = "Zwcl:Glass:Goods:Property";

    public static final String ZWCL_GLASS_GOODS_SKU_CACHE = "Zwcl:Glass:Goods:Sku";

    public static final String ZWCL_GLASS_GOODS_SPU_CACHE = "Zwcl:Glass:Goods:Spu";

    public static final String ZWCL_CONCLUSION_TAG_CACHE = "Zwcl:Glass:Conclusion:Tag";

    public static final String ZWCL_GLASS_GOODS_PROPNAME_CACHE = "Zwcl:Glass:Goods:PropertyName";

    public static final String ZWCL_GLASS_GOODS_PROPVALUE_CACHE = "Zwcl:Glass:Goods:PropertyValue";

    public void deleteCache(String key){
        redisTemplate.delete(key);
    }
}
