package com.zwcl.glass.goods.service;

import com.zwcl.glass.goods.entity.GoodsSku;
import com.zwcl.common.mybatis.service.BaseService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
public interface GoodsSkuService extends BaseService<GoodsSku> {
    List<GoodsSku> getSkusBySpuId(Integer spuId);
}
