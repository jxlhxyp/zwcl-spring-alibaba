package com.zwcl.glass.goods.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.common.core.exception.BusinessException;
import com.zwcl.common.core.utils.BeanUtils;
import com.zwcl.glass.goods.dto.TagSpuDto;
import com.zwcl.glass.goods.dto.UserSpuDto;
import com.zwcl.glass.goods.entity.GoodsSpu;
import com.zwcl.glass.goods.enums.GlassCateEnum;
import com.zwcl.glass.goods.service.GoodsCommonService;
import com.zwcl.glass.goods.service.GoodsRecommend;
import com.zwcl.glass.goods.service.GoodsRecommendFactory;
import com.zwcl.glass.goods.service.GoodsRecommendService;
import com.zwcl.glass.goods.vo.GoodsImagesVo;
import com.zwcl.glass.goods.vo.GoodsSummaryVo;
import com.zwcl.glass.tools.api.dto.UserTagsDto;
import com.zwcl.glass.tools.api.entity.UserReport;
import com.zwcl.glass.tools.api.enums.TestCodeEnum;
import com.zwcl.glass.tools.api.feign.UserReportFacadeClient;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/recommend")
public class GoodsRecommendController {

//    @Autowired
//    private GoodsRecommendService recommendService;

    @Autowired
    private GoodsCommonService goodsCommonService;

    @Autowired
    private GoodsRecommendFactory goodsRecommendFactory;

    //根据tags搜索商品分页列表
    @PostMapping("/spuByUser")
    public Page<GoodsSummaryVo> getSpuPage(@RequestBody UserSpuDto dto)  {
        TestCodeEnum codeEnum = TestCodeEnum.of(dto.getPaperCode());
        if(null==codeEnum.getServiceName()){
            throw new BusinessException("当前类型商品不支持推荐");
        }
        GoodsRecommend goodsRecommend = goodsRecommendFactory.getService(codeEnum);
        UserReport userReport = goodsRecommend.getUserReportTags(dto.getUserId(),dto.getPaperCode());
        //根据用户标签搜索数据
        List<GoodsSpu> userSpuList =new ArrayList<>();
        if(null!=userReport.getTestTags() && userReport.getTestTags().size()>0) {
            String userTags = userReport.getTestTags().get(dto.getPaperCode());
            if(StringUtils.isNotBlank(userTags)) {
                userSpuList = goodsRecommend.getSpusByUserTag(userTags,dto.getTags());
                //userSpuList = recommendService.getSpusByUserTag(userReport.getTestTags().get(dto.getPaperCode()));
                if (null == userSpuList || userSpuList.size() == 0) {
                    //TODO:加载默认的展示
                    //userSpuList = goodsCommonService.getSpuByCateCode(GlassCateEnum.GLASS.getValue());
                    return null;
                }
            }
        }else {                                    //TODO:加载默认的展示
            //userSpuList = goodsCommonService.getSpuByCateCode(GlassCateEnum.GLASS.getValue());
            return null;
        }
        if(null!=userSpuList && userSpuList.size()>0) {
            //分页，合并图片
            Page<GoodsSpu> curPage = BeanUtils.getCurPage(dto.getCurrent(), dto.getSize(), userSpuList);
            Page<GoodsSummaryVo> page = BeanUtils.convertPageResult(curPage, GoodsSummaryVo.class);
            for (GoodsSummaryVo item : page.getRecords()) {
                GoodsImagesVo masterImg = goodsCommonService.getMasterSpuImage(item.getId());
                if(null!=masterImg) {
                    item.setMasterImgUrl(masterImg.getLink());
                }
            }
            return page;
        }
        return null;
    }
}
