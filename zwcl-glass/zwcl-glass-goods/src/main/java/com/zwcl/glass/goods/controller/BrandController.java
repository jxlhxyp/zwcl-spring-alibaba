package com.zwcl.glass.goods.controller;


import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.glass.goods.entity.Brand;
import com.zwcl.glass.goods.service.BrandService;
import com.zwcl.glass.goods.service.GoodsBaseService;
import com.zwcl.glass.goods.service.GoodsCommonService;
import com.zwcl.glass.goods.service.impl.BrandServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
@RestController
@RequestMapping("/brand")
public class BrandController {

    @Autowired
    private GoodsCommonService goodsCommonService;

    @Autowired
    private BrandServiceImpl brandService;

    /**
     * 列表
     */
    @GetMapping("/list")
    public List<Brand> getBrandList(Boolean isAll) throws Exception {
        List<Brand> list = goodsCommonService.getBrandList(isAll);
        return list;
    }

    @GetMapping("/refreshCache")
    public void refreshCache(){
        brandService.deleteCache(GoodsBaseService.ZWCL_GLASS_GOODS_BRAND_CACHE+"::all");
        brandService.getBrandList();
    }
}

