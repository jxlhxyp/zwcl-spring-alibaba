package com.zwcl.glass.goods.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.glass.goods.dto.TagSpuDto;
import com.zwcl.glass.goods.entity.GoodsSpu;
import com.zwcl.common.mybatis.service.BaseService;
import com.zwcl.glass.goods.vo.GoodsDetailVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
public interface GoodsSpuService extends BaseService<GoodsSpu> {

    List<GoodsSpu> getSpusById(Integer cateId);

    Page<GoodsSpu> getSpusByTag(Page<GoodsSpu> page, TagSpuDto dto);

    List<GoodsSpu> listSpuByTag(String[] tags);

    Map<String,String> getConclusionAndTagMap(String paperCode);
}
