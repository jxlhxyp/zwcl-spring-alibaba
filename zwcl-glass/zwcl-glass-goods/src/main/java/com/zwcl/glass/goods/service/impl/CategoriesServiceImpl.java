package com.zwcl.glass.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zwcl.glass.goods.entity.Brand;
import com.zwcl.glass.goods.entity.Categories;
import com.zwcl.glass.goods.mapper.CategoriesMapper;
import com.zwcl.glass.goods.service.CategoriesService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import com.zwcl.glass.goods.service.GoodsBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
@Service
public class CategoriesServiceImpl extends GoodsBaseService<CategoriesMapper, Categories> implements CategoriesService {

    @Override
    @Cacheable(value = ZWCL_GLASS_GOODS_CATEGORIES_CACHE, key = "'all'")
    public List<Categories> getCategoriesList() {
        return this.list(new LambdaQueryWrapper<Categories>().orderByAsc(Categories::getSort));
    }

    @Override
    public Categories getCategoriesByName(String name) {
        return this.getOne(new LambdaQueryWrapper<Categories>().eq(Categories::getCateName,name));
    }

}
