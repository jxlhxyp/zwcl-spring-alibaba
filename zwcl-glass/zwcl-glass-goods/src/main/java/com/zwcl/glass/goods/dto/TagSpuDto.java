package com.zwcl.glass.goods.dto;

import com.zwcl.common.core.domain.dto.BaseDto;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;


@Data
public class TagSpuDto extends BaseDto {

    @NotBlank(message = "搜索关键字不能为空")
    private List<String> tags;

    /**
     * 可以为空
     */
    private Integer cateId;

}
