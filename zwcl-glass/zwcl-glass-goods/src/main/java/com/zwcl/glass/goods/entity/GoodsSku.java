package com.zwcl.glass.goods.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("glass_goods_sku")
public class GoodsSku extends BaseEntity<GoodsSku> {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品ID
     */
    private Integer spuId;

    /**
     * sku编码
     */
    private String skuNo;

    /**
     * sku名称
     */
    private String skuName;

    /**
     * SKU库存
     */
    private Integer stock;

    /**
     * 商品售价
     */
    private BigDecimal price;

    /**
     * 商品属性表ID，以逗号分隔，这个是不是要移动到spu
     */
    private String properties;


    private String propertiesTags;

    /**
     * 状态:1启用,0禁用
     */
    private Integer status;


    private String outerSkuId;

    private BigDecimal jdPrice;

    private BigDecimal tbPrice;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
