package com.zwcl.glass.goods.controller;


import com.zwcl.glass.goods.service.GoodsBaseService;
import com.zwcl.glass.goods.service.PropertyValueService;
import com.zwcl.glass.goods.service.impl.GoodsSpuServiceImpl;
import com.zwcl.glass.goods.service.impl.PropertyNameServiceImpl;
import com.zwcl.glass.goods.service.impl.PropertyValueServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
@RestController
@RequestMapping("/propertyName")
public class PropertyNameController {
    @Autowired
    private PropertyNameServiceImpl propertyNameService;

    @GetMapping("/refreshCache")
    public void refreshCache(){
        propertyNameService.deleteCache(GoodsBaseService.ZWCL_GLASS_GOODS_PROPNAME_CACHE+"::all");
        propertyNameService.getPropertyNameList();
    }
}

