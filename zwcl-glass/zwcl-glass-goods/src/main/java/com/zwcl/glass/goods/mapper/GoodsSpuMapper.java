package com.zwcl.glass.goods.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.glass.goods.dto.TagSpuDto;
import com.zwcl.glass.goods.entity.GoodsSpu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
public interface GoodsSpuMapper extends BaseMapper<GoodsSpu> {

    Page<GoodsSpu> getSpusByTag(Page<GoodsSpu> page, @Param("dto") TagSpuDto dto);
}
