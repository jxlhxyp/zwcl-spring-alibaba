package com.zwcl.glass.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zwcl.glass.goods.entity.GoodsProperty;
import com.zwcl.glass.goods.entity.GoodsSpu;
import com.zwcl.glass.goods.mapper.GoodsPropertyMapper;
import com.zwcl.glass.goods.service.GoodsBaseService;
import com.zwcl.glass.goods.service.GoodsPropertyService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
@Service
public class GoodsPropertyServiceImpl extends GoodsBaseService<GoodsPropertyMapper, GoodsProperty> implements GoodsPropertyService {

    @Override
    @Cacheable(value = ZWCL_GLASS_GOODS_PROPERTY_CACHE, key = "#a0")
    public List<GoodsProperty> getGoodsPropertysBySpuId(Integer spuId) {
        return this.list(new LambdaQueryWrapper<GoodsProperty>().eq(GoodsProperty::getSpuId, spuId));
    }
}
