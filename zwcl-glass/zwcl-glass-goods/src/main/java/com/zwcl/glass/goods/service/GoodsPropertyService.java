package com.zwcl.glass.goods.service;

import com.zwcl.glass.goods.entity.GoodsProperty;
import com.zwcl.common.mybatis.service.BaseService;
import com.zwcl.glass.goods.entity.GoodsSpu;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
public interface GoodsPropertyService extends BaseService<GoodsProperty> {
    List<GoodsProperty> getGoodsPropertysBySpuId(Integer spuId);
}
