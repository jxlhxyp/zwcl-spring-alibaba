package com.zwcl.glass.goods.controller;


import com.zwcl.glass.goods.service.GoodsBaseService;
import com.zwcl.glass.goods.service.impl.GoodsImagesServiceImpl;
import com.zwcl.glass.goods.service.impl.GoodsPropertyServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
@RestController
@RequestMapping("/goodsProperty")
public class GoodsPropertyController {

    @Autowired
    private GoodsPropertyServiceImpl propertyService;

    @GetMapping("/refreshCache")
    public void refreshCache(Integer spuId){
        propertyService.deleteCache(GoodsBaseService.ZWCL_GLASS_GOODS_PROPERTY_CACHE+"::"+spuId.toString());
        propertyService.getGoodsPropertysBySpuId(spuId);
    }
}

