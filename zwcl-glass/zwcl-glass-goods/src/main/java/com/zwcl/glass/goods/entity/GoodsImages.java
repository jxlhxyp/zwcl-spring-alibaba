package com.zwcl.glass.goods.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("glass_goods_images")
public class GoodsImages extends BaseEntity<GoodsImages> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品ID
     */
    private Integer spuId;

    /**
     * 图片URL地址
     */
    private String link;

    /**
     * 图片位置
     */
    private Integer position;

    /**
     * 是否主图: 1是,0否
     */
    private Boolean isMaster;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
