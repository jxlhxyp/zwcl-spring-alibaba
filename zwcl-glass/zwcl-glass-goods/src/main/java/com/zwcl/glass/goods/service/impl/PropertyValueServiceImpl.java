package com.zwcl.glass.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zwcl.glass.goods.entity.PropertyName;
import com.zwcl.glass.goods.entity.PropertyValue;
import com.zwcl.glass.goods.mapper.PropertyValueMapper;
import com.zwcl.glass.goods.service.GoodsBaseService;
import com.zwcl.glass.goods.service.PropertyValueService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
@Service
public class PropertyValueServiceImpl extends GoodsBaseService<PropertyValueMapper, PropertyValue> implements PropertyValueService {

    @Override
    @Cacheable(value = ZWCL_GLASS_GOODS_PROPVALUE_CACHE, key = "'all'")
    public List<PropertyValue> getPropertyValueList() {
        return this.list();
    }

    @Override
    public PropertyValue getPropertyValueByName(Integer nameId, String value) {
        return this.getOne(new LambdaQueryWrapper<PropertyValue>().eq(PropertyValue::getPropNameId,nameId).eq(PropertyValue::getValue,value));
    }
}
