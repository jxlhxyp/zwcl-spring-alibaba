package com.zwcl.glass.goods.vo;

import com.zwcl.glass.goods.service.GoodsCommonService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

@Data
public class GoodsPropertyVo {

//    @Autowired
//    private GoodsCommonService goodsCommonService;

    /**
     * 属性名ID
     */
    private Integer propNameId;

    private String propName;
//    public String getPropName(){
//        return goodsCommonService.getPropNameMap().get(propNameId).getTitle();
//    }

    /**
     * 属性值ID
     */
    private Integer propValueId;

    private String propValue;
//    public String getPropValue(){
//        return goodsCommonService.getPropValueMap().get(propValueId).getValue();
//    }
}
