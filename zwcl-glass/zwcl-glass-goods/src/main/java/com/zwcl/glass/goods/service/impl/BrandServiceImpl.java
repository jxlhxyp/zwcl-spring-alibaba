package com.zwcl.glass.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zwcl.glass.goods.entity.Brand;
import com.zwcl.glass.goods.entity.PropertyName;
import com.zwcl.glass.goods.mapper.BrandMapper;
import com.zwcl.glass.goods.service.BrandService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import com.zwcl.glass.goods.service.GoodsBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
@Service
public class BrandServiceImpl extends GoodsBaseService<BrandMapper, Brand> implements BrandService {

    @Override
    @Cacheable(value = ZWCL_GLASS_GOODS_BRAND_CACHE, key = "'all'")
    public List<Brand> getBrandList() {
        return this.list(new LambdaQueryWrapper<Brand>().orderByAsc(Brand::getSort));
    }

    @Override
    public Brand getBrandByName(String name) {
        return this.getOne(new LambdaQueryWrapper<Brand>().eq(Brand::getBrandName,name));
    }

}
