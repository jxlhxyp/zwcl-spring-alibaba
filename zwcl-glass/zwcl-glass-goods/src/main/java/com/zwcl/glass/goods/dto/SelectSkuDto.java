package com.zwcl.glass.goods.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Map;
import java.util.Set;

@Data
public class SelectSkuDto {

    /**
     * 规格属性map
     */
    @NotNull(message = "规格属性不能为空")
    //private Map<Integer,Integer> propertyMap;
    private Set<String> propertySet;

    /**
     * spu商品id
     */
    @NotNull(message = "spuId不能为空")
    private Integer spuId;

}
