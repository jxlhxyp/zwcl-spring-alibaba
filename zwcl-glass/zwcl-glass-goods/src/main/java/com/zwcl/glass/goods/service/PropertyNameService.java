package com.zwcl.glass.goods.service;

import com.zwcl.glass.goods.entity.PropertyName;
import com.zwcl.common.mybatis.service.BaseService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
public interface PropertyNameService extends BaseService<PropertyName> {
    List<PropertyName> getPropertyNameList();

    PropertyName getPropertyNameByTitle(Integer cateId,String title);
}
