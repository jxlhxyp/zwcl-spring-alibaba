package com.zwcl.glass.goods.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("glass_goods_property")
public class GoodsProperty extends BaseEntity<GoodsProperty> {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品ID
     */
    private Integer spuId;

    /**
     * 属性名ID
     */
    private Integer propNameId;

    /**
     * 属性值ID
     */
    private Integer propValueId;

    /**
     * 是否在售有效
     */
    private Boolean isSale;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
