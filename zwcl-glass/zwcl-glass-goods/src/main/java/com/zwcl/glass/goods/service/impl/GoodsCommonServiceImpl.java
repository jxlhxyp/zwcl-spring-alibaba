package com.zwcl.glass.goods.service.impl;

import com.zwcl.common.core.exception.BusinessException;
import com.zwcl.common.core.utils.BeanUtils;
import com.zwcl.glass.goods.entity.*;
import com.zwcl.glass.goods.service.*;
import com.zwcl.glass.goods.vo.GoodsDetailVo;
import com.zwcl.glass.goods.vo.GoodsImagesVo;
import com.zwcl.glass.goods.vo.GoodsPropertyVo;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 公共服务粘合层，方便其他实体类使用缓存或者事务的注解
 */
@Service
public class GoodsCommonServiceImpl  implements GoodsCommonService {

    @Autowired
    private CategoriesService categoriesService;

    @Autowired
    private BrandService brandService;

    @Autowired
    private GoodsImagesService goodsImagesService;

    @Autowired
    private GoodsSpuService goodsSpuService;

    @Autowired
    private GoodsPropertyService goodsPropertyService;

    @Autowired
    private PropertyNameService propertyNameService;

    @Autowired
    private PropertyValueService propertyValueService;

    @Autowired
    private GoodsSkuService goodsSkuService;

    @Override
    public List<Brand> getBrandList(Boolean isAll){
        List<Brand> list = brandService.getBrandList();
        return isAll? list: list.stream().filter(x->!x.getDelFlag()).collect(Collectors.toList());
    }

    @Override
    public List<Categories> getCategoriesList(Boolean isAll) {
        List<Categories> list = categoriesService.getCategoriesList();
        return isAll? list: list.stream().filter(x->!x.getDelFlag()).collect(Collectors.toList());
    }

    @Override
    public List<GoodsImagesVo> getSpuImageList(Integer spuId, Boolean isAll) {
        List<GoodsImagesVo> list = goodsImagesService.getImagesBySpuId(spuId);
        return isAll? list: list.stream().filter(x->!x.getDelFlag()).collect(Collectors.toList());
    }

    @Override
    public GoodsImagesVo getMasterSpuImage(Integer spuId){
        List<GoodsImagesVo> list=getSpuImageList(spuId,false);
        return list.stream().filter(x->x.getIsMaster()).findFirst().orElse(null);
    }

    /**
     * 通过类别id获取spu
     * @param cateId
     * @param isAll
     * @return
     */
    @Override
    public List<GoodsSpu> getGoodsSpuList(Integer cateId, Boolean isAll) {
        List<GoodsSpu> list = goodsSpuService.getSpusById(cateId);
        return isAll? list: list.stream().filter(x->!x.getDelFlag() && x.getIsSale()==1).collect(Collectors.toList());
    }

    @Override
    public Map<Integer, PropertyName> getPropNameMap() {
        return propertyNameService.getPropertyNameList().stream().collect(Collectors.toMap((p) -> p.getId(), (p) -> p));
    }

    @Override
    public Map<Integer, PropertyValue> getPropValueMap() {
        return propertyValueService.getPropertyValueList().stream().collect(Collectors.toMap((p) -> p.getId(), (p) -> p));
    }

    @Override
    public List<GoodsProperty> getGoodsProperty(Integer spuId, Boolean isAll){
        List<GoodsProperty> list = goodsPropertyService.getGoodsPropertysBySpuId(spuId);
        return isAll? list: list.stream().filter(x->!x.getDelFlag() && x.getIsSale()).collect(Collectors.toList());
    }

    @Override
    public List<GoodsSku> getGoodsSkuList(Integer spuId, Boolean isAll) {
        List<GoodsSku> list = goodsSkuService.getSkusBySpuId(spuId);
        return isAll? list: list.stream().filter(x->!x.getDelFlag() && x.getStatus()==1).collect(Collectors.toList());
    }

    /**
     * 可能有父子关系的
     * @param cateCode
     * @return
     */
    @Override
    public List<GoodsSpu> getSpuByCateCode(String cateCode) {
        List<Categories> list = categoriesService.getCategoriesList();
        Categories categories=list.stream().filter(x->x.getCateCode().equals(cateCode)).findFirst().orElse(null);
        if(categories==null){
            throw new BusinessException("不存在该分类");
        }
        Integer cateId= categories.getId();
        //TODO:找寻父子分类
        List<Integer> cateIds= Arrays.asList(cateId);
        //TODO:商品太多可能会撑爆内存
        List<GoodsSpu> allSpuList =new ArrayList<>();
        cateIds.forEach(itemId->{
            allSpuList.addAll(getGoodsSpuList(itemId,false));
        });
        return allSpuList;
    }
}
