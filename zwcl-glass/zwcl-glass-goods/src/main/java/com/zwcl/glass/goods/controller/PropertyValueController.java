package com.zwcl.glass.goods.controller;


import com.zwcl.glass.goods.service.GoodsBaseService;
import com.zwcl.glass.goods.service.impl.PropertyNameServiceImpl;
import com.zwcl.glass.goods.service.impl.PropertyValueServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
@RestController
@RequestMapping("/propertyValue")
public class PropertyValueController {
    @Autowired
    private PropertyValueServiceImpl propertyValueService;

    @GetMapping("/refreshCache")
    public void refreshCache(){
        propertyValueService.deleteCache(GoodsBaseService.ZWCL_GLASS_GOODS_PROPVALUE_CACHE+"::all");
        propertyValueService.getPropertyValueList();
    }
}

