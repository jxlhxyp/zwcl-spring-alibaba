package com.zwcl.glass.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zwcl.glass.goods.entity.GoodsSku;
import com.zwcl.glass.goods.entity.GoodsSpu;
import com.zwcl.glass.goods.mapper.GoodsSkuMapper;
import com.zwcl.glass.goods.mapper.GoodsSpuMapper;
import com.zwcl.glass.goods.service.GoodsBaseService;
import com.zwcl.glass.goods.service.GoodsSkuService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
@Service
public class GoodsSkuServiceImpl extends GoodsBaseService<GoodsSkuMapper, GoodsSku> implements GoodsSkuService {


    /**
     * 由于没有自定义的id生成器，key采用#cateId的方式会报错
     * @param spuId
     * @return
     */
    @Override
    @Cacheable(value = ZWCL_GLASS_GOODS_SKU_CACHE, key = "#a0")
    public List<GoodsSku> getSkusBySpuId(Integer spuId){
        return this.list(new LambdaQueryWrapper<GoodsSku>().eq(GoodsSku::getSpuId, spuId));
    }
}
