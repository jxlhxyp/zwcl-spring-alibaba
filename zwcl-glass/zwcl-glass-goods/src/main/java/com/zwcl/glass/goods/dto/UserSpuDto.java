package com.zwcl.glass.goods.dto;

import com.zwcl.common.core.domain.dto.BaseDto;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Data
public class UserSpuDto extends BaseDto {

    @NotNull(message = "用户id不能为空")
    private Integer userId;

    /**
     * 传试卷的编码，只有测试了试卷，才有推荐
     */
    @NotNull(message = "类型编码不能为空")
    private String paperCode;

    @NotBlank(message = "搜索关键字不能为空")
    private List<String> tags;
}
