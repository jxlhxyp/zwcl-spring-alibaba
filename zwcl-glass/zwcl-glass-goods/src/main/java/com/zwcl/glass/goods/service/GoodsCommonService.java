package com.zwcl.glass.goods.service;

import com.zwcl.glass.goods.entity.*;
import com.zwcl.glass.goods.vo.GoodsDetailVo;
import com.zwcl.glass.goods.vo.GoodsImagesVo;

import java.util.List;
import java.util.Map;

public interface GoodsCommonService {

    List<Brand> getBrandList(Boolean isAll);

    List<Categories> getCategoriesList(Boolean isAll);

    List<GoodsImagesVo> getSpuImageList(Integer spuId, Boolean isAll);

    GoodsImagesVo getMasterSpuImage(Integer spuId);

    List<GoodsSpu> getGoodsSpuList(Integer cateId, Boolean isAll);

    List<GoodsProperty> getGoodsProperty(Integer spuId, Boolean isAll);

    Map<Integer, PropertyName> getPropNameMap();

    Map<Integer, PropertyValue> getPropValueMap();

    List<GoodsSku> getGoodsSkuList(Integer spuId, Boolean isAll);

    List<GoodsSpu> getSpuByCateCode(String cateCode);
}
