package com.zwcl.glass.goods.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("glass_property_name")
public class PropertyName extends BaseEntity<PropertyName> {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 属性名
     */
    private String title;

    /**
     * 分类ID
     */
    private Integer cateId;

//    /**
//     * 是否允许别名: 1是0否
//     */
//    private Integer isAllowAlias;
//
//    /**
//     * 是否颜色属性: 1是0否
//     */
//    private Integer isColor;
//
//    /**
//     * 是否枚举: 1是0否
//     */
//    private Integer isEnum;
//
//    /**
//     * 是否输入属性: 1是0否
//     */
//    private Integer isInput;

    /**
     * 是否关键属性: 1是0否
     */
    private Integer isKey;

//    /**
//     * 是否销售属性:1是0否
//     */
//    private Integer isSale;
//
//    /**
//     * 是否搜索字段: 1是0否
//     */
//    private Integer isSearch;
//
//    /**
//     * 是否必须属性: 1是0否
//     */
//    private Integer isMust;
//
//    /**
//     * 是否多选: 1是0否
//     */
//    private Integer isMulti;

    /**
     * 状态: 1启用,0禁用
     */
    private Integer status;

    /**
     * 排序字段
     */
    private Integer sort;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
