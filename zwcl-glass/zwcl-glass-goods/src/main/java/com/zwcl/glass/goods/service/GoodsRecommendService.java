package com.zwcl.glass.goods.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.glass.goods.dto.TagSpuDto;
import com.zwcl.glass.goods.dto.UserSpuDto;
import com.zwcl.glass.goods.entity.GoodsSpu;
import com.zwcl.glass.tools.api.entity.UserReport;

import java.util.List;
import java.util.Map;

public interface GoodsRecommendService {

    List<GoodsSpu> getSpusByUserTag(String userTags);

    UserReport getUserReportTags(Integer userId);

    Integer computeSpuScore(String spuTags,Map<String,String> userTags);
}
