package com.zwcl.glass.goods.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 商品概要输出
 */
@Data
public class GoodsSummaryVo {
    private Integer id;

    /**
     * 商品编号，唯一
     */
    private String spuNo;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 品牌ID
     */
    private Integer brandId;

    /**
     * 分类ID
     */
    private Integer cateId;

    /**
     * 最低售价
     */
    private BigDecimal lowPrice;

    /**
     * 最高售价
     */
    private BigDecimal hightPrice;

    /**
     * 商品标签
     */
    private String tags;

    /**
     * 商品内容
     */
    private String content;

    /**
     * 商品描述
     */
    private String summary;

    /**
     * 总销售量
     */
    private Integer totalSaleNum;

    /**
     * 推荐指数
     */
    private Integer recommendRate;


    private Integer score;

    /**
     * 主图url
     */
    private String masterImgUrl;

    /**
     * 去掉sku那一级后，增加的到此处的参数
     */
    private String outerSkuId;

    private String outerSkuUrl;

    private BigDecimal jdPrice;

    private BigDecimal tbPrice;
}
