package com.zwcl.glass.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.zwcl.common.core.utils.BeanUtils;
import com.zwcl.glass.goods.entity.Categories;
import com.zwcl.glass.goods.entity.GoodsImages;
import com.zwcl.glass.goods.mapper.GoodsImagesMapper;
import com.zwcl.glass.goods.service.GoodsBaseService;
import com.zwcl.glass.goods.service.GoodsImagesService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import com.zwcl.glass.goods.vo.GoodsImagesVo;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-1
 */
@Service
public class GoodsImagesServiceImpl extends GoodsBaseService<GoodsImagesMapper, GoodsImages> implements GoodsImagesService {

    @Override
    @Cacheable(value = ZWCL_GLASS_GOODS_IMAGES_CACHE, key = "#spuId")
    public List<GoodsImagesVo> getImagesBySpuId(Integer spuId){
        List<GoodsImages> imagesList = this.list(new LambdaQueryWrapper<GoodsImages>().eq(GoodsImages::getSpuId, spuId));
        List<GoodsImagesVo> imagesVoList= BeanUtils.convertList(imagesList,GoodsImagesVo.class);
        return imagesVoList;
    }

}
