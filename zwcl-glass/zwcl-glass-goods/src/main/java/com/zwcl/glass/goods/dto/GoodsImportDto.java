package com.zwcl.glass.goods.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * 商品录入的dto
 */
@Data
public class GoodsImportDto {
    @ExcelProperty(index = 0)
    private String goodsNo;
    @ExcelProperty(index = 1)
    private String cateName;
    @ExcelProperty(index = 2)
    private String brandName;

    @ExcelProperty(index = 3)
    private String goodsName;
    @ExcelProperty(index = 4)
    private String goodsImgs;
    @ExcelProperty(index = 5)
    private String price;

    @ExcelProperty(index = 6)
    private String tags;
    @ExcelProperty(index = 7)
    private String summary;
    @ExcelProperty(index = 8)
    private String outerSkuUrl;

    @ExcelProperty(index = 9)
    private String jdPrice;
    @ExcelProperty(index = 10)
    private String tbPrice;
}
