package com.zwcl.glass.goods.dto;

import com.zwcl.common.core.domain.dto.BaseDto;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CateSpuDto extends BaseDto {
    @NotNull(message = "类别Id不能为空")
    private Integer cateId;
}
