package com.zwcl.glass.goods.service;

import com.zwcl.glass.goods.entity.Brand;
import com.zwcl.common.mybatis.service.BaseService;

import java.util.List;

/**
 * <p>
 *  服务类，要加缓存
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-13
 */
public interface BrandService extends BaseService<Brand> {

    List<Brand> getBrandList();

    Brand getBrandByName(String name);
}
