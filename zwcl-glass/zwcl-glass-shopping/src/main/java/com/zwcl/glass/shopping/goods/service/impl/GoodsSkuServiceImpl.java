package com.zwcl.glass.shopping.goods.service.impl;

import com.zwcl.glass.shopping.goods.entity.GoodsSku;
import com.zwcl.glass.shopping.goods.mapper.GoodsSkuMapper;
import com.zwcl.glass.shopping.goods.service.GoodsSkuService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-19
 */
@Service
public class GoodsSkuServiceImpl extends BaseServiceImpl<GoodsSkuMapper, GoodsSku> implements GoodsSkuService {

}
