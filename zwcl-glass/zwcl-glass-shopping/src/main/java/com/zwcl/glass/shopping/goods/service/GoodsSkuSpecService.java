package com.zwcl.glass.shopping.goods.service;

import com.zwcl.glass.shopping.goods.entity.GoodsSkuSpec;
import com.zwcl.common.mybatis.service.BaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-19
 */
public interface GoodsSkuSpecService extends BaseService<GoodsSkuSpec> {

}
