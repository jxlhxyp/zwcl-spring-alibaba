package com.zwcl.glass.shopping.goods.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("goods_spu")
public class GoodsSpu extends BaseEntity<GoodsSpu> {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品编号，唯一
     */
    private String spuNo;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 品牌ID
     */
    private Integer brandId;

    /**
     * 分类ID
     */
    private Integer cateId;

    /**
     * 最低售价
     */
    private BigDecimal lowPrice;

    /**
     * 最高售价
     */
    private BigDecimal hightPrice;

    /**
     * 商品内容
     */
    private String content;

    /**
     * 商品描述
     */
    private String summary;

    /**
     * 销售量，但是现在，需要拉取有赞的单才能统计
     */
    private Integer totalSaleNum;

    /**
     * 推荐指数
     */
    private Integer recommendRate;

    /**
     * 京东价格
     */
    private BigDecimal jdPrice;

    /**
     * 淘宝价格
     */
    private BigDecimal tbPrice;

    /**
     * 上架状态: 1是0是
     */
    private Integer isSale;

    /**
     * 是否为礼物赠送，暂时不用
     */
    private Boolean isGift;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
