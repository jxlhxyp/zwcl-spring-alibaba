package com.zwcl.glass.shopping.goods.service.impl;

import com.zwcl.glass.shopping.goods.entity.GoodsCategories;
import com.zwcl.glass.shopping.goods.mapper.GoodsCategoriesMapper;
import com.zwcl.glass.shopping.goods.service.GoodsCategoriesService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-19
 */
@Service
public class GoodsCategoriesServiceImpl extends BaseServiceImpl<GoodsCategoriesMapper, GoodsCategories> implements GoodsCategoriesService {

}
