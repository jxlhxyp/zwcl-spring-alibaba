package com.zwcl.glass.shopping.goods.service.impl;

import com.zwcl.glass.shopping.goods.entity.GoodsImages;
import com.zwcl.glass.shopping.goods.mapper.GoodsImagesMapper;
import com.zwcl.glass.shopping.goods.service.GoodsImagesService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-19
 */
@Service
public class GoodsImagesServiceImpl extends BaseServiceImpl<GoodsImagesMapper, GoodsImages> implements GoodsImagesService {

}
