package com.zwcl.glass.shopping.goods.service.impl;

import com.zwcl.glass.shopping.goods.entity.GoodsComment;
import com.zwcl.glass.shopping.goods.mapper.GoodsCommentMapper;
import com.zwcl.glass.shopping.goods.service.GoodsCommentService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-19
 */
@Service
public class GoodsCommentServiceImpl extends BaseServiceImpl<GoodsCommentMapper, GoodsComment> implements GoodsCommentService {

}
