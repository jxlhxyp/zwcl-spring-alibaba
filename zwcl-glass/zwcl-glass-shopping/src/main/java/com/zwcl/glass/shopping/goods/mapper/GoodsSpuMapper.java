package com.zwcl.glass.shopping.goods.mapper;

import com.zwcl.glass.shopping.goods.entity.GoodsSpu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-19
 */
public interface GoodsSpuMapper extends BaseMapper<GoodsSpu> {

}
