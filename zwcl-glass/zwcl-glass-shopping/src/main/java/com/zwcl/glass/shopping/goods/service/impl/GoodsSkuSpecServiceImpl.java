package com.zwcl.glass.shopping.goods.service.impl;

import com.zwcl.glass.shopping.goods.entity.GoodsSkuSpec;
import com.zwcl.glass.shopping.goods.mapper.GoodsSkuSpecMapper;
import com.zwcl.glass.shopping.goods.service.GoodsSkuSpecService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-19
 */
@Service
public class GoodsSkuSpecServiceImpl extends BaseServiceImpl<GoodsSkuSpecMapper, GoodsSkuSpec> implements GoodsSkuSpecService {

}
