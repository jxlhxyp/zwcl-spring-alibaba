package com.zwcl.glass.shopping.goods.service.impl;

import com.zwcl.glass.shopping.goods.entity.GoodsSpu;
import com.zwcl.glass.shopping.goods.mapper.GoodsSpuMapper;
import com.zwcl.glass.shopping.goods.service.GoodsSpuService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-19
 */
@Service
public class GoodsSpuServiceImpl extends BaseServiceImpl<GoodsSpuMapper, GoodsSpu> implements GoodsSpuService {

}
