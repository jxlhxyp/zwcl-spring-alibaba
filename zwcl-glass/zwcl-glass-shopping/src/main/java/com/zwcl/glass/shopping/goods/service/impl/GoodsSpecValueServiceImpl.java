package com.zwcl.glass.shopping.goods.service.impl;

import com.zwcl.glass.shopping.goods.entity.GoodsSpecValue;
import com.zwcl.glass.shopping.goods.mapper.GoodsSpecValueMapper;
import com.zwcl.glass.shopping.goods.service.GoodsSpecValueService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-19
 */
@Service
public class GoodsSpecValueServiceImpl extends BaseServiceImpl<GoodsSpecValueMapper, GoodsSpecValue> implements GoodsSpecValueService {

}
