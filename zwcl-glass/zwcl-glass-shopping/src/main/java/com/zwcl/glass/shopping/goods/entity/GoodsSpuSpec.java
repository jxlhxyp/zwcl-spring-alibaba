package com.zwcl.glass.shopping.goods.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("goods_spu_spec")
public class GoodsSpuSpec extends BaseEntity<GoodsSpuSpec> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品ID
     */
    private Integer spuId;

    /**
     * 规格名ID
     */
    private Integer specNameId;

    /**
     * 规格值ID
     */
    private Integer specValueId;

    /**
     * 1在售，0停售
     */
    private Boolean isSale;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
