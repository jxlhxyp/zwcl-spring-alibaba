package com.zwcl.glass.shopping;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

// 本地Nacos有时候启动会连接不上，failed to req API:/api//nacos/v1/ns/instance after all servers
// 删掉本地nacos文件夹下的data文件夹，重新启动
//TODO:采用@Cacheable注解的缓存，最好也要能够设置缓存的超时时间
@SpringCloudApplication
@MapperScan("com.zwcl.glass.shopping.*.mapper")
@EnableDiscoveryClient
public class ShoppingApplication {
    public static void main(String[] args){
        SpringApplication.run(ShoppingApplication.class, args);
        System.out.println("启动成功");
    }
}