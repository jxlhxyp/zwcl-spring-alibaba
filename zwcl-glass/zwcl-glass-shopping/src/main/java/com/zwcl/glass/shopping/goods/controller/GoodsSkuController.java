package com.zwcl.glass.shopping.goods.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-19
 */
@RestController
@RequestMapping("/goodsSku")
public class GoodsSkuController {

}

