package com.zwcl.glass.shopping.goods.service.impl;

import com.zwcl.glass.shopping.goods.entity.GoodsSpuSpec;
import com.zwcl.glass.shopping.goods.mapper.GoodsSpuSpecMapper;
import com.zwcl.glass.shopping.goods.service.GoodsSpuSpecService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-19
 */
@Service
public class GoodsSpuSpecServiceImpl extends BaseServiceImpl<GoodsSpuSpecMapper, GoodsSpuSpec> implements GoodsSpuSpecService {

}
