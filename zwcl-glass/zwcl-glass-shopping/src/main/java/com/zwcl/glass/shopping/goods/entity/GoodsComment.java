package com.zwcl.glass.shopping.goods.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("goods_comment")
public class GoodsComment extends BaseEntity<GoodsComment> {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 针对的商品id
     */
    private Integer spuId;

    /**
     * 评论内容
     */
    private String comment;

    /**
     * 评论中的图片
     */
    private String images;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
