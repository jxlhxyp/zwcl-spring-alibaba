package com.zwcl.glass.shopping.goods.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("goods_sku")
public class GoodsSku extends BaseEntity<GoodsSku> {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品ID
     */
    private Integer spuId;

    /**
     * spu编码
     */
    private String spuNo;

    /**
     * sku编码
     */
    private String skuNo;

    /**
     * sku名称
     */
    private String skuName;

    /**
     * SKU库存
     */
    private Integer stock;

    /**
     * 商品售价
     */
    private BigDecimal price;

    /**
     * 商品原价
     */
    private BigDecimal originPrice;

    /**
     * 商品的个性化标签
     */
    private String tags;

    /**
     * 京东价格
     */
    private BigDecimal jdPrice;

    /**
     * 淘宝价格
     */
    private BigDecimal tbPrice;

    /**
     * 是否为礼物赠送，暂时不用
     */
    private Boolean isGift;

    /**
     * 状态:1启用,0禁用
     */
    private Integer status;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
