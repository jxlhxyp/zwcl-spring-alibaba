package com.zwcl.glass.shopping.goods.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("goods_sku_spec")
public class GoodsSkuSpec extends BaseEntity<GoodsSkuSpec> {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * sku的Id
     */
    private Integer skuId;

    /**
     * 属性名称
     */
    private String specName;

    /**
     * 属性值
     */
    private String specValue;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
