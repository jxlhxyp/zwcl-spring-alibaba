package com.zwcl.glass.shopping.goods.service.impl;

import com.zwcl.glass.shopping.goods.entity.GoodsBrand;
import com.zwcl.glass.shopping.goods.mapper.GoodsBrandMapper;
import com.zwcl.glass.shopping.goods.service.GoodsBrandService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-19
 */
@Service
public class GoodsBrandServiceImpl extends BaseServiceImpl<GoodsBrandMapper, GoodsBrand> implements GoodsBrandService {

}
