package com.zwcl.glass.shopping.goods.service.impl;

import com.zwcl.glass.shopping.goods.entity.GoodsSpecName;
import com.zwcl.glass.shopping.goods.mapper.GoodsSpecNameMapper;
import com.zwcl.glass.shopping.goods.service.GoodsSpecNameService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-19
 */
@Service
public class GoodsSpecNameServiceImpl extends BaseServiceImpl<GoodsSpecNameMapper, GoodsSpecName> implements GoodsSpecNameService {

}
