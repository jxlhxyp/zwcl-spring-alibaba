package com.zwcl.glass.tools.service;

import com.zwcl.glass.tools.entity.PaperProblem;
import com.zwcl.common.mybatis.service.BaseService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-07
 */
public interface PaperProblemService extends BaseService<PaperProblem> {
    /**
     * 保存
     *
     * @param paperProblem
     * @return
     * @throws Exception
     */
    boolean savePaperProblem(PaperProblem paperProblem) throws Exception;

    /**
     * 修改
     *
     * @param paperProblem
     * @return
     * @throws Exception
     */
    boolean updatePaperProblem(PaperProblem paperProblem) throws Exception;

    /**
     * 删除
     *
     * @param id
     * @return
     * @throws Exception
     */
    boolean deletePaperProblem(Long id) throws Exception;


    /**
     * 根据试卷id取试卷题目信息
     * @param paperId
     * @return
     */
    List<PaperProblem> getProblemsByPaperId(Long paperId);
}
