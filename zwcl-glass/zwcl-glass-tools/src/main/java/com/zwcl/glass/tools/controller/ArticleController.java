package com.zwcl.glass.tools.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.common.core.validator.groups.Add;
import com.zwcl.common.core.validator.groups.Update;
import com.zwcl.glass.tools.dto.SearchArticleDto;
import com.zwcl.glass.tools.entity.Article;
import com.zwcl.glass.tools.entity.ContentRead;
import com.zwcl.glass.tools.entity.UserCollect;
import com.zwcl.glass.tools.enums.CollectTypeEnum;
import com.zwcl.glass.tools.service.ArticleService;
import com.zwcl.glass.tools.service.ContentReadService;
import com.zwcl.glass.tools.service.UserCollectService;
import com.zwcl.glass.tools.vo.ArticleDetailVo;
import com.zwcl.glass.tools.vo.ArticleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
@RestController
@RequestMapping("/article")
public class ArticleController {
    @Autowired
    private ArticleService articleService;

    @Autowired
    private ContentReadService contentReadService;

    @Autowired
    private UserCollectService userCollectService;

    /**
     * 添加
     */
    @PostMapping("/add")
    //@OperationLog(name = "添加", type = OperationLogType.ADD)
    //@ApiOperation(value = "添加", response = ApiResult.class)
    public ApiResult<Boolean> addArticle(@Validated(Add.class) @RequestBody Article article) throws Exception {
        boolean flag = articleService.save(article);
        return ApiResult.result(flag);
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    //@OperationLog(name = "修改", type = OperationLogType.UPDATE)
    //@ApiOperation(value = "修改", response = ApiResult.class)
    public ApiResult<Boolean> updateArticle(@Validated(Update.class) @RequestBody Article article) throws Exception {
        boolean flag = articleService.updateById(article);
        return ApiResult.result(flag);
    }

    /**
     * 删除
     */
    @PostMapping("/delete/{id}")
    //@OperationLog(name = "删除", type = OperationLogType.DELETE)
    //@ApiOperation(value = "删除", response = ApiResult.class)
    public ApiResult<Boolean> deleteArticle(@PathVariable("id") Integer id) throws Exception {
        boolean flag = articleService.deleteEntity(id);
        return ApiResult.result(flag);
    }

    /**
     * 获取文章详情
     * @param id
     * @return
     */
    @GetMapping("/detail")
    public ArticleDetailVo getArticleContentById(@RequestParam Long id,@RequestParam Integer userId){
        ArticleDetailVo articleDetailVo = articleService.getArticleById(id);
        //这里要看自己的点赞和收藏
        UserCollect userCollect=userCollectService.getUserCollect(userId,id,CollectTypeEnum.ARTICLE.getValue());
        if(userCollect!=null){
            articleDetailVo.setIsCollect(userCollect.getIsCollect());
            articleDetailVo.setIsLike(userCollect.getIsLike());
        }
        return articleDetailVo;
    }

    /**
     * 搜索文章
     * @param dto
     * @return
     */
    @PostMapping("/search")
    public Page<ArticleVo> searchArticles(@RequestBody SearchArticleDto dto){
        Page<ArticleVo> page = new Page<>(dto.getCurrent(), dto.getSize());
        Page<ArticleVo> pageResult = articleService.searchArticles(page, dto);
        //这里要看点赞数
        List<Integer> idList = pageResult.getRecords().stream().map(x->x.getId()).collect(Collectors.toList());
        List<ContentRead> contentReadList = contentReadService.batchGetContentRead(idList, CollectTypeEnum.ARTICLE.getValue());
        Map<Long,ContentRead> contentReadMap=contentReadList.stream().collect(Collectors.toMap(x->x.getContentId(),y->y));
        for (ArticleVo item : pageResult.getRecords()){
            ContentRead contentRead=contentReadMap.get(Long.valueOf(item.getId()));
            if(null!=contentRead){
                item.setLikeNum(contentRead.getLikeNum());
                item.setReadNum(contentRead.getReadNum());
                item.setCollectNum(contentRead.getCollectNum());
            }
        }
        return pageResult;
    }
}

