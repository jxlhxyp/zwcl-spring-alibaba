package com.zwcl.glass.tools.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xieyongping
 * @since 2021-02-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("glass_user_collect")
public class UserCollect extends BaseEntity<UserCollect> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 1:文章，有其他的收藏再扩展 2:视频
     */
    private Integer contentType;

    /**
     * 收藏内容的id
     */
    private Long contentId;

    private Boolean isCollect;

    private Boolean isLike;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
