package com.zwcl.glass.tools.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.glass.tools.dto.CollectContentDto;
import com.zwcl.glass.tools.dto.SearchArticleDto;
import com.zwcl.glass.tools.dto.UserCollectDto;
import com.zwcl.glass.tools.entity.UserCollect;
import com.zwcl.common.mybatis.service.BaseService;
import com.zwcl.glass.tools.vo.ArticleVo;
import com.zwcl.glass.tools.vo.CollectContentVo;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-02-22
 */
public interface UserCollectService extends BaseService<UserCollect> {

    Boolean confirmCollect(@RequestBody UserCollectDto dto);

    /**
     * 取消收藏
     * @param dto
     * @return
     */
    Boolean cancleCollect(@RequestBody UserCollectDto dto);

    Page<CollectContentVo> searchCollects(Page<CollectContentVo> page, CollectContentDto dto);

    UserCollect getUserCollect(Integer userId,Long contentId,  Integer contentType);
}
