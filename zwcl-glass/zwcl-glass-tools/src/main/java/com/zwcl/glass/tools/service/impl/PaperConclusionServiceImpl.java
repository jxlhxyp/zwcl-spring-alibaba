package com.zwcl.glass.tools.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zwcl.glass.tools.entity.PaperConclusion;
import com.zwcl.glass.tools.mapper.PaperConclusionMapper;
import com.zwcl.glass.tools.service.PaperConclusionService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 规则得出结论，先默认为相与 服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-07
 */
@Service
public class PaperConclusionServiceImpl extends BaseServiceImpl<PaperConclusionMapper, PaperConclusion> implements PaperConclusionService {

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean savePaperConclusion(PaperConclusion paperConclusion) throws Exception {
        return super.save(paperConclusion);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updatePaperConclusion(PaperConclusion paperConclusion) throws Exception {
        return super.updateById(paperConclusion);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deletePaperConclusion(Long id) throws Exception {
        return super.removeById(id);
    }

    @Override
    public List<PaperConclusion> getPaperConclusionList(Integer paperId) {
        QueryWrapper<PaperConclusion> queryWrapper=new QueryWrapper<>();
        queryWrapper.lambda().eq(PaperConclusion::getPaperId,paperId).eq(PaperConclusion::getIsValid,1);
        return this.list(queryWrapper);
    }

    @Override
    public List<PaperConclusion> getPaperConclusionListByCode(String paperCode) {
        QueryWrapper<PaperConclusion> queryWrapper=new QueryWrapper<>();
        queryWrapper.lambda().eq(PaperConclusion::getPaperCode,paperCode).eq(PaperConclusion::getIsValid,1).orderByAsc(PaperConclusion::getId);
        return this.list(queryWrapper);
    }

    @Override
    public Map<String, String> getConclusionAndTagMap(String paperCode) {
        QueryWrapper<PaperConclusion> queryWrapper=new QueryWrapper<>();
        queryWrapper.lambda().select(PaperConclusion::getConclusionContent,PaperConclusion::getConclusionTags);
        queryWrapper.lambda().eq(PaperConclusion::getPaperCode,paperCode).eq(PaperConclusion::getIsValid,1).orderByAsc(PaperConclusion::getId);
        List<PaperConclusion> list = this.list(queryWrapper);
        Map<String,String> resultMap=new HashMap<>();
        //拆开做对应
        list.forEach(item->{
            if(StringUtils.isNotBlank(item.getConclusionContent()) && StringUtils.isNotBlank(item.getConclusionTags())){
                String[] contentArr=item.getConclusionContent().split(";");
                Map<String,String> contentMap=new HashMap<>();
                for(int i=0;i<contentArr.length;i++){
                    String[] contentTypeArr=contentArr[i].split(":");
                    contentMap.put(contentTypeArr[0].trim(),contentTypeArr[1].trim());
                }
                String[] tagsArr=item.getConclusionTags().split(";");
                Map<String,String> tagsMap=new HashMap<>();
                for(int i=0;i<tagsArr.length;i++){
                    String[] tagsTypeArr=tagsArr[i].split(":");
                    tagsMap.put(tagsTypeArr[0].trim(),tagsTypeArr[1].trim());
                }
                //找出Key的关键字标签
                for(Map.Entry<String,String> contentItem : contentMap.entrySet()){
                    if(tagsMap.containsKey(contentItem.getKey())){
                        resultMap.put(contentItem.getKey()+":"+contentItem.getValue(),contentItem.getKey()+":"+tagsMap.get(contentItem.getKey()));
                    }
                }
            }
        });
        return resultMap;
    }

}
