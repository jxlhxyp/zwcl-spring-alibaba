package com.zwcl.glass.tools.entity;

import lombok.Data;

import java.util.List;

/**
 * 冲突规则
 */
@Data
public class ConflictRule {
    private  Integer id;

    private Integer paperId;

    private Integer ruleType;

    private List<ResultRule> ruleContent;

    private Boolean isValid;
}
