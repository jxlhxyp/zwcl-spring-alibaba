package com.zwcl.glass.tools.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.zwcl.common.core.validator.groups.Update;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.io.Serializable;
import java.util.Date;

/**
 * 
 *
 * @author xyp
 * @since 2020-10-28
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName("glass_test_record")
//@ApiModel(value = "TestRecord对象")
public class TestRecord extends BaseEntity<TestRecord> {
    private static final long serialVersionUID = 1L;

    @NotNull(message = "id不能为空", groups = {Update.class})
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @NotNull(message = "试卷id不能为空")
    //@ApiModelProperty("试卷id")
    private Integer paperId;

    @NotNull(message = "试卷编码不能为空")
    //@ApiModelProperty("试卷id")
    private String paperCode;

    @NotNull(message = "用户Id不能为空")
    //@ApiModelProperty("用户Id")
    private Integer userId;

    //@ApiModelProperty("测试结果")
    private String testResult;

    //@ApiModelProperty("自动结论，存储一些id分割即可")
    private String autoConclusion;

    private String autoTags;

    //@ApiModelProperty("人工结论")
    private String manualConclusion;

    //@ApiModelProperty("测试时间")
    private Date testTime;

    //@ApiModelProperty("用户手机号")
    private String phone;

    //@ApiModelProperty("备注")
    private String remark;

    //@ApiModelProperty("版本")
    @Null(message = "版本不用传")
    @Version
    private Integer version;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
