package com.zwcl.glass.tools.entity;

import lombok.Data;

import java.util.List;

@Data
public class ResultRuleEx {
    /**
     * 选项符合的判断规则
     */
    private List<OptionRule> optionRules;

    /**
     * 值范围的判断规则
     */
    private List<ValueRule> valueRules;

    /**
     * 直接格式化显示的规则
     */
    private ShowRule showRule;
}
