package com.zwcl.glass.tools.controller;


import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.common.core.validator.groups.Add;
import com.zwcl.common.core.validator.groups.Update;
import com.zwcl.glass.tools.api.dto.UserTagsDto;
import com.zwcl.glass.tools.api.entity.UserReport;
import com.zwcl.glass.tools.api.enums.TestCodeEnum;
import com.zwcl.glass.tools.entity.UserData;
import com.zwcl.glass.tools.service.TestRecordService;
import com.zwcl.glass.tools.service.UserDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
@RestController
@RequestMapping("/userData")
public class UserDataController {

    @Autowired
    private UserDataService userDataService;

    @Autowired
    private TestRecordService testRecordService;

    /**
     * 添加
     */
    @PostMapping("/add")
    public ApiResult<Boolean> addUserData(@Validated(Add.class) @RequestBody UserData userData) throws Exception {
        boolean flag = userDataService.save(userData);
        return ApiResult.result(flag);
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public ApiResult<Boolean> updateArticle(@Validated(Update.class) @RequestBody UserData userData) throws Exception {
        boolean flag = userDataService.updateById(userData);
        return ApiResult.result(flag);
    }

    /**
     * 删除
     */
    @PostMapping("/delete/{id}")
    public ApiResult<Boolean> deleteUserData(@PathVariable("id") Long id) throws Exception {
        boolean flag = userDataService.deleteEntity(id);
        return ApiResult.result(flag);
    }

    @GetMapping("/list")
    public List<UserData> listUserData(@RequestParam Long userId,@RequestParam Integer relationshipId){
        List<UserData> userData = userDataService.getUserDatasByUserId(userId,relationshipId);
        return userData;
    }

    /**
     * 最新的验光数据
     * @param userId    当前用户的id
     * @param relationshipId 与其相关联人的关系id
     * @return
     */
    @GetMapping("/last")
    public UserData getUserLastData(@RequestParam Long userId,@RequestParam Integer relationshipId){
        return userDataService.getUserLastData(userId,relationshipId);
    }

    /**
     * 获取用户有的测试报告
     * @return
     */
    @GetMapping("/report")
    public UserReport getUserReport(Integer userId){
        List<String> paperCodes= TestCodeEnum.getMap().entrySet().stream().map(x->x.getKey()).collect(Collectors.toList());
        UserReport userReport = testRecordService.getUserTestReport(paperCodes,userId);
        return userReport;
    }

    /**
     * 获取用户的画像
     * @param dto
     * @return
     */
    @PostMapping("/userTags")
    public UserReport getUserReport(@RequestBody UserTagsDto dto){
        UserReport userReport = testRecordService.getUserTestReport(dto.getPaperCodes(),dto.getUserId());
        return userReport;
    }

}

