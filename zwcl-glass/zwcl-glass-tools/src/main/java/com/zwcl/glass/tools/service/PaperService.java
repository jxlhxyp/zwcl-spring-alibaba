package com.zwcl.glass.tools.service;

import com.zwcl.common.core.exception.BusinessException;
import com.zwcl.glass.tools.entity.Paper;
import com.zwcl.common.mybatis.service.BaseService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-07
 */
public interface PaperService extends BaseService<Paper> {

    /**
     * 获取试卷对象
     *
     * @param
     * @return
     * @throws Exception
     */
    List<Paper> getPaperList(Integer paperType) throws BusinessException;

    Paper findPaperByCode(String paperCode);

    /**
     * 保存
     *
     * @param glassPaper
     * @return
     * @throws Exception
     */
    boolean savePaper(Paper glassPaper) throws Exception;

    /**
     * 修改
     *
     * @param glassPaper
     * @return
     * @throws Exception
     */
    boolean updatePaper(Paper glassPaper) throws Exception;

    /**
     * 删除
     *
     * @param id
     * @return
     * @throws Exception
     */
    boolean deletePaper(Integer id) throws Exception;

    boolean updateTestNum(Integer id);
}
