package com.zwcl.glass.tools.dto;

import com.zwcl.common.core.domain.dto.BaseDto;
import lombok.Data;

@Data
public class SearchArticleDto extends BaseDto {

    private Integer categoryId;

    private String keyWords;

}
