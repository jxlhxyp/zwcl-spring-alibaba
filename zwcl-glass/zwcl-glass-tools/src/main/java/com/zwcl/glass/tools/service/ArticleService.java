package com.zwcl.glass.tools.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.glass.tools.dto.SearchArticleDto;
import com.zwcl.glass.tools.entity.Article;
import com.zwcl.common.mybatis.service.BaseService;
import com.zwcl.glass.tools.vo.ArticleDetailVo;
import com.zwcl.glass.tools.vo.ArticleVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
public interface ArticleService extends BaseService<Article> {

    ArticleDetailVo getArticleById(Long id);

    Page<ArticleVo> searchArticles(Page<ArticleVo> page,SearchArticleDto dto);

    Boolean deleteEntity(Integer id);
}
