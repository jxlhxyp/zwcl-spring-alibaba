package com.zwcl.glass.tools.controller;


import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.common.core.validator.groups.Add;
import com.zwcl.common.core.validator.groups.Update;
import com.zwcl.glass.tools.entity.PaperProblem;
import com.zwcl.glass.tools.service.PaperProblemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-07
 */
@RestController
@RequestMapping("/problem")
@Slf4j
public class PaperProblemController {
    @Autowired
    private PaperProblemService paperProblemService;

    /**
     * 添加
     */
    @PostMapping("/add")
    //@OperationLog(name = "添加", type = OperationLogType.ADD)
    //@ApiOperation(value = "添加", response = ApiResult.class)
    public ApiResult<Boolean> addPaperProblem(@Validated(Add.class) @RequestBody PaperProblem paperProblem) throws Exception {
        boolean flag = paperProblemService.savePaperProblem(paperProblem);
        return ApiResult.result(flag);
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    //@OperationLog(name = "修改", type = OperationLogType.UPDATE)
    //@ApiOperation(value = "修改", response = ApiResult.class)
    public ApiResult<Boolean> updatePaperProblem(@Validated(Update.class) @RequestBody PaperProblem paperProblem) throws Exception {
        boolean flag = paperProblemService.updatePaperProblem(paperProblem);
        return ApiResult.result(flag);
    }

    /**
     * 删除
     */
    @PostMapping("/delete/{id}")
    //@OperationLog(name = "删除", type = OperationLogType.DELETE)
    //@ApiOperation(value = "删除", response = ApiResult.class)
    public ApiResult<Boolean> deletePaperProblem(@PathVariable("id") Long id) throws Exception {
        boolean flag = paperProblemService.deletePaperProblem(id);
        return ApiResult.result(flag);
    }

    /**
     * 获取详情
     */
    @GetMapping("/info/{id}")
    //@OperationLog(name = "详情", type = OperationLogType.INFO)
    //@ApiOperation(value = "详情", response = PaperProblem.class)
    public ApiResult<PaperProblem> getPaperProblem(@PathVariable("id") Long id) throws Exception {
        PaperProblem paperProblem = paperProblemService.getById(id);
        return ApiResult.ok(paperProblem);
    }

}

