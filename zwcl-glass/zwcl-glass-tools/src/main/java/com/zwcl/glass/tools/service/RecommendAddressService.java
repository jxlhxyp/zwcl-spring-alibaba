package com.zwcl.glass.tools.service;

import com.zwcl.glass.tools.dto.MapNearDto;
import com.zwcl.glass.tools.entity.RecommendAddress;
import com.zwcl.common.mybatis.service.BaseService;

import java.io.InputStream;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-02-25
 */
public interface RecommendAddressService extends BaseService<RecommendAddress> {

    void deleteCityData(List<String> cityList);

    List<RecommendAddress> searchRecommondAddress(MapNearDto dto);
}
