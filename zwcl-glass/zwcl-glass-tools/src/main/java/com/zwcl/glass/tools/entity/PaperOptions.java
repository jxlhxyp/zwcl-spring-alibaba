package com.zwcl.glass.tools.entity;

import lombok.Data;

/**
 * 试卷中的选择题
 */
@Data
public class PaperOptions {

    private String option;

    private OptionContent content;

    private String value;

    private Integer[] showTi;

    /**
     * 其他的内容
     */
    private OptionOther other;

    public class OptionContent{

        public String text;

        public String img;

        public String imgDesc;

    }

    public class OptionOther{
        public Integer recommend;
    }
}

