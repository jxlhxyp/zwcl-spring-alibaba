package com.zwcl.glass.tools.entity;

import lombok.Data;

import java.util.List;

@Data
public class ValueRule {
    /**
     * 题目id的数组
     */
    private List<Integer> problemIds;

    /**
     * 统计值的最小值
     */
    private Double countMin;

    /**
     * 统计值的最大值
     */
    private Double countMax;

    /**
     * 1为计算折射率，2为统计个数比较
     */
    private Integer type;
}
