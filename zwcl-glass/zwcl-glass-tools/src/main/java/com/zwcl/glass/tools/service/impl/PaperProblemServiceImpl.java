package com.zwcl.glass.tools.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.glass.tools.entity.PaperProblem;
import com.zwcl.glass.tools.mapper.PaperProblemMapper;
import com.zwcl.glass.tools.service.PaperProblemService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-07
 */
@Service
public class PaperProblemServiceImpl extends BaseServiceImpl<PaperProblemMapper, PaperProblem> implements PaperProblemService {

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean savePaperProblem(PaperProblem paperProblem) throws Exception {
        return super.save(paperProblem);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updatePaperProblem(PaperProblem paperProblem) throws Exception {
        return super.updateById(paperProblem);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deletePaperProblem(Long id) throws Exception {
        return super.removeById(id);
    }

    @Override
    public List<PaperProblem> getProblemsByPaperId(Long paperId) {
        QueryWrapper<PaperProblem> queryWrapper=new QueryWrapper<>();
        queryWrapper.lambda().eq(PaperProblem::getPaperId,paperId).eq(PaperProblem::getIsValid,1).eq(PaperProblem::getDelFlag,false).orderByAsc(PaperProblem::getOrderIndex);;
        return this.list(queryWrapper);
    }

}
