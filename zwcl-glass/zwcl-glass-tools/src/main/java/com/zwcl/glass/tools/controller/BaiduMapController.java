package com.zwcl.glass.tools.controller;

import com.alibaba.fastjson.JSONArray;
import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.common.core.utils.JsonUtils;
import com.zwcl.glass.tools.dto.MapNearDto;
import com.zwcl.glass.tools.entity.ArticleComment;
import com.zwcl.glass.tools.entity.LocationPoint;
import com.zwcl.glass.tools.entity.PoiLocation;
import com.zwcl.glass.tools.entity.RecommendAddress;
import com.zwcl.glass.tools.service.RecommendAddressService;
import com.zwcl.glass.tools.utils.BaiduMapUtils;
import com.zwcl.glass.tools.utils.MapCountUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping("/baidu")
public class BaiduMapController {
    @Autowired
    private RecommendAddressService recommendAddressService;
    /**
     * 添加
     */
    @PostMapping("/near")
    public List<PoiLocation> addArticle(@RequestBody MapNearDto dto) throws Exception {
        List<PoiLocation> list = new ArrayList<>();
        //1.先查数据库
        List<RecommendAddress> recommendAddressList=recommendAddressService.searchRecommondAddress(dto);
        if (!CollectionUtils.isEmpty(recommendAddressList)){
            for(RecommendAddress item : recommendAddressList){
                PoiLocation poiLocation=new PoiLocation();
                poiLocation.setName(item.getName());
                poiLocation.setCity(item.getCity());
                poiLocation.setAddress(item.getAddress());
                LocationPoint point = new LocationPoint();
                point.setLat(new BigDecimal(item.getLat()));
                point.setLng(new BigDecimal(item.getLng()));
                poiLocation.setLocation(point);
                poiLocation.setRecommendNum(item.getRecommendNum());
                poiLocation.setRecommendReason(item.getRecommendReason());
                poiLocation.setDescription(item.getDescription());
                Double distance = MapCountUtil.getDistance(dto.getLng(), dto.getLat(), point.getLng(), point.getLat());
                poiLocation.setDistance(distance);
                if(distance<=dto.getRadius()) {
                    list.add(poiLocation);
                }
            }
        }else {
            //2.数据库没有数据则查百度
            JSONArray result = BaiduMapUtils.getNearPoiInfo(dto.getLat().toString(), dto.getLng().toString(), dto.getKeyWords(), dto.getRadius());
            list = JsonUtils.jsonToList(JsonUtils.objectToJson(result), PoiLocation.class);
            //如果太慢了，则可以单独去读取
            for (PoiLocation item : list) {
                Double distance = MapCountUtil.getDistance(dto.getLng(), dto.getLat(), item.getLocation().getLng(), item.getLocation().getLat());
                item.setDistance(distance);
            }
        }
        Collections.sort(list, new Comparator<PoiLocation>() {
            @Override
            public int compare(PoiLocation A1, PoiLocation A2) {
                return A1.getDistance().compareTo(A2.getDistance());
            }
        });
        return list;
    }
}
