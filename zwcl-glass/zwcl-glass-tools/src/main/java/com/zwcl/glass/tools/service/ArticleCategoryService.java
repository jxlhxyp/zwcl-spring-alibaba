package com.zwcl.glass.tools.service;

import com.zwcl.glass.tools.entity.ArticleCategory;
import com.zwcl.common.mybatis.service.BaseService;
import sun.rmi.runtime.Log;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
public interface ArticleCategoryService extends BaseService<ArticleCategory> {

    List<ArticleCategory> getArticleCategoryList();

    ArticleCategory getArticleCategoryById(Integer id);

    Boolean deleteEntity(Integer id);
}
