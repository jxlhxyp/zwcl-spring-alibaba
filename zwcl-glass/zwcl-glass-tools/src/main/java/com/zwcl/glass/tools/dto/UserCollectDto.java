package com.zwcl.glass.tools.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserCollectDto {
    @NotNull(message = "用户id不能为空")
    private Integer userId;

    @NotNull(message = "内容id不能为空")
    private Long contentId;

    @NotNull(message = "内容类型不能为空")
    private Integer contentType;

    /**
     * 操作收藏，操作收藏时，点赞不用传
     */
    private Boolean isCollect;

    /**
     * 操作点赞
     */
    private Boolean isLike;

}
