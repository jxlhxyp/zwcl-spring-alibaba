package com.zwcl.glass.tools.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xieyongping
 * @since 2021-02-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("glass_recommend_address")
public class RecommendAddress extends BaseEntity<RecommendAddress> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 地址名称
     */
    private String name;

    /**
     * 1.眼镜店  2. 验光中心 3. 综合医院 4. 眼科医院
     */
    private Integer poiType;

    /**
     * 城市
     */
    private String city;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 经度
     */
    private String lng;

    /**
     * 纬度
     */
    private String lat;

    /**
     * 推荐指数
     */
    private Integer recommendNum;

    /**
     * 推荐理由
     */
    private String recommendReason;

    /**
     * 简介
     */
    private String description;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
