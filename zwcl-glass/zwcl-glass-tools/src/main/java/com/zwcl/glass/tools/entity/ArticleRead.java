package com.zwcl.glass.tools.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("glass_article_read")
public class ArticleRead extends BaseEntity<ArticleRead> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 文章id
     */
    @NotNull(message = "文章ID不能为空")
    private Long articleId;

    /**
     * 阅读数
     */
    private Integer readNum;

    /**
     * 点赞数
     */
    private Integer likeNum;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
