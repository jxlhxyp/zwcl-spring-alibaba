package com.zwcl.glass.tools.dto;

import lombok.Data;

import java.util.List;

/**
 * 查用户测试记录时，传userId，paperCode比较好
 */
@Data
public class HasTestDto {

    private Integer userId;
    /**
     * 可以不传
     */
    private String phone;

    private Integer paperId;


    private String paperCode;

    /**
     *  true 查询所有 false查询最新的一条
     */
    private Boolean getAll;
}
