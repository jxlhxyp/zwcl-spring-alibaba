package com.zwcl.glass.tools.entity;

import com.zwcl.common.core.domain.entity.BaseEntity;
import lombok.Data;

/**
 * 用户作答的答案
 */
@Data
public class AnswerResult{
    /**
     * 题目id
     */
    private Integer problemId;

    /**
     * 题型，增加
     */
    private Integer problemType;

    /**
     * 选择题结果，传"A","C"
     * 滑块题结果,传"rightFlood:0.75","rightShortSighted:3.00"这样
     */
    private String[] result;

    /**
     * double值类型
     */
    private String[] value;

    /**
     * 直接格式化展示的值
     */
    private String[] showValue;

}
