package com.zwcl.glass.tools.controller;


import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.common.core.utils.JsonUtils;
import com.zwcl.common.core.validator.groups.Add;
import com.zwcl.common.core.validator.groups.Update;
import com.zwcl.glass.tools.entity.*;
import com.zwcl.glass.tools.service.PaperConclusionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 规则得出结论，先默认为相与 前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-07
 */
@RestController
@RequestMapping("/conclusion")
@Slf4j
public class PaperConclusionController {
    @Autowired
    private PaperConclusionService paperConclusionService;

    /**
     * 添加规则得出结论，先默认为相与
     */
    @PostMapping("/add")
    //@OperationLog(name = "添加规则得出结论，先默认为相与", type = OperationLogType.ADD)
    //@ApiOperation(value = "添加规则得出结论，先默认为相与", response = ApiResult.class)
    public ApiResult<Boolean> addPaperConclusion(@Validated(Add.class) @RequestBody PaperConclusion paperConclusion) throws Exception {
        boolean flag = paperConclusionService.savePaperConclusion(paperConclusion);
        return ApiResult.result(flag);
    }

    /**
     * 修改规则得出结论，先默认为相与
     */
    @PostMapping("/update")
    //@OperationLog(name = "修改规则得出结论，先默认为相与", type = OperationLogType.UPDATE)
    //@ApiOperation(value = "修改规则得出结论，先默认为相与", response = ApiResult.class)
    public ApiResult<Boolean> updatePaperConclusion(@Validated(Update.class) @RequestBody PaperConclusion paperConclusion) throws Exception {
        boolean flag = paperConclusionService.updatePaperConclusion(paperConclusion);
        return ApiResult.result(flag);
    }

    /**
     * 删除规则得出结论，先默认为相与
     */
    @PostMapping("/delete/{id}")
    //@OperationLog(name = "删除规则得出结论，先默认为相与", type = OperationLogType.DELETE)
    //@ApiOperation(value = "删除规则得出结论，先默认为相与", response = ApiResult.class)
    public ApiResult<Boolean> deletePaperConclusion(@PathVariable("id") Long id) throws Exception {
        boolean flag = paperConclusionService.deletePaperConclusion(id);
        return ApiResult.result(flag);
    }

    /**
     * 获取规则得出结论，先默认为相与详情
     */
    @GetMapping("/info/{id}")
    //@OperationLog(name = "规则得出结论，先默认为相与详情", type = OperationLogType.INFO)
    //@ApiOperation(value = "规则得出结论，先默认为相与详情", response = PaperConclusion.class)
    public ApiResult<PaperConclusion> getPaperConclusion(@PathVariable("id") Long id) throws Exception {
        PaperConclusion paperConclusion = paperConclusionService.getById(id);
        return ApiResult.ok(paperConclusion);
    }

    /**
     * 获取试卷的结论规则
     */
    @GetMapping("/content/{paperId}")
    //@OperationLog(name = "详情", type = OperationLogType.INFO)
    //@ApiOperation(value = "详情", response = Paper.class)
    public ApiResult<List<ConclusionRule>> getPaperConclusionRules(@PathVariable("paperId") Integer paperId) throws Exception {
        List<PaperConclusion> paperConclusions = paperConclusionService.getPaperConclusionList(paperId);
        List<ConclusionRule> voList=new ArrayList<>();
        paperConclusions.forEach(item->{
            ConclusionRule vo=new ConclusionRule();
            BeanUtils.copyProperties(item,vo);
            List<ResultRule> resultRules= JsonUtils.jsonToList(item.getRuleContent(), ResultRule.class);
            vo.setRuleContent(resultRules);
            voList.add(vo);
        });
        return ApiResult.ok(voList);
    }

    /**
     * 获取试卷的结论规则
     */
    @GetMapping("/contentEx/{paperCode}")
    //@OperationLog(name = "详情", type = OperationLogType.INFO)
    //@ApiOperation(value = "详情", response = Paper.class)
    public ApiResult<List<ConclusionRuleEx>> getPaperConclusionRulesEx(@PathVariable("paperCode") String paperCode) throws Exception {
        List<PaperConclusion> paperConclusions = paperConclusionService.getPaperConclusionListByCode(paperCode);
        List<ConclusionRuleEx> voList=new ArrayList<>();
        paperConclusions.forEach(item->{
            ConclusionRuleEx vo =new ConclusionRuleEx();
            BeanUtils.copyProperties(item,vo);
            ResultRuleEx resultRule = JsonUtils.jsonToPojo(item.getRuleContent(),ResultRuleEx.class);
            vo.setRuleContent(resultRule);
            voList.add(vo);
        });
        return ApiResult.ok(voList);
    }

    /**
     * 获取试卷的结论规则
     */
    @GetMapping("/tagMap")
    //@OperationLog(name = "详情", type = OperationLogType.INFO)
    //@ApiOperation(value = "详情", response = Paper.class)
    public Map<String,String> getConclusionAndTagMap(@RequestParam("paperCode") String paperCode) {
        return paperConclusionService.getConclusionAndTagMap(paperCode);
    }
}

