package com.zwcl.glass.tools.vo;

import com.zwcl.glass.tools.entity.AnswerResult;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class TestRecordVO {

    private Integer id;

    private Integer paperId;

    private Integer userId;

    private List<AnswerResult> testResultList;

    private String autoConclusion;

    private String autoTags;

    private String manualConclusion;

    private Date testTime;

    private String phone;
}
