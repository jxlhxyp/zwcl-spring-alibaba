package com.zwcl.glass.tools.vo;

import lombok.Data;

@Data
public class UserTestRecordVo {

    private String paperCode;

    private Integer paperId;

    private Integer userId;

    private Integer testTime;
}
