package com.zwcl.glass.tools.entity;

import lombok.Data;

import java.util.List;

@Data
public class ConclusionRule {
    private  Integer id;

    private Integer paperId;

    private String conclusionContent;

    private String typeCode;

    private List<ResultRule> ruleContent;

    private String conclusionPic;

    private Boolean isValid;
}
