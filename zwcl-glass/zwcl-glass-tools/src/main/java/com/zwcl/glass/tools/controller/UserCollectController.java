package com.zwcl.glass.tools.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.glass.tools.dto.CollectContentDto;
import com.zwcl.glass.tools.dto.UserCollectDto;
import com.zwcl.glass.tools.entity.UserCollect;
import com.zwcl.glass.tools.enums.CollectTypeEnum;
import com.zwcl.glass.tools.service.ArticleService;
import com.zwcl.glass.tools.service.ContentReadService;
import com.zwcl.glass.tools.service.UserCollectService;
import com.zwcl.glass.tools.vo.ArticleDetailVo;
import com.zwcl.glass.tools.vo.ArticleVo;
import com.zwcl.glass.tools.vo.CollectContentVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  用户收藏数据
 * </p>
 *
 * @author xieyongping
 * @since 2021-02-22
 */
@RestController
@RequestMapping("/userCollect")
public class UserCollectController {

    @Autowired
    private UserCollectService userCollectService;

    @Autowired
    private ArticleService articleService;

    /**
     * 确认收藏
     * @param dto
     * @return
     */
    @PostMapping("/operation")
    public Boolean confirmCollect(@RequestBody UserCollectDto dto){
        return userCollectService.confirmCollect(dto);
    }

//    /**
//     * 取消收藏
//     * @param dto
//     * @return
//     */
//    @PostMapping("/cancel")
//    public Boolean cancleCollect(@RequestBody UserCollectDto dto){
//        return userCollectService.cancleCollect(dto);
//    }



    @GetMapping("/type")
    public Map<Integer,String> getCollectType(){
        return CollectTypeEnum.getMap();
    }

    /**
     * 查询用户的收藏列表
     */
    @GetMapping("/page")
    public Page<CollectContentVo> getCollectPage(@RequestBody CollectContentDto dto){
        Page<CollectContentVo> page = new Page<>(dto.getCurrent(), dto.getSize());
        Page<CollectContentVo> pageResult = userCollectService.searchCollects(page, dto);
        //再循环找相关的数据
        List<CollectContentVo> userCollectList=pageResult.getRecords();
        for(CollectContentVo item : userCollectList){
            if(item.getContentType().equals(CollectTypeEnum.ARTICLE.getValue())){      //文章
                ArticleDetailVo detailVo = articleService.getArticleById(item.getContentId());
                if(detailVo!=null){
                    item.setContentEntity(detailVo);
                }
            }
        }
        return pageResult;
    }
}

