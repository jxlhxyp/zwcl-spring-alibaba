package com.zwcl.glass.tools.service;

import com.zwcl.glass.tools.entity.ContentRead;
import com.zwcl.common.mybatis.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
public interface ContentReadService extends BaseService<ContentRead> {

    Boolean saveContentReadCount(ContentRead contentRead);

    ContentRead getContentReadById(Long contentId,Integer contentType);

    Boolean saveContentOperation(ContentRead contentRead);

    List<ContentRead> batchGetContentRead(List<Integer> idList, Integer contentType);
}
