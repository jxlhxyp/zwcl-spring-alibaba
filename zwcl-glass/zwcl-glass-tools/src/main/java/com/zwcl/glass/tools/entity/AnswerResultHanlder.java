package com.zwcl.glass.tools.entity;

import lombok.Data;

/**
 * 用户做答的答案进行中间处理后
 * 选择题不用处理了
 * 如果是折射率滑块题，需要计算成值类型
 * 预留给以后支持泛型
 */
@Data
public class AnswerResultHanlder {
    /**
     * 题目id
     */
    private Integer problemId;

    /**
     * 0，选项类型
     * 1，值类型
     * 2， 个数统计类型
     * 3，值直接格式化显示
     */
    private Integer valueType;

    /**
     * 选择题结果，传"A","C"
     * String[]
     * 滑块题结果,传"rightFlood:0.75","rightShortSighted:3.00"这样
     * Map<String,Float>
     */
    private String[] result;

    /**
     * 最终的值
     */
    private double value;

    /**
     * 直接格式化暂时的值
     */
    private String[] showValue;
}
