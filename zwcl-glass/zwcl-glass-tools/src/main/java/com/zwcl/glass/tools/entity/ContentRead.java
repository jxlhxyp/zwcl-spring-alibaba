package com.zwcl.glass.tools.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("glass_content_read")
public class ContentRead extends BaseEntity<ContentRead> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 文章id
     */
    @NotNull(message = "内容ID不能为空")
    private Long contentId;

    @NotNull(message = "内容类型不能为空")
    private Integer contentType;
    /**
     * 阅读数
     */
    private Integer readNum;

    /**
     * 点赞数
     */
    private Integer likeNum;

    /**
     * 收藏数
     */
    private Integer collectNum;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
