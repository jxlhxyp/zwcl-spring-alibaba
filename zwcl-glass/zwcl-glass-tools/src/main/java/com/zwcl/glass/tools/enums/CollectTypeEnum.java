package com.zwcl.glass.tools.enums;

import com.zwcl.common.core.exception.BusinessException;
import com.zwcl.common.core.utils.JsonUtils;
import com.zwcl.common.core.utils.StringUtilsEx;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.IntStream;

public enum  CollectTypeEnum {

    ARTICLE(1, "文章"),

    VEDIO(2, "视频"),


    ;
    /**
     * value
     */
    private Integer value;

    /**
     * desc
     */
    private String desc;




    private static final Map<Integer, String> map;


    static {
        CollectTypeEnum[] enums = CollectTypeEnum.values();
        int size = enums.length;
        map = IntStream.range(0, size).collect(LinkedHashMap::new, (map, desc) -> {
            map.put(enums[desc].getValue(), enums[desc].getDesc());
        }, Map::putAll);

    }


    /**
     * @param value          value值
     * @param desc           desc值
     */
    CollectTypeEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    /**
     * 返回value
     *
     * @return value
     */
    public Integer getValue() {
        return this.value;
    }

    /**
     * 返回desc
     *
     * @return desc 字符串
     */
    public String getDesc() {
        return this.desc;
    }


    /**
     * 根据value获取desc
     *
     * @param value value
     * @return 处理结果
     */
    public static String getDesc(Integer value) {
        return getMap().get(value) == null ? StringUtilsEx.EMPTY : getMap().get(value);
    }


    public static CollectTypeEnum of(String value) {
        CollectTypeEnum[] values = CollectTypeEnum.values();
        for (CollectTypeEnum anEnum : values) {
            if (anEnum.getValue().equals(value)) {
                return anEnum;
            }
        }
        throw new BusinessException("不存在的商品类别");
    }

    /**
     * 获取map
     *
     * @return 返回map
     */
    public static Map<Integer, String> getMap() {
        return map;
    }

    /**
     * 获取json
     *
     * @return 处理结果
     * @throws BusinessException 自定义异常
     */
    public static String getJson() throws BusinessException {
        return JsonUtils.objectToJson(getMap());
    }
}
