package com.zwcl.glass.tools.controller;


import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.common.core.validator.groups.Add;
import com.zwcl.common.core.validator.groups.Update;
import com.zwcl.glass.tools.entity.ArticleCategory;
import com.zwcl.glass.tools.entity.Paper;
import com.zwcl.glass.tools.service.ArticleCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
@RestController
@RequestMapping("/articleCategory")
public class ArticleCategoryController {

    @Autowired
    private ArticleCategoryService articleCategoryService;

    /**
     * 添加
     */
    @PostMapping("/add")
    //@OperationLog(name = "添加", type = OperationLogType.ADD)
    //@ApiOperation(value = "添加", response = ApiResult.class)
    public ApiResult<Boolean> addArticleCategory(@Validated(Add.class) @RequestBody ArticleCategory articleCategory) throws Exception {
        boolean flag = articleCategoryService.save(articleCategory);
        return ApiResult.result(flag);
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    //@OperationLog(name = "修改", type = OperationLogType.UPDATE)
    //@ApiOperation(value = "修改", response = ApiResult.class)
    public ApiResult<Boolean> updateArticleCategory(@Validated(Update.class) @RequestBody ArticleCategory articleCategory) throws Exception {
        boolean flag = articleCategoryService.updateById(articleCategory);
        return ApiResult.result(flag);
    }

    /**
     * 删除
     */
    @PostMapping("/delete/{id}")
    //@OperationLog(name = "删除", type = OperationLogType.DELETE)
    //@ApiOperation(value = "删除", response = ApiResult.class)
    public ApiResult<Boolean> deleteArticleCategory(@PathVariable("id") Integer id) throws Exception {
        boolean flag = articleCategoryService.deleteEntity(id);
        return ApiResult.result(flag);
    }

    //TODO:以后进行缓存处理
    @GetMapping("/list")
    public List<ArticleCategory> listArticleCategory(){
        return articleCategoryService.getArticleCategoryList();
    }

}

