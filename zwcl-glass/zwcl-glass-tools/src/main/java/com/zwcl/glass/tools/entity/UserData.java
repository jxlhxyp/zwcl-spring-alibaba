package com.zwcl.glass.tools.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zwcl.common.core.domain.entity.BaseEntity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *  用户录入的档案
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("glass_user_data")
public class UserData extends BaseEntity<UserData> {

    private static final long serialVersionUID = 1L;

    /**
     * 20
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Integer userId;

    private Integer relationshipId;

    /**
     * 左眼度数
     */
    private BigDecimal leftDegree;

    /**
     * 左眼散光
     */
    private BigDecimal leftAstigmatism;

    /**
     * 左眼轴向
     */
    private Integer leftAxial;

    /**
     * 右眼度数
     */
    private BigDecimal rightDegree;

    /**
     * 右眼散光
     */
    private BigDecimal rightAstigmatism;

    /**
     * 右眼轴向
     */
    private Integer rightAxial;

    /**
     * 瞳距
     */
    private Integer ipd;

    /**
     * 左瞳高
     */
    private BigDecimal leftIph;

    /**
     * 左棱镜
     */
    private Integer leftPrism;

    /**
     * 左下加光
     */
    private BigDecimal leftLowLight;

    /**
     * 右瞳高
     */
    private BigDecimal rightIph;

    /**
     * 右棱镜
     */
    private Integer rightPrism;

    /**
     * 右下加光
     */
    private BigDecimal rightLowLight;

    /**
     * 屈光状态，1：远视  2：老花
     */
    private Integer refractionStatus;

    /** 验光时间 */
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate optometryTime;

    /**
     * 上传的图片
     */
    private String picUrl;



    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
