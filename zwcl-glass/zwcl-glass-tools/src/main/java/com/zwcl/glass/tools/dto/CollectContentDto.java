package com.zwcl.glass.tools.dto;

import com.zwcl.common.core.domain.dto.BaseDto;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CollectContentDto extends BaseDto {
    @NotNull(message = "用户id不能为空")
    private Integer userId;

    @NotNull(message = "收藏类型不能为空")
    private Integer collectType;
}
