package com.zwcl.glass.tools.controller;


import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.common.core.utils.JsonUtils;
import com.zwcl.common.core.validator.groups.Add;
import com.zwcl.common.core.validator.groups.Update;
import com.zwcl.glass.tools.entity.Paper;
import com.zwcl.glass.tools.entity.PaperOptions;
import com.zwcl.glass.tools.entity.PaperProblem;
import com.zwcl.glass.tools.entity.PaperSlider;
import com.zwcl.glass.tools.enums.ProblemTypeEnum;
import com.zwcl.glass.tools.service.PaperProblemService;
import com.zwcl.glass.tools.service.PaperService;
import com.zwcl.glass.tools.service.TestRecordService;
import com.zwcl.glass.tools.vo.PaperProblemVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-07
 */
@Slf4j
@RestController
@RequestMapping("/paper")
//@Module("")
//@Api(value = "API", tags = {"试卷"})
public class PaperController {

    @Autowired
    private PaperService glassPaperService;

    @Autowired
    private PaperProblemService paperProblemService;

    @Autowired
    private TestRecordService testRecordService;

    /**
     * 添加
     */
    @PostMapping("/add")
    //@OperationLog(name = "添加", type = OperationLogType.ADD)
    //@ApiOperation(value = "添加", response = ApiResult.class)
    public ApiResult<Boolean> addPaper(@Validated(Add.class) @RequestBody Paper glassPaper) throws Exception {
        boolean flag = glassPaperService.savePaper(glassPaper);
        return ApiResult.result(flag);
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    //@OperationLog(name = "修改", type = OperationLogType.UPDATE)
    //@ApiOperation(value = "修改", response = ApiResult.class)
    public ApiResult<Boolean> updatePaper(@Validated(Update.class) @RequestBody Paper glassPaper) throws Exception {
        boolean flag = glassPaperService.updatePaper(glassPaper);
        return ApiResult.result(flag);
    }

    /**
     * 删除
     */
    @PostMapping("/delete/{id}")
    //@OperationLog(name = "删除", type = OperationLogType.DELETE)
    //@ApiOperation(value = "删除", response = ApiResult.class)
    public ApiResult<Boolean> deletePaper(@PathVariable("id") Integer id) throws Exception {
        boolean flag = glassPaperService.deletePaper(id);
        return ApiResult.result(flag);
    }

    /**
     * 获取详情
     */
    @GetMapping("/info/{id}")
    //@OperationLog(name = "详情", type = OperationLogType.INFO)
    //@ApiOperation(value = "详情", response = Paper.class)
    public ApiResult<Paper> getPaper(@PathVariable("id") Long id) throws Exception {
        Paper glassPaper = glassPaperService.getById(id);
        return ApiResult.ok(glassPaper);
    }

    /**
     * 获取试卷详情
     */
    @GetMapping("/content/{id}")
    //@OperationLog(name = "详情", type = OperationLogType.INFO)
    //@ApiOperation(value = "详情-有用", response = Paper.class)
    public ApiResult<List<PaperProblemVO>> getPaperContent(@PathVariable("id") Long id) throws Exception {
        List<PaperProblem> paperProblems = paperProblemService.getProblemsByPaperId(id);
        List<PaperProblemVO> voList=new ArrayList<>();
        paperProblems.forEach(item->{
            //根据类型转换为具体的题型
            if(item.getProblemType().equals(ProblemTypeEnum.SINGLE_OPTION.getValue())
                    || item.getProblemType().equals(ProblemTypeEnum.MUTI_OPTION.getValue())) {
                List<PaperOptions> optionsInfos = JsonUtils.jsonToList(item.getDefinition(), PaperOptions.class);
                PaperProblemVO<PaperOptions> vo=new PaperProblemVO();
                BeanUtils.copyProperties(item,vo);
                vo.setDefinitionEntity(optionsInfos);
                voList.add(vo);
            }else if(item.getProblemType().equals(ProblemTypeEnum.SLIDER.getValue())){
                List<PaperSlider> sliderInfos = JsonUtils.jsonToList(item.getDefinition(), PaperSlider.class);
                PaperProblemVO<PaperSlider> vo=new PaperProblemVO();
                BeanUtils.copyProperties(item,vo);
                vo.setDefinitionEntity(sliderInfos);
                voList.add(vo);
            }
        });
        return ApiResult.ok(voList);
    }

    /**
     * 分页列表
     */
    @GetMapping("/list")
    //@OperationLog(name = "试卷列表", type = OperationLogType.PAGE)
    //@ApiOperation(value = "试卷列表-有用", response = Paper.class)
    public ApiResult<List<Paper>> getPaperList(Integer paperType,Integer userId) throws Exception {
        if(null == paperType){
            paperType=1;
        }
        List<Paper> paperList= glassPaperService.getPaperList(paperType);
        if(userId!=null) {
            //取当前用户对试卷的测试次数
            List<String> paperCodes = paperList.stream().map(x -> x.getPaperCode()).collect(Collectors.toList());
            Map<String, Integer> codeMap = testRecordService.getUserTestTimes(paperCodes, userId);
            for (Paper paper : paperList) {
                if(codeMap.containsKey(paper.getPaperCode())) {
                    paper.setUserTestTimes(codeMap.get(paper.getPaperCode()));
                }
            }
        }
        return ApiResult.ok(paperList);
    }
}

