package com.zwcl.glass.tools.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("glass_user_info")
public class UserInfo extends BaseEntity<UserInfo> {

    private static final long serialVersionUID = 1L;

    /**
     * 20
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Integer userId;

    /**
     * 0正常，1近视，2散光
     */
    private Integer eyeType;

    /**
     * 左眼近视度数
     */
    private String leftShortSight;

    /**
     * 右眼近视度数
     */
    private String rightShortSight;

    /**
     * 左眼散光度数
     */
    private String leftFlood;

    /**
     * 右眼散光度数
     */
    private String rightFlood;

    /**
     * 瞳距
     */
    private String ipd;

    /**
     * 上传的图片
     */
    private String picUrl;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
