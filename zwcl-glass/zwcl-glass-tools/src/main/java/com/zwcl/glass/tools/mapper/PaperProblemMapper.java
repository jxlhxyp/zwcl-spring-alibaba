package com.zwcl.glass.tools.mapper;

import com.zwcl.glass.tools.entity.PaperProblem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-07
 */
public interface PaperProblemMapper extends BaseMapper<PaperProblem> {

}
