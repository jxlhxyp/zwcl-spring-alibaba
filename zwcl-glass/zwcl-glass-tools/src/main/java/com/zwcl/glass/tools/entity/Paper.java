package com.zwcl.glass.tools.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.zwcl.common.core.validator.groups.Update;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.io.Serializable;
import java.util.Date;

/**
 * 
 *
 * @author xyp
 * @since 2020-10-27
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName("glass_paper")
//@ApiModel(value = "Paper对象")
public class Paper extends BaseEntity<Paper> {
    private static final long serialVersionUID = 1L;

    @NotNull(message = "id不能为空", groups = {Update.class})
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    //@ApiModelProperty("试卷名称")
    private String name;

    /**
     * 试卷概要
     */
    private String summary;

    //@ApiModelProperty("试卷描述")
    private String description;

    //@ApiModelProperty("试卷得出结论后的统一性提示或者链接跳转")
    private String unifyNotice;

    //@ApiModelProperty("试卷顺序")
    private Integer paperOrder;

    private Integer testNum;

    /**
     * 试卷类型,1为试卷，2为工具，工具类型不需要录入题目
     */
    private Integer paperType;

    /**
     * 试卷编码
     */
    private String paperCode;

    //@ApiModelProperty("备注")
    private String remark;

    @TableField(exist = false)
    private Integer userTestTimes=0;

    //@ApiModelProperty("版本")
    @Null(message = "版本不用传")
    @Version
    private Integer version;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
