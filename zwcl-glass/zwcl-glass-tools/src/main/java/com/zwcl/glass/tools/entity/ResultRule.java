package com.zwcl.glass.tools.entity;

import lombok.Data;

/**
 * 结论规则以及冲突规则
 */
@Data
public class ResultRule{
    private Integer problemId;

    //TODO:感觉目前只能支持1个选项，题与题是与的关系，题内选项与选项，是或的关系
    private String[] options;
}
