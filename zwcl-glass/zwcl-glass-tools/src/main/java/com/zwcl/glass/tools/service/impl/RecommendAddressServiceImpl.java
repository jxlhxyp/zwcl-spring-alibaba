package com.zwcl.glass.tools.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zwcl.glass.tools.dto.MapNearDto;
import com.zwcl.glass.tools.entity.RecommendAddress;
import com.zwcl.glass.tools.enums.AddressTypeEnum;
import com.zwcl.glass.tools.mapper.RecommendAddressMapper;
import com.zwcl.glass.tools.service.RecommendAddressService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-02-25
 */
@Service
public class RecommendAddressServiceImpl extends BaseServiceImpl<RecommendAddressMapper, RecommendAddress> implements RecommendAddressService {


    @Override
    public void deleteCityData(List<String> cityList) {
        this.remove(new LambdaQueryWrapper<RecommendAddress>().in(RecommendAddress::getCity,cityList));
    }

    @Override
    public List<RecommendAddress> searchRecommondAddress(MapNearDto dto) {
        Integer addressType = AddressTypeEnum.of(dto.getKeyWords()).getValue();
        return list(new LambdaQueryWrapper<RecommendAddress>().eq(RecommendAddress::getPoiType,addressType).like(RecommendAddress::getCity,dto.getCity()));
    }
}
