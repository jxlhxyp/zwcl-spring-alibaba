package com.zwcl.glass.tools.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class LocationPoint {
    private BigDecimal lat;

    private BigDecimal lng;
}
