package com.zwcl.glass.tools.service;

import com.zwcl.glass.tools.entity.UserRelationship;
import com.zwcl.common.mybatis.service.BaseService;
import org.apache.catalina.User;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-03
 */
public interface UserRelationshipService extends BaseService<UserRelationship> {
    List<UserRelationship> getUserRelationshipByUserId(Long userId);

    Boolean deleteEntity(Integer id);

    Boolean isExist(UserRelationship userRelationship);

    Integer  countUserRelationship(Integer userId,Integer relationship);
}
