package com.zwcl.glass.tools.service;

import com.zwcl.glass.tools.entity.UserData;
import com.zwcl.common.mybatis.service.BaseService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
public interface UserDataService extends BaseService<UserData> {

    List<UserData> getUserDatasByUserId(Long userId,Integer relationshipId);

    Boolean deleteEntity(Long id);

    UserData getUserLastData(Long userId,Integer relationshipId);
}
