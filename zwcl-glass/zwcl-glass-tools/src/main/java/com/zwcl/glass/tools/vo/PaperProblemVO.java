package com.zwcl.glass.tools.vo;

import lombok.Data;

import java.util.List;

/**
 * TODO：题目展现到前台的结构体，需要兼容多种题型
 */
@Data
public class PaperProblemVO<T> {

    private Integer id;

    private String paperId;

    private Integer problemType;

    private String subject;

    private String annotation;

//   TODO:题目类型会进行变化

//    /**
//     * 选项类型的题目
//     */
//    private List<PaperOptions> optionList;
//
//    /**
//     * 滑块类型的题目
//     */
//    private List<PaperSlider> sliderList;

    /**
     * 某类型的题目描述
     */
    private List<T> definitionEntity;

    private String picUrls;

    private String h5Urls;

    private Integer orderIndex;

    private Boolean isValid;

}
