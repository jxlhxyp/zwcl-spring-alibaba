package com.zwcl.glass.tools.mapper;

import com.zwcl.glass.tools.entity.PaperConclusion;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 规则得出结论，先默认为相与 Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-07
 */
public interface PaperConclusionMapper extends BaseMapper<PaperConclusion> {

}
