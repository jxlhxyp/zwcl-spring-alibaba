package com.zwcl.glass.tools.service;

import com.zwcl.glass.tools.entity.ArticleComment;
import com.zwcl.common.mybatis.service.BaseService;
import com.zwcl.glass.tools.vo.ArticleCommentVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
public interface ArticleCommentService extends BaseService<ArticleComment> {

    List<ArticleCommentVo> getArticleComments(Long articleId);
}
