package com.zwcl.glass.tools.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zwcl.glass.tools.entity.UserData;
import com.zwcl.glass.tools.mapper.UserDataMapper;
import com.zwcl.glass.tools.service.UserDataService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
@Service
public class UserDataServiceImpl extends BaseServiceImpl<UserDataMapper, UserData> implements UserDataService {

    @Override
    public List<UserData> getUserDatasByUserId(Long userId,Integer relationshipId) {
        return this.list(new LambdaQueryWrapper<UserData>().eq(UserData::getUserId,userId).eq(UserData::getRelationshipId,relationshipId)
                .eq(UserData::getDelFlag,false).orderByDesc(UserData::getCreateTime));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean deleteEntity(Long id) {
        UserData userData =new UserData();
        userData.setId(id);
        userData.setDelFlag(true);
        return updateById(userData);
    }

    @Override
    public UserData getUserLastData(Long userId,Integer relationshipId) {
        return getUserDatasByUserId(userId,relationshipId).stream().findFirst().orElse(null);
    }

}
