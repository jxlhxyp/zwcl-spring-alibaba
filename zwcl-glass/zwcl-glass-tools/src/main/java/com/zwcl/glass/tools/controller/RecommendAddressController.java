package com.zwcl.glass.tools.controller;


import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.glass.tools.service.impl.AddressImportServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2021-02-25
 */
@RestController
@RequestMapping("/address")
public class RecommendAddressController {

    @Autowired
    private AddressImportServiceImpl addressImportService;
    /**
     * 导入
     *
     * @param file
     * @return
     */
    @PostMapping("/import")
    public ApiResult importData(@RequestParam("file") MultipartFile file) throws IOException {
        addressImportService.importData(file.getInputStream());
        return ApiResult.ok();
    }
}

