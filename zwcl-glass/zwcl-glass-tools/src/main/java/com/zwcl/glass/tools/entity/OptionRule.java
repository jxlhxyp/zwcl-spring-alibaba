package com.zwcl.glass.tools.entity;

import lombok.Data;

@Data
public class OptionRule {
    private Integer problemId;

    //目前只能支持1个选项，题与题是与的关系，题内选项与选项，是或的关系
    private String[] options;

}
