package com.zwcl.glass.tools.enums;

import com.zwcl.common.core.exception.BusinessException;
import com.zwcl.common.core.utils.JsonUtils;
import com.zwcl.common.core.utils.StringUtilsEx;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.IntStream;

public enum  AddressTypeEnum {
    GLASS_SHOP(1, "眼镜店"),

    OPTOMETRY_CENTER(2, "验光中心"),

    HOSPITAL(3, "医院"),

    EYE_HOSPITAL(4, "眼科医院"),
    ;
    /**
     * value
     */
    private Integer value;

    /**
     * desc
     */
    private String desc;




    private static final Map<Integer, String> map;


    static {
        AddressTypeEnum[] enums = AddressTypeEnum.values();
        int size = enums.length;
        map = IntStream.range(0, size).collect(LinkedHashMap::new, (map, desc) -> {
            map.put(enums[desc].getValue(), enums[desc].getDesc());
        }, Map::putAll);

    }


    /**
     * @param value          value值
     * @param desc           desc值
     */
    AddressTypeEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    /**
     * 返回value
     *
     * @return value
     */
    public Integer getValue() {
        return this.value;
    }

    /**
     * 返回desc
     *
     * @return desc 字符串
     */
    public String getDesc() {
        return this.desc;
    }


    /**
     * 根据value获取desc
     *
     * @param value value
     * @return 处理结果
     */
    public static String getDesc(Integer value) {
        return getMap().get(value) == null ? StringUtilsEx.EMPTY : getMap().get(value);
    }


    public static AddressTypeEnum of(String desc) {
        AddressTypeEnum[] values = AddressTypeEnum.values();
        for (AddressTypeEnum anEnum : values) {
            if (anEnum.getDesc().equals(desc)) {
                return anEnum;
            }
        }
        throw new BusinessException("不存在的地址类型");
    }

    /**
     * 获取map
     *
     * @return 返回map
     */
    public static Map<Integer, String> getMap() {
        return map;
    }

    /**
     * 获取json
     *
     * @return 处理结果
     * @throws BusinessException 自定义异常
     */
    public static String getJson() throws BusinessException {
        return JsonUtils.objectToJson(getMap());
    }
}
