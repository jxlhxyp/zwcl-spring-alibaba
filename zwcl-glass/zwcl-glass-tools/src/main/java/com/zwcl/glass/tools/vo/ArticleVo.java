package com.zwcl.glass.tools.vo;

import lombok.Data;

@Data
public class ArticleVo {

    private Integer id;

    /**
     * 文章标题
     */
    private String title;

    /**
     * 所属类别
     */
    private Integer categoryId;

    /**
     * 文章标签，|分割的格式，根据这个搜索
     */
    private String articleTags;

    private String imgUrl;

    /**
     * 投放状态(0:待发布,1已发布，2已下架)
     */
    private Integer state;

    /**
     * 排序号
     */
    private Integer sort;

    /**
     * 阅读数
     */
    private Integer readNum=0;

    /**
     * 点赞数
     */
    private Integer likeNum=0;

    /**
     * 收藏数
     */
    private Integer collectNum=0;

}
