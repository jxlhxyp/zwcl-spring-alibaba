package com.zwcl.glass.tools.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.zwcl.common.core.utils.http.HttpClientUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.HttpGet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;

@Slf4j
public class BaiduMapUtils {

    //Baidu地图api密钥
    //private static final String ak = "vyR0aF4PMZA3gdmnoLpxMDeNh5RPcE1O";
    private static final String ak="03c191741fa52a27375ba9684e7b0970";

    //TODO:以下这个链接，可以获取到数据
    //http://api.map.baidu.com/place/v2/search?query=%E9%AA%8C%E5%85%89&location=23.11251,113.34166&radius=10000&output=json&ak=03c191741fa52a27375ba9684e7b0970

    // 调用百度地图API根据地址，获取坐标
    public static String getCoordinate(String address) throws JSONException {
        if (address != null && !"".equals(address)) {
            address = address.replaceAll("\\s*", "").replace("#", "栋");
            String url = "http://api.map.baidu.com/geocoder/v2/?address=" + address + "&output=json&ak=" + ak;
            String json = loadJSON(url);
            //String json = HttpUtils.sendGet(url,null);
            if (json != null && !"".equals(json)) {
                JSONObject obj = JSONObject.parseObject(json);
                if ("0".equals(obj.getString("status"))) {
                    double lng = obj.getJSONObject("result").getJSONObject("location").getDouble("lng"); // 经度
                    double lat = obj.getJSONObject("result").getJSONObject("location").getDouble("lat"); // 纬度
                    DecimalFormat df = new DecimalFormat("#.#####");
                    return df.format(lat) + "," + df.format(lng);
                }
            }
        }
        return null;
    }

    // 调用百度地图API根据坐标获取地址
    public static String getReverseCoordinate(String location) {
        String city = null;
        if (location != null) {
            String result = HttpUtils.sendGet(
                    "http://api.map.baidu.com/geocoder/v2/?ak=" + ak + "&output=json&pois=l&location="
                            + location,null);
            com.alibaba.fastjson.JSONObject jsonObjectAdds = com.alibaba.fastjson.JSONObject.parseObject(result);
            String province = jsonObjectAdds.getJSONObject("result").getJSONObject("addressComponent")
                    .getString("province");// 省
            city = jsonObjectAdds.getJSONObject("result").getJSONObject("addressComponent").getString("city");// 市

            System.out.println("province:" + province);
            System.out.println("city:" + city);
        }
        return city;
    }

    public static JSONArray getNearPoiInfo(String lat, String lng, String keyWords, Integer radius){
        //http://api.map.baidu.com/place/v2/search?query=银行&location=39.915,116.404&radius=2000&output=xml&ak=
        String url = String.format("http://api.map.baidu.com//place/v2/search?query=%s&location=%s,%s&radius=%s&output=json&ak=%s",keyWords,lat,lng,radius,ak);
        String json = HttpClientUtils.makeHttpRequestStringResult(url, HttpGet.METHOD_NAME);
        JSONObject result=JSONObject.parseObject(json);
        if(result.getInteger("status")==0){              //成功
            JSONArray resultArr=result.getJSONArray("results");
            return resultArr;
        }else {
            return null;
        }
    }



    public static String loadJSON(String url) {
        StringBuilder json = new StringBuilder();
        try {
            URL oracle = new URL(url);
            URLConnection yc = oracle.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream(), "UTF-8"));
            String inputLine = null;
            while ((inputLine = in.readLine()) != null) {
                json.append(inputLine);
            }
            in.close();
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        }
        return json.toString();
    }
}
