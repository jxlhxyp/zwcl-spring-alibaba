package com.zwcl.glass.tools.entity;

import lombok.Data;

import java.util.List;

@Data
public class ShowRule {
    private List<Integer> problemIds;
}
