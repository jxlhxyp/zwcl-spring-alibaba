package com.zwcl.glass.tools.controller;


import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.common.core.validator.groups.Add;
import com.zwcl.common.core.validator.groups.Update;
import com.zwcl.glass.tools.entity.Article;
import com.zwcl.glass.tools.entity.UserInfo;
import com.zwcl.glass.tools.service.UserInfoService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
@RestController
@RequestMapping("/userInfo")
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;

    /**
     * 添加
     */
    @PostMapping("/add")
    public ApiResult<Boolean> addUserInfo(@Validated(Add.class) @RequestBody UserInfo userInfo) throws Exception {
        boolean flag = userInfoService.save(userInfo);
        return ApiResult.result(flag);
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public ApiResult<Boolean> updateArticle(@Validated(Update.class) @RequestBody UserInfo userInfo) throws Exception {
        boolean flag = userInfoService.updateById(userInfo);
        return ApiResult.result(flag);
    }

    /**
     * 删除
     */
    @PostMapping("/delete/{id}")
    public ApiResult<Boolean> deleteUserInfo(@PathVariable("id") Long id) throws Exception {
        boolean flag = userInfoService.deleteEntity(id);
        return ApiResult.result(flag);
    }

    @GetMapping("/list")
    public List<UserInfo> listUserInfo(@RequestParam Long userId){
        List<UserInfo> userInfos=userInfoService.getUserInfosByUserId(userId);
        return userInfos;
    }

}

