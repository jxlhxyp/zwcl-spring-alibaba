package com.zwcl.glass.tools.mapper;

import com.zwcl.glass.tools.entity.TestRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zwcl.glass.tools.vo.UserTestRecordVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-07
 */
public interface TestRecordMapper extends BaseMapper<TestRecord> {

    /**
     * 获取用户各种最新的测试记录
     * @return
     */
    List<TestRecord> selectUserLastTestRecord(@Param("userId") Integer userId,@Param("paperCodes") List<String> paperCodes);

    List<UserTestRecordVo> countUserTestNum(@Param("userId") Integer userId,@Param("paperCodes") List<String> paperCodes);
}
