package com.zwcl.glass.tools.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("glass_user_relationship")
public class UserRelationship extends BaseEntity<UserRelationship> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 当前用户id
     */
    private Integer userId;

    /**
     * 0:自己 1：父母  2：孩子  3. 爱人  4.朋友  5. 同事  9.其他
     */
    private Integer relationship;

    @TableField(exist = false)
    private String relationshipStr;
    public String getRelationshipStr(){
        String desc="";
        switch(relationship){
            case 0 :
                desc="自己";
                break; //可选
            case 1 :
                desc="爱人";
                break; //可选
            case 2 :
                desc="父母";
                break; //可选
            case 3 :
                desc="孩子";
                break; //可选
            case 4 :
                desc="朋友";
                break; //可选
            case 5 :
                desc="同事";
                break; //可选
            case 9 :
                desc="其他";
                break; //可选
            default : //可选
                desc="其他";
                break; //可选
        }
        return desc;
    }

    /**
     * 档案用户名称
     */
    private String userName;

    /**
     * true 男，false女
     */
    private Boolean sex;

    /**
     * 生日
     */
    private String birthday;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
