package com.zwcl.glass.tools.vo;

import com.zwcl.glass.tools.entity.ArticleComment;
import lombok.Data;

import java.util.List;

@Data
public class ArticleCommentVo {

    ArticleComment parent;

    /**
     * 这个要根据时间排序
     */
    List<ArticleComment> childs;
}
