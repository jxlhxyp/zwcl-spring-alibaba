package com.zwcl.glass.tools.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.glass.tools.dto.SearchArticleDto;
import com.zwcl.glass.tools.entity.Article;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zwcl.glass.tools.vo.ArticleVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
public interface ArticleMapper extends BaseMapper<Article> {
    Page<ArticleVo> searchArticles(Page<ArticleVo> page, @Param("dto") SearchArticleDto dto);
}
