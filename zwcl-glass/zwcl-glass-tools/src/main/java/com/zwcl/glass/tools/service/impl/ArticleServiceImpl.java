package com.zwcl.glass.tools.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.common.core.exception.BusinessException;
import com.zwcl.glass.tools.dto.SearchArticleDto;
import com.zwcl.glass.tools.entity.Article;
import com.zwcl.glass.tools.entity.ContentRead;
import com.zwcl.glass.tools.enums.CollectTypeEnum;
import com.zwcl.glass.tools.mapper.ArticleMapper;
import com.zwcl.glass.tools.service.ContentReadService;
import com.zwcl.glass.tools.service.ArticleService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import com.zwcl.glass.tools.vo.ArticleDetailVo;
import com.zwcl.glass.tools.vo.ArticleVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
@Service
public class ArticleServiceImpl extends BaseServiceImpl<ArticleMapper, Article> implements ArticleService {

    @Autowired
    private ContentReadService contentReadService;

    @Autowired
    private ArticleMapper articleMapper;

    @Override
    public ArticleDetailVo getArticleById(Long id) {
        Article article=this.getById(id);
        ContentRead contentRead=contentReadService.getContentReadById(id, CollectTypeEnum.ARTICLE.getValue());
        ArticleDetailVo articleVo = new ArticleDetailVo();
        BeanUtils.copyProperties(article, articleVo);
        if(null!=contentRead) {
//            articleVo.setReadId(contentRead.getId());
//            articleVo.setReadNum(contentRead.getReadNum());
//            articleVo.setLikeNum(contentRead.getLikeNum());
//            articleVo.setCollectNum(contentRead.getCollectNum());
        }
        return articleVo;
    }

    @Override
    public Page<ArticleVo> searchArticles(Page<ArticleVo> page,SearchArticleDto dto) {
        try {
            return articleMapper.searchArticles(page,dto);
        }catch (Exception ex){
            throw new BusinessException("搜索文章失败");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean deleteEntity(Integer id) {
        Article article=new Article();
        article.setId(id);
        article.setDelFlag(true);
        return updateById(article);
    }
}
