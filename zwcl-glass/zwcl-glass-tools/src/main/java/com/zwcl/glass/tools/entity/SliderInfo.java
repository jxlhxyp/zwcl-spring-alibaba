package com.zwcl.glass.tools.entity;

import lombok.Data;

/**
 * 滑块结构体
 */
@Data
public class SliderInfo {
    private float min;

    private float max;

    private float step;
}
