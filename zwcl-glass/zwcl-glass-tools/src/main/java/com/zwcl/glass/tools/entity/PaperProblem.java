package com.zwcl.glass.tools.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.zwcl.common.core.validator.groups.Update;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.io.Serializable;
import java.util.Date;

/**
 * 
 *
 * @author xyp
 * @since 2020-10-27
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName("glass_paper_problem")
//@ApiModel(value = "PaperProblem对象")
public class PaperProblem extends BaseEntity<PaperProblem> {
    private static final long serialVersionUID = 1L;

    @NotNull(message = "id不能为空", groups = {Update.class})
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @NotBlank(message = "不能为空")
    private String paperId;

    @NotBlank(message = "不能为空")
    private String paperCode;

    //@ApiModelProperty("11 单选题   12 多选题   21 滑块设值    31 填空  41 问答")
    private Integer problemType;

    private String subject;

    private String annotation;

    private String definition;

    private String picUrls;

    private String h5Urls;

    private Integer orderIndex;

    private Boolean isValid;

    //@ApiModelProperty("备注")
    private String remark;

    //@ApiModelProperty("版本")
    @Null(message = "版本不用传")
    @Version
    private Integer version;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
