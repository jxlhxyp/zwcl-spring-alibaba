package com.zwcl.glass.tools.entity;

import lombok.Data;

/**
 * 试卷中的滑块
 */
@Data
public class PaperSlider {
    private String subject;

    private String key;

    private SliderInfo content;

    /**
     * 滑块值的生成方式，block：区间平均值，precise：精准值
     */
    private String valueShow;
}
