package com.zwcl.glass.tools.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.zwcl.common.core.validator.groups.Update;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

/**
 * <p>
 * 
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("glass_article_category")
public class ArticleCategory extends BaseEntity<ArticleCategory> {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "id不能为空", groups = {Update.class})
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 类别编码
     */
    private String code;

    /**
     * 类别名称
     */
    private String name;

    /**
     * 排序号
     */
    private Integer sort;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
