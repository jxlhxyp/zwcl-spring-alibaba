package com.zwcl.glass.tools.mapper;

import com.zwcl.glass.tools.entity.UserRelationship;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-03
 */
public interface UserRelationshipMapper extends BaseMapper<UserRelationship> {

}
