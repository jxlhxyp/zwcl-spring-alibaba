package com.zwcl.glass.tools.enums;

import com.zwcl.common.core.exception.BusinessException;
import com.zwcl.common.core.utils.JsonUtils;
import com.zwcl.common.core.utils.StringUtilsEx;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.IntStream;


/**
 * 题目类型枚举
 *
 * @author xyp
 * @email xyp
 * @date 2020/10/25
 */
public enum ProblemTypeEnum {

    /**
     * 自营
     */
    SINGLE_OPTION(11, "单选题", "PaperOption"),

    /**
     * 易加油
     */
    MUTI_OPTION(12,"多选题","PaperOption"),

    /**
     * 车主邦
     */
    SLIDER(21,"滑块设值","PaperSlider"),

    ;
    /**
     * value
     */
    private Integer value;

    /**
     * desc
     */
    private String desc;

    /**
     * service实体
     */
    private String serviceName;

    private static final Map<Integer, String> map;

    private static final Map<Integer, String> serviceMap;

    static {
        ProblemTypeEnum[] enums = ProblemTypeEnum.values();
        int size = enums.length;
        map = IntStream.range(0, size).collect(LinkedHashMap::new, (map, desc) -> {
            map.put(enums[desc].getValue(), enums[desc].getDesc());
        }, Map::putAll);

        serviceMap = IntStream.range(0, size).collect(LinkedHashMap::new, (map, serviceType) -> {
            map.put(enums[serviceType].getValue(), enums[serviceType].getServiceName());
        }, Map::putAll);
    }


    /**
     * @param value          value值
     * @param desc           desc值
     * @param serviceName    serviceName值
     */
    ProblemTypeEnum(Integer value, String desc,  String serviceName) {
        this.value = value;
        this.desc = desc;
        this.serviceName = serviceName;
    }

    /**
     * 返回value
     *
     * @return value
     */
    public Integer getValue() {
        return this.value;
    }

    /**
     * 返回desc
     *
     * @return desc 字符串
     */
    public String getDesc() {
        return this.desc;
    }

    /**
     * 返回serviceName
     *
     * @return
     */
    public String getServiceName() {
        return this.serviceName;
    }

    /**
     * 根据value获取desc
     *
     * @param value value
     * @return 处理结果
     */
    public static String getDesc(Integer value) {
        return getMap().get(value) == null ? StringUtilsEx.EMPTY : getMap().get(value);
    }

    /**
     * 根据value获取类型
     *
     * @param value value
     * @return 处理结果
     */
    public static String getServiceName(Integer value) {
        return serviceMap.get(value) == null ? StringUtilsEx.EMPTY : serviceMap.get(value);
    }

    public static ProblemTypeEnum of(Integer value) {
        ProblemTypeEnum[] values = ProblemTypeEnum.values();
        for (ProblemTypeEnum anEnum : values) {
            if (anEnum.getValue().equals(value)) {
                return anEnum;
            }
        }
        throw new BusinessException("不存在的题目类型");
    }

    /**
     * 获取map
     *
     * @return 返回map
     */
    public static Map<Integer, String> getMap() {
        return map;
    }

    /**
     * 获取json
     *
     * @return 处理结果
     * @throws BusinessException 自定义异常
     */
    public static String getJson() throws BusinessException {
        return JsonUtils.objectToJson(getMap());
    }
}
