package com.zwcl.glass.tools.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zwcl.glass.tools.entity.UserData;
import com.zwcl.glass.tools.entity.UserRelationship;
import com.zwcl.glass.tools.mapper.UserRelationshipMapper;
import com.zwcl.glass.tools.service.UserRelationshipService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-03
 */
@Service
public class UserRelationshipServiceImpl extends BaseServiceImpl<UserRelationshipMapper, UserRelationship> implements UserRelationshipService {
    @Override
    public List<UserRelationship> getUserRelationshipByUserId(Long userId) {
        return this.list(new LambdaQueryWrapper<UserRelationship>().eq(UserRelationship::getUserId,userId).eq(UserRelationship::getDelFlag,false).orderByAsc(UserRelationship::getRelationship));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean deleteEntity(Integer id) {
        UserRelationship userData =new UserRelationship();
        userData.setId(id);
        userData.setDelFlag(true);
        return updateById(userData);
    }

    @Override
    public Boolean isExist(UserRelationship userRelationship) {
        LambdaQueryWrapper<UserRelationship> queryWrapper=new LambdaQueryWrapper<UserRelationship>()
                .eq(UserRelationship::getUserId,userRelationship.getUserId())
                .eq(UserRelationship::getRelationship,userRelationship.getRelationship())
                .eq(UserRelationship::getUserName,userRelationship.getUserName().trim())
                .eq(UserRelationship::getDelFlag,false);
        UserRelationship userRelationshipDb = getOne(queryWrapper);
        return null!=userRelationshipDb? true : false;
    }

    @Override
    public Integer countUserRelationship(Integer userId, Integer relationship) {
        LambdaQueryWrapper<UserRelationship> queryWrapper=new LambdaQueryWrapper<UserRelationship>()
                .eq(UserRelationship::getUserId,userId)
                .eq(UserRelationship::getRelationship,relationship)
                .eq(UserRelationship::getDelFlag,false);
        return count(queryWrapper);
    }

}
