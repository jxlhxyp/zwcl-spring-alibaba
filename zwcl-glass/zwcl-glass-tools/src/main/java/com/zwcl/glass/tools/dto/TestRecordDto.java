package com.zwcl.glass.tools.dto;

import com.zwcl.glass.tools.entity.AnswerResult;
import lombok.Data;

import java.util.List;

@Data
public class TestRecordDto {
    private Integer userId;

    private Integer paperId;

    private String paperCode;

    private String phone;

    /**
     * 测试答案，根据接口选传
     */
    private List<AnswerResult> resultList;
}
