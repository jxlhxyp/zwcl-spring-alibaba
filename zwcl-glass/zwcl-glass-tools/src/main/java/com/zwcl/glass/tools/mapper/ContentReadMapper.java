package com.zwcl.glass.tools.mapper;

import com.zwcl.glass.tools.entity.ContentRead;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
public interface ContentReadMapper extends BaseMapper<ContentRead> {
    Boolean saveContentReadCount(@Param("dto") ContentRead dto);
}
