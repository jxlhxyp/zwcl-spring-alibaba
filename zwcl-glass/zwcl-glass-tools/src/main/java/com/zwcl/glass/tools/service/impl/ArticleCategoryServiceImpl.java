package com.zwcl.glass.tools.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zwcl.glass.tools.entity.Article;
import com.zwcl.glass.tools.entity.ArticleCategory;
import com.zwcl.glass.tools.mapper.ArticleCategoryMapper;
import com.zwcl.glass.tools.service.ArticleCategoryService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
@Service
public class ArticleCategoryServiceImpl extends BaseServiceImpl<ArticleCategoryMapper, ArticleCategory> implements ArticleCategoryService {

    @Override
    public List<ArticleCategory> getArticleCategoryList() {
        return this.list(new LambdaQueryWrapper<ArticleCategory>().eq(ArticleCategory::getDelFlag,false).orderByAsc(ArticleCategory::getSort));
    }

    @Override
    public ArticleCategory getArticleCategoryById(Integer id) {
        Wrapper<ArticleCategory> queryWrapper=new LambdaQueryWrapper<ArticleCategory>().eq(ArticleCategory::getId,id);
        return getOne(queryWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean deleteEntity(Integer id) {
        ArticleCategory articleCategory=new ArticleCategory();
        articleCategory.setId(id);
        articleCategory.setDelFlag(true);
        return updateById(articleCategory);
    }

}
