package com.zwcl.glass.tools.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.common.core.exception.BusinessException;
import com.zwcl.common.core.utils.BeanUtils;
import com.zwcl.glass.tools.dto.CollectContentDto;
import com.zwcl.glass.tools.dto.UserCollectDto;
import com.zwcl.glass.tools.entity.ContentRead;
import com.zwcl.glass.tools.entity.UserCollect;
import com.zwcl.glass.tools.mapper.UserCollectMapper;
import com.zwcl.glass.tools.service.ContentReadService;
import com.zwcl.glass.tools.service.UserCollectService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import com.zwcl.glass.tools.vo.CollectContentVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-02-22
 */
@Service
public class UserCollectServiceImpl extends BaseServiceImpl<UserCollectMapper, UserCollect> implements UserCollectService {

    @Autowired
    private ContentReadService contentReadService;

    @Autowired
    private UserCollectMapper userCollectMapper;

    /**
     * 这个方法已经改成了支持点赞和收藏的功能，不仅仅局限在收藏了
     * @param dto
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean confirmCollect(UserCollectDto dto) {
        Boolean succFlag = true;
        UserCollect userCollect=new UserCollect();
        BeanUtils.copyProperties(dto,userCollect);
        //是否有这条记录，有则更新，无则插入
        LambdaQueryWrapper<UserCollect> queryWrapper= new LambdaQueryWrapper<UserCollect>().eq(UserCollect::getUserId,dto.getUserId()).eq(UserCollect::getContentId,dto.getContentId()).eq(UserCollect::getContentType,dto.getContentType());
        UserCollect dbData = getOne(queryWrapper);
        if(dbData==null){
            succFlag = this.save(userCollect);
        }else {
            if(dto.getIsCollect()!=null) {
                dbData.setIsCollect(dto.getIsCollect());
            }
            if(dto.getIsLike()!=null){
                dbData.setIsLike(dto.getIsLike());
            }
            succFlag = this.updateById(dbData);
        }
        //如果没有这条数据
        if(succFlag) {
            ContentRead contentRead = new ContentRead();
            contentRead.setContentId(dto.getContentId());
            contentRead.setContentType(dto.getContentType());
            if(dto.getIsCollect()!=null) {
                contentRead.setCollectNum(dto.getIsCollect()?1:-1);
            }else {
                contentRead.setCollectNum(0);
            }
            if(dto.getIsLike()!=null){
                contentRead.setLikeNum(dto.getIsLike()?1:-1);
            }else {
                contentRead.setLikeNum(0);
            }
            contentRead.setReadNum(0);
            succFlag = contentReadService.saveContentOperation(contentRead);
        }
        return succFlag;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean cancleCollect(UserCollectDto dto) {
        Boolean succFlag = true;
        LambdaQueryWrapper<UserCollect> queryWrapper= new LambdaQueryWrapper<UserCollect>().eq(UserCollect::getUserId,dto.getUserId()).eq(UserCollect::getContentId,dto.getContentId()).eq(UserCollect::getContentType,dto.getContentType());
        succFlag=this.remove(queryWrapper);
        if(succFlag) {
            ContentRead contentRead = new ContentRead();
            contentRead.setContentId(dto.getContentId());
            contentRead.setContentType(dto.getContentType());
            contentRead.setCollectNum(-1);
            contentRead.setLikeNum(0);
            contentRead.setReadNum(0);
            succFlag = contentReadService.saveContentOperation(contentRead);
        }
        return succFlag;
    }

    @Override
    public Page<CollectContentVo> searchCollects(Page<CollectContentVo> page, CollectContentDto dto) {
        try {
            return userCollectMapper.searchCollects(page,dto);
        }catch (Exception ex){
            throw new BusinessException("搜索收藏内容失败");
        }
    }

    @Override
    public UserCollect getUserCollect(Integer userId,Long contentId,  Integer contentType) {
        return this.getOne( new LambdaQueryWrapper<UserCollect>().eq(UserCollect::getUserId,userId).eq(UserCollect::getContentId,contentId).eq(UserCollect::getContentType,contentType));
    }
}
