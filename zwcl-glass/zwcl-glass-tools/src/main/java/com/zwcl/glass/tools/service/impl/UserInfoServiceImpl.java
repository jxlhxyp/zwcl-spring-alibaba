package com.zwcl.glass.tools.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zwcl.glass.tools.entity.ArticleCategory;
import com.zwcl.glass.tools.entity.UserInfo;
import com.zwcl.glass.tools.mapper.UserInfoMapper;
import com.zwcl.glass.tools.service.UserInfoService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
@Service
public class UserInfoServiceImpl extends BaseServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {

    @Override
    public List<UserInfo> getUserInfosByUserId(Long userId) {
        return this.list(new LambdaQueryWrapper<UserInfo>().eq(UserInfo::getUserId,userId).eq(UserInfo::getDelFlag,false).orderByDesc(UserInfo::getCreateTime));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean deleteEntity(Long id) {
        UserInfo userInfo=new UserInfo();
        userInfo.setId(id);
        userInfo.setDelFlag(true);
        return updateById(userInfo);
    }

}
