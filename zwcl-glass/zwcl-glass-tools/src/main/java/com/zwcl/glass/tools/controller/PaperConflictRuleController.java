package com.zwcl.glass.tools.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 冲突规则，回头加工好存储在缓存 前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-07
 */
@RestController
@RequestMapping("/paperConflictRule")
public class PaperConflictRuleController {

}

