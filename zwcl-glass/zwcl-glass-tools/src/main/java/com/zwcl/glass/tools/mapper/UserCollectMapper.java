package com.zwcl.glass.tools.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.glass.tools.dto.CollectContentDto;
import com.zwcl.glass.tools.dto.SearchArticleDto;
import com.zwcl.glass.tools.entity.UserCollect;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zwcl.glass.tools.vo.ArticleVo;
import com.zwcl.glass.tools.vo.CollectContentVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2021-02-22
 */
public interface UserCollectMapper extends BaseMapper<UserCollect> {
    Page<CollectContentVo> searchCollects(Page<CollectContentVo> page, @Param("dto") CollectContentDto dto);
}
