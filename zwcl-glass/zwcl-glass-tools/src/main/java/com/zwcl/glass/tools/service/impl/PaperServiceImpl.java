package com.zwcl.glass.tools.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zwcl.common.core.exception.BusinessException;
import com.zwcl.glass.tools.entity.Paper;
import com.zwcl.glass.tools.mapper.PaperMapper;
import com.zwcl.glass.tools.service.PaperService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-07
 */
@Service
public class PaperServiceImpl extends BaseServiceImpl<PaperMapper, Paper> implements PaperService {

    @Autowired
    private PaperMapper paperMapper;

    @Override
    public List<Paper> getPaperList(Integer paperType) throws BusinessException {
        QueryWrapper<Paper> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(Paper::getPaperType,paperType);
        wrapper.lambda().eq(Paper::getDelFlag,0);
        wrapper.lambda().orderByAsc(Paper::getPaperOrder);
        return this.list(wrapper);
    }

    @Override
    public Paper findPaperByCode(String paperCode) {
        QueryWrapper<Paper> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(Paper::getPaperCode,paperCode);
        return this.getOne(wrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean savePaper(Paper glassPaper) throws Exception {
        return super.save(glassPaper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean updatePaper(Paper glassPaper) throws Exception {
        return super.updateById(glassPaper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deletePaper(Integer id) throws Exception {
        return super.removeById(id);
    }

    /**
     * 更新测试的人次
     * @param id
     * @return
     */
    @Override
    public boolean updateTestNum(Integer id) {
        return paperMapper.updateTestNum(id);
    }

}
