package com.zwcl.glass.tools.mapper;

import com.zwcl.glass.tools.entity.RecommendAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2021-02-25
 */
public interface RecommendAddressMapper extends BaseMapper<RecommendAddress> {
    /**
     * 批量插入
     *
     * @param list
     */
    void batchInsert(@Param("list") List<RecommendAddress> list);
}
