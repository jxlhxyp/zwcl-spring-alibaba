package com.zwcl.glass.tools.entity;

import lombok.Data;

import java.util.List;

@Data
public class ConclusionRuleEx {
    private  Integer id;

    private Integer paperId;

    private String conclusionContent;

    private String conclusionTags;

    private String typeCode;

    private ResultRuleEx ruleContent;

    private String conclusionPic;

    private Boolean isValid;

    private Integer priority;
}
