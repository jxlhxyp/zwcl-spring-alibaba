package com.zwcl.glass.tools.service;

import com.zwcl.glass.tools.api.entity.UserReport;
import com.zwcl.glass.tools.dto.HasTestDto;
import com.zwcl.glass.tools.dto.TestRecordDto;
import com.zwcl.glass.tools.entity.AnswerResult;
import com.zwcl.glass.tools.entity.AnswerResultHanlder;
import com.zwcl.glass.tools.entity.TestRecord;
import com.zwcl.common.mybatis.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-07
 */
public interface TestRecordService extends BaseService<TestRecord> {
    /**
     * 保存
     *
     * @param testRecord
     * @return
     * @throws Exception
     */
    boolean saveTestRecord(TestRecord testRecord) throws Exception;

    /**
     * 修改
     *
     * @param testRecord
     * @return
     * @throws Exception
     */
    boolean updateTestRecord(TestRecord testRecord) throws Exception;

    /**
     * 删除
     *
     * @param id
     * @return
     * @throws Exception
     */
    boolean deleteTestRecord(Long id) throws Exception;


    /**
     * 20201029 自己增加的接口
     */
    UserReport getUserTestReport(List<String> paperCodes, Integer userId);

    List<TestRecord> getUserLastTestRecords(List<String> paperCodes,Integer userId);

    List<TestRecord> getUserTestRecords(HasTestDto dto);

    Integer getUserTestTimes(HasTestDto dto);

    Map<String,Integer> getUserTestTimes(List<String> paperCodes,Integer userId);

    TestRecord getUserTestRecord(TestRecordDto dto);

    List<String> generateTestConclusion(TestRecordDto dto);

    Map<String,List<String>> generateTestConclusionEx(TestRecordDto dto);

    /**
     * 获取结果进一步处理的结果
     */
    List<AnswerResultHanlder> handlerAnswerResult(List<AnswerResult> answerResults);

}
