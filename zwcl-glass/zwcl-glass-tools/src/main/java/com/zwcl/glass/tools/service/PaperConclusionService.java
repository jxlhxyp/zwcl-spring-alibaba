package com.zwcl.glass.tools.service;

import com.zwcl.glass.tools.dto.TestRecordDto;
import com.zwcl.glass.tools.entity.PaperConclusion;
import com.zwcl.common.mybatis.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 规则得出结论，先默认为相与 服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-07
 */
public interface PaperConclusionService extends BaseService<PaperConclusion> {
    /**
     * 保存
     *
     * @param paperConclusion
     * @return
     * @throws Exception
     */
    boolean savePaperConclusion(PaperConclusion paperConclusion) throws Exception;

    /**
     * 修改
     *
     * @param paperConclusion
     * @return
     * @throws Exception
     */
    boolean updatePaperConclusion(PaperConclusion paperConclusion) throws Exception;

    /**
     * 删除
     *
     * @param id
     * @return
     * @throws Exception
     */
    boolean deletePaperConclusion(Long id) throws Exception;


    /**
     * 获取试卷的结论规则
     */
    List<PaperConclusion> getPaperConclusionList(Integer paperId);

    List<PaperConclusion> getPaperConclusionListByCode(String paperCode);

    Map<String,String> getConclusionAndTagMap(String paperCode);
}
