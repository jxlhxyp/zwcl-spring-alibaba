package com.zwcl.glass.tools.vo;

import lombok.Data;

/**
 * 收藏内容
 */
@Data
public class CollectContentVo<T> {
    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 收藏内容
     */
    private Long contentId;

    /**
     * 1:文章，有其他的收藏再扩展 2:视频
     */
    private Integer contentType;

    private Boolean isLike;

    private Boolean isCollect;

    /**
     * 内容实体
     */
    private T contentEntity;
}
