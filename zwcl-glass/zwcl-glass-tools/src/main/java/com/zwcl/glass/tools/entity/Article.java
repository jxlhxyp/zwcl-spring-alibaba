package com.zwcl.glass.tools.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("glass_article")
public class Article extends BaseEntity<Article> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 文章标题
     */
    private String title;

    /**
     * 所属类别
     */
    private Integer categoryId;

    /**
     * 1，H5链接   2. 自定义内容
     */
    private Integer contentType;

    /**
     * 文章链接
     */
    private String articleUrl;

    /**
     * 封面图片
     */
    private String imgUrl;

    /**
     * 文章内容
     */
    private String articleContent;

    /**
     * 文章标签，|分割的格式，根据这个搜索
     */
    private String articleTags;

    /**
     * 投放状态(0:待发布,1已发布，2已下架)
     */
    private Integer state;

    /**
     * 排序号
     */
    private Integer sort;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
