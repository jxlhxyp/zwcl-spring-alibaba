package com.zwcl.glass.tools.controller;


import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.common.core.enums.ExceptionCode;
import com.zwcl.common.core.exception.BusinessException;
import com.zwcl.common.core.validator.groups.Add;
import com.zwcl.glass.tools.entity.Article;
import com.zwcl.glass.tools.entity.ArticleCategory;
import com.zwcl.glass.tools.entity.ContentRead;
import com.zwcl.glass.tools.service.ContentReadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
@RestController
@RequestMapping("/contentRead")
public class ContentReadController {

    @Autowired
    private ContentReadService contentReadService;

    /**
     * 更新阅读量+1，-1； 和更新点赞数+1，-1
     */
    @PostMapping("/count")
    public ApiResult<Boolean> updateReadCount(@RequestBody ContentRead contentRead) throws Exception {
        return ApiResult.ok(contentReadService.saveContentOperation(contentRead));
    }

}

