package com.zwcl.glass.tools.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * 地址导入的dto
 */
@Data
public class AddressImportDto {

    @ExcelProperty(index = 0)
    private Integer no;
    /**
     * 地址名称
     */
    @ExcelProperty(index = 1)
    private String name;

    /**
     * 1.眼镜店  2. 验光中心 3. 综合医院 4. 眼科医院
     */
    @ExcelProperty(index = 2)
    private String  poiTypeStr;

    /**
     * 城市
     */
    @ExcelProperty(index = 3)
    private String city;

    /**
     * 详细地址
     */
    @ExcelProperty(index = 4)
    private String address;

    /**
     * 经度
     */
    @ExcelProperty(index = 5)
    private String lng;

    /**
     * 纬度
     */
    @ExcelProperty(index = 6)
    private String lat;

    /**
     * 推荐指数
     */
    @ExcelProperty(index = 7)
    private Integer recommendNum;

    /**
     * 推荐理由
     */
    @ExcelProperty(index = 8)
    private String recommendReason;

    /**
     * 简介
     */
    @ExcelProperty(index = 9)
    private String description;
}
