package com.zwcl.glass.tools.mapper;

import com.zwcl.glass.tools.entity.ContentRead;
import com.zwcl.glass.tools.entity.Paper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-07
 */
public interface PaperMapper extends BaseMapper<Paper> {
    Boolean updateTestNum(Integer paperId);
}
