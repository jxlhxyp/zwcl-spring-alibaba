package com.zwcl.glass.tools.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zwcl.glass.tools.entity.ArticleRead;
import com.zwcl.glass.tools.mapper.ArticleReadMapper;
import com.zwcl.glass.tools.service.ArticleReadService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Wrapper;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
@Service
public class ArticleReadServiceImpl extends BaseServiceImpl<ArticleReadMapper, ArticleRead> implements ArticleReadService {

    @Autowired
    private ArticleReadMapper articleReadMapper;

    /**
     * 每次判断文章id是否存在的话，可能效率极低
     * 要带上readId
     * @param articleRead
     * @return
     */
    @Override
    @Transactional
    public Boolean saveArticalReadCount(ArticleRead articleRead) {
        return articleReadMapper.saveArticalReadCount(articleRead);
    }

    @Override
    public ArticleRead getArticalReadById(Long articalId) {
        return getOne(new LambdaQueryWrapper<ArticleRead>().eq(ArticleRead::getArticleId,articalId));
    }
}
