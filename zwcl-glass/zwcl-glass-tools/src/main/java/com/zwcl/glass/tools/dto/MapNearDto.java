package com.zwcl.glass.tools.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class MapNearDto {
    @NotNull(message = "纬度不能为空")
    private BigDecimal lat;

    @NotNull(message = "经度不能为空")
    private BigDecimal lng;

    /**
     * 关键字
     */
    @NotBlank(message = "搜索关键字")
    private String keyWords;

    /**
     * 方圆距离
     */
    @NotNull(message = "方圆距离")
    private Integer radius;

    /**
     * 城市
     */
    private String city;
}
