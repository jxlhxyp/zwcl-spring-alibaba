package com.zwcl.glass.tools;

import com.zwcl.common.swagger.annotation.EnableCustomSwagger2;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

// 本地Nacos有时候启动会连接不上，failed to req API:/api//nacos/v1/ns/instance after all servers
// 删掉本地nacos文件夹下的data文件夹，重新启动
@SpringCloudApplication
@MapperScan("com.zwcl.glass.tools.mapper")
@EnableDiscoveryClient
@EnableCustomSwagger2
public class GlassToolsApplication {
    public static void main(String[] args){
        SpringApplication.run(GlassToolsApplication.class, args);
        System.out.println("启动成功");
    }
}
