package com.zwcl.glass.tools.service;

import com.zwcl.glass.tools.entity.UserInfo;
import com.zwcl.common.mybatis.service.BaseService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
public interface UserInfoService extends BaseService<UserInfo> {

    List<UserInfo> getUserInfosByUserId(Long userId);

    Boolean deleteEntity(Long id);
}
