package com.zwcl.glass.tools.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.common.core.validator.groups.Add;
import com.zwcl.common.core.validator.groups.Update;
import com.zwcl.glass.tools.entity.Article;
import com.zwcl.glass.tools.entity.ArticleComment;
import com.zwcl.glass.tools.service.ArticleCommentService;
import com.zwcl.glass.tools.service.ArticleService;
import com.zwcl.glass.tools.vo.ArticleCommentVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
@RestController
@RequestMapping("/articleComment")
public class ArticleCommentController {

    @Autowired
    private ArticleCommentService articleCommentService;

    /**
     * 添加
     */
    @PostMapping("/add")
    //@OperationLog(name = "添加", type = OperationLogType.ADD)
    //@ApiOperation(value = "添加", response = ApiResult.class)
    public ApiResult<Boolean> addArticleComment(@Validated(Add.class) @RequestBody ArticleComment articleComment) throws Exception {
        boolean flag = articleCommentService.save(articleComment);
        return ApiResult.result(flag);
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    //@OperationLog(name = "修改", type = OperationLogType.UPDATE)
    //@ApiOperation(value = "修改", response = ApiResult.class)
    public ApiResult<Boolean> updateArticleComment(@Validated(Update.class) @RequestBody ArticleComment articleComment) throws Exception {
        boolean flag = articleCommentService.updateById(articleComment);
        return ApiResult.result(flag);
    }

    /**
     * 删除
     */
    @PostMapping("/delete/{id}")
    //@OperationLog(name = "删除", type = OperationLogType.DELETE)
    //@ApiOperation(value = "删除", response = ApiResult.class)
    public ApiResult<Boolean> deleteArticleComment(@PathVariable("id") Long id) throws Exception {
        boolean flag = articleCommentService.removeById(id);
        return ApiResult.result(flag);
    }


    /**
     * 查询文章的评论
     */
    @GetMapping("/list")
    public List<ArticleCommentVo> getArticleComments(@RequestParam Long articleId) throws Exception {
        return articleCommentService.getArticleComments(articleId);
    }


}

