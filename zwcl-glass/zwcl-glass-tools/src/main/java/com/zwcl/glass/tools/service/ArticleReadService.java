package com.zwcl.glass.tools.service;

import com.zwcl.glass.tools.entity.ArticleRead;
import com.zwcl.common.mybatis.service.BaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-11
 */
public interface ArticleReadService extends BaseService<ArticleRead> {

    Boolean saveArticalReadCount(ArticleRead articleRead);

    ArticleRead getArticalReadById(Long articalId);
}
