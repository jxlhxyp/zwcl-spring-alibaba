package com.zwcl.glass.tools.service;

import com.zwcl.glass.tools.entity.AnswerResultHanlder;
import com.zwcl.glass.tools.entity.ConclusionRule;
import com.zwcl.glass.tools.entity.ConclusionRuleEx;
import com.zwcl.glass.tools.entity.ConflictRule;

import java.util.List;
import java.util.Map;

/**
 * 生成结论服务
 */
public interface RuleHandlerService {

    /**
     * TODO:规则字段需要改
     * @param answerResults
     * @return
     */
    List<String> generateConclusion(List<AnswerResultHanlder> answerResults, List<ConclusionRule> conclusionRules);

    /**
     * 校验冲突规则
     * @param answerResults
     * @return
     */
    List<String> verifyConflictRule(List<AnswerResultHanlder> answerResults, List<ConflictRule> conflictRules);

    Map<String,List<String>> generateConclusionEx(List<AnswerResultHanlder> answerResults, List<ConclusionRuleEx> conclusionRules);

}
