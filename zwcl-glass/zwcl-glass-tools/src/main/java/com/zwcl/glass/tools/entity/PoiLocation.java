package com.zwcl.glass.tools.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class PoiLocation {
    private String name;

    private String address;

    private String province;

    private String city;

    private String area;

    private String phone;

    private String street_id;

    private Double distance;

    private LocationPoint location;

    //扩展增加字段
    private Integer recommendNum;

    private String recommendReason;

    private String description;
}
