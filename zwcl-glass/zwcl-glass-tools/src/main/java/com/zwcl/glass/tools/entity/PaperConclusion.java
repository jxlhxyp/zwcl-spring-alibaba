package com.zwcl.glass.tools.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.zwcl.common.core.validator.groups.Update;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 规则得出结论，先默认为相与
 *
 * @author xyp
 * @since 2020-10-28
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName("glass_paper_conclusion")
//@ApiModel(value = "PaperConclusion对象")
public class PaperConclusion extends BaseEntity<PaperConclusion> {
    private static final long serialVersionUID = 1L;

    @NotNull(message = "id不能为空", groups = {Update.class})
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    //@ApiModelProperty("所属试卷")
    private String paperId;

    private String paperCode;

    //@ApiModelProperty("将结论归类，重复的只显示一次")
    private String typeCode;

    //@ApiModelProperty("结论内容")
    private String conclusionContent;

    //@ApiModelProperty("结论的图片展示，逗号分隔")
    private String conclusionPic;

    /**
     * 结论的关键字
     */
    private String conclusionTags;

    //@ApiModelProperty("得出某个结论的规则")
    private String ruleContent;

    private Integer priority;

    private Boolean isValid;

    //@ApiModelProperty("备注")
    private String remark;

    //@ApiModelProperty("版本")
    @Version
    private Integer version;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
