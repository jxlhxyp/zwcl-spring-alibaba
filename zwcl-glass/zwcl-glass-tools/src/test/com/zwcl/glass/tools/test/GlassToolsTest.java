package com.zwcl.glass.tools.test;

import com.alibaba.fastjson.JSONArray;
import com.zwcl.common.component.upload.cloud.OSSFactory;
import com.zwcl.glass.tools.GlassToolsApplication;
import com.zwcl.glass.tools.entity.Paper;
import com.zwcl.glass.tools.service.PaperService;
import com.zwcl.glass.tools.utils.BaiduMapUtils;
import com.zwcl.glass.tools.utils.GaodeMapUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;

//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = {GlassToolsApplication.class})// 指定启动类
@Slf4j
public class GlassToolsTest extends BaseTest {

    @Autowired
    private OSSFactory ossFactory;

    @Autowired
    private PaperService paperService;


    @Test
    public void testUpload() throws FileNotFoundException {
        log.info("开始。。。。。");
        String url = ossFactory.build().uploadSuffix(new FileInputStream(new File("C:\\Users\\Administrator\\Desktop\\车牌识别\\9.png")),".png","dev");
        System.out.println(url);
        log.info("结束。。。。。");
    }

    @Test
    public void mapTest(){
        String jsonStr = GaodeMapUtils.getKeywordsAddByLbs("验光中心","广州市");
        System.out.println(jsonStr);

        jsonStr = GaodeMapUtils.getKeywordsAddByCity("验光","广州");
        System.out.println(jsonStr);
    }

    @Test
    public void baiduMapTest(){
        String jsonStr = BaiduMapUtils.getCoordinate("广报中心");
        System.out.println(jsonStr);

        JSONArray jsonArr = BaiduMapUtils.getNearPoiInfo("23.11251","113.34166","验光",10000);
        System.out.println(jsonArr);
    }

    @Test
    public void setContains(){
        Set<Long> loginIdSet =new HashSet() {{
            add(643873575727665152L);
            add(664467214111477760L);
            add(703201089331269632L);
            add(717313652134383616L);
            add(721070201470324736L);
            add(722038268400635904L);
            add(743406771602853888L);
            add(806479748204204032L);
        }};
        Long loginId = 703201089331269632L;
        log.info("包含：{}",loginIdSet.contains(loginId));
    }

    @Test
    public void testIterator(){
        //造数据
        List<String> list = new ArrayList<>();
        list.add("111");
        list.add("222");
        list.add("333");
        //使用迭代器
        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            String data = iterator.next().toString();
            if ("222".equals(data)) {
                iterator.remove();
            }
        }
        //打印看下迭代后结果
        for (String aa : list) {
            System.out.println(aa);
        }
    }

    @Test
    //@Transactional(rollbackFor = Exception.class)
    public void testTransction() throws Exception {
        int i=0;
        try {
            Paper paper=new Paper();
            paper.setId(10000);
            paper.setName("测试事务机制");
            paper.setPaperCode("Test");
            paperService.savePaper(paper);
            int j= 10/i;
        }catch (Exception ex){
            log.error("保存失败：{}",ex);
            throw new Exception();
        }
    }
}
