package com.zwcl.glass.tools.api.feign;

import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.glass.tools.api.dto.UserTagsDto;
import com.zwcl.glass.tools.api.entity.UserReport;
import org.apache.ibatis.annotations.Param;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * 注意：Feign调用，同名情况下，还需要加contextId进行区分，否则编译报错；若改名，在调用时，又可能报rabbion无法找到服务的错误
 */
@FeignClient(name = "zwcl-glass-tools",contextId = "upload",path = "/glass/tools")
public interface UploadFileFacadeClient {

    @PostMapping("/upload/uploadFiles")
    ApiResult uploadFiles(@Param("files") MultipartFile[] files);

    @PostMapping("/upload/uploadFile")
    ApiResult uploadFile(@Param("file") MultipartFile file);
}
