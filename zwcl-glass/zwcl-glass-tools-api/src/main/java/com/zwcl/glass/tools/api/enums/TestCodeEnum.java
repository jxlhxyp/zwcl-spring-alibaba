package com.zwcl.glass.tools.api.enums;

import com.zwcl.common.core.exception.BusinessException;
import com.zwcl.common.core.utils.JsonUtils;
import com.zwcl.common.core.utils.StringUtilsEx;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.IntStream;

public enum TestCodeEnum {

    YAN_GUANG("YanGuang", "验光选择",null),


    JING_PIAN("JingPian","镜片选择","GlassSpuRecommend"),


    YA_JIAN_KANG("YaJianKang","视力亚健康测试",null),


    SHI_PI_LAO("ShiPiLao", "视疲劳测试",null),


    JING_Kuang("JingKuang","镜框选择","FrameSpuRecommend"),


    LUO_YAN_SAN_GUANG("LuoYanSanGuang","自测裸眼散光状态",null),


    JIAO_ZHENG_SAN_GUANG("JiaoZhengSanGuang","自测眼镜散光是否矫正",null),


    HONG_LV("HongLv", "红绿试验",null),


    GLASS_TONG_JU("GlassTongJu","眼镜瞳距自测",null),


    EYE_TONG_JU("EyeTongJu","眼睛瞳距自测",null),


    NEAR_ZHU_SHI_LI("NearZhuShiLi", "近用主视力眼自测",null),


    FAR_ZHU_SHI_LI("FarZhuShiLi","远用主视力眼自测",null),


    XIE_SHI("XieShi","近视眼隐性斜视自测",null),

    ;


    private String code;

    /**
     * desc
     */
    private String desc;

    /**
     * 对应的推荐服务名称
     */
    private String serviceName;


    private static final Map<String, String> map;


    static {
        TestCodeEnum[] enums = TestCodeEnum.values();
        int size = enums.length;
        map = IntStream.range(0, size).collect(LinkedHashMap::new, (map, desc) -> {
            map.put(enums[desc].getCode(), enums[desc].getDesc());
        }, Map::putAll);
    }



    TestCodeEnum(String code, String desc,String serviceName) {
        this.code = code;
        this.desc = desc;
        this.serviceName = serviceName;
    }

    /**
     * 返回value
     *
     * @return value
     */
    public String getCode() {
        return this.code;
    }

    /**
     * 返回desc
     *
     * @return desc 字符串
     */
    public String getDesc() {
        return this.desc;
    }

    public String getServiceName() {        return this.serviceName;    }

    /**
     * 根据value获取desc
     *
     * @param code code
     * @return 处理结果
     */
    public static String getDesc(String code) {
        return getMap().get(code) == null ? StringUtilsEx.EMPTY : getMap().get(code);
    }


    public static TestCodeEnum of(String code) {
        TestCodeEnum[] values = TestCodeEnum.values();
        for (TestCodeEnum anEnum : values) {
            if (anEnum.getCode().equals(code)) {
                return anEnum;
            }
        }
        throw new BusinessException("不存在的试卷类型");
    }

    /**
     * 获取map
     *
     * @return 返回map
     */
    public static Map<String, String> getMap() {
        return map;
    }

    /**
     * 获取json
     *
     * @return 处理结果
     * @throws BusinessException 自定义异常
     */
    public static String getJson() throws BusinessException {
        return JsonUtils.objectToJson(getMap());
    }
}
