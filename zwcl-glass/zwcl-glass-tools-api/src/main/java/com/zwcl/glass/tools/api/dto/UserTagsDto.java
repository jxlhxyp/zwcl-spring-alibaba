package com.zwcl.glass.tools.api.dto;

import lombok.Data;

import java.util.List;

@Data
public class UserTagsDto {
    private List<String> paperCodes;

    private Integer userId;
}
