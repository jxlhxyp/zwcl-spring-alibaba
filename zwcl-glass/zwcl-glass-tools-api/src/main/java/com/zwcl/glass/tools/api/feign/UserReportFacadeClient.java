package com.zwcl.glass.tools.api.feign;

import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.glass.tools.api.dto.UserTagsDto;
import com.zwcl.glass.tools.api.entity.UserReport;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 注意：Feign调用，同名情况下，还需要加contextId进行区分，否则编译报错；若改名，在调用时，又可能报rabbion无法找到服务的错误
 */
@FeignClient(name = "zwcl-glass-tools",contextId = "userData",path = "/glass/tools")
public interface UserReportFacadeClient {
    /**
     * 获取用户有的测试报告
     * @return
     */
    @PostMapping("/userData/userTags")
    ApiResult<UserReport> getUserReportTags(@RequestBody UserTagsDto dto);
}
