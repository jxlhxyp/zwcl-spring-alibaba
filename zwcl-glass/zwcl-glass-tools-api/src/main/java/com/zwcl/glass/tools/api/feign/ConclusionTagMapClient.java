package com.zwcl.glass.tools.api.feign;

import com.zwcl.common.core.domain.entity.ApiResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(name = "zwcl-glass-tools",contextId = "conclusionTag",path = "/glass/tools")
public interface ConclusionTagMapClient {
    @GetMapping("/conclusion/tagMap")
    ApiResult<Map<String,String>> getConclusionAndTagMap(@RequestParam("paperCode") String paperCode);
}
