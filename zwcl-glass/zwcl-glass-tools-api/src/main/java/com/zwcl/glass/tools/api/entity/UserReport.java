package com.zwcl.glass.tools.api.entity;

import lombok.Data;

import java.util.Map;

/**
 * 用户报告
 */
@Data
public class UserReport {

    //最新的用户报告
    private Map<String,String> testReports;

    //最新的用户画像
    private Map<String,String> testTags;

}
