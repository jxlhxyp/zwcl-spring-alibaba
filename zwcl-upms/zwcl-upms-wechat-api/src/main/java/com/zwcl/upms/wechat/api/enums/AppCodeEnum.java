package com.zwcl.upms.wechat.api.enums;

import com.zwcl.common.core.enums.ExceptionCode;
import com.zwcl.common.core.exception.BusinessException;
import com.zwcl.common.core.utils.JsonUtils;
import com.zwcl.common.core.utils.StringUtilsEx;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.IntStream;

public enum AppCodeEnum {
    /** 会员 **/
    ETC_MEMBER("Etc_Member", "ETC会员", 10),
    /** 启用 **/
    GLASS_PAPER_TEST("Glass_Test", "眼镜测试",10);

    /**
     * value
     */
    private String value;

    /**
     * desc
     */
    private String desc;

    private Integer appType;

    /**
     * value desc map
     */
    private static final Map<String, String> map;

    static {
        AppCodeEnum[] enums = AppCodeEnum.values();
        int size = enums.length;
        map = IntStream.range(0, size).collect(LinkedHashMap::new, (map, desc) -> {
            map.put(enums[desc].getValue(), enums[desc].getDesc());
        }, Map::putAll);
    }

    /**
     * @param value value值
     * @param desc  desc值
     */
    AppCodeEnum(String value, String desc, Integer appType) {
        this.value = value;
        this.desc = desc;
        this.appType = appType;
    }

    /**
     * 返回value
     *
     * @return value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * 返回desc
     *
     * @return desc 字符串
     */
    public String getDesc() {
        return this.desc;
    }

    public Integer getAppType() {
        return this.appType;
    }

    /**
     * 根据value获取desc
     *
     * @param  value
     * @return 处理结果
     */
    public static String getDesc(String value) {
        return getMap().get(value) == null ? StringUtilsEx.EMPTY : getMap().get(value);
    }

    /**
     * 获取map
     *
     * @return 返回map
     */
    public static Map<String, String> getMap() {
        return map;
    }

    /**
     * 获取json
     *
     * @return 处理结果
     * @throws BusinessException 自定义异常
     */
    public static String getJson() throws BusinessException {
        return JsonUtils.objectToJson(getMap());
    }

    public static AppCodeEnum of(String value) {
        AppCodeEnum[] values = AppCodeEnum.values();
        for (AppCodeEnum anEnum : values) {
            if (anEnum.getValue().equals(value)) {
                return anEnum;
            }
        }
        throw new BusinessException(ExceptionCode.BUSINESS_EXCEPTION.getCode(), "不存在的应用编码");
    }
}
