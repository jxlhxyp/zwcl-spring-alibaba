package com.zwcl.upms.wechat.api.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class WxUserVo {

    private Integer id;

    /**
     * 所属应用
     */
    private String appCode;

    /**
     * 用户类型，10 微信小程序用户，11：微信公众号用户，20：H5用户
     */
    private Integer appType;

    /**
     * 用户token
     */
    private String userToken;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 用户头像
     */
    private String avatarUrl;

    /**
     * 用户性别
     */
    private Integer gender;

    /**
     * 城市
     */
    private String city;

    /**
     * 省份
     */
    private String province;

    /**
     * 国家
     */
    private String country;

    /**
     * 最后登录时间
     * 重要：代码生成器没有加，这里需要加上
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastLoginTime;

}
