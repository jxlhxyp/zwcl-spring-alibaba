package com.zwcl.upms.wechat.api.entity;

import com.zwcl.common.core.domain.entity.CacheUser;
import lombok.Data;

@Data
public class CacheWxUser extends CacheUser {

    private String sessionKey;

    private String openId;

}
