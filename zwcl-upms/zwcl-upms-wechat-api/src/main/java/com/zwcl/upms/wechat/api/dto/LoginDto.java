package com.zwcl.upms.wechat.api.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;


@Data
public class LoginDto {

    @NotBlank(message = "登录Code不能为空")
    private String loginCode;

    @NotBlank(message = "应用编码不能为空")
    private String appCode;

}
