package com.zwcl.upms.wechat.api.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Map;

@Data
public class TemplateMsgDto {

    @NotBlank(message = "应用编码不能为空")
    private String appCode;
    /**
     * 订阅消息的模板id
     */
    @NotBlank(message = "消息模板id不能为空")
    private String templateId;

    /**
     * 消息发往的用户id，token或者userId只传其中之一即可。
     */
    //private String userId;

    /**
     * 消息发往的用户token，token或者userId只传其中之一即可。
     */
    @NotBlank(message = "用户token不能为空")
    private String userToken;

    private Map<String,String> msgParam;
}
