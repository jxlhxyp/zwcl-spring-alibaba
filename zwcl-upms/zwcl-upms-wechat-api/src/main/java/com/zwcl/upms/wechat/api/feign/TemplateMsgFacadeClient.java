package com.zwcl.upms.wechat.api.feign;

import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.upms.wechat.api.dto.TemplateMsgDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@FeignClient(value = "zwcl-upms-wechat")
public interface TemplateMsgFacadeClient {

    //TODO:路径别写错了
    //响应一定要使用ApiResult包装，否则会拿不到数据
    //下面的名称可以不同，参数需相同
    //实体参数加上@RequestBody注解
    @PostMapping("/upms/template/sendMsg")
    ApiResult sendTemplateMsg(@RequestBody TemplateMsgDto dto);
}

