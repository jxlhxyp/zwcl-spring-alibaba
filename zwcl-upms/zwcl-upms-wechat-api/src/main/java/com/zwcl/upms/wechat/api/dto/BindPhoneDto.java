package com.zwcl.upms.wechat.api.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class BindPhoneDto implements Serializable {

    /**
     * 加密密文
     */
    @NotBlank(message = "加密密文不能为空")
    private String encryptedData;

    /**
     * 加密算法初始向量
     */
    @NotBlank(message = "加密算法初始向量不能为空")
    private String iv;

    /**
     * 用户唯一token
     */
    @NotBlank(message = "用户token不能为空")
    private String userToken;

}