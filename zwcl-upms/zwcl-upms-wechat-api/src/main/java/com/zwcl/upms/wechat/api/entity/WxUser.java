package com.zwcl.upms.wechat.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.zwcl.common.core.domain.entity.BaseEntity;;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xieyongping
 * @since 2020-11-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("wx_user")
public class WxUser extends BaseEntity<WxUser> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 所属应用
     */
    private String appCode;

    /**
     * 用户类型，10 微信小程序用户，11：微信公众号用户，20：H5用户
     */
    private Integer appType;

    /**
     * 用户token
     */
    private String userToken;

    /**
     * 第三方应用程序Id
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String thirdId;

    /**
     * 第三方联合Id
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String unionId;

    /**
     * 电话号码
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String phone;

    /**
     * 小程序的登录sessionKey
     */
    @TableField("sessionKey")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String sessionKey;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 用户头像
     */
    private String avatarUrl;

    /**
     * 用户性别
     */
    private Integer gender;

    /**
     * 城市
     */
    private String city;

    /**
     * 省份
     */
    private String province;

    /**
     * 国家
     */
    private String country;

    /**
     * 最后登录时间
     * 重要：代码生成器没有加，这里需要加上
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastLoginTime;

    /**
     * 备注
     */
    private String remark;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
