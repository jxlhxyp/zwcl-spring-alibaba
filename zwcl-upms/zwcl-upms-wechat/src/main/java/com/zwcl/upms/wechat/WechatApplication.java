package com.zwcl.upms.wechat;

import com.zwcl.common.swagger.annotation.EnableCustomSwagger2;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * TODO: nacos的动态刷新配置，好像还有些问题。
 * @author xyp
 */
@SpringCloudApplication
//TODO:重要，一定要修改启动包的扫描路径，要不然无法启动
@MapperScan("com.zwcl.upms.wechat.mapper")
@EnableDiscoveryClient
@EnableCustomSwagger2
public class WechatApplication {
    public static void main(String[] args){
        SpringApplication.run(WechatApplication.class, args);
        System.out.println("启动成功");
    }
}
