package com.zwcl.upms.wechat.entity;

import lombok.Data;

@Data
public class TemplateParam {

    private String key;
    private String value;

}