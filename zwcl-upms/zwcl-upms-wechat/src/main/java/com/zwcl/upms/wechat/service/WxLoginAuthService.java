package com.zwcl.upms.wechat.service;

import com.alibaba.fastjson.JSONObject;
import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.upms.wechat.api.dto.BindPhoneDto;

/**
 * Created by Administrator on 2018/9/19.
 */
public interface WxLoginAuthService {

    JSONObject getWxSession(String code,String appCode);

    ApiResult wechatLogin(String code, String appCode);

    ApiResult checkLogin(String token);

    ApiResult getDecodeUser(String iv, String encryptedData, String thirdSession, String appCode);

    String getAccessToken(String appCode);

    ApiResult bindPhone(BindPhoneDto dto);
}
