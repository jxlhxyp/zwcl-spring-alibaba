package com.zwcl.upms.wechat.controller;


import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.common.core.exception.BusinessException;
import com.zwcl.upms.wechat.api.entity.WxUser;
import com.zwcl.upms.wechat.api.vo.WxUserVo;
import com.zwcl.upms.wechat.service.WxUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 *  TODO:这个类定位提供微信用户等后台查询接口，甚至是内部的feign调用等
 * </p>
 *
 * @author xieyongping
 * @since 2020-11-16
 */
@RestController
@RequestMapping("/wxUser")
public class WxUserController {

    @Autowired
    WxUserService wxUserService;

    @GetMapping("/getById")
    public WxUserVo getUser(Integer userId){
        WxUser wxUser = wxUserService.getById(userId);
        WxUserVo wxUserVo=new WxUserVo();
        BeanUtils.copyProperties(wxUser,wxUserVo);
        return wxUserVo;
    }

    @GetMapping("/getByToken")
    public ApiResult getUserByToken(String token){
        WxUser wxUser = wxUserService.findWxUserByToken(token);
        if(null==wxUser){
            return ApiResult.fail("未查询到用户信息，可能token已失效");
        }
        WxUserVo wxUserVo=new WxUserVo();
        BeanUtils.copyProperties(wxUser,wxUserVo);
        return ApiResult.ok(wxUserVo);
    }
}

