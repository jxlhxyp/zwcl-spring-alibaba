package com.zwcl.upms.wechat.properties;

import lombok.Data;

@Data
public class MiniProgramProperties {
    private String appCode;

    private String appId;

    private String appSecret;
}
