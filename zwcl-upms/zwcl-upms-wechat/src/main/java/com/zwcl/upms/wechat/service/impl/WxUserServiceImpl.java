package com.zwcl.upms.wechat.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zwcl.upms.wechat.api.entity.WxUser;
import com.zwcl.upms.wechat.mapper.WxUserMapper;
import com.zwcl.upms.wechat.service.WxUserService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2020-11-16
 */
@Service
public class WxUserServiceImpl extends BaseServiceImpl<WxUserMapper, WxUser> implements WxUserService {

    @Override
    public WxUser findWxUserByOpenId(String openId) {
        QueryWrapper<WxUser> wrapper=new QueryWrapper<>();
        wrapper.lambda().eq(WxUser::getThirdId,openId);
        return this.getOne(wrapper);
    }

    @Override
    public WxUser findWxUserByToken(String token) {
        QueryWrapper<WxUser> wrapper=new QueryWrapper<>();
        wrapper.lambda().eq(WxUser::getUserToken,token);
        return this.getOne(wrapper);
    }
}
