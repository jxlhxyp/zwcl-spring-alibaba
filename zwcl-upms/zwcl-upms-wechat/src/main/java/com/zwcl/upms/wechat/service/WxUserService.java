package com.zwcl.upms.wechat.service;

import com.zwcl.upms.wechat.api.entity.WxUser;
import com.zwcl.common.mybatis.service.BaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2020-11-16
 */
public interface WxUserService extends BaseService<WxUser> {

    WxUser findWxUserByOpenId(String openId);

    WxUser findWxUserByToken(String token);

}
