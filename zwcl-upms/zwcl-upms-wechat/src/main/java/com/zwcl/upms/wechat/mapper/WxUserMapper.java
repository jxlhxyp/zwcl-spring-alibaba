package com.zwcl.upms.wechat.mapper;

import com.zwcl.upms.wechat.api.entity.WxUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2020-11-16
 */
public interface WxUserMapper extends BaseMapper<WxUser> {

}
