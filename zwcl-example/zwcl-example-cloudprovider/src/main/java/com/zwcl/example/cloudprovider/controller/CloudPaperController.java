package com.zwcl.example.cloudprovider.controller;


import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.common.core.exception.BusinessException;
import com.zwcl.common.web.utils.RequestDetailContext;
import com.zwcl.example.cloudprovider.dto.PaperDto;
import com.zwcl.example.facade.dto.CloudPaperDto;
import com.zwcl.example.facade.entity.CloudPaper;
import com.zwcl.example.cloudprovider.service.CloudPaperService;
import com.zwcl.example.facade.vo.CloudPaperVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2020-11-14
 */
@RestController
@RequestMapping("/paper")
@Slf4j
public class CloudPaperController {
    @Autowired
    private CloudPaperService paperService;

    /**
     * 获取详情
     */
    @GetMapping("/info/{id}")
    public ApiResult<CloudPaper> getPaper(@PathVariable("id") Long id) throws BusinessException {
        CloudPaper glassPaper = paperService.getById(id);
        return ApiResult.ok(glassPaper);
    }

    /**
     * 获取详情
     */
    @GetMapping("/infoEx")
    //@SentinelResource(value = "getPaper")
    public CloudPaper getPaperEx(@Param("id") Long id) throws BusinessException {
        log.info("用户id:" + RequestDetailContext.getLoginUserId().toString());
        log.info("请求链路ID:"+RequestDetailContext.getTraceId().toString());
        //CloudPaper glassPaper = paperService.getById(id);
        CloudPaper glassPaper = paperService.getPaperById(id);
        return glassPaper;
    }

    /**
     * 获取详情
     */
    @GetMapping("/in")
    public CloudPaper PaperEx( Long id) throws BusinessException {
        CloudPaper glassPaper = paperService.getById(id);
        return glassPaper;
    }

    @GetMapping("/test")
    public Integer test() throws BusinessException {
        return 10;
    }

    @PostMapping("/dtoTime")
    public String dtoTime(@RequestBody PaperDto dto) throws BusinessException {
        DateTimeFormatter df1 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime time = LocalDateTime.now();
        String localTime = df1.format(time);
        DateTimeFormatter df2 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime ldt = LocalDateTime.parse("2018-06-01 00:00:00",df2);
        System.out.println("LocalDateTime转成String类型的时间："+localTime);
        System.out.println("String类型的时间转成LocalDateTime："+ldt);
        return dto.getName()+dto.getCreateTime();
    }

    @GetMapping("/test1")
    public ApiResult<String> test1() throws BusinessException {
        return ApiResult.ok("hello");
    }

    @PostMapping("/list")
    public Page<CloudPaperVo> list(@RequestBody CloudPaperDto dto) {
        Page<CloudPaperVo> page = new Page<>(dto.getCurrent(), dto.getSize());
        Page<CloudPaperVo> pageResult = paperService.showPlanList(page, dto);
        return pageResult;
    }

    /**
     * 添加
     */
    @PostMapping("/add")
    public ApiResult<Boolean> addPaper(@RequestBody CloudPaper glassPaper) throws BusinessException {
        boolean flag = paperService.save(glassPaper);
        return ApiResult.result(flag);
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public ApiResult<Boolean> updatePaper( @RequestBody CloudPaper glassPaper) throws BusinessException {
        boolean flag = paperService.updateById(glassPaper);
        Integer i=1/0;
        return ApiResult.result(flag);
    }
//
//    /**
//     * 删除
//     */
//    @PostMapping("/delete/{id}")
//    @OperationLog(name = "删除", type = OperationLogType.DELETE)
//    @ApiOperation(value = "删除", response = ApiResult.class)
//    public ApiResult<Boolean> deletePaper(@PathVariable("id") Long id) throws Exception {
//        boolean flag = glassPaperService.deletePaper(id);
//        return ApiResult.result(flag);
//    }



}

