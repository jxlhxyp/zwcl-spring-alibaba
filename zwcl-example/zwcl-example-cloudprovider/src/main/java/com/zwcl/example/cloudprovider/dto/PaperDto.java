package com.zwcl.example.cloudprovider.dto;

import cn.hutool.core.date.DateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class PaperDto {

    /**
     * 试卷名称
     */
    private String name;

    /**
     * 试卷描述
     */
    private String description;

    //TODO: postman字符串日期传递，无法在请求接受时转化为时间类型。导致后续dto向entity转化时，需要手动处理。
    //注意：yyyy-MM-dd 对应 LocalDate    yyyy-MM-dd HH:mm:ss 对应 LocalDateTime    dateTime类型会报错
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
}
