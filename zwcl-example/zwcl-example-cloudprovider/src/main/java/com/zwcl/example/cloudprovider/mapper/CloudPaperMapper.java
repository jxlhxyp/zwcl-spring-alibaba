package com.zwcl.example.cloudprovider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.example.facade.dto.CloudPaperDto;
import com.zwcl.example.facade.entity.CloudPaper;
import com.zwcl.example.facade.vo.CloudPaperVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2020-11-14
 */
public interface CloudPaperMapper extends BaseMapper<CloudPaper> {
    Page<CloudPaperVo> showPaperList(Page<CloudPaperVo> page, @Param("dto") CloudPaperDto dto);
}
