package com.zwcl.example.cloudprovider;

import com.alicp.jetcache.anno.config.EnableCreateCacheAnnotation;
import com.alicp.jetcache.anno.config.EnableMethodCache;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author xyp
 */
//TODO:看看是使用SpringCloudApplication
@SpringCloudApplication//(scanBasePackages="com.zwcl.example.facade")
//@SpringBootApplication(scanBasePackages="com.zwcl.*")
@MapperScan("com.zwcl.example.cloudprovider.mapper")
//@ComponentScan(basePackages = "com.zwcl.example.**")
@EnableDiscoveryClient
@EnableMethodCache(basePackages = "com.zwcl.example.cloudprovider")
@EnableCreateCacheAnnotation
public class CloudProviderApplication {
    public static void main(String[] args){
        SpringApplication.run(CloudProviderApplication.class, args);
        System.out.println("启动成功");
    }
}
