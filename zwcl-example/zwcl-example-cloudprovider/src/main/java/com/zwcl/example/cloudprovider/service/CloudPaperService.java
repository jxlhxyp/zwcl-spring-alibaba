package com.zwcl.example.cloudprovider.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.common.mybatis.service.BaseService;
import com.zwcl.example.facade.dto.CloudPaperDto;
import com.zwcl.example.facade.entity.CloudPaper;
import com.zwcl.example.facade.vo.CloudPaperVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2020-11-14
 */
public interface CloudPaperService extends BaseService<CloudPaper> {

    Page<CloudPaperVo> showPlanList(Page<CloudPaperVo> page, CloudPaperDto dto);

    CloudPaper getPaperById(Long id);
}
