package com.zwcl.example.provider.sign;

import lombok.Data;

@Data
public class JsonRequest {
    //接口id 可空
    private String serviceId;
    //请求唯一id 非空
    private String requestId;
    //商户id 非空
    private String appId;
    //参数签名 非空
    private String sign;
    //对称加密key 非空
    private String aseKey;
    //时间戳，精确到毫秒 非空
    private long timestamp;
    //请求的业务参数(AES加密后传入) 可空
    private String body;
}