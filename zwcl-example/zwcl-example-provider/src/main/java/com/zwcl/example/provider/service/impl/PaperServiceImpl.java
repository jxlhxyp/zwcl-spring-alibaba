package com.zwcl.example.provider.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.example.provider.dto.PaperDto;
import com.zwcl.example.provider.entity.Paper;
import com.zwcl.example.provider.mapper.PaperMapper;
import com.zwcl.example.provider.service.PaperService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import com.zwcl.example.provider.vo.PaperVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2020-11-14
 */
@Service
public class PaperServiceImpl extends BaseServiceImpl<PaperMapper, Paper> implements PaperService {

    @Autowired
    private PaperMapper paperMapper;

    @Override
    public Page<PaperVo> showPlanList(Page<PaperVo> page, PaperDto dto) {
        Page<PaperVo> pageList= paperMapper.showPaperList(page,dto);
        return pageList;
    }
}
