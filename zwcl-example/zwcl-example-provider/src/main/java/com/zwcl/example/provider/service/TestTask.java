package com.zwcl.example.provider.service;

import com.zwcl.common.core.utils.thread.ITask;
import com.zwcl.common.core.utils.thread.MultiThreadUtils;
import com.zwcl.common.core.utils.thread.ResultBean;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * 具体执行业务任务 需要 实现ITask接口  在execute中重写业务逻辑
 * TestTask<BR>
 * 创建人:Arvin_Mr <BR>
 * 时间：2020年4月12日-下午7:52:50<BR>
 * @version 2.0
 *
 */
public class TestTask implements ITask<ResultBean<String>, Integer> {

    @Override
    public ResultBean<String> execute(Integer e, Map<String, Object> params) {
        /**
         * 具体业务逻辑：将list中的元素加上辅助参数中的数据返回
         */
        int addNum = Integer.valueOf(String.valueOf(params.get("addNum")));
        e = e + addNum;
        ResultBean<String> resultBean = ResultBean.newInstance();
        resultBean.setData(e.toString());
        return resultBean;
    }

    @Override
    public ResultBean<String> batchExecute(List<Integer> data, Map<String, Object> params) {
        int addNum = Integer.valueOf(String.valueOf(params.get("addNum")));
        Integer total=0;
        for(int i=0;i<data.size();i++){
            total = total + data.get(i);
        }
        ResultBean<String> resultBean = ResultBean.newInstance();
        resultBean.setData(total.toString());
        return resultBean;
    }


    public static void main(String[] args) {
        // 需要多线程处理的大量数据list
        List<Integer> data = new ArrayList<>(10000);
        for(int i = 0; i < 10000; i ++){
            data.add(i + 1);
        }
        // 创建多线程处理任务
        MultiThreadUtils<String,Integer> threadUtils = MultiThreadUtils.newInstance(5);
        ITask<ResultBean<String>, Integer> task = new TestTask();
        // 辅助参数  加数
        Map<String, Object> params = new HashMap<>();
        params.put("addNum", 4);
        // 执行多线程处理，并返回处理结果
        ResultBean<List<ResultBean<String>>> resultBean = threadUtils.execute(data, params, task);

        System.out.println("所有线程完成");

        MultiThreadUtils<String,Integer> threadUtilsBatch = MultiThreadUtils.newInstance(5,true);
        ITask<ResultBean<String>, Integer> task1 = new TestTask();
        // 辅助参数  加数
        Map<String, Object> paramsEx = new HashMap<>();
        paramsEx.put("addNum", 5);
        // 执行多线程处理，并返回处理结果
        ResultBean<List<ResultBean<String>>> resultBeanEx = threadUtilsBatch.execute(data, paramsEx, task1);

        System.out.println("所有线程完成");
    }

}