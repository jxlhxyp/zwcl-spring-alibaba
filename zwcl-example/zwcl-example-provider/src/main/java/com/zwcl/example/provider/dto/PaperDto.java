package com.zwcl.example.provider.dto;

import com.zwcl.common.core.domain.dto.BaseDto;
import lombok.Data;

@Data
public class PaperDto extends BaseDto {
    private String name;
}
