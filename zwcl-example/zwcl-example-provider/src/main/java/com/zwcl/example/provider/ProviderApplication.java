package com.zwcl.example.provider;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author xyp
 */
//TODO:看看是使用SpringCloudApplication
//TODO: 注意修改扫描包的路径
//@EnableDiscoveryClient
//@SpringCloudApplication
//@ComponentScan(basePackages = {"com.zwcl.common.*","com.zwcl.example.*"})
@SpringBootApplication
@MapperScan("com.zwcl.example.provider.mapper")
public class ProviderApplication {
    public static void main(String[] args)
    {
        SpringApplication.run(ProviderApplication.class, args);
        System.out.println("启动成功");
    }
}
