package com.zwcl.example.provider.sign;

import lombok.Data;

@Data
public class RequestDto {
    private String name;
    private int age;
    private String hobby;
}