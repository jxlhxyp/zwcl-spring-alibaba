package com.zwcl.example.provider.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.common.core.exception.BusinessException;
import com.zwcl.example.provider.dto.PaperDto;
import com.zwcl.example.provider.dto.TestSignDto;
import com.zwcl.example.provider.entity.Paper;
import com.zwcl.example.provider.service.PaperService;
import com.zwcl.example.provider.vo.PaperVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2020-11-14
 */
@RestController
@RequestMapping("/paper")
@Slf4j
public class PaperController {
    @Autowired
    private PaperService paperService;

    /**
     * 获取详情
     */
    @GetMapping("/info/{id}")
    public ApiResult<Paper> getPaper(@PathVariable("id") Long id) throws BusinessException {
        Paper glassPaper = paperService.getById(id);
        return ApiResult.ok(glassPaper);
    }

    /**
     * 获取详情
     */
    @GetMapping("/infoEx")
    public Paper getPaperEx(@Param("id") Long id) throws BusinessException {
        //测试获取当前用户
        Paper glassPaper = paperService.getById(id);
        return glassPaper;
    }

    /**
     * 获取详情
     */
    @PostMapping("/infoEx1")
    public Paper getPaperEx(@RequestBody Map<String,Object> dto) throws BusinessException {
        //测试获取当前用户
        log.info(dto.toString());
        return null;
    }

    /**
     * 获取详情
     */
    @PostMapping("/infoEx2")
    public Paper getPaperEx(@RequestBody TestSignDto dto) throws BusinessException {
        //测试获取当前用户
        log.info(dto.toString());
        return null;
    }

    /**
     * 获取详情
     */
    @GetMapping("/in")
    public Paper PaperEx(Long id) throws BusinessException {
        Paper glassPaper = paperService.getById(id);
        return glassPaper;
    }

    @GetMapping("/test")
    public Integer test() throws BusinessException {
        return 10;
    }

    @GetMapping("/test1")
    public ApiResult<String> test1() throws BusinessException {
        return ApiResult.ok("hello");
    }

    @GetMapping("/test2")
    public String test2() throws BusinessException {
        return "test";
    }

    @PostMapping("/list")
    public Page<PaperVo> list(@RequestBody PaperDto dto) {
        Page<PaperVo> page = new Page<>(dto.getCurrent(), dto.getSize());
        Page<PaperVo> pageResult = paperService.showPlanList(page, dto);
        return pageResult;
    }

    /**
     * 添加
     */
    @PostMapping("/add")
    public ApiResult<Boolean> addPaper(@RequestBody Paper glassPaper) throws BusinessException {
        boolean flag = paperService.save(glassPaper);
        return ApiResult.result(flag);
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public ApiResult<Boolean> updatePaper( @RequestBody Paper glassPaper) throws BusinessException {
        boolean flag = paperService.updateById(glassPaper);
        Integer i=1/0;
        return ApiResult.result(flag);
    }
//
//    /**
//     * 删除
//     */
//    @PostMapping("/delete/{id}")
//    @OperationLog(name = "删除", type = OperationLogType.DELETE)
//    @ApiOperation(value = "删除", response = ApiResult.class)
//    public ApiResult<Boolean> deletePaper(@PathVariable("id") Long id) throws Exception {
//        boolean flag = glassPaperService.deletePaper(id);
//        return ApiResult.result(flag);
//    }



}

