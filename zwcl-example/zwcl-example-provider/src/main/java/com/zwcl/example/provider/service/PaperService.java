package com.zwcl.example.provider.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.example.provider.dto.PaperDto;
import com.zwcl.example.provider.entity.Paper;
import com.zwcl.common.mybatis.service.BaseService;
import com.zwcl.example.provider.vo.PaperVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2020-11-14
 */
public interface PaperService extends BaseService<Paper> {

    Page<PaperVo> showPlanList(Page<PaperVo> page, PaperDto dto);
}
