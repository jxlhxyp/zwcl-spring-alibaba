package com.zwcl.example.provider.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
@Configuration
@ConfigurationProperties(prefix = "out-access")
public class OutAccessConfigEx {
    private Map<String,String> clients;

    private Set<String> outUrls;
}
