package com.zwcl.example.provider.sign;//package com.zwcl.example.provider.sign;
//
//import org.apache.commons.lang3.StringUtils;
//
//import java.util.HashMap;
//import java.util.Map;
//import java.util.UUID;
//
///**
// * @author: Longer
// * @date: 2020/8/23
// * @description: 测试
// */
//public class RequestTest {
//    public static void main(String[] args) {
//        /****先给调用方分配一组RSA密钥和一个appId****/
//        //初始化RSA密钥
//        Map<String, Object> init = RSAUtil.init();
//        //私钥
//        String privateKey = RSAUtil.getPrivateKey(init);
//        //公钥
//        String publicKey = RSAUtil.getPublicKey(init);
//        //appId，32位的uuid
//        String appId = getUUID32();
//        /****先给调用方分配一组RSA密钥和一个appId****/
//
//        /*****调用方（请求方）*****/
//        //业务参数
//        Map<String,Object>  businessParams = new HashMap<>();
//        businessParams.put("name","Longer");
//        businessParams.put("job","程序猿");
//        businessParams.put("hobby","打篮球");
//
//        JsonRequest jsonRequest = new JsonRequest();
//        jsonRequest.setRequestId(getUUID32());
//        jsonRequest.setAppId(appId);
//        jsonRequest.setTimestamp(System.currentTimeMillis());
//        //使用appId的前16位作为AES密钥，并对密钥进行rsa公钥加密
//        String aseKey = appId.substring(0, 16);
//        byte[] enStr = RSAUtil.encryptByPublicKey(aseKey, publicKey);
//        String aseKeyStr = HexUtils.bytesToHexString(enStr);
//        jsonRequest.setAseKey(aseKeyStr);
//        //请求的业务参数进行加密
//        String body = "";
//        try {
//            body = AESUtil.encrypt(JacksonUtil.beanToJson(businessParams), aseKey, appId.substring(16));
//        } catch (Exception e) {
//            throw new RuntimeException("报文加密异常", e);
//        }
//        jsonRequest.setBody(body);
//        //签名
//        Map<String, Object> paramMap = RSAUtil.bean2Map(jsonRequest);
//        paramMap.remove("sign");
//        // 参数排序
//        Map<String, Object> sortedMap = RSAUtil.sort(paramMap);
//        // 拼接参数：key1Value1key2Value2
//        String urlParams = RSAUtil.groupStringParam(sortedMap);
//        //私钥签名
//        String sign = RSAUtil.sign(HexUtils.hexStringToBytes(urlParams), privateKey);
//        jsonRequest.setSign(sign);
//
//        /*****调用方（请求方）*****/
//
//        /*****接收方（自己的系统）*****/
//        //参数判空（略）
//        //appId校验（略）
//        //本条请求的合法性校验《唯一不重复请求；时间合理》（略）
//        //验签
//        Map<String, Object> paramMap2 = RSAUtil.bean2Map(jsonRequest);
//        paramMap2.remove("sign");
//        //参数排序
//        Map<String, Object> sortedMap2 = RSAUtil.sort(paramMap2);
//        //拼接参数：key1Value1key2Value2
//        String urlParams2 = RSAUtil.groupStringParam(sortedMap2);
//        //签名验证
//        boolean verify = RSAUtil.verify(HexUtils.hexStringToBytes(urlParams2), publicKey, jsonRequest.getSign());
//        if (!verify) {
//            throw new RuntimeException("签名验证失败");
//        }
//        //私钥解密，获取aseKey
//        String aseKey2 = RSAUtil.decryptByPrivateKey(HexUtils.hexStringToBytes(jsonRequest.getAseKey()), privateKey);
//        if (!StringUtils.isEmpty(jsonRequest.getBody())) {
//            // 解密请求报文
//            String requestBody = "";
//            try {
//                requestBody = AESUtil.decrypt(jsonRequest.getBody(), aseKey, jsonRequest.getAppId().substring(16));
//            } catch (Exception e) {
//                throw new RuntimeException("请求参数解密异常");
//            }
//            System.out.println("业务参数解密结果："+requestBody);
//        }
//        /*****接收方（自己的系统）*****/
//    }
//
//    public static String getUUID32() {
//        String uuid = UUID.randomUUID().toString();
//        uuid = uuid.replace("-", "");
//        return uuid;
//    }
//}
