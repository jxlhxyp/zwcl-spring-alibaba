package com.zwcl.example.provider.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zwcl.example.provider.dto.PaperDto;
import com.zwcl.example.provider.entity.Paper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zwcl.example.provider.vo.PaperVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2020-11-14
 */
public interface PaperMapper extends BaseMapper<Paper> {
    Page<PaperVo> showPaperList(Page<PaperVo> page, @Param("dto") PaperDto dto);
}
