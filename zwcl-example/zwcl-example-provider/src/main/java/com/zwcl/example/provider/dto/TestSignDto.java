package com.zwcl.example.provider.dto;

import lombok.Data;

@Data
public class TestSignDto {
    private String transactionNo;

    private String sourceFrom;
}
