package com.zwcl.example.facade.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zwcl.common.core.domain.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 *  代码生成时，实体继承自BaseEntity，则会自带字段填充功能
 *  不做继承设定，则继承自默认的Model类
 * </p>
 *
 * @author xieyongping
 * @since 2020-11-14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("glass_paper")
public class CloudPaper extends BaseEntity<CloudPaper> {   // extends Model<Paper>

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 试卷名称
     */
    private String name;

    /**
     * 试卷描述
     */
    private String description;

    /**
     * 试卷得出结论后的统一性提示或者链接跳转
     */
    private String unifyNotice;

    /**
     * 试卷顺序
     */
    private Integer paperOrder;

    /**
     * 备注
     */
    private String remark;

    /**
     * 版本
     */
    private Integer version;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
