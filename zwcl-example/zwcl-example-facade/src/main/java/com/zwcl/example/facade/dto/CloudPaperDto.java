package com.zwcl.example.facade.dto;

import com.zwcl.common.core.domain.dto.BaseDto;
import lombok.Data;

@Data
public class CloudPaperDto extends BaseDto {
    private String name;
}
