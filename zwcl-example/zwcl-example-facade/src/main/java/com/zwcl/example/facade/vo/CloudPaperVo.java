package com.zwcl.example.facade.vo;

import lombok.Data;

@Data
public class CloudPaperVo {
    private Integer id;

    /**
     * 试卷名称
     */
    private String name;

    /**
     * 试卷描述
     */
    private String description;

    /**
     * 试卷得出结论后的统一性提示或者链接跳转
     */
    private String unifyNotice;

    /**
     * 试卷顺序
     */
    private Integer paperOrder;
}
