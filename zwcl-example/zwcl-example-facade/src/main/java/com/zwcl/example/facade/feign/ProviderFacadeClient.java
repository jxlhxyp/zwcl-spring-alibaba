package com.zwcl.example.facade.feign;

import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.example.facade.entity.CloudPaper;
import org.apache.ibatis.annotations.Param;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "zwcl-example-cloudprovider")
public interface ProviderFacadeClient {

    //TODO:路径别写错了
    //响应一定要使用ApiResult包装，否则会拿不到数据
    //下面的名称可以不同，参数需相同
    //实体参数加上@RequestBody注解
    @GetMapping("/provider/paper/infoEx")
    ApiResult<CloudPaper> getPaperInfoTest(@RequestParam("id") Long id);
}
