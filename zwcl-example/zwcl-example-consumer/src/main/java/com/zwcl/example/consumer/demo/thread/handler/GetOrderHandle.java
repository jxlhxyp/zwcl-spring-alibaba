//package com.zwcl.example.consumer.demo.thread.handler;
//
//import com.alibaba.fastjson.TypeReference;
//import com.alibaba.fastjson.annotation.JSONField;
//import com.zwcl.common.core.thread.handler.AbstractHttpHandle;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//public class GetOrderHandle extends AbstractHttpHandle<GetOrderHandle.Request, OrderEntity> implements ISingleton {
//
//    private static final String handleDesc = "通过php接口获取订单信息";
//    private static final String url = "https://%s/third/order/get-one-by-sn";
//    private static final String method = "POST";
//
//
//    @Override
//    public String getHandleDesc() {
//        return handleDesc;
//    }
//
//    @Override
//    public String getUrl() {
//        return url;
//    }
//
//    @Override
//    public String getMethod() {
//        return method;
//    }
//
//    @Override
//    public AbstractHttpHandle.Response<OrderEntity> checkAndReturn() {
//        return response;
//    }
//
//    @Override
//    public TypeReference<Response<OrderEntity>> getTypeReference() {
//        return new TypeReference<Response<OrderEntity>>() {
//        };
//    }
//
//    @Data
//    @AllArgsConstructor
//    @NoArgsConstructor
//    public static class Request {
//        @JSONField(name = "order_sn")
//        private String orderSn;
//
//        public static Request build(String orderSn){
//            return new Request(orderSn);
//        }
//    }
//
//
//    public static GetOrderHandle singleton(){
//        return SingletonHolder.INSTANCE.getSingleton();
//    }
//
//    private enum SingletonHolder{
//        INSTANCE;
//        private GetOrderHandle singleton = new GetOrderHandle();
//        public GetOrderHandle getSingleton() {
//            return singleton;
//        }
//    }
//
//}
