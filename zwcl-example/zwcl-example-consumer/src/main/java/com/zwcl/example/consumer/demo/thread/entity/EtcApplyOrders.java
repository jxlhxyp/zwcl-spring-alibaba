//package com.etc.baseinfo.biz.entity;
//
//import com.baomidou.mybatisplus.annotation.TableId;
//import com.baomidou.mybatisplus.annotation.TableName;
//import com.etc.baseinfo.api.enums.ApplyOrderStatusEnum;
//import com.etc.baseinfo.biz.entity.reconsitution.OrderEntity;
//import com.etc.baseinfo.biz.service.EtcUsersCardsService;
//import com.etc.baseinfo.biz.utils.SpringUtils;
//import lombok.Data;
//import org.springframework.util.CollectionUtils;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import java.math.BigDecimal;
//import java.time.LocalDateTime;
//import java.util.Collections;
//import java.util.List;
//
///**
// * @Description: 申请订单数据
// * @Date: 2020/1/9 16:47
// * @author: suxinyong
// */
//@Entity
//@Data
//@TableName("etc_apply_orders")
//public class EtcApplyOrders {
//
//    @TableId
//    private Long id;
//
//    /**
//     * 订单流水号
//     */
//    @Column(name = "order_sn")
//    private String orderSn;
//
//    /**
//     * 最新一次推送发卡方用的 订单号
//     */
//    @Column(name = "push_order_sn")
//    private String pushOrderSn;
//
//    /**
//     * 1 普通 2 预支付
//     */
//    @Column(name = "type")
//    private Integer type;
//
//    /**
//     * 下单人uid
//     */
//    @Column(name = "uid")
//    private Integer uid;
//
//    /**
//     * 下单人昵称
//     */
//    @Column(name = "nickname")
//    private String nickname;
//
//    /**
//     * 卡种 etc_cards.id
//     */
//    private Integer cardId;
//
//    /**
//     * 卡名称（冗余）
//     */
//    private String cardName;
//
//    /**
//     * 申请场景（ApplyBusiness::SCENES）
//     */
//    private Integer scene;
//
//    /**
//     * 身份证信息 etc_users_idcards.id
//     */
//    private Integer idcardInfoId;
//
//    /**
//     * 1 大陆居民身份证 2 港澳回乡证 3 台胞证
//     */
//    private Integer idcardType;
//
//    /**
//     * 行驶证信息 etc_users_vehicles.id
//     */
//    private Integer vehicleInfoId;
//
//    /**
//     * 订单状态（详见 models/ApplyOrder 常量）
//     */
//    private Integer status;
//
//    /**
//     * 1 默认 2 已提交地址 3 已支付
//     */
//    private Integer prepayStatus;
//
//    /**
//     * 0 没有保证金 1 使用保证金 2 保证金已使用（退款）
//     */
//    private Integer depositStatus;
//
//    /**
//     * 0 手机未验证 1 手机已验证
//     */
//    private Integer phoneVerified;
//
//    /**
//     * 是否有过成功签约，不能判断当前签约是否正常  0 未有签约 1已有签约
//     */
//    private Integer hasOnceSigned;
//
//    /**
//     * 是否已提交收货地址
//     */
//    private Integer hasSaveAddress;
//
//    /**
//     * 是否需要人工审核:0 不需要 1 需要
//     */
//    private Integer needAudit;
//
//    /**
//     * 审核原因
//     */
//    private String auditReason;
//
//    /**
//     * 1 用户在订单状态为收货之前，声称收到了货
//     */
//    private Integer declareReceived;
//
//    /**
//     * 发卡方的订单流水号
//     */
//    private String thirdOrderSn;
//
//    /**
//     * 邮寄联系人
//     */
//    private String sendName;
//
//    /**
//     * 邮寄联系手机
//     */
//    private String sendPhone;
//
//    /**
//     * 邮寄地区
//     */
//    private String sendArea;
//
//    /**
//     * 邮寄地址
//     */
//    private String sendAddress;
//
//    /**
//     * 总计需支付（元）
//     */
//    private BigDecimal needPay;
//
//    /**
//     * 设备费（元）
//     */
//    private BigDecimal obuFee;
//
//    /**
//     * 运费（元）
//     */
//    private BigDecimal freightFee;
//
//    /**
//     * 预充值费（元）
//     */
//    private BigDecimal prechargeFee;
//
//    /**
//     * 腾讯征信是否良好
//     */
//    private Integer xinyongGood;
//
//    /**
//     * 腾讯征信具体分数
//     */
//    private Integer xinyongScore;
//
//    /**
//     * 腾讯意愿度
//     */
//    private String evaluateWilling;
//
//    /**
//     * 我方支付订单流水号（支付成功）
//     */
//    private String paymentSn;
//
//    /**
//     * 我方退款流水号
//     */
//    private String refundSn;
//
//    /**
//     * 优惠券号
//     */
//    private String couponCode;
//
//    /**
//     * 标识是否用券
//     */
//    private Integer couponUsed;
//
//    /**
//     * 1 微信卡券 2 内部券
//     */
//    private Integer couponSource;
//
//    /**
//     * 是否有代金券
//     */
//    private Integer hasCashCoupon;
//
//    /**
//     * 代金券id
//     */
//    private String cashCouponId;
//
//    /**
//     * 审核失败原因
//     */
//    private String rejectMsg;
//
//    /**
//     * 订单取消原因
//     */
//    private String cancelMsg;
//
//    /**
//     * ETC通行卡号
//     */
//    private String cardNo;
//
//    /**
//     * OBU设备电子标签号
//     */
//    private String obuDeviceSn;
//
//    /**
//     * 车牌号（冗余）
//     */
//    private String plateNo;
//
//    /**
//     * 订单支付时间
//     */
//    private LocalDateTime paidAt;
//
//    /**
//     * 物流公司
//     */
//    private String expressCorp;
//
//    /**
//     * 物流运单号
//     */
//    private String expressNumber;
//
//    /**
//     * 第三方系统的账号
//     */
//    private String issuerAccount;
//
//    /**
//     * 备注
//     */
//    private String remark;
//
//    /**
//     * 被拒绝后是否继续申办 1 是 2 非(拒绝后重新申办)
//     */
//    private Integer canContinue;
//
//    /**
//     * 临时状态
//     */
//    private String tmpStatus;
//
//    private String rescanInfo;
//
//    /**
//     * 0未发送 1已发送上传行驶证提醒  2已发送提交收货地址提醒  3已发送签约提醒  4已发送支付提醒
//     */
//    private Integer recallNotify;
//
//    /**
//     * 重发通知时间
//     */
//    private LocalDateTime recallNotifyAt;
//
//    /**
//     * 发货时间
//     */
//    private LocalDateTime shippedAt;
//
//    /**
//     * 签收时间
//     */
//    private LocalDateTime receivedAt;
//
//    private LocalDateTime createdAt;
//
//    private LocalDateTime updatedAt;
//
//    /**
//     * 激活类型 0:未区分1:发行1.0 2:发行2.0
//     */
//    private Integer activateType;
//
//    /**
//     * 订单来源：1.didi, 2.weiche, 3.kuaigou
//     */
//    private Integer thirdType;
//
//    /**
//     * 是否已催办
//     */
//    private Integer isPressed;
//
//    /**
//     * 修改declare_received的时间
//     */
//    private LocalDateTime declareReceivedTime;
//
//    /**
//     * 首次激活时间
//     */
//    private LocalDateTime firstActivatedAt;
//
//    /**
//     * 支付方式（包括免费）1 省份免费 2 申办信用卡免费 3腾讯大王申办 4 绑定银行支付申办
//     */
//    private Integer freeType;
//
//    /**
//     * 卡所属银行/机构与类别(不仅仅是信用卡） 2平安银行信用卡 4 交通银行信用卡 5 腾讯大王卡 6招商银行
//     */
//    private Integer creditCardBank;  // purchase_party
//
//    /**
//     * 支付方式确认状态 0 未知状态 1 确定成功 2确定失败 3扣费失败
//     */
//    private Integer purchaseTypeConfirmed;
//
//    /**
//     * 是否线下审核 1 是 0 不是
//     */
//    private Integer isLocalAudit;
//
//    /**
//     * 线下审核人员账户
//     */
//    private String localAuditOperator;
//
//    /**
//     * 线下渠道ID
//     */
//    private Integer channelId;
//
//    /**
//     * 申办流程:1 普通2.0订单流程 2 地推2.0流程
//     */
//    private Integer flowType;
//
//    /**
//     * 营业执照信息 etc_users_biz_license.id
//     */
//    private Integer bizInfoId;
//
//    /**
//     * 车辆归属 1个人 2单位
//     */
//    private Integer vehicleBelong;
//
//    /**
//     * 催货状态：0：初始；10：已催发货；20：无催发货已催快递；21：已催发货已催快递
//     */
//    private Integer urgeStatus;
//
//    /**
//     * 通行扣费支付渠道 1、我方自己发起的微信扣费 2、米大师发起的扣费 3、招行代扣 4、绑定银行扣费
//     */
//    private Integer tollPayChannel;
//
//    /**
//     * 推广渠道来源标识
//     */
//    private String source;
//
//    /**
//     * 订单综合状态
//     */
//    private Integer compositeStatus;
//
//
//    public static EtcApplyOrders of(OrderEntity entity) {
//        if (entity == null) {
//            return null;
//        }
//        EtcApplyOrders orders = new EtcApplyOrders();
//        orders.setOrderSn(entity.getOrder_sn());
//        orders.setUid(entity.getUid().intValue());
//        orders.setNickname(entity.getNickname());
//        orders.setCardId(entity.getCard_id());
//        orders.setCardName(entity.getCard_name());
//        orders.setScene(entity.getScene());
////        orders.setIdcardInfoId(entity.getid); // 身份证id
////        orders.setIdcardType(entity.ge); // 身份证id类型
////        orders.setVehicleInfoId(entity.get); // 行驶证信息id
//        orders.setStatus(orders.statusConvert(entity.getStatus())); // 订单状态，可能需要转换
////        orders.setPrepayStatus(entity.ge); // 预支付状态
//        orders.setDepositStatus(entity.getDeposit_status());
////        orders.setPhoneVerified(entity.); // 手机是否验证
//        orders.setHasOnceSigned(entity.getHas_once_signed());
////        orders.setHasSaveAddress(entity.gethas); // 是否已提交收货地址
////        orders.setNeedAudit(entity.getne); // 是否需要人工审核
////        orders.setAuditReason(entity.get); // 审核原因
//        orders.setDeclareReceived(entity.getDeclare_received());
////        orders.setThirdOrderSn(entity.ge); // 第三方流水号
//        orders.setSendName(entity.getSend_name());
//        orders.setSendArea(entity.getSend_area());
//        orders.setSendAddress(entity.getSend_address());
//        orders.setNeedPay(entity.getNeed_pay());
////        orders.setObuFee(entity.geto); // 设备费
//        orders.setFreightFee(entity.getFreight_fee());
////        orders.setPrechargeFee(entity.getpr); // 预充值费
////        orders.setXinyongGood(entity.getxin); // 腾讯征信是否良好
////        orders.setXinyongScore(entity.getxinyong); // 腾讯征信具体分数
////        orders.setEvaluateWilling(entity.getev); // 腾讯愿意度
//        orders.setPaymentSn(entity.getPayment_sn());
//        orders.setRefundSn(entity.getRefund_sn());
//        orders.setCouponCode(entity.getCoupon_code());
//        orders.setCouponUsed(entity.getCoupon_used());
//        orders.setCouponSource(entity.getCoupon_source());
////        orders.setHasCashCoupon(entity.()); // 是否有代金券
//        orders.setCashCouponId(entity.getCash_coupon_id());
////        orders.setRejectMsg(entity.getr)// 审核失败原因
//        orders.setCancelMsg(entity.getCancel_msg());
////        orders.setCardNo(entity.getCard_id()); // 卡号
////        orders.setObuDeviceSn(entity.getobu); // 设备标签号
//        orders.setPlateNo(entity.getPlate_no());
////        orders.setPaidAt(entity.getp); // 支付订单时间
////        orders.setExpressCorp(entity.getex); // 物流公司
////        orders.setExpressNumber(entity.getex); // 物流订单号
//        orders.setRemark(entity.getRemark());
////        orders.setCanContinue(entity.getcan); // 被拒绝后是否继续申办
////        orders.setTmpStatus(entity.gettmp); //临时状态
////        orders.setRescanInfo(entity.getresca);
//
//
////        orders.setRecallNotify(entity.getre); // 0未发送 1已发送上传行驶证提醒  2已发送提交收货地址提醒  3已发送签约提醒  4已发送支付提醒
////        orders.setRecallNotifyAt(entity.getrecaln); //重发通知时间
////        orders.setShippedAt(entity.getsh); // 发货时间
////        orders.setReceivedAt(entity.getrece); // 签收时间
//        orders.setCreatedAt(entity.getCreated_at());
//        orders.setUpdatedAt(entity.getUpdated_at());
////        orders.setActivateType(entity.getac); // 激活类型 0:未区分1:发行1.0 2:发行2.0
//        orders.setThirdType(entity.getThird_type());
////        orders.setIsPressed(entity.getisp); // 是否已催办
////        orders.setFirstActivatedAt(entity.getfir); // 首次激活时间
////        orders.setFreeType(entity.getfree); // 支付方式（包括免费）1 省份免费 2 申办信用卡免费 3腾讯大王申办 4 绑定银行支付申办
//        orders.setCreditCardBank(entity.getPurchase_party()); // 卡所属银行 ！！！！ 一定要的
//        orders.setPurchaseTypeConfirmed(entity.getPurchase_type_confirmed());
////        orders.setIsLocalAudit(entity.getlo); // 是否线下审核
////        orders.setLocalAuditOperator(entity.getl); // 线下审核人员账户
////        orders.setChannelId(entity.getch); // 线下渠道id
//        orders.setFlowType(entity.getFlow_type());
////        orders.setBizInfoId(entity.getinfo); // 营业执照信息
//        orders.setVehicleBelong(entity.getVehicle_belong());
//        orders.setUrgeStatus(entity.getUrge_status()); // 状态确认是否一致
////        orders.setTollPayChannel(entity.gettoll); // 通行扣费支付渠道 通行扣费支付渠道 1、我方自己发起的微信扣费 2、米大师发起的扣费 3、招行代扣 4、绑定银行扣费
//        orders.setSource(entity.getSource().toString());
////        orders.setCompositeStatus(entity.getcom); // 订单综合状态
//
//        return orders;
//    }
//
//
//    public Integer statusConvert(Integer oldStatus){
//        if(oldStatus == null) return null;
//        // 转换取消订单
//        if(oldStatus.equals(5)){
//            return ApplyOrderStatusEnum.STATUS_CANCELED.getValue();
//        }
//        // 转换激活订单
//        if(this.isActive(this.orderSn)){
//            return ApplyOrderStatusEnum.STATUS_FINISHED.getValue();
//        }
//        return oldStatus;
//    }
//
//    private boolean isActive(String orderSn){
//        EtcUsersCardsService cardsService = SpringUtils.getBean(EtcUsersCardsService.class);
//        List<EtcUsersCards> cards = cardsService.listByOrderSns(Collections.singletonList(orderSn));
//        if(CollectionUtils.isEmpty(cards)) return false;
//        return cards.get(0).getActivatedAt() != null;
//    }
//
//}
