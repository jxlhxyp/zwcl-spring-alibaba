//package com.zwcl.example.consumer.demo.thread.task;
//
//import com.etc.baseinfo.biz.entity.EtcApplyOrders;
//import com.zwcl.example.consumer.demo.thread.AbstractPHPOrderTask;
//
//
///**
// * 数据库根据订单号查询订单信息
// */
//public class DBGetOrderTask extends AbstractPHPOrderTask<EtcApplyOrders> {
//
//
//    private static final String taskName = "数据库根据订单号查询订单信息";
//    private final EtcApplyOrdersService applyOrdersService;
//    private final String orderSn;
//
//    public DBGetOrderTask(String orderSn) {
//        this.orderSn = orderSn;
//        this.applyOrdersService = SpringUtils.getBean(EtcApplyOrdersService.class);
//        this.isExecute = false;
//    }
//
//
//    @Override
//    public String instance() {
//        return taskName;
//    }
//
//    @Override
//    public EtcApplyOrders call() throws Exception {
//        return this.applyOrdersService.getByOrderSn(this.orderSn);
//    }
//}
