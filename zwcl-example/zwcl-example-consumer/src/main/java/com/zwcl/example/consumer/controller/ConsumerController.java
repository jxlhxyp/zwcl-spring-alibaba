package com.zwcl.example.consumer.controller;

import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.common.log.annotation.Log;
import com.zwcl.common.log.enums.BusinessType;
import com.zwcl.common.log.enums.OperatorType;
import com.zwcl.example.consumer.dto.Testdto;
import com.zwcl.example.facade.entity.CloudPaper;
import com.zwcl.example.facade.feign.ProviderFacadeClient;
import feign.FeignException;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
public class ConsumerController {

    @Autowired
    private ProviderFacadeClient providerFacadeClient;

    @GetMapping("/hi-feign")
    @Log(title = "测试feign调用",operatorType = OperatorType.MOBILE,isMonitor = true)
    public ApiResult hiFeign(@RequestParam("id") Long id){
        try {
            ApiResult<CloudPaper> paper = providerFacadeClient.getPaperInfoTest(id);
            return paper;
        }catch (FeignException.FeignClientException ex){
            log.error("feign调用发现异常了");
            throw ex;
        }
    }

    @PostMapping("/logtest")
    @Log(title = "测试Log注解",operatorType = OperatorType.MOBILE,isMonitor = true)
    public ApiResult logTest(@RequestBody Testdto testdto){
        Testdto testdto1=new Testdto();
        testdto1.setUserId(10);
        testdto1.setUserName("王兰");
        return ApiResult.ok(testdto1);
    }
}
