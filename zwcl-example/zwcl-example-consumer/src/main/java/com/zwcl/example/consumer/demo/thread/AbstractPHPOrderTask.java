package com.zwcl.example.consumer.demo.thread;


import com.zwcl.common.component.thread.AbstractSimpleTask;

public abstract class AbstractPHPOrderTask<T> extends AbstractSimpleTask<T> {

    public boolean isExecute;

    @Override
    public abstract String instance();

    @Override
    public abstract T call() throws Exception;

    @Override
    public boolean isExecute(){
        return this.isExecute;
    }

    @Override
    public void setExecuted(){
        this.isExecute = true;
    }


}
