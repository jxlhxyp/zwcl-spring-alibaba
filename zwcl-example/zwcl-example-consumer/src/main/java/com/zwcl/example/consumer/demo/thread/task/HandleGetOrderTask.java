//package com.zwcl.example.consumer.demo.thread.task;
//
//import com.etc.baseinfo.biz.entity.EtcApplyOrders;
//import com.zwcl.example.consumer.demo.thread.AbstractPHPOrderTask;
//import com.zwcl.example.consumer.demo.thread.handler.GetOrderHandle;
//
//public class HandleGetOrderTask extends AbstractPHPOrderTask<EtcApplyOrders> {
//
//    private final static String taskName = "Http请求根据订单号查询订单信息";
//    private final String host;
//    private final String orderSn;
//
//    public HandleGetOrderTask(String host, String orderSn) {
//        this.host = host;
//        this.orderSn = orderSn;
//    }
//
//
//    @Override
//    public String instance() {
//        return taskName;
//    }
//
//    @Override
//    public EtcApplyOrders call() throws Exception {
//        GetOrderHandle handle = GetOrderHandle.singleton();
//        GetOrderHandle.Request param = GetOrderHandle.Request.build(this.orderSn);
//        OrderEntity orderEntity = handle.execute(this.host, param).getData();
//        return EtcApplyOrders.of(orderEntity);
//    }
//}
