package com.zwcl.example.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * core包中已经引入了feigin，此处只要加允许feign调用的注解
 * EnableFeignClients中不加basePackages设置，编译报错
 * TODO:如果加了日志处理，feign调用的时候，一定要扫描日志的feign接口
 */
@EnableDiscoveryClient
@SpringCloudApplication
@EnableFeignClients(basePackages = {"com.zwcl.example.facade","com.zwcl.manage.system.api"})
public class ConsumerApplication {
    public static void main(String[] args)
    {
        SpringApplication.run(ConsumerApplication.class, args);
        System.out.println("启动成功");
    }
}
