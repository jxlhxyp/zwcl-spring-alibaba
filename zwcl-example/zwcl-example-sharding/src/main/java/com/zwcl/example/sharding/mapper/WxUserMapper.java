package com.zwcl.example.sharding.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zwcl.example.sharding.entity.WxUser;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2020-11-16
 */
public interface WxUserMapper extends BaseMapper<WxUser> {

}
