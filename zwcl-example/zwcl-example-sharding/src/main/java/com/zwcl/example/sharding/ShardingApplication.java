package com.zwcl.example.sharding;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author xyp
 * 分库分表测试
 * 1、 测试复合分片规则
 * 2、 测试简单的分片规则
 * 3、 测试有些表不做分库
 */
@SpringCloudApplication
@MapperScan("com.zwcl.example.sharding.mapper")
@EnableDiscoveryClient
public class ShardingApplication {
    public static void main(String[] args){
        SpringApplication.run(ShardingApplication.class, args);
        System.out.println("启动成功");
    }
}
