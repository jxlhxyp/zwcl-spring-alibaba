package com.zwcl.example.sharding.strategy;

import com.zwcl.example.sharding.constant.ShardingConstant;
import com.zwcl.example.sharding.util.ShardingUtil;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import java.math.BigDecimal;
import java.util.Collection;


public class UserIdShardingTbAlgorithm  implements PreciseShardingAlgorithm<String> {
    @Override
    public String doSharding(Collection<String> collection, PreciseShardingValue<String> preciseShardingValue) {
        String userId = preciseShardingValue.getValue();
        String index = ShardingUtil.fillZero(String.valueOf(ShardingUtil.getTbIndexByMod(userId,3,6)+1), ShardingConstant.TABLE_SUFFIX_LENGTH);
        return preciseShardingValue.getLogicTableName()+"_"+index;
    }
}
