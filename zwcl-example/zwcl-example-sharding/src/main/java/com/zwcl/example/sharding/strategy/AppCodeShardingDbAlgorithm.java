package com.zwcl.example.sharding.strategy;

import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;

public class AppCodeShardingDbAlgorithm implements PreciseShardingAlgorithm<String> {
    @Override
    public String doSharding(Collection<String> collection, PreciseShardingValue<String> preciseShardingValue) {
        if(preciseShardingValue.getValue().equals("Glass"))
            return "ms-ds0";
        if(preciseShardingValue.getValue().equals("RoadNet")){
            return "ms-ds1";
        }
        if(preciseShardingValue.getValue().equals("Zwcl")){
            return "ms-ds2";
        }
        return null;
    }
}
