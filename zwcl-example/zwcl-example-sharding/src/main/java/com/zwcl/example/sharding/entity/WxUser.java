package com.zwcl.example.sharding.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zwcl.common.core.domain.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 *
 * 小程序用户根据业务应用分库，根据业务应用查其下的用户，根据userId查，根据token查，根据thirdId即openId查
 * userId根据thirdId生成，token用thirdId进行对称加密，这样token解密后，可以根据复合分片规则，找寻库和表。
 * 如果借鉴那个demo的思想，那么就很难根据业务应用编码进行分库。
 * </p>
 *
 * @author xieyongping
 * @since 2020-11-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("wx_user")
public class WxUser extends BaseEntity<WxUser> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户表自增id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 所属应用
     */
    private String appCode;

    /**
     * 用户类型，10 微信小程序用户，11：微信公众号用户，20：H5用户
     */
    private Integer appType;

    /**
     * 用户token
     */
    private String userToken;

    /**
     * 第三方应用程序Id
     */
    private String thirdId;

    /**
     * 第三方联合Id
     */
    private String unionId;

    /**
     * 电话号码
     */
    private String phone;

    /**
     * 小程序的登录sessionKey
     */
    @TableField("sessionKey")
    private String sessionKey;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 用户头像
     */
    private String avatarUrl;

    /**
     * 用户性别
     */
    private Integer gender;

    /**
     * 城市
     */
    private String city;

    /**
     * 省份
     */
    private String province;

    /**
     * 国家
     */
    private String country;

    /**
     * 最后登录时间
     * 重要：代码生成器没有加，这里需要加上
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastLoginTime;

    /**
     * 备注
     */
    private String remark;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
