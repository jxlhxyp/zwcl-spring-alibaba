package com.zwcl.example.sharding.service;

import com.zwcl.common.mybatis.service.BaseService;
import com.zwcl.example.sharding.entity.WxUser;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2020-11-16
 */
public interface WxUserService extends BaseService<WxUser> {

    WxUser findWxUserByOpenId(String openId);

    WxUser findWxUserByToken(String token);
}
