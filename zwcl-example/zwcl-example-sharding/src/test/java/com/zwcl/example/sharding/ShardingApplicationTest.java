//package com.zwcl.example.sharding;
//
//import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
//import com.zwcl.common.core.utils.MD5Utils;
//import com.zwcl.example.sharding.constant.DbAndTableEnum;
//import com.zwcl.example.sharding.entity.WxUser;
//import com.zwcl.example.sharding.sequence.KeyGenerator;
//import com.zwcl.example.sharding.service.WxUserService;
//import lombok.extern.slf4j.Slf4j;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.math.BigDecimal;
//
//import static org.junit.jupiter.api.Assertions.*;
//TODO: 测试类中的KeyGenerator使用到了redis，package的时候失败，但是运行却是成功的
//@RunWith(SpringRunner.class)
//@SpringBootTest
//@Slf4j
//public class ShardingApplicationTest {
//
//    @Autowired
//    private WxUserService wxUserService;
//
//    @Autowired
//    private KeyGenerator keyGenerator;
//
//    private String[] AppArr=new String[]{"Glass","Zwcl","RoadNet"};
//
//    @Test
//    public void testInsertUserInfo() {
//        for (int i=0;i<300;i++){
//            String openId= keyGenerator.getUUID();
//            BigDecimal no=BigDecimal.valueOf(i);
//            BigDecimal[] results = no.divideAndRemainder(new BigDecimal(3));
//            String userId=keyGenerator.generateKey(DbAndTableEnum.WX_USER,openId);
//            WxUser wxUser=new WxUser();
//            wxUser.setThirdId(openId);
//            wxUser.setUserId(userId);
//            wxUser.setUserToken(MD5Utils.getMd5(openId));
//            wxUser.setAppCode(AppArr[results[1].intValue()]);
//            wxUserService.save(wxUser);
//        }
//    }
//
//    @Test
//    public void testSelectUser(){
//        String useId="WU000001012012091643563384300164";
//        LambdaQueryWrapper<WxUser> lambdaQueryWrapper=new LambdaQueryWrapper<WxUser>()
//                .eq(WxUser::getUserId,useId);
//        log.info(wxUserService.getOne(lambdaQueryWrapper).toString());
//    }
//}
