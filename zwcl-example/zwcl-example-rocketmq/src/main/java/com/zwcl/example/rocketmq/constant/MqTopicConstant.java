package com.zwcl.example.rocketmq.constant;

public class MqTopicConstant {

    public static final String ADMIN_CARWASH_REFUND = "admin_carwash_refund";

    /**
     * 默认标签
     */
    public static final String DEFULT_TAG = "TagA";
}
