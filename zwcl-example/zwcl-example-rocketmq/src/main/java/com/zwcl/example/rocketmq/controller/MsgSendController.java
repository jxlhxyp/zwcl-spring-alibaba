package com.zwcl.example.rocketmq.controller;

import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.example.rocketmq.entity.MsgTestEntity;
import com.zwcl.example.rocketmq.producer.MqProducer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/send")
public class MsgSendController {

    @Autowired
    private MqProducer mqProducer;

    @PostMapping("/test")
    public boolean sendMsg(@RequestBody MsgTestEntity msgTestEntity){
        return mqProducer.sendMsg(msgTestEntity);
    }

    @GetMapping("/test1")
    public boolean testSendMsg(){
        Map<String,String> map=new HashMap<>();
        map.put("1","v1");
        map.put("2","v2");
        return mqProducer.wxNormalRefundCallBack(map);
    }
}
