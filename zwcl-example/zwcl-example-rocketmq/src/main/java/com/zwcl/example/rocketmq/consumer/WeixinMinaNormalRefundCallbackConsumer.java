package com.zwcl.example.rocketmq.consumer;

import com.alibaba.fastjson.JSON;
import com.zwcl.common.mq.RocketMQListenerAwareNew;
import com.zwcl.example.rocketmq.constant.MqTopicConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description 微信小程序退款回调（普通商户）
 * 测试接收map
 * @Author xyp
 * @Date 2020/03/18
 */
@Slf4j
@Service
@RocketMQMessageListener(topic = MqTopicConstant.ADMIN_CARWASH_REFUND, selectorExpression = MqTopicConstant.DEFULT_TAG, consumerGroup = MqTopicConstant.ADMIN_CARWASH_REFUND)
public class WeixinMinaNormalRefundCallbackConsumer extends RocketMQListenerAwareNew<Map<String,String>> {
    @Override
    public Boolean consumerMessage(Map<String, String> message) {
        log.info("消费消息：{}", message);
        return true;
    }
}
