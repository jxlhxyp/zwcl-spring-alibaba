package com.zwcl.example.rocketmq;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author xyp
 */
@SpringBootApplication
public class RocketmqApplication {
    public static void main(String[] args)
    {
        SpringApplication.run(RocketmqApplication.class, args);
        System.out.println("启动成功");
    }
}
