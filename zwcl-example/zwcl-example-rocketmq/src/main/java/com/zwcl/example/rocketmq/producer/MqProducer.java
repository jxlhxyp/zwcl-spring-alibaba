package com.zwcl.example.rocketmq.producer;

import com.zwcl.common.mq.BasePayload;
import com.zwcl.common.mq.MqConstant;
import com.zwcl.common.mq.RocketMQProductUtils;
import com.zwcl.example.rocketmq.constant.MqTopicConstant;
import com.zwcl.example.rocketmq.entity.MsgTestEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@Slf4j
public class MqProducer {

    @Autowired
    private RocketMQProductUtils rocketMQProductUtils;

    public Boolean sendMsg(MsgTestEntity msgTestEntity){
        rocketMQProductUtils.syncSend(MqTopicConstant.ADMIN_CARWASH_REFUND,null,MqTopicConstant.DEFULT_TAG,msgTestEntity);
        return true;
    }

    /**
     * 自身发送，自身消费
     * @param params
     * @return
     */
    public Boolean wxNormalRefundCallBack(Map<String, String> params){
        try {
            BasePayload basePayload=new BasePayload<Map<String, String>>(params);
            Boolean flag =rocketMQProductUtils.syncSend(MqTopicConstant.ADMIN_CARWASH_REFUND,null,MqTopicConstant.DEFULT_TAG,basePayload);
            if(!flag)
                log.error("（普通商户）微信退款回调通知自身失败");
            return flag;
        }catch (Exception ex){
            log.error("（普通商户）微信退款回调通知自身失败，{}",ex);
            return false;
        }
    }
}
