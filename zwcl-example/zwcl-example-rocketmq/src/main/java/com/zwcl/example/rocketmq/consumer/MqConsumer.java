//package com.zwcl.example.rocketmq.consumer;
//
//import com.zwcl.common.mq.RocketMQListenerAwareNew;
//import com.zwcl.example.rocketmq.constant.MqTopicConstant;
//import com.zwcl.example.rocketmq.entity.MsgTestEntity;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
//import org.springframework.stereotype.Service;
//
////TODO:拿公司的topic测试，发送出去的消息，有的接受不到。可能是被线上其他的消费者消费了
//@Service
//@Slf4j
//@RocketMQMessageListener(topic = MqTopicConstant.ADMIN_CARWASH_REFUND, selectorExpression = MqTopicConstant.DEFULT_TAG, consumerGroup = MqTopicConstant.ADMIN_CARWASH_REFUND)
//public class MqConsumer extends RocketMQListenerAwareNew<MsgTestEntity> {
//
//    @Override
//    public Boolean consumerMessage(MsgTestEntity message) {
//        log.info("消费消息：{}", message);
//        return true;
//    }
//}
