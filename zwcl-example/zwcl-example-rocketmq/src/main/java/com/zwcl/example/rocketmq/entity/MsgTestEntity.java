package com.zwcl.example.rocketmq.entity;

import com.zwcl.common.mq.BasePayload;
import lombok.Data;

@Data
public class MsgTestEntity extends BasePayload {
    private Integer userId;

    private String userName;
}
