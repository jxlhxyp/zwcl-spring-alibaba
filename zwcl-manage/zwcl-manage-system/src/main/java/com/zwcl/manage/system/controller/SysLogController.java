package com.zwcl.manage.system.controller;


import com.zwcl.manage.system.api.entity.SysLog;
import com.zwcl.manage.system.service.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 操作日志记录 前端控制器
 * 用日志来试验分库分表
 * </p>
 *
 * @author xieyongping
 * @since 2020-11-25
 */
@RestController
@RequestMapping("/sysLog")
public class SysLogController {

    @Autowired
    private SysLogService sysLogService;

    @PostMapping("/save")
    public Boolean saveLog(@RequestBody SysLog sysLog){
        return sysLogService.save(sysLog);
    }
}

