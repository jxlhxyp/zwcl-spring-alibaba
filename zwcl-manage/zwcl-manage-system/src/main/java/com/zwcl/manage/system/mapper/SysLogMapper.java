package com.zwcl.manage.system.mapper;

import com.zwcl.manage.system.api.entity.SysLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 操作日志记录 Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2020-11-25
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}
