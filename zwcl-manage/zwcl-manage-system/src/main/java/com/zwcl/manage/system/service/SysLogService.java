package com.zwcl.manage.system.service;

import com.zwcl.manage.system.api.entity.SysLog;
import com.zwcl.common.mybatis.service.BaseService;

/**
 * <p>
 * 操作日志记录 服务类
 * </p>
 *
 * @author xieyongping
 * @since 2020-11-25
 */
public interface SysLogService extends BaseService<SysLog> {

}
