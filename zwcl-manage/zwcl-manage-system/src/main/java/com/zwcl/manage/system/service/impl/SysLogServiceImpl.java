package com.zwcl.manage.system.service.impl;

import com.zwcl.manage.system.api.entity.SysLog;
import com.zwcl.manage.system.mapper.SysLogMapper;
import com.zwcl.manage.system.service.SysLogService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作日志记录 服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2020-11-25
 */
@Service
public class SysLogServiceImpl extends BaseServiceImpl<SysLogMapper, SysLog> implements SysLogService {

}
