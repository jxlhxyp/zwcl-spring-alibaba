package com.zwcl.manage.system;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author xyp
 */
@SpringCloudApplication
//TODO:重要，一定要修改启动包的扫描路径，要不然无法启动
@MapperScan("com.zwcl.manage.system.mapper")
@EnableDiscoveryClient
public class ManageSystemApplication {
    public static void main(String[] args){
        SpringApplication.run(ManageSystemApplication.class, args);
        System.out.println("启动成功");
    }
}
