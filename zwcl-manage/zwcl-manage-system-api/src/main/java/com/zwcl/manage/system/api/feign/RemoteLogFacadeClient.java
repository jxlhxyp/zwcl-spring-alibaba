package com.zwcl.manage.system.api.feign;

import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.manage.system.api.entity.SysLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "zwcl-manage-system",path = "/manage/system")
public interface RemoteLogFacadeClient {
    //实体参数加上@RequestBody注解
    @PostMapping("/sysLog/save")
    ApiResult<Boolean> saveLog(@RequestBody SysLog sysLog);
}
