package com.zwcl.payment.gateway.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 请求数据 前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-13
 */
@RestController
@RequestMapping("/paymentRequest")
public class PaymentRequestController {

}

