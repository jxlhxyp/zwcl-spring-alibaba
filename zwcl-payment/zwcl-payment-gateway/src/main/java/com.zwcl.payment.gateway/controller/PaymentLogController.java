package com.zwcl.payment.gateway.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 支付流水日志表 前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-13
 */
@RestController
@RequestMapping("/paymentLog")
public class PaymentLogController {

}

