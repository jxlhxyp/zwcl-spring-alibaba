package com.zwcl.payment.gateway.controller;


import com.alibaba.fastjson.JSON;
import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.payment.gateway.dto.PayParam;
import com.zwcl.payment.gateway.enums.CallTypeEnum;
import com.zwcl.payment.gateway.enums.RequestTypeEnum;
import com.zwcl.payment.gateway.service.PaymentRequestService;
import com.zwcl.payment.gateway.service.impl.PaymentServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import sun.rmi.runtime.Log;

/**
 * <p>
 * 支付流水表 前端控制器
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-13
 */
@RestController
@RequestMapping("/unified")
@Slf4j
public class PaymentController {

    @Autowired
    private PaymentRequestService paymentRequestService;

    @Autowired
    private PaymentServiceImpl paymentService;

    /**
     * 发起支付统一入口
     * @return
     */
    @RequestMapping("/release/pay")
    public String unifiedPay(@RequestBody @Validated(PayParam.PayValid.class) PayParam param) {
        //异步时无法返回这个请求的id
        Long requestId= paymentRequestService.addRequest(CallTypeEnum.HTTP.getValue(), RequestTypeEnum.PAY.getValue(), JSON.toJSONString(param));
        //获取预支付信息
        String payInfo=paymentService.payRequest(param);
        return payInfo;
    }

}

