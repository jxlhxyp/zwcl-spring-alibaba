package com.zwcl.payment.gateway.dto;

import com.zwcl.common.core.annotation.EnumValid;
import com.zwcl.payment.gateway.enums.OrderTypeEnum;
import com.zwcl.payment.gateway.enums.PayWayEnum;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @Description 发起支付 请求接收实体
 * @Author xyp
 * @Date 2020/04/16 17:27
 */
@Data
public class PayParam {

    @NotNull(message = "用户ID不能为空", groups = {PayValid.class})
    private Integer userId;//用户ID

    @NotBlank(message = "订单号不能为空", groups = {PayValid.class, CancelValid.class})
    private String orderNo;//业务订单号

    @NotBlank(message = "openId不能为空", groups = {PayValid.class})
    private String openId;//openId

    @NotNull(message = "支付金额不能为空", groups = {PayValid.class})
    @DecimalMin(value = "0.01", message = "支付金额不能小于0.01元", groups = {PayValid.class})
    private BigDecimal payAmount;//支付金额，单位：元

    @NotNull(message = "支付渠道不能为空", groups = {PayValid.class})
    @EnumValid(message = "暂不支持该支付渠道", type = PayWayEnum.class, groups = {PayValid.class})
    private Integer payWay;//支付渠道

    @NotNull(message = "订单类型不能为空", groups = {PayValid.class, CancelValid.class})
    @EnumValid(message = "暂不支持该订单类型", type = OrderTypeEnum.class, groups = {PayValid.class, CancelValid.class})
    private Integer orderType;//订单类型

    @NotBlank(message = "支付商品描述不能为空", groups = {PayValid.class})
    private String productName;//支付商品描述

    /**
     * 当前默认选择1，以后可能有多个
     */
    private Integer mchType;//商户账号类型

    //private Boolean unlockTag = true;//解锁标记

    /**
     * 输入实体，用来检验定义的组别
     */
    public interface PayValid {}

    /**
     * 输入实体，用来检验定义的组别
     */
    public interface CancelValid {}

}
