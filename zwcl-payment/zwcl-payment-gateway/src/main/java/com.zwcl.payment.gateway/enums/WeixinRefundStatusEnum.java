package com.zwcl.payment.gateway.enums;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description: 微信退款状态
 * @Date: 2020/1/10 14:01
 * @author: xyp
 */
public enum WeixinRefundStatusEnum {

    SUCCESS("SUCCESS", "退款成功"),

    REFUNDCLOSE("REFUNDCLOSE", "退款关闭"),

    PROCESSING("PROCESSING", "退款处理中"),

    CHANGE("CHANGE", "退款异常");


    private String value;
    private String desc;
    private static final Map<String,String> map;

    static {
        map = Arrays.stream(WeixinRefundStatusEnum.values())
                .collect(Collectors.toMap(WeixinRefundStatusEnum::getValue, WeixinRefundStatusEnum::getDesc));
    }

    /**
     * @param value value值
     * @param desc  desc值
     */
    WeixinRefundStatusEnum(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    /**
     * 返回value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * 返回desc
     */
    public String getDesc() {
        return this.desc;
    }

    /**
     * 根据value获取desc
     * @param value value
     * @return desc
     */
    public static String getDesc(String value) {
        return getMap().get(value) == null ? "": getMap().get(value);
    }

    /**
     * 获取map
     */
    public static Map<String, String> getMap() {
        return map;
    }

}

