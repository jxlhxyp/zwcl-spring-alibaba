package com.zwcl.payment.gateway.enums;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 行为类型
 *
 * @author kunye
 * @date 2019/8/24
 */
public enum RequestTypeEnum {

    PAY(1, "支付"),

    WITHHOLD(2, "免密代扣"),

    RECHARGE(4, "充值"),

    TRANSFER(5, "转账"),

    REFUND(6, "退款"),

    PAY_QUERY(7, "支付查询"),

    REFUND_QUERY(8, "退款查询"),

    CANCEL(9, "取消订单"),

    ;

    private Integer value;
    private String desc;
    private static final Map<Integer,String> map;

    static {
        map = Arrays.stream(RequestTypeEnum.values())
                .collect(Collectors.toMap(RequestTypeEnum::getValue, RequestTypeEnum::getDesc));
    }

    /**
     * @param value value值
     * @param desc  desc值
     */
    RequestTypeEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    /**
     * 返回value
     */
    public Integer getValue() {
        return this.value;
    }

    /**
     * 返回desc
     */
    public String getDesc() {
        return this.desc;
    }

    /**
     * 根据value获取desc
     * @param value value
     * @return desc
     */
    public static String getDesc(Integer value) {
        return getMap().get(value) == null ? "": getMap().get(value);
    }

    /**
     * 获取map
     */
    public static Map<Integer, String> getMap() {
        return map;
    }

    /**
     * 是否存在
     * @param value
     * @return
     */
    public static Boolean isExist(Integer value){
        return map.containsKey(value);
    }
}
