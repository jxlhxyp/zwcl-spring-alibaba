package com.zwcl.payment.gateway.enums;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 调用方式
 * xyp
 */
public enum CallTypeEnum {

    HTTP(1, "接口调用"),

    MQ(2, "Mq调用"),

    ;

    private Integer value;
    private String desc;
    private static final Map<Integer,String> map;

    static {
        map = Arrays.stream(CallTypeEnum.values())
                .collect(Collectors.toMap(CallTypeEnum::getValue, CallTypeEnum::getDesc));
    }

    /**
     * @param value value值
     * @param desc  desc值
     */
    CallTypeEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    /**
     * 返回value
     */
    public Integer getValue() {
        return this.value;
    }

    /**
     * 返回desc
     */
    public String getDesc() {
        return this.desc;
    }

    /**
     * 根据value获取desc
     * @param value value
     * @return desc
     */
    public static String getDesc(Integer value) {
        return getMap().get(value) == null ? "": getMap().get(value);
    }

    /**
     * 获取map
     */
    public static Map<Integer, String> getMap() {
        return map;
    }

    /**
     * 是否存在
     * @param value
     * @return
     */
    public static Boolean isExist(Integer value){
        return map.containsKey(value);
    }
}
