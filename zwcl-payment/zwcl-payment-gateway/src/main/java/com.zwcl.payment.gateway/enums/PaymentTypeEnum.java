package com.zwcl.payment.gateway.enums;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 支付类型
 *
 * @author xyp
 * @date 2019/8/24
 */
public enum PaymentTypeEnum {

    ORDER(1, "下单支付"),

    AUTO(2, "免密支付"),

    ;

    private Integer value;
    private String desc;
    private static final Map<Integer,String> map;

    static {
        map = Arrays.stream(PaymentTypeEnum.values())
                .collect(Collectors.toMap(PaymentTypeEnum::getValue, PaymentTypeEnum::getDesc));
    }

    /**
     * @param value value值
     * @param desc  desc值
     */
    PaymentTypeEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    /**
     * 返回value
     */
    public Integer getValue() {
        return this.value;
    }

    /**
     * 返回desc
     */
    public String getDesc() {
        return this.desc;
    }

    /**
     * 根据value获取desc
     * @param value value
     * @return desc
     */
    public static String getDesc(Integer value) {
        return getMap().get(value) == null ? "": getMap().get(value);
    }

    /**
     * 获取map
     */
    public static Map<Integer, String> getMap() {
        return map;
    }

    /**
     * 是否存在
     * @param value
     * @return
     */
    public static Boolean isExist(Integer value){
        return map.containsKey(value);
    }

}
