package com.zwcl.payment.gateway.enums;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 支付渠道
 * 注意：渠道变动时，需要修改相应的渠道Bean别名
 *
 * @author kunye
 * @date 2020/2/17
 */
public enum PayWayEnum {

    WECHAT_MINA_NORMAL(1, "微信小程序（普通商户模式）"),

    WECHAT_MINA(2, "微信小程序（服务商模式）");

    private Integer value;
    private String desc;
    private static final Map<Integer,String> mapValueVsDesc;
    private static final Map<Integer,PayWayEnum> mapValueVsEnum;

    static {
        mapValueVsDesc = Arrays.stream(PayWayEnum.values())
                .collect(Collectors.toMap(PayWayEnum::getValue, PayWayEnum::getDesc));
        mapValueVsEnum = Arrays.stream(PayWayEnum.values())
                .collect(Collectors.toMap(PayWayEnum::getValue, Function.identity()));
    }

    /**
     * @param value value值
     * @param desc  desc值
     */
    PayWayEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    /**
     * 返回value
     */
    public Integer getValue() {
        return this.value;
    }

    /**
     * 返回desc
     */
    public String getDesc() {
        return this.desc;
    }

    /**
     * 根据value获取desc
     * @param value value
     * @return desc
     */
    public static String getDesc(Integer value) {
        return getMapValueVsDesc().get(value) == null ? "": getMapValueVsDesc().get(value);
    }

    /**
     * 根据value获取Enum
     * @param value value
     * @return Enum
     */
    public static PayWayEnum getEnum(Integer value) {
        return getMapValueVsDesc().get(value) == null ? null : getMapValueVsEnum().get(value);
    }

    /**
     * 获取mapValueVsDesc
     */
    public static Map<Integer, String> getMapValueVsDesc() {
        return mapValueVsDesc;
    }

    /**
     * 获取mapValueVsEnum
     */
    public static Map<Integer, PayWayEnum> getMapValueVsEnum() {
        return mapValueVsEnum;
    }

    /**
     * 是否存在
     * @param value
     * @return
     */
    public static Boolean isExist(Integer value){
        return mapValueVsDesc.containsKey(value);
    }

}
