package com.zwcl.payment.gateway.enums;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description: 退款状态
 * @Date: 2020/03/05 14:01
 * @author: xyp
 */
public enum RefundStatusEnum {

    UNREFUND(1, "待退款"),

    REFUNDING(2, "退款中"),

    SUCCESS(3, "退款成功"),

    FAIL(4, "退款失败");

    private Integer value;
    private String desc;
    private static final Map<Integer,String> map;

    static {
        map = Arrays.stream(RefundStatusEnum.values())
                .collect(Collectors.toMap(RefundStatusEnum::getValue, RefundStatusEnum::getDesc));
    }

    /**
     * @param value value值
     * @param desc  desc值
     */
    RefundStatusEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    /**
     * 返回value
     */
    public Integer getValue() {
        return this.value;
    }

    /**
     * 返回desc
     */
    public String getDesc() {
        return this.desc;
    }

    /**
     * 根据value获取desc
     * @param value value
     * @return desc
     */
    public static String getDesc(Integer value) {
        return getMap().get(value) == null ? "": getMap().get(value);
    }

    /**
     * 获取map
     */
    public static Map<Integer, String> getMap() {
        return map;
    }

}

