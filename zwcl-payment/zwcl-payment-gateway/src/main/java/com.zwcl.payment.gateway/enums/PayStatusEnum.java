package com.zwcl.payment.gateway.enums;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description: 支付状态
 * @Date: 2020/1/10 14:01
 * @author: xyp
 */
public enum PayStatusEnum {

    UNPAID(1, "待支付"),  //创建订单时

    PAID(2, "已支付"),    //支付成功

    FAIL(3, "支付失败"),  //支付失败

    PAYING(4,"支付中"),   //支付后调到微信去，但是可能没有收到回调

    CANCEL(5, "已取消");  //用户取消支付

    private Integer value;
    private String desc;
    private static final Map<Integer,String> map;

    static {
        map = Arrays.stream(PayStatusEnum.values())
                .collect(Collectors.toMap(PayStatusEnum::getValue, PayStatusEnum::getDesc));
    }

    /**
     * @param value value值
     * @param desc  desc值
     */
    PayStatusEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    /**
     * 返回value
     */
    public Integer getValue() {
        return this.value;
    }

    /**
     * 返回desc
     */
    public String getDesc() {
        return this.desc;
    }

    /**
     * 根据value获取desc
     * @param value value
     * @return desc
     */
    public static String getDesc(Integer value) {
        return getMap().get(value) == null ? "": getMap().get(value);
    }

    /**
     * 获取map
     */
    public static Map<Integer, String> getMap() {
        return map;
    }

}

