package com.zwcl.payment.gateway.enums;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 处理状态
 *
 * @author kunye
 * @date 2019/8/24
 */
public enum HandleStatusEnum {

    UNDEAL(0, "未处理"),

    DEALED(1, "已处理");

    private Integer value;
    private String desc;
    private static final Map<Integer,String> map;

    static {
        map = Arrays.stream(HandleStatusEnum.values())
                .collect(Collectors.toMap(HandleStatusEnum::getValue, HandleStatusEnum::getDesc));
    }

    /**
     * @param value value值
     * @param desc  desc值
     */
    HandleStatusEnum(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    /**
     * 返回value
     */
    public Integer getValue() {
        return this.value;
    }

    /**
     * 返回desc
     */
    public String getDesc() {
        return this.desc;
    }

    /**
     * 根据value获取desc
     * @param value value
     * @return desc
     */
    public static String getDesc(Integer value) {
        return getMap().get(value) == null ? "": getMap().get(value);
    }

    /**
     * 获取map
     */
    public static Map<Integer, String> getMap() {
        return map;
    }

}
