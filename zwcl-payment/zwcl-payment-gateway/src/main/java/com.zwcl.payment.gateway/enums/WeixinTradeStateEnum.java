package com.zwcl.payment.gateway.enums;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description: 微信交易状态
 * @Date: 2020/1/10 14:01
 * @author: xyp
 */
public enum WeixinTradeStateEnum {

    SUCCESS("SUCCESS", "支付成功"),

    REFUND("REFUND", "转入退款"),

    NOTPAY("NOTPAY", "未支付"),

    CLOSED("CLOSED", "已关闭"),

    ACCEPT("ACCEPT", "已接收，等待扣款"),

    REVOKED("REVOKED", "已撤销（刷卡支付）"),

    USERPAYING("USERPAYING", "用户支付中"),

    PAYBACK("PAY_BACK", "用户还款"),

    PAYERROR("PAYERROR", "支付失败（其他原因）"),

    PAYFAIL("PAY_FAIL", "支付失败（其他原因）");

    private String value;
    private String desc;
    private static final Map<String,String> map;

    static {
        map = Arrays.stream(WeixinTradeStateEnum.values())
                .collect(Collectors.toMap(WeixinTradeStateEnum::getValue, WeixinTradeStateEnum::getDesc));
    }

    /**
     * @param value value值
     * @param desc  desc值
     */
    WeixinTradeStateEnum(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    /**
     * 返回value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * 返回desc
     */
    public String getDesc() {
        return this.desc;
    }

    /**
     * 根据value获取desc
     * @param value value
     * @return desc
     */
    public static String getDesc(String value) {
        return getMap().get(value) == null ? "": getMap().get(value);
    }

    /**
     * 获取map
     */
    public static Map<String, String> getMap() {
        return map;
    }

}

