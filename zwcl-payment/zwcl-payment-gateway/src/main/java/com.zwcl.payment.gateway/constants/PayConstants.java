package com.zwcl.payment.gateway.constants;

import java.time.format.DateTimeFormatter;

/**
 * @Description 公共常量
 * @Author tanljs
 * @Date 2020/1/9
 * @Email Alex.tan@wetax.com.cn
 */
public class PayConstants {

    public static final String ZWCL_PAYMENT_LOCK_PREFIX="Zwcl_Payment_Lock_";

    /*重试*/
    public static final String CACHE_KEY_RETRY = "Zwcl:Payment:Retry";

    public static final Integer ZWCL_PAY_EXPIRE_TIME = 5*60*1000;

    /*微信商户号*/
    public static final String CACHE_KEY_PAYMENT_WEIXIN_ACCOUNT = "Zwcl:Payment:Weixin:Account";

    /*微信小程序支付回调url（服务商）*/
    public static final String WEIXIN_MINA_PAY_CALLBACK_URL = "/payment/release/callback/weixin/mina/pay";
    /*微信小程序支付回调url（普通商户）*/
    public static final String WEIXIN_MINA_NORMAL_PAY_CALLBACK_URL = "/payment/release/callback/weixin/mina/normal/pay";
    /*微信小程序退款回调url（服务商）*/
    public static final String WEIXIN_MINA_REFUND_CALLBACK_URL = "/payment/release/callback/weixin/mina/refund";
    /*微信小程序退款回调url（普通商户）*/
    public static final String WEIXIN_MINA_NORMAL_REFUND_CALLBACK_URL = "/payment/release/callback/weixin/mina/normal/refund";

    /*测试环境支付金额*/
    public static final double TEST_PAY_FEE = 0.01;
    /*
     *渠道发起扣费/退款延时等级，0表示不延时（默认），1对应1s....以此类推
     * 1s 5s 10s 30s 1m 2m 3m 4m 5m 6m 7m 8m 9m 10m 20m 30m 1h 2h
     * */
    public static final Integer PAYWAY_DELAYLEVEL = 4;

    public static final DateTimeFormatter YYYYMMDD = DateTimeFormatter.ofPattern("yyyyMMdd");
    public static final DateTimeFormatter HHMMSS = DateTimeFormatter.ofPattern("HHmmss");
    public static final DateTimeFormatter YYYYMMDDHH = DateTimeFormatter.ofPattern("yyyyMMddHH");
    public static final DateTimeFormatter YYYYMMDDHHMMSS = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    public static final DateTimeFormatter YYYYMMDDHHMMSS2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static final DateTimeFormatter YYYYMMDDHHMMSSSSS = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
    public static final DateTimeFormatter YYYYMMDDTHHMMSSZONE = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss+08:00");


}
