package com.zwcl.payment.gateway.constants;

/**
 * @Description MQ常量
 * @Author tanljs
 * @Date 2020/1/9
 * @Email Alex.tan@wetax.com.cn
 */
public class PayMqConstants {
    //=====================topic========================
    /** 支付TOPIC */
    public static final String ZWCL_PAY_TOPIC = "zwcl_pay_topic";

    //======================tag=========================
    /** 微信小程序回调通知（服务商） */
    public static final String ZWCL_TAG_WEIXIN_MINA_CALLBACK = "zwcl_tag_weixin_mina_callback";
    /** 微信小程序退款回调通知（服务商）  */
    public static final String ZWCL_TAG_WEIXIN_MINA_REFUND_CALLBACK = "zwcl_tag_weixin_mina_refund_callback";
    /** 微信小程序回调通知（普通商户）  */
    public static final String ZWCL_TAG_WEIXIN_MINA_NORMAL_CALLBACK = "zwcl_tag_weixin_mina_normal_callback";
    /** 微信小程序退款回调通知（普通商户） */
    public static final String ZWCL_TAG_WEIXIN_MINA_NORMAL_REFUND_CALLBACK = "zwcl_tag_weixin_mina_normal_refund_callback";

    /** 发起支付*/
    public static final String ZWCL_TAG_ORDER_AUTO_PAY = "zwcl_tag_order_auto_pay";
    /** 发起退款*/
    public static final String ZWCL_TAG_ORDER_AUTO_REFUND = "zwcl_tag_order_auto_refund";

    /** 发起支付主动查询*/
    public static final String ZWCL_TAG_ORDER_MANUAL_PAY_CHECK = "zwcl_tag_order_manual_pay_check";
    /** 发起退款主动查询*/
    public static final String ZWCL_TAG_ORDER_MANUAL_REFUND_CHECK = "zwcl_tag_order_manual_refund_check";

    /** 取消待支付订单 */
    public static final String ZWCL_TAG_CANCEL_UNPAID_ORDER = "zwcl_tag_cancel_unpaid_order";

    /** 统一支付回调TAG */
    public static final String ZWCL_TAG_ORDER_PAY_CALL_BACK = "zwcl_tag_order_pay_callback_";
    /** 统一退款回调TAG */
    public static final String ZWCL_TAG_ORDER_REFUND_CALL_BACK = "zwcl_tag_order_refund_callback_";

    /** 同步支付记录到MongoDB*/
    public static final String ZWCL_PAY_TAG_SYNC_PAYMENT_MONGO = "zwcl_pay_tag_sync_payment_mongo";

    //======================group=========================
    /** 发起支付主动查询*/
    public static final String ZWCL_GROUP_ORDER_MANUAL_PAY_CHECK = "zwcl_group_order_manual_pay_check";
    /** 发起退款主动查询*/
    public static final String ZWCL_GROUP_ORDER_MANUAL_REFUND_CHECK = "zwcl_group_order_manual_refund_check";
    /** 取消待支付订单 */
    public static final String ZWCL_GROUP_CANCEL_UNPAID_ORDER = "zwcl_group_cancel_unpaid_order";

    /** 发起扣费 */
    public static final String ZWCL_PAY_GROUP_ADDPAY = "zwcl_pay_group_addpay";
    /** 发起退款 */
    public static final String ZWCL_PAY_GROUP_ADDREFUND = "zwcl_pay_group_addRefund";

    /** 同步支付记录到MongoDB*/
    public static final String ZWCL_PAY_GROUP_SYNC_PAYMENT_MONGO = "zwcl_pay_group_sync_payment_mongo";

    /** 微信小程序回调通知（普通商户）  */
    public static final String ZWCL_GROUP_WEIXIN_MINA_NORMAL_CALLBACK = "zwcl_group_weixin_mina_normal_callback";
    /** 微信小程序退款回调通知（普通商户） */
    public static final String ZWCL_GROUP_WEIXIN_MINA_NORMAL_REFUND_CALLBACK = "zwcl_group_weixin_mina_normal_refund_callback";


}
