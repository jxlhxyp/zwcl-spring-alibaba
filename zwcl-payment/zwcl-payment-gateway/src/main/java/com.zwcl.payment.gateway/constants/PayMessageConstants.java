package com.zwcl.payment.gateway.constants;

/**
 * @Description 信息提示常量类
 * @Author Jason
 * @Date 2019/7/23 15:30
 * @Email jason@wetax.com.cn
 */
public class PayMessageConstants {

    public static final String CHECK_SIGNATURE_FAILURE = "校验签名失败";

    public static final String CHECK_FEE_FAILURE = "校验金额失败";

    public static final String ILLEGAL_OPERATION = "不允许进行该操作，当前交易状态为：";

    public static final String PAYMENT_NOT_EXIST = "支付订单不存在";

    public static final String REFUND_EXCEED_THE_LIMIT = "超过退款上限，无法退款";

    public static final String REFUND_LESS_THAN_ZERO = "金额少于0元，无法退款";
}
