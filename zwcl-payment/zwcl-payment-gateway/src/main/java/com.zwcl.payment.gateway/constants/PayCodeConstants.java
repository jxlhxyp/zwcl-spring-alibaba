package com.zwcl.payment.gateway.constants;

/**
 * @Description 状态码constant
 * @Author kunye
 * @Date 2020/01/16
 * @Email kunye.guo@wetax.com.cn
 */
public class PayCodeConstants {

    /*正常*/
    public static final int OK = 0;

    /*错误*/
    public static final int ERROR = -1;

    /*微信查询不到订单*/
    public static final int WEIXIN_NOT_FOUND_ORDER = -1003;

    /*订单已支付*/
    public static final int WEIXIN_ORDER_PAID = -1004;

    /*支付流水不存在*/
    public static final int PAYMENT_NOT_EXIST = -1005;

}
