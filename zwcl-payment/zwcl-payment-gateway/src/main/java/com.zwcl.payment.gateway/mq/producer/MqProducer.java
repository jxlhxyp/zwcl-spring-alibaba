package com.zwcl.payment.gateway.mq.producer;

import com.zwcl.common.mq.BasePayload;
import com.zwcl.common.mq.RocketMQProductUtils;
import com.zwcl.payment.gateway.constants.PayMqConstants;
import com.zwcl.payment.gateway.vo.PaymentRespVO;
import com.zwcl.payment.gateway.vo.RefundRespVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@Slf4j
public class MqProducer {

    @Autowired
    private RocketMQProductUtils rocketMQProductUtils;

    /**
     * 给业务方发送消息
     * @param paymentRespVO
     * @return
     */
    public Boolean bizMqPaymentCallback(PaymentRespVO paymentRespVO){
        try {
            BasePayload basePayload=new BasePayload<PaymentRespVO>(paymentRespVO);
            Boolean flag = rocketMQProductUtils.syncSend(PayMqConstants.ZWCL_PAY_TOPIC,null,PayMqConstants.ZWCL_TAG_ORDER_PAY_CALL_BACK,basePayload);
            if(!flag)
                log.error("发送支付回调失败");
            return flag;
        }catch (Exception ex){
            log.error("发送支付回调失败，{}",ex);
            return false;
        }
    }

    /**
     * 给业务方发送消息
     * @param orderType
     * @param refundRespVO
     * @return
     */
    public Boolean bizMqRefundCallback(Integer orderType, RefundRespVO refundRespVO){
        try {
            BasePayload basePayload=new BasePayload<RefundRespVO>(refundRespVO);
            Boolean flag = rocketMQProductUtils.syncSend(PayMqConstants.ZWCL_PAY_TOPIC,null,PayMqConstants.ZWCL_TAG_ORDER_REFUND_CALL_BACK+orderType,basePayload);
            if(!flag)
                log.error("发送退款回调失败");
            return flag;
        }catch (Exception ex){
            log.error("发送退款回调失败，{}",ex);
            return false;
        }
    }

    /**
     * 自身发送，自身消费
     * @param params
     * @return
     */
    public Boolean wxNormalPayCallBack(Map<String, String> params){
        try {
            BasePayload basePayload=new BasePayload<Map<String, String>>(params);
            Boolean flag =rocketMQProductUtils.syncSend(PayMqConstants.ZWCL_PAY_TOPIC,null,PayMqConstants.ZWCL_TAG_WEIXIN_MINA_NORMAL_CALLBACK,basePayload);
            if(!flag)
                log.error("（普通商户）微信支付回调通知自身失败");
            return flag;
        }catch (Exception ex){
            log.error("（普通商户）微信支付回调通知自身失败，{}",ex);
            return false;
        }
    }

    /**
     * 自身发送，自身消费
     * @param params
     * @return
     */
    public Boolean wxNormalRefundCallBack(Map<String, String> params){
        try {
            BasePayload basePayload=new BasePayload<Map<String, String>>(params);
            Boolean flag =rocketMQProductUtils.syncSend(PayMqConstants.ZWCL_PAY_TOPIC,null,PayMqConstants.ZWCL_TAG_WEIXIN_MINA_NORMAL_REFUND_CALLBACK,basePayload);
            if(!flag)
                log.error("（普通商户）微信退款回调通知自身失败");
            return flag;
        }catch (Exception ex){
            log.error("（普通商户）微信退款回调通知自身失败，{}",ex);
            return false;
        }
    }

    /**
     * 自身发送，自身消费
     * @param params
     * @return
     */
    public Boolean wxPayCallBack(Map<String, String> params){
        try {
            BasePayload basePayload=new BasePayload<Map<String, String>>(params);
            Boolean flag = rocketMQProductUtils.syncSend(PayMqConstants.ZWCL_PAY_TOPIC,null,PayMqConstants.ZWCL_TAG_WEIXIN_MINA_CALLBACK,basePayload);
            if(!flag)
                log.error("（商户）微信支付回调通知自身失败");
            return flag;
        }catch (Exception ex){
            log.error("（商户）微信支付回调通知自身失败，{}",ex);
            return false;
        }
    }

    /**
     * 自身发送，自身消费
     * @param params
     * @return
     */
    public Boolean wxRefundCallBack(Map<String, String> params){
        try {
            BasePayload basePayload=new BasePayload<Map<String, String>>(params);
            Boolean flag = rocketMQProductUtils.syncSend(PayMqConstants.ZWCL_PAY_TOPIC,null,PayMqConstants.ZWCL_TAG_WEIXIN_MINA_REFUND_CALLBACK,basePayload);
            if(!flag)
                log.error("（商户）微信退款回调通知自身失败");
            return flag;
        }catch (Exception ex){
            log.error("（商户）微信退款回调通知自身失败，{}",ex);
            return false;
        }
    }

    public Boolean cancelOrder(String message,int delayLevel){
        try {
            BasePayload basePayload=new BasePayload<String>(message);
            Boolean flag = rocketMQProductUtils.syncSend(PayMqConstants.ZWCL_PAY_TOPIC,null,PayMqConstants.ZWCL_TAG_CANCEL_UNPAID_ORDER,basePayload,delayLevel);
            if(!flag)
                log.error("超时取消订单通知自身失败");
            return flag;
        }catch (Exception ex){
            log.error("超时取消订单通知自身失败，{}",ex);
            return false;
        }
    }


}
