package com.zwcl.payment.gateway.mq.cosumer;

import com.alibaba.fastjson.JSON;
import com.zwcl.common.mq.RocketMQListenerAwareNew;
import com.zwcl.payment.gateway.channel.WeixinMinaNormalService;
import com.zwcl.payment.gateway.constants.PayMqConstants;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description 微信小程序退款回调（普通商户）
 * @Author xyp
 * @Date 2020/03/18
 */
@Slf4j
@Component
@RocketMQMessageListener(topic = PayMqConstants.ZWCL_PAY_TOPIC,
        selectorExpression = PayMqConstants.ZWCL_TAG_WEIXIN_MINA_NORMAL_REFUND_CALLBACK,
        consumerGroup = PayMqConstants.ZWCL_GROUP_WEIXIN_MINA_NORMAL_REFUND_CALLBACK,
        consumeThreadMax = 10)
public class WeixinMinaNormalRefundCallbackConsumer extends RocketMQListenerAwareNew<Map<String,String>> {
    @Autowired
    private WeixinMinaNormalService weixinMinaNormalService;
    //@Autowired
    //private WarnService warnService;

    @Override
    public Boolean consumerMessage(Map<String, String> message) {
        try {
            weixinMinaNormalService.refundCallback(message);
        } catch (Exception e) {
            log.error("微信小程序退款异常，参数：{}", message, e);
            //warnService.warnFromException(e, "微信小程序退款异常，参数："+ message);
            return false;
        }
        return true;
    }
}
