package com.zwcl.payment.gateway.mq.cosumer;

import com.zwcl.common.mq.RocketMQListenerAwareNew;
import com.zwcl.payment.gateway.constants.PayMqConstants;
import com.zwcl.payment.gateway.service.impl.PaymentServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description 超时取消订单消费
 * @Author xyp
 * @Date 2020/9/20
 */
@Slf4j
@Component
@RocketMQMessageListener(topic = PayMqConstants.ZWCL_PAY_TOPIC,
        selectorExpression = PayMqConstants.ZWCL_TAG_CANCEL_UNPAID_ORDER,
        consumerGroup = PayMqConstants.ZWCL_GROUP_CANCEL_UNPAID_ORDER,
        consumeThreadMax = 5)
public class CancelOrderConsumer extends RocketMQListenerAwareNew<String> {

    @Autowired
    private PaymentServiceImpl paymentService;
    //@Autowired
    //private WarnService warnService;

    @Override
    public Boolean consumerMessage(String message) {
        try {
            String paymentNo =message;
            paymentService.cancelOrder(paymentNo);
        } catch (Exception e) {
            log.error("取消订单异常", e);
            //warnService.warnFromException(e, "取消订单异常，参数："+ message);
            return false;
        }
        return true;
    }

}
