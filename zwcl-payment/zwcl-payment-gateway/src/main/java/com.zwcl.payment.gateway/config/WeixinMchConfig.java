package com.zwcl.payment.gateway.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

/**
 * @Description 微信商户号配置
 * @Author xyp
 * @Date 2020/02/20
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "weixin")
public class WeixinMchConfig {

    private Map<String, String> certificate;
    private String payUrl;
    private String refundUrl;
    private String orderQueryUrl;
    private String refundQueryUrl;
    private String closeUrl;
    private String apiV3Key;

    /**
     * 获取商户证书
     * @param mchId 商户ID
     * @return
     */
    public String getCert(String mchId){
        return certificate.get(mchId) == null ? "" : certificate.get(mchId);
    }

    /**
     * 是否有商户证书
     * @param mchId
     * @return
     */
    public Boolean isSupport(String mchId){
        return certificate.containsKey(mchId);
    }
}
