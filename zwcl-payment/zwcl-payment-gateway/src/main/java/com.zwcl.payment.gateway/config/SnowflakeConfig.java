package com.zwcl.payment.gateway.config;

import cn.hutool.core.lang.Snowflake;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description
 * @Author tanljs
 * @Date 2020/1/13
 * @Email Alex.tan@wetax.com.cn
 */
@Configuration
public class SnowflakeConfig {

    @Bean
    public Snowflake snowflake() {
        return new Snowflake(10, 0);
    }
}
