package com.zwcl.payment.gateway.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 退款流水表
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)      //支持链式语法
@TableName("payment_refund")
public class PaymentRefund extends BaseEntity<PaymentRefund> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
      private Long id;

    /**
     * 退款流水号
     */
    private String refundNo;

    /**
     * 支付流水号
     */
    private String paymentNo;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 订单类型
     */
    private Integer orderType;

    /**
     * 原支付金额
     */
    private BigDecimal payAmount;

    /**
     * 需退款金额
     */
    private BigDecimal needRefundAmount;

    /**
     * 退款原因
     */
    private String reason;

    /**
     * 状态，1-待退款，2-退款中，3-已退款，4-退款失败
     */
    private Integer refundStatus;

    /**
     * 是否手动退款，1-是，2-否
     */
    private Integer manual;

    /**
     * 支付渠道
     */
    private Integer payWay;

    /**
     * 退款完成时间
     */
    private LocalDateTime refundTime;

    /**
     * 回调时间
     */
    private LocalDateTime callbackTime;

    /**
     * 实际退款金额（元）
     */
    private BigDecimal refundAmount;

    /**
     * 第三方退款单号
     */
    private String outRefundNo;

    /**
     * 第三方支付单号
     */
    private String outTradeNo;

    /**
     * 商户类型
     */
    private Integer mchType;

    /**
     * 支付类型
     */
    private Integer paymentType;

    /**
     * 退款入账账户
     */
    private String refundRecvAccout;

    /**
     * 失败码
     */
    private String errCode;

    /**
     * 失败信息
     */
    private String errMsg;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
