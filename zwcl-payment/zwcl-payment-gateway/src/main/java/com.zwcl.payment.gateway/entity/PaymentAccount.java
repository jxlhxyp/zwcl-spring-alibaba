package com.zwcl.payment.gateway.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 微信商户账号
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("payment_account")
public class PaymentAccount extends BaseEntity<PaymentAccount> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 业务
     */
    private String bussinessName;

    /**
     * 账号名称
     */
    private String accoutName;

    /**
     * 商户账号类型
     */
    private Integer mchType;

    private String appId;

    private String subAppId;

    /**
     * 微信支付商户号 
     */
    private String mchId;

    /**
     * 微信支付子商户号 
     */
    private String subMchId;

    /**
     * 对应的mch_id商户号的api密钥
     */
    private String secretKey;

    /**
     * 对应的sub_mch_id商户号的api密钥
     */
    private String subSecretKey;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
