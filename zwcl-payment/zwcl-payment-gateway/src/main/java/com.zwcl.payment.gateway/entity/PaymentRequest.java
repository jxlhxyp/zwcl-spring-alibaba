package com.zwcl.payment.gateway.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 请求数据
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)      //支持链式语法
@TableName("payment_request")
public class PaymentRequest extends BaseEntity<PaymentRequest> {

    private static final long serialVersionUID = 1L;

    //@TableId(type= IdType.INPUT)
    private Long id;

    /**
     * 状态，0-未处理，1-已处理
     */
    private Integer requestStatus;

    /**
     * 调用方式
     */
    private Integer callType;


    /**
     * 请求类型,1-http 2-Mq
     */
    private Integer requestType;

    /**
     * 请求参数
     */
    private String content;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
