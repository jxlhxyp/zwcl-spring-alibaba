package com.zwcl.payment.gateway.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 支付流水表
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)      //支持链式语法
@TableName("payment_main")
public class PaymentMain extends BaseEntity<PaymentMain> {

    private static final long serialVersionUID = 1L;

      private Long id;

    /**
     * 支付流水号
     */
    private String paymentNo;

    /**
     * 下单人uid
     */
    private Integer userId;

    /**
     * openId
     */
    private String openId;

    /**
     * 业务订单号
     */
    private String orderNo;

    /**
     * 订单类型
     */
    private Integer orderType;

    /**
     * 微信支付订单号
     */
    private String transactionId;

    /**
     * 状态，1-待支付，2-已支付，3-支付失败，4-已取消
     */
    private Integer payStatus;

    /**
     * 状态，0-无退款，1-有退款
     */
    private Integer refundStatus;

    /**
     * 订单原金额（元）
     */
    private BigDecimal originAmount;

    /**
     * 实际支付金额（元）
     */
    private BigDecimal payAmount;

    /**
     * 优惠金额
     */
    private BigDecimal discountAmount;

    /**
     * 手续费
     */
    private BigDecimal serviceAmount;

    /**
     * 退款金额
     */
    private BigDecimal refundAmount;

    /**
     * 支付类型，1-手动支付，2-免密支付
     */
    private Integer paymentType;

    /**
     * 发起支付时间
     */
    private LocalDateTime topayTime;

    /**
     * 支付完成时间
     */
    private LocalDateTime payTime;

    /**
     * 回调时间
     */
    private LocalDateTime callbackTime;

    /**
     * 支付渠道，1-微信支付  2-支付宝支付
     */
    private Integer payWay;

    /**
     * 商户类型
     */
    private Integer mchType;

    /**
     * 支付产品id
     */
    private String productId;

    /**
     * 支付产品名称
     */
    private String productName;

    /**
     * 银行类型，微信回调时的字段
     */
    private String bankType;

    /**
     * 失败码
     */
    private String errCode;

    /**
     * 失败信息
     */
    private String errMsg;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
