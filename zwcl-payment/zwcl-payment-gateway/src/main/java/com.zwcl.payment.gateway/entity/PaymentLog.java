package com.zwcl.payment.gateway.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 支付流水日志表
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)      //支持链式语法
@TableName("payment_log")
public class PaymentLog extends BaseEntity<PaymentLog> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
      private Long id;

    /**
     * 支付流水号
     */
    private String paymentNo;

    /**
     * 状态，1-待支付，2-已支付，3-支付失败，4-已取消
     */
    private Integer payStatus;

    /**
     * 操作信息
     */
    private String operationInfo;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
