package com.zwcl.payment.gateway.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 退款流水日志表
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)      //支持链式语法
@TableName("payment_refund_log")
public class PaymentRefundLog extends BaseEntity<PaymentRefundLog> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
      private Long id;

    /**
     * 退款流水号
     */
    private String refundNo;

    /**
     * 状态，1-待退款，2-退款中，3-已退款，4-退款失败
     */
    private Integer refundStatus;

    /**
     * 操作信息
     */
    private String operationInfo;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
