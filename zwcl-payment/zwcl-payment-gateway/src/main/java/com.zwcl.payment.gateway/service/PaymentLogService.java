package com.zwcl.payment.gateway.service;

import com.zwcl.payment.gateway.entity.PaymentLog;
import com.zwcl.common.mybatis.service.BaseService;

/**
 * <p>
 * 支付流水日志表 服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-13
 */
public interface PaymentLogService extends BaseService<PaymentLog> {

    Boolean addLog(PaymentLog paymentLog);
}
