package com.zwcl.payment.gateway.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.zwcl.payment.gateway.constants.PayCodeConstants;
import com.zwcl.payment.gateway.entity.PaymentRefund;
import com.zwcl.payment.gateway.enums.RefundStatusEnum;
import com.zwcl.payment.gateway.mapper.PaymentRefundMapper;
import com.zwcl.payment.gateway.mq.producer.MqProducer;
import com.zwcl.payment.gateway.service.PaymentRefundLogService;
import com.zwcl.payment.gateway.service.PaymentRefundService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import com.zwcl.payment.gateway.vo.RefundRespVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * <p>
 * 退款流水表 服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-13
 */
@Service
@Slf4j
public class PaymentRefundServiceImpl extends BaseServiceImpl<PaymentRefundMapper, PaymentRefund> implements PaymentRefundService {

    @Autowired
    private PaymentRefundLogService paymentRefundLogService;

    @Autowired
    private MqProducer mqProducer;

    /**
     * 发起渠道阶段错误处理
     * @param paymentRefund
     * @param msg
     */
    @Override
    public void replyRefundErr(PaymentRefund paymentRefund, String msg){
        this.replyRefundErr(paymentRefund, msg, String.valueOf(PayCodeConstants.ERROR));
    }

    /**
     * 发起渠道阶段错误处理
     * @param paymentRefund
     * @param msg
     * @param code
     */
    public void replyRefundErr(PaymentRefund paymentRefund, String msg, String code){
        paymentRefund.setErrCode(code);
        paymentRefund.setErrMsg(msg);
        paymentRefund.setCallbackTime(LocalDateTime.now());
        paymentRefund.setRefundStatus(RefundStatusEnum.FAIL.getValue());
        //退款订单状态为失败
        this.update(
                new PaymentRefund().setRefundStatus(paymentRefund.getRefundStatus())
                        .setCallbackTime(paymentRefund.getCallbackTime())
                        .setErrCode(paymentRefund.getErrCode())
                        .setErrMsg(paymentRefund.getErrMsg()),
                new UpdateWrapper<PaymentRefund>().lambda().eq(PaymentRefund::getRefundNo, paymentRefund.getRefundNo())
        );

        //添加日志
        paymentRefundLogService.addLog(paymentRefund.getRefundNo(), paymentRefund.getRefundStatus(), JSON.toJSONString(paymentRefund));

        //mq回调
        RefundRespVO vo = new RefundRespVO();
        BeanUtils.copyProperties(paymentRefund,vo);

        mqProducer.bizMqRefundCallback(paymentRefund.getOrderType(),vo);

        log.info("支付订单【{}】退款失败，refundNo={}，原因：{}", paymentRefund.getPaymentNo(), paymentRefund.getRefundNo(), msg);
    }

    /**
     * 获取退款流水
     * @param refundNo 退款流水号
     * @return
     */
    @Override
    public PaymentRefund getByNo(String refundNo){
        if(StringUtils.isBlank(refundNo)){
            return null;
        }
        return this.getOne(new QueryWrapper<PaymentRefund>().lambda().eq(PaymentRefund::getRefundNo, refundNo));
    }
}
