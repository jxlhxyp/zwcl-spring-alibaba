package com.zwcl.payment.gateway.service.impl;

import com.zwcl.payment.gateway.entity.PaymentLog;
import com.zwcl.payment.gateway.mapper.PaymentLogMapper;
import com.zwcl.payment.gateway.service.PaymentLogService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 支付流水日志表 服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-13
 */
@Service
public class PaymentLogServiceImpl extends BaseServiceImpl<PaymentLogMapper, PaymentLog> implements PaymentLogService {

    @Override
    public Boolean addLog(PaymentLog paymentLog) {
        return this.save(paymentLog);
    }
}
