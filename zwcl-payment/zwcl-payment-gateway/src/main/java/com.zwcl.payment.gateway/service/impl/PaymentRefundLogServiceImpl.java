package com.zwcl.payment.gateway.service.impl;

import com.zwcl.payment.gateway.entity.PaymentRefundLog;
import com.zwcl.payment.gateway.mapper.PaymentRefundLogMapper;
import com.zwcl.payment.gateway.service.PaymentRefundLogService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 退款流水日志表 服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-15
 */
@Service
public class PaymentRefundLogServiceImpl extends BaseServiceImpl<PaymentRefundLogMapper, PaymentRefundLog> implements PaymentRefundLogService {

    @Autowired
    private PaymentRefundLogMapper paymentRefundLogMapper;

    public void addLog(String refundNo, Integer status, String content){
        this.save(new PaymentRefundLog()
                .setRefundNo(refundNo)
                .setRefundStatus(status)
                .setOperationInfo(content));
    }

    public void addBatchLog(List<PaymentRefundLog> list){
        paymentRefundLogMapper.addBatch(list);
    }
}
