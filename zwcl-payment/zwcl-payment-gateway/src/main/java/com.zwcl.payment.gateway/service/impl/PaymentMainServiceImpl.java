package com.zwcl.payment.gateway.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zwcl.common.core.utils.JsonUtils;
import com.zwcl.common.core.utils.SequenceUtil;
import com.zwcl.payment.gateway.dto.PayParam;
import com.zwcl.payment.gateway.entity.PaymentLog;
import com.zwcl.payment.gateway.entity.PaymentMain;
import com.zwcl.payment.gateway.enums.PayStatusEnum;
import com.zwcl.payment.gateway.enums.PaymentTypeEnum;
import com.zwcl.payment.gateway.mapper.PaymentMainMapper;
import com.zwcl.payment.gateway.service.PaymentLogService;
import com.zwcl.payment.gateway.service.PaymentMainService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 支付流水表 服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-13
 */
@Service
public class PaymentMainServiceImpl extends BaseServiceImpl<PaymentMainMapper, PaymentMain> implements PaymentMainService {

    @Value("${spring.profiles.active}")
    private String env;

    @Autowired
    private PaymentLogService paymentLogService;

    @Override
    public List<PaymentMain> getPayNoByOrderNo(String orderNo, Integer orderType) {
        List<PaymentMain> paymentMains = this.list(new LambdaQueryWrapper<PaymentMain>().eq(PaymentMain::getOrderNo,orderNo).eq(PaymentMain::getOrderType,orderType).orderByAsc(PaymentMain::getId));
        return paymentMains;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PaymentMain savePaymentAndLog(PayParam param) {
        //保存流水
        PaymentMain payment = new PaymentMain();
        payment.setPaymentNo(SequenceUtil.generateNo(param.getOrderType()));
        payment.setUserId(param.getUserId());
        payment.setOrderNo(param.getOrderNo());
        payment.setProductName(StringUtils.equalsAny(env, "dev", "test") ? "ETC测试" : param.getProductName());
        payment.setPayStatus(PayStatusEnum.UNPAID.getValue());
        //支付金额
        payment.setPayAmount(param.getPayAmount());
        payment.setPaymentType(PaymentTypeEnum.ORDER.getValue());
        payment.setOrderType(param.getOrderType());
        payment.setOpenId(param.getOpenId());
        payment.setPayWay(param.getPayWay());
        payment.setMchType(param.getMchType());
        this.save(payment);

        //添加日志
        PaymentLog log=new PaymentLog();
        log.setPaymentNo(payment.getPaymentNo());
        log.setPayStatus(payment.getPayStatus());
        log.setOperationInfo(JsonUtils.objectToJson(param));
        paymentLogService.addLog(log);
        return payment;
    }

    /**
     * 获取支付流水
     * @param paymentNo 支付流水号
     * @return
     */
    public PaymentMain getByNo(String paymentNo){
        if(StringUtils.isBlank(paymentNo)){
            return null;
        }
        return this.getOne(new QueryWrapper<PaymentMain>().lambda().eq(PaymentMain::getPaymentNo, paymentNo));
    }

}
