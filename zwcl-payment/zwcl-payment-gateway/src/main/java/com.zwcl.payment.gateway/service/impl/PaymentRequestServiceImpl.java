package com.zwcl.payment.gateway.service.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.zwcl.payment.gateway.entity.PaymentRequest;
import com.zwcl.payment.gateway.enums.HandleStatusEnum;
import com.zwcl.payment.gateway.mapper.PaymentRequestMapper;
import com.zwcl.payment.gateway.service.PaymentRequestService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 请求数据 服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-13
 */
@Service
public class PaymentRequestServiceImpl extends BaseServiceImpl<PaymentRequestMapper, PaymentRequest> implements PaymentRequestService {
//
//    private ExecutorService executor = new ThreadPoolExecutor(3, 5,
//            0L, TimeUnit.MILLISECONDS,
//            new LinkedBlockingQueue<Runnable>());

    /**
     * 异步记录请求
     * @param param
     */
    @Override
    @Async
    public Long addRequest(Integer  callType, Integer requestType, String param){
//        executor.execute(() -> {
//            this.save(new PaymentRequest()
//                    .setStatus(HandleStatusEnum.DEALED.getValue())
//                    .setContent(param)
//                    .setType(type));
//        });
        PaymentRequest paymentRequest = new PaymentRequest()
                //.setId(IdWorker.getId())
                .setRequestStatus(HandleStatusEnum.DEALED.getValue())
                .setContent(param)
                .setCallType(callType)
                .setRequestType(requestType);
        this.save(paymentRequest);
        return paymentRequest.getId(); //由于是异步的原因，无法返回Id到上面一个方法中
    }
}
