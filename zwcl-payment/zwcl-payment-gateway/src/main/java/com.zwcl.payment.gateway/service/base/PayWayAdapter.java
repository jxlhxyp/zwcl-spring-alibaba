package com.zwcl.payment.gateway.service.base;

import com.zwcl.common.core.exception.BusinessException;
import com.zwcl.payment.gateway.entity.PaymentMain;
import com.zwcl.payment.gateway.entity.PaymentRefund;

/**
 * @Description 渠道适配器
 * @Author xyp
 * @Date 2020/3/11 13:52
 */
public abstract class PayWayAdapter implements PayWayClient{

    @Override
    public void refund(PaymentRefund paymentRefund) {
        throw new BusinessException("该支付渠道暂不支持退款");
    }

    @Override
    public void payQueryHandle(PaymentMain payment) {
        throw new BusinessException("该支付渠道暂不支持主动查询处理支付交易状态");
    }

    @Override
    public void refundQueryHandle(PaymentRefund paymentRefund) {
        throw new BusinessException("该支付渠道暂不支持主动查询处理退款交易状态");
    }

    @Override
    public String pay(PaymentMain payment) {
        throw new BusinessException("该支付渠道暂不支持统一下单");
    }

}
