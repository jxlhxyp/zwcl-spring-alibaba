package com.zwcl.payment.gateway.service.base;

import com.zwcl.payment.gateway.entity.PaymentMain;
import com.zwcl.payment.gateway.entity.PaymentRefund;

/**
 * 支付第三方渠道接口
 * @Author guobaikun
 * @Date 2020/02/26 13:52
 * @Email kunye.guo@wetax.com.cn
 */
public interface PayWayClient {

    String pay(PaymentMain payment);//统一下单

    void refund(PaymentRefund paymentRefund);//发起退款

    void payQueryHandle(PaymentMain payment);//主动查询处理支付交易状态

    void refundQueryHandle(PaymentRefund paymentRefund);//主动查询处理退款交易状态

}
