package com.zwcl.payment.gateway.service;

import com.zwcl.payment.gateway.entity.PaymentAccount;
import com.zwcl.common.mybatis.service.BaseService;

/**
 * <p>
 * 微信商户账号 服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-15
 */
public interface PaymentAccountService extends BaseService<PaymentAccount> {
    Integer getMchType(Integer orderType, Integer payWay);

    PaymentAccount getByType(Integer mchType);

    String getSecretKey(String mchId);

    String getSubSecretKey(String subMchId);
}
