package com.zwcl.payment.gateway.service;

import com.zwcl.payment.gateway.entity.PaymentRefund;
import com.zwcl.common.mybatis.service.BaseService;

/**
 * <p>
 * 退款流水表 服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-13
 */
public interface PaymentRefundService extends BaseService<PaymentRefund> {

    void replyRefundErr(PaymentRefund paymentRefund, String msg);

    void replyRefundErr(PaymentRefund paymentRefund, String msg, String code);

    PaymentRefund getByNo(String refundNo);
}
