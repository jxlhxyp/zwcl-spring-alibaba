package com.zwcl.payment.gateway.service;

import com.zwcl.payment.gateway.entity.PaymentRequest;
import com.zwcl.common.mybatis.service.BaseService;

/**
 * <p>
 * 请求数据 服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-13
 */
public interface PaymentRequestService extends BaseService<PaymentRequest> {

    Long addRequest(Integer  callType, Integer requestType, String param);
}
