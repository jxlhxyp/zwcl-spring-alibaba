package com.zwcl.payment.gateway.service;

import com.zwcl.payment.gateway.dto.PayParam;
import com.zwcl.payment.gateway.entity.PaymentMain;
import com.zwcl.common.mybatis.service.BaseService;

import java.util.List;

/**
 * <p>
 * 支付流水表 服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-13
 */
public interface PaymentMainService extends BaseService<PaymentMain> {
   List<PaymentMain> getPayNoByOrderNo(String orderNo, Integer orderType);

   PaymentMain savePaymentAndLog(PayParam param);

   PaymentMain getByNo(String paymentNo);
}
