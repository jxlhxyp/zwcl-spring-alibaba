package com.zwcl.payment.gateway.service;

import com.zwcl.payment.gateway.entity.PaymentRefundLog;
import com.zwcl.common.mybatis.service.BaseService;

import java.util.List;

/**
 * <p>
 * 退款流水日志表 服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-15
 */
public interface PaymentRefundLogService extends BaseService<PaymentRefundLog> {
    void addLog(String refundNo, Integer status, String content);

    void addBatchLog(List<PaymentRefundLog> list);


}
