package com.zwcl.payment.gateway.mapper;

import com.zwcl.payment.gateway.entity.PaymentAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 微信商户账号 Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-15
 */
public interface PaymentAccountMapper extends BaseMapper<PaymentAccount> {

}
