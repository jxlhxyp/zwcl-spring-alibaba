package com.zwcl.payment.gateway.mapper;

import com.zwcl.payment.gateway.entity.PaymentRefundLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 退款流水日志表 Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-15
 */
public interface PaymentRefundLogMapper extends BaseMapper<PaymentRefundLog> {
    int addBatch(List<PaymentRefundLog> list);
}
