package com.zwcl.payment.gateway.mapper;

import com.zwcl.payment.gateway.entity.PaymentRefund;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 退款流水表 Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-13
 */
public interface PaymentRefundMapper extends BaseMapper<PaymentRefund> {

}
