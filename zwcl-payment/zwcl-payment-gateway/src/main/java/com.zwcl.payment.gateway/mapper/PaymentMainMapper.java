package com.zwcl.payment.gateway.mapper;

import com.zwcl.payment.gateway.entity.PaymentMain;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付流水表 Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-13
 */
public interface PaymentMainMapper extends BaseMapper<PaymentMain> {

}
