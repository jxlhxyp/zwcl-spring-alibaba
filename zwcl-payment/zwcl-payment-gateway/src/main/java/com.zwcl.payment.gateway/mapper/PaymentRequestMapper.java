package com.zwcl.payment.gateway.mapper;

import com.zwcl.payment.gateway.entity.PaymentRequest;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 请求数据 Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-13
 */
public interface PaymentRequestMapper extends BaseMapper<PaymentRequest> {

}
