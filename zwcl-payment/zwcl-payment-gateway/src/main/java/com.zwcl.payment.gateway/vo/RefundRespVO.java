package com.zwcl.payment.gateway.vo;

import com.zwcl.common.mq.BasePayload;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 退款业务回调字段
 * @Author guobaikun
 * @Date 2020/03/06 10:55
 * @Email kunye.guo@wetax.com.cn
 */
@Data
@Accessors(chain = true)
public class RefundRespVO {

	private String refundNo;//退款流水号

	private String paymentNo;//支付流水号

	private Integer userId;//下单人uid

	private Integer refundStatus;//状态，1-待退款，2-退款中，3-已退款，4-退款失败

	private BigDecimal refundAmount;//退款金额

	private BigDecimal payAmount;//原支付金额

	private LocalDateTime refundTime;//退款完成时间

	private String orderNo;//业务订单号

	private Integer orderType;//订单类型

	private Integer manual;//是否手动退款，1-是，2-否

	private String outRefundNo;//第三方退款单号

	private String outTradeNo;//第三方支付单号

	private String errCode;//失败码

	private String errMsg;//失败信息

	private String reason;//退款原因

	private Integer payWay;//支付渠道

	private Integer paymentType;//支付类型 TODO mq回调时是否需要返回

	private String refundRecvAccout;//退款入账账户

}
