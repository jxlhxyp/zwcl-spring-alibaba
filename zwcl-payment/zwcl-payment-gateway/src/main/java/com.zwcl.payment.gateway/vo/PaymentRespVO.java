package com.zwcl.payment.gateway.vo;

import com.zwcl.common.mq.BasePayload;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 支付业务回调字段
 * @Author guobaikun
 * @Date 2020/1/10 10:55
 * @Email kunye.guo@wetax.com.cn
 */
@Data
@Accessors(chain = true)
public class PaymentRespVO {

	private String paymentNo;//支付流水号

	private Integer userId;//下单人uid

	private String openId;//用户openid

	private Integer paymentMode;//1 微信签约代扣（包括交通行业代扣） 2 车主平签约代扣

	private Integer payStatus;//状态，1-待支付，2-已支付，3-支付失败

	private BigDecimal payAmount;//支付金额（元）

	private Integer paymentType;//支付类型，1-手动支付，2-免密支付

	private LocalDateTime payTime;//支付完成时间

	private Integer orderType;//订单类型

	private String transactionId;//微信支付订单号

	private String orderNo;//订单号

	private BigDecimal discountAmount;//优惠金额

	private String errCode;//错误码

	private String errMsg;//失败信息

	private Integer payWay;//支付渠道

	private Integer mchType;//商户账号类型

	private String bankType;//银行类型

}
