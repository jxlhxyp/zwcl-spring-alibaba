package com.zwcl.payment.gateway;

import com.alicp.jetcache.anno.config.EnableCreateCacheAnnotation;
import com.alicp.jetcache.anno.config.EnableMethodCache;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author xyp
 * 通用支付网关项目
 * TODO:暂时无法启动，还差数据库没有配置
 * 该项目要注意以下几点：
 * 1. 幂等性
 * 2. 分库分表
 * 3. 兼容各业务的支付订单
 * 4. 利用消息解耦，可靠性要求高，采用rabbit或者Rocket
 * 5. 抽象支付渠道，对外提供统一的接口
 * 6. 请求日志记录以及数据的对账
 * 7. 多单联合渠道的支付
 */
@SpringCloudApplication
@MapperScan("com.zwcl.payment.gateway.mapper")
@EnableDiscoveryClient
@EnableMethodCache(basePackages = "com.zwcl.payment.gateway")
@EnableCreateCacheAnnotation
public class PaymentGatewayApplication {
    public static void main(String[] args){
        SpringApplication.run(PaymentGatewayApplication.class, args);
        System.out.println("启动成功");
    }
}
