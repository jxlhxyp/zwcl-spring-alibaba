package com.zwcl.common.web.annotation;


import com.zwcl.common.web.config.CorsFilterAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 开启跨域支持
 * TODO: 这个注解的代码，没有测试过
 * @author Levin
 * @since 2019-01-01
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({CorsFilterAutoConfiguration.class})
public @interface EnableCorsFilter {

}