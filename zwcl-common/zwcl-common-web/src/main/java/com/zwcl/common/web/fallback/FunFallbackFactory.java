package com.zwcl.common.web.fallback;

import com.zwcl.common.web.handler.FunFeignFallback;
import feign.Target;
import feign.hystrix.FallbackFactory;
import lombok.AllArgsConstructor;
import org.springframework.cglib.proxy.Enhancer;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: ZhiHu.Liu
 * @date: 2020/3/27 10:28 上午
 */
@AllArgsConstructor
public class FunFallbackFactory<T> implements FallbackFactory<T> {
    private final Target<T> target;

    @Override
    @SuppressWarnings("unchecked")
    public T create(Throwable cause) {
        final Class<T> targetType = target.type();
        final String targetName = target.name();
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(targetType);
        enhancer.setUseCache(true);
        enhancer.setCallback(new FunFeignFallback<>(targetType, targetName, cause));
        return (T) enhancer.create();
    }
}
