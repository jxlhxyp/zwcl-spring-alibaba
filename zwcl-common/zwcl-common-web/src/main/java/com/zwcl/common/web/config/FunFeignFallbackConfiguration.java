package com.zwcl.common.web.config;

import com.alibaba.csp.sentinel.SphU;
import com.zwcl.common.web.fallback.FunSentinelFeign;
import feign.Feign;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;

/**
 * Created by IntelliJ IDEA.
 * 不用在每个feign处定义实现fallback,统一化处理
 * @author: ZhiHu.Liu
 * @date: 2020/3/27 11:02 上午
 */
@Configuration
public class FunFeignFallbackConfiguration {
    //重命名bean，否则和sentinel包中的有冲突
    @Bean("funFeignSentinelBuilder")
    @Scope("prototype")
    @ConditionalOnClass({SphU.class, Feign.class})
    @ConditionalOnProperty(name = "feign.sentinel.enabled")
    @Primary
    public Feign.Builder feignSentinelBuilder() {
        return FunSentinelFeign.builder();
    }
}
