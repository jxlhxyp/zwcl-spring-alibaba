package com.zwcl.common.web.domain;

import lombok.Data;

@Data
public class SignRequest {
    //请求唯一id，可空，用来做接口幂等性
    private String requestId;

    //商户id 非空
    private String appId;

//    //对称加密key 非空
//    private String appKey;

    //参数签名 非空
    private String sign;

    //时间戳，精确到毫秒 非空
    private long timestamp;

    //请求的业务参数 可空
    private String requestData;
}
