package com.zwcl.common.web.domain;

import lombok.Data;

/**
 * 外部调用，如果需要记录请求方时，增加这个字段。
 * 继承这个类即可
 */
@Data
public class BaseOutEntity {
    private String appId;
}
