/*
 * Copyright 2019-2029 geekidea(https://github.com/geekidea)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zwcl.common.web.utils;


import com.zwcl.common.web.domain.RequestDetail;
import org.apache.commons.lang3.StringUtils;

/**
 * 记录请求详情信息到当前线程中，可在任何地方获取
 * TODO:大量请求时，可能造成内存泄漏
 * threadlocal实现简单，不自带内存生命周期管理，但是它和io很像，如果数据量大，它所占用的内存空间无法释放会导致内存泄漏
 * TODO: 可以借鉴netty中的InternalThreadLocal，提高性能
 * @author geekidea
 * @date 2020/3/26
 **/
public class RequestDetailContext {

    private static ThreadLocal<RequestDetail> threadLocal = new ThreadLocal<>();

    /**
     * 设置请求信息到当前线程中
     *
     * @param requestDetail
     */
    public static void setRequestDetail(RequestDetail requestDetail) {
        threadLocal.set(requestDetail);
    }

    /**
     * 从当前线程中获取请求信息
     */
    public static RequestDetail getRequestDetail() {
        return threadLocal.get();
    }

    public static String getLoginUserId() {
        String userId = threadLocal.get().getUserId();
        return StringUtils.isBlank(userId) ? "":userId;
    }

    public static String getLoginUserName() {
        String userName=threadLocal.get().getUserName();
        return StringUtils.isBlank(userName) ? "":userName;
    }

    public static String getTraceId() {
        String traceId=threadLocal.get().getTraceId();
        return StringUtils.isBlank(traceId) ? "":traceId;
    }
    /**
     * 销毁
     */
    public static void remove() {
        threadLocal.remove();
    }

}
