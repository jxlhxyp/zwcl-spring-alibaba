package com.zwcl.common.component.thread;


import java.util.concurrent.Callable;

public interface SimpleTask<Result> extends Callable<Result> {

    // 获取任务名称
    // 如何保证名称唯一呢，这里可以使用uuid
    String name();

    // 用于执行异步任务
    @Override
    Result call() throws Exception;

    // 是否已经执行
    boolean isExecute();
}
