package com.zwcl.common.component.upload.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "upload.tencent")
public class TencentCosConfig {

    private String secretId;

    private String secretKey;

    private String region;

    private String bucketName;

    private String upHost;

    private String readHost;


//后端上传的的配置例子
//    ##腾讯云cos相关配置
//    tencentcos:
//    secretId: AKIDAByLVoUoZFkzc0zb8VZRBC3mMjt9eswn
//    secretKey: xgBRWO4Sdqpc9UZR1NehfR1SgoqiU5QO
//    bucket: marketing-1259440055
//    region: ap-shanghai
//    durationSeconds: 1800
//    baseUrl: https://marketing-cos.golcer.cn    这个参数没有用上
//    readUrl: https://marketing.golcer.cn
//    rootDir: /marketplan/

}
