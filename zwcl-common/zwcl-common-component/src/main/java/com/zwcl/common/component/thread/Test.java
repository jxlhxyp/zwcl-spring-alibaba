package com.zwcl.common.component.thread;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Test {
    static class A extends AbstractSimpleTask<Integer> {
        private boolean isExecuted;
        @Override
        public String instance() {
            return "A";
        }
        @Override
        public Integer call() throws Exception {
            return 1;
        }
        @Override
        public boolean isExecute() {
            return isExecuted;
        }
        @Override
        public void setExecuted() {
            this.isExecuted = true;
        }
    }


    static class B extends AbstractSimpleTask<Integer> {
        private boolean isExecuted;
        @Override
        public String instance() {
            return "B";
        }
        @Override
        public Integer call() throws Exception {
            return 2;
        }
        @Override
        public boolean isExecute() {
            return isExecuted;
        }
        @Override
        public void setExecuted() {
            this.isExecuted = true;
        }
    }


    private static final ExecutorService service = Executors.newCachedThreadPool();
    public static void main(String[] args) {
        AsyncBox<AbstractSimpleTask<Integer>, Integer> box = new AsyncBox<>(service);
        A a = new A();
        B b = new B();
        box.addTask(a);
        box.addTask(b);

        List<Integer> data = box.execute().merge(a, b).getData();
        System.out.println(data);
        for (Map.Entry<Thread, List<AbstractSimpleTask<Integer>>> threadListEntry : box.getTaskBox().entrySet()) {
            System.out.println(threadListEntry);
        }
    }
}

