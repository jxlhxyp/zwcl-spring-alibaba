package com.zwcl.common.component.thread;

import java.util.UUID;

/**
 * 后面为了方便拓展，加抽象类进行处理
 *
 * @param <Result>
 */
public abstract class AbstractSimpleTask<Result> implements SimpleTask<Result> {

    // 获取子实例的名称
    public abstract String instance();

    /**
     * 由于考虑到使用全局的AsyncBox,用于区别同一名称的实例，这里使用UUID来做区分
     */
    @Override
    public String name() {
        return String.format(
                "%s&%s",
                instance(),
                UUID.randomUUID().toString().replace("-", "")
        );
    }

    @Override
    public abstract Result call() throws Exception;


    /**
     * 查询任务是否执行
     */
    @Override
    public abstract boolean isExecute();


    /**
     * 设置任务已经执行
     */
    public abstract void setExecuted();

}
