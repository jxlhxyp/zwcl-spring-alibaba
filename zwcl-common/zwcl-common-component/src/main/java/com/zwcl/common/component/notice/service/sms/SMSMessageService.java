package com.zwcl.common.component.notice.service.sms;

import com.zwcl.common.component.notice.config.SmsConfig;
import com.zwcl.common.core.utils.http.HttpClientUtils;
import org.apache.http.client.methods.HttpPost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: Jame
 * @Date: 2019/9/19 13:50
 * @Description:短信推送服务实现类
 */
@Component
public class SMSMessageService {

    private static final Logger logger = LoggerFactory.getLogger(SMSMessageService.class);

    @Autowired
    private SmsConfig smsConfig;

    /**
     * 单个短信发送
     *
     * @param mobile
     * @param content
     * @return
     */
    public Boolean send(String mobile, String content) {
        logger.info("send sms begin. mobile={}, content={}", mobile, content);
        if (StringUtils.isEmpty(mobile) || StringUtils.isEmpty(content)) {
            return false;
        }
        Map paramMap = buildParamMap(Arrays.asList(mobile), content);
        //大于0提交成功；-1余额不足；-2帐号或密码错误；-3连接服务商失败；-5其他错误，一般为网络问题，IP受限等
        String responseCode = HttpClientUtils.makeHttpRequestStringResult(smsConfig.getSmsMessageSendUrl(), HttpPost.METHOD_NAME, paramMap);
        logger.info("send sms finished. mobile={}, content={}, responseCode={}", mobile, content, responseCode);
        return StringUtils.isEmpty(responseCode) ? false : Integer.valueOf(responseCode) <= 0 ? false : true;
    }

    /**
     * 批量短信发送
     *
     * @param mobiles
     * @param content
     * @return
     */
    public Boolean batchSend(List<String> mobiles, String content) {
        logger.info("batch send sms begin. mobiles={}, content={}", mobiles, content);
        if (CollectionUtils.isEmpty(mobiles) || StringUtils.isEmpty(content)) {
            return false;
        }
        Map paramMap = buildParamMap(mobiles, content);
        //大于0提交成功；-1余额不足；-2帐号或密码错误；-3连接服务商失败；-5其他错误，一般为网络问题，IP受限等
        String responseCode =HttpClientUtils.makeHttpRequestStringResult(smsConfig.getSmsMessageSendUrl(), HttpPost.METHOD_NAME, paramMap);
        logger.info("batch send sms finished. mobiles={}, content={}, responseCode={}", mobiles, content, responseCode);
        return StringUtils.isEmpty(responseCode) ? false : Integer.valueOf(responseCode) <= 0 ? false : true;
    }

    /**
     * 构造请求参数
     *
     * @param mobiles
     * @param content
     * @return
     */
    private Map buildParamMap(List<String> mobiles, String content) {
        StringBuffer mobileSb = new StringBuffer();
        for (String mobile : mobiles) {
            mobileSb.append(";").append(mobile);
        }
        Map paramMap = new HashMap();
        paramMap.put("account", smsConfig.getSmsMessageAccount());
        paramMap.put("password", smsConfig.getSmsMessagePassword());
        paramMap.put("destmobile", mobileSb.substring(1));
        paramMap.put("msgText", addSignature(content));
        //用户自定义成功提交返回值(纯数字，最少6位数且必须小于2147483647)，如留空系统提供默认返回值
        paramMap.put("userTaskID", 123456);
        return paramMap;
    }

    /**
     * 添加企业签名
     *
     * @param content
     * @return
     */
    private String addSignature(String content) {
        return new StringBuffer(content).append(smsConfig.getSmsMessageSignature()).toString();
    }

}
