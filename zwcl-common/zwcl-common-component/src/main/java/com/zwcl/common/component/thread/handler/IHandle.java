package com.zwcl.common.component.thread.handler;

/**
 * 用户发送http请求
 */
public interface IHandle<Param, Response> {


    /**
     * 初始化
     *
     * @param host  域名
     * @param param 参数
     */
    IHandle<Param, Response> init(String host, Param param);

    /**
     * 发送http请求
     */
    IHandle<Param, Response> request();

    /**
     * 处理并获取响应结果
     */
    Response response();


    /**
     * 执行并返回结果
     *
     * @param host  域名
     * @param param 参数
     */
    default Response execute(String host, Param param) {
        return this.init(host, param).request().response();
    }


}
