package com.zwcl.common.component.notice.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Description 告警邮件列表配置
 * @Author tanljs
 * @Date 2019/9/23
 * @Email Alex.tan@wetax.com.cn
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "notice.mail")
public class NoticeMailConfig {
    private String[] list;
}
