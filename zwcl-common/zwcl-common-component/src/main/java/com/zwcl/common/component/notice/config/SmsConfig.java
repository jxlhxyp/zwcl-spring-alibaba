package com.zwcl.common.component.notice.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Description 短信配置
 * @Author tanljs
 * @Date 2019/9/20
 * @Email Alex.tan@wetax.com.cn
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "notice.sms")
public class SmsConfig {
    /**
     * 短信发送url
     */
    private String smsMessageSendUrl;
    /**
     * 发送短信账户
     */
    private String smsMessageAccount;
    /**
     * 发送短信密码
     */
    private String smsMessagePassword;
    /**
     * 发送短信signature
     */
    private String smsMessageSignature;
}
