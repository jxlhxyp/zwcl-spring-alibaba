package com.zwcl.common.component.notice.service.mail;

import com.sun.mail.util.MailSSLSocketFactory;
import com.zwcl.common.component.async.AsyncSendMail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.math.BigDecimal;
import java.security.GeneralSecurityException;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Description 邮件发送service
 * @Author tanljs
 * @Date 2019/9/23
 * @Email Alex.tan@wetax.com.cn
 */
@Slf4j
@Component
public class MailSenderService {

    @Autowired
    private Environment env;

    private JavaMailSenderImpl javaMailSender;

    private ExecutorService executor = Executors.newFixedThreadPool(10);

    @PostConstruct
    public void initParam() {
        javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setHost(env.getProperty("notice.mail.host"));
        javaMailSender.setUsername(env.getProperty("notice.mail.user"));
        javaMailSender.setPassword(env.getProperty("notice.mail.pwd"));
        javaMailSender.setDefaultEncoding("utf-8");
        javaMailSender.setPort(Integer.parseInt(env.getProperty("notice.mail.port")));

        Properties properties = new Properties();
        MailSSLSocketFactory sf = null;
        try {
            sf = new MailSSLSocketFactory();
            sf.setTrustAllHosts(true);
            properties.put("mail.smtp.ssl.enable", "true");
            properties.put("mail.smtp.ssl.socketFactory", sf);
//            properties.put("mail.smtp.auth", "true");
        } catch (GeneralSecurityException e) {
            log.error("邮件发送service初始化失败", e);
        }
        javaMailSender.setJavaMailProperties(properties);
    }

    /**
     * 发送邮件
     *
     * @param title
     * @param toUsers
     * @param content
     * @return
     */
    public boolean sendEmail(String title, String[] toUsers, String content) {
        try {
            if (toUsers == null || toUsers.length == 0) {
                return false;
            }
            MimeMessage mailMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper messageHelper = new MimeMessageHelper(mailMessage, true);
            messageHelper.setTo(toUsers);
            messageHelper.setSubject(title);
            messageHelper.setText(content, true);
            messageHelper.setFrom(javaMailSender.getUsername());
            javaMailSender.send(mailMessage);
        } catch (MessagingException e) {
            log.error("邮件发送失败,title={},users={},content={}", title, toUsers, content, e);
            return false;
        } catch (Exception ex) {
            log.error("邮件发送失败,title={},users={},content={}", title, toUsers, content, ex);
            return false;
        }
        return true;
    }

    /**
     * 异步发送邮件，不等待结果
     * @param title
     * @param toUsers
     * @param content
     */
    public void sendEmailAsync(String title, String[] toUsers, String content) {
        executor.submit(new AsyncSendMail(this, title, toUsers, content));
    }

    public static void main(String args[]) {
        BigDecimal a = new BigDecimal(10);
        BigDecimal b = new BigDecimal(10.0);
        System.out.println(a.doubleValue() == b.doubleValue());
    }
}
