package com.zwcl.common.component.notice.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @Description 告警手机号列表配置
 * @Author tanljs
 * @Date 2019/9/19
 * @Email Alex.tan@wetax.com.cn
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "notice.phone")
public class NoticePhoneConfig {
    private List<String> list;
}
