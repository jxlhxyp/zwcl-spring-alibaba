/*
 *
 *  *  Copyright (C) 2018  the sun<463540703@qq.com>
 *
 *  *  AG-Enterprise 企业版源码
 *  *  郑重声明:
 *  *  如果你从其他途径获取到，请告知老A传播人，奖励1000。
 *  *  老A将追究授予人和传播人的法律责任!
 *
 *  *  This program is free software; you can redistribute it and/or modify
 *  *  it under the terms of the GNU General Public License as published by
 *  *  the Free Software Foundation; either version 2 of the License, or
 *  *  (at your option) any later version.
 *
 *  *  This program is distributed in the hope that it will be useful,
 *  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *  GNU General Public License for more details.
 *
 *  *  You should have received a copy of the GNU General Public License along
 *  *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

package com.zwcl.common.component.upload.cloud;


import com.alibaba.fastjson.JSONObject;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.model.ObjectMetadata;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.model.PutObjectResult;
import com.qcloud.cos.model.StorageClass;
import com.qcloud.cos.region.Region;
import com.zwcl.common.component.upload.config.CloudStorageConfig;
import com.zwcl.common.core.exception.BaseException;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import com.qcloud.cos.auth.COSCredentials;
import org.apache.commons.lang3.StringUtils;

/**
 * 腾讯云存储
 * @author ace
 */
public class QcloudCloudStorageService extends CloudStorageService {
    private COSClient client;

    public QcloudCloudStorageService(CloudStorageConfig config){
        this.config = config;
        //初始化
        init();
    }

    private void init(){
//    	Credentials credentials = new Credentials(config.getQcloudAppId(), config.getQcloudSecretId(),
//                config.getQcloudSecretKey());
//
//    	//初始化客户端配置
//        ClientConfig clientConfig = new ClientConfig();
//        //设置bucket所在的区域，华南：gz 华北：tj 华东：sh
//        clientConfig.setRegion(config.getQcloudRegion());
//
//    	client = new COSClient(clientConfig, credentials);

        // 1 初始化秘钥信息
        COSCredentials cred = new BasicCOSCredentials(config.getQcloudSecretId(), config.getQcloudSecretKey());
        // 2 设置bucket的区域
        ClientConfig clientConfig = new ClientConfig(new Region(config.getQcloudRegion()));
        // 3 生成cos客户端
        client = new COSClient(cred, clientConfig);
    }

    @Override
    public String upload(byte[] data, String path) {
        //腾讯云必需要以"/"开头
        if(!path.startsWith("/")) {
            path = "/" + path;
        }
//        这是以前的代码 //上传到腾讯云
//        UploadFileRequest request = new UploadFileRequest(config.getQcloudBucketName(), path, data);
//        String response = client.uploadFile(request);
//        JSONObject jsonObject = JSONObject.fromObject(response);
//        if(jsonObject.getInt("code") != 0) {
//            throw new BaseException("文件上传失败，" + jsonObject.getString("message"));
//        }
//        return config.getQcloudDomain() + path;

        // 判断文件大小（小文件上传建议不超过20M）
        int length = data.length;
        // 获取文件流
        InputStream byteArrayInputStream = new ByteArrayInputStream(data);
        ObjectMetadata objectMetadata = new ObjectMetadata();
        // 从输入流上传必须制定content length, 否则http客户端可能会缓存所有数据，存在内存OOM的情况
        objectMetadata.setContentLength(length);
        // 默认下载时根据cos路径key的后缀返回响应的contenttype, 上传时设置contenttype会覆盖默认值
        // objectMetadata.setContentType("image/jpeg");
        PutObjectRequest putObjectRequest = new PutObjectRequest(config.getQcloudBucketName(), path, byteArrayInputStream, objectMetadata);
        // 设置 Content type, 默认是 application/octet-stream
        putObjectRequest.setMetadata(objectMetadata);
        // 设置存储类型, 默认是标准(Standard), 低频(standard_ia)
        putObjectRequest.setStorageClass(StorageClass.Standard_IA);
        PutObjectResult putObjectResult = client.putObject(putObjectRequest);
        String eTag = putObjectResult.getETag();
        System.out.println(eTag);
        // 关闭客户端
        client.shutdown();
        // http://{buckname}-{appid}.cosgz.myqcloud.com/image/1545012027692.jpg
        return config.getQcloudDomain() + path;
    }

    @Override
    public String upload(InputStream inputStream, String path) {
    	try {
            byte[] data = IOUtils.toByteArray(inputStream);
            return this.upload(data, path);
        } catch (IOException e) {
            throw new BaseException("上传文件失败");
        }
    }

    @Override
    public String uploadSuffix(byte[] data, String suffix,String env) {
        if(StringUtils.isEmpty(env)){
            env="prod";
        }
        return upload(data, getPath(config.getQcloudPrefix()+"/"+env, suffix));
    }

    @Override
    public String uploadSuffix(InputStream inputStream, String suffix,String env) {
        if(StringUtils.isEmpty(env)){
            env="prod";
        }
        return upload(inputStream, getPath(config.getQcloudPrefix()+"/"+env, suffix));
    }
}
