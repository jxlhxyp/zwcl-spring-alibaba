package com.zwcl.common.component.async;

import com.zwcl.common.component.notice.service.mail.MailSenderService;

/**
 * @Description 异步发送email
 * @Author tanljs
 * @Date 2019/9/23
 * @Email Alex.tan@wetax.com.cn
 */
public class AsyncSendMail implements Runnable {

    private MailSenderService mailSenderService;
    private String title;
    private String[] toUsers;
    private String content;

    public AsyncSendMail(MailSenderService mailSenderService, String title, String[] toUsers, String content) {
        this.content = content;
        this.mailSenderService = mailSenderService;
        this.title = title;
        this.toUsers = toUsers;
    }

    @Override
    public void run() {
        mailSenderService.sendEmail(title, toUsers, content);
    }
}
