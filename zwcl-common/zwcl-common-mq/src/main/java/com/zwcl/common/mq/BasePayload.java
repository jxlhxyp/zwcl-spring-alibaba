package com.zwcl.common.mq;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 改为非继承方式，继承方式会导致消费封装类中的InitializingBean时，Map类型获取Class超类错误
 * @param <T>
 */
@Data
@Accessors(chain = true)      //支持链式语法
public class BasePayload<T> {
    private String bizId;

    private T message;

    public BasePayload(T msg){
        this.message=msg;
    }

    public BasePayload(){}
}
