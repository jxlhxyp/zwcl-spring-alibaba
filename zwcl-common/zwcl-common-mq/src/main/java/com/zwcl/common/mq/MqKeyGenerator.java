package com.zwcl.common.mq;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * @Title: MqKeyGenerator
 * @Description: 生成业务key
 * @date : 2020/11/4 15:31
 */
@Component
public class MqKeyGenerator {

    private volatile String bizCacheKey;
    private volatile String transCacheKey;
    private volatile String consumerCacheKey;

    private static final String BIZ_OPERATION_PREFIX = "mm_biz_";
    private final static String PREFIX_IDEMPOTENT = "mm_idempotent_";
    private static final String TRANS_PREFIX = "mm_trans_";
    private final String separator ="_";

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RocketMQProperties properties;
    @Value("${spring.application.name}")
    private String applicationName;

    private String getBizCacheKey(String producerGroup) {
        if (bizCacheKey == null) {
            synchronized (this) {
                if (bizCacheKey == null) {
                    this.bizCacheKey = BIZ_OPERATION_PREFIX + producerGroup + separator;
                }
            }
        }
        return bizCacheKey;
    }

    public String getConsumerCacheKey(String producerGroup){
        if(consumerCacheKey == null){
            synchronized (this){
                if(consumerCacheKey == null){
                    consumerCacheKey = PREFIX_IDEMPOTENT + producerGroup + separator;
                }
            }
        }
        return consumerCacheKey;
    }

    private String getTransCacheKey(String producerGroup) {
        if (transCacheKey == null) {
            synchronized (this) {
                if (transCacheKey == null) {
                    transCacheKey = TRANS_PREFIX + producerGroup + separator;
                }
            }
        }
        return transCacheKey;
    }

    /**
     * 业务操作的id,全局唯一，防止消费者重复消费
     *
     * @return
     */
    public String getBizOperationId() {
        String producerGroup = getDefaultProducerGroupName();
        String bizCacheKey = getBizCacheKey(producerGroup);
        return bizCacheKey + redisTemplate.opsForValue().increment(bizCacheKey);
    }

    /**
     * 获取消费者端锁的key
     * @param bizId
     * @return
     */
    public String getConsumerLockKey(String bizId) {
        String producerGroup = getDefaultProducerGroupName();
        return getConsumerCacheKey(producerGroup) + bizId;
    }

    /**
     * 在同一producer获得新的事务Id,用于检查事务状态全局唯一
     *
     * @return
     */
    private String getNewTransactionId() {
        String producerGroup = getDefaultProducerGroupName();
        String transCacheKey = getTransCacheKey(producerGroup);
        return transCacheKey + redisTemplate.opsForValue().increment(transCacheKey);
    }

    private String getDefaultProducerGroupName(){
        String producerGroup = properties.getProducerGroup();
        if(producerGroup == null){
            producerGroup = applicationName;
        }
        return producerGroup;
    }
}