package com.zwcl.common.mq;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Data
@Configuration
@ConfigurationProperties(prefix = "rocketmq")
public class RocketMQProperties {
    private String nameServer;

    private String producerGroup;

}
