package com.zwcl.common.log.service;


import com.zwcl.manage.system.api.entity.SysLog;
import com.zwcl.manage.system.api.feign.RemoteLogFacadeClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * 异步调用日志服务
 * 
 * @author ruoyi
 */
@Service
public class AsyncLogService
{
    @Autowired
    private RemoteLogFacadeClient remoteLogFacadeClient;

    /**
     * 保存系统日志记录
     */
    @Async
    public void saveSysLog(SysLog sysOperLog)
    {
        remoteLogFacadeClient.saveLog(sysOperLog);
    }
}
