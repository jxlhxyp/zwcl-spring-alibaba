package com.zwcl.common.core.utils.http;

/**
 * Created by Administrator on 2018/9/19.
 */

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/12/7.
 */
@Component
public class RestTemplateHelper {
    /*RestTemplate工具*/
    @Autowired
    private RestTemplate restTemplate;


    /**
     * 根据url获取接口信息
     *
     * @param url 接口地址
     * @return 调用接口返回信息
     * @author liumingzhi 2017/06/06
     */
    public Object getForObject(String url) {
        String result = restTemplate.getForObject(url, String.class);
        if (result.startsWith("[")) {
            return JSONArray.parseArray(result);
        }
        return JSONObject.parseObject(result);
    }

    public Object getForObjectByHeaders(String url, Map<String, Object> map) {

        HttpHeaders headers = new HttpHeaders();
        for (String key : map.keySet()) {
            headers.add(key, map.get(key).toString());
        }
        HttpEntity<String> formEntity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, formEntity, String.class);
        String result = response.getBody();

        if (result.startsWith("[")) {
            return JSONArray.parseArray(result);
        }
        return JSONObject.parseObject(result);
    }


    /**
     * 根据接口地址+参数获取接口信息
     *
     * @param url 接口地址
     * @param map 接口参数
     * @return 接口返回信息
     * @author liumingzhi 2017/06/16
     */
    public Object getForObjectByMap(String url, Map<String, Object> map) {
        Map<String, Object> uriVariables = new HashMap<String, Object>();
        for (String key : map.keySet()) {
            uriVariables.put(key, map.get(key));
        }
        String result = restTemplate.getForObject(url, String.class, uriVariables);
        if (result.startsWith("[")) {
            return JSONArray.parseArray(result);
        }
        return JSONObject.parseObject(result);

    }


    /**
     * 根据headers参数获取接口信息
     * @param url 接口地址
     * @param map 参数
     * @param responseType 类型
     * @param <T> 类
     * @return 对应类型
     * @author liumingzhi 2017/07/06
     */
    public <T> ResponseEntity<T> getForObjectByHeaders(String url, Map<String, Object> map, Class<T> responseType) {
        HttpHeaders headers = new HttpHeaders();
        for (String key : map.keySet()) {
            headers.add(key, map.get(key).toString());
        }
        HttpEntity<String> formEntity = new HttpEntity<String>(null, headers);
        return restTemplate.exchange(url, HttpMethod.GET, formEntity, responseType);
    }

    /**
     * post参数提交给接口,回传信息
     *
     * @param url          接口地址
     * @param request      参数
     * @param responseType 提交类型
     * @param <T>          返回类型
     * @return 接口返回信息
     * @author liumingzhi 2016/06/06
     */
    public <T> T postForObject(String url, Object request, Class<T> responseType) {
        T result = restTemplate.postForObject(url, request, responseType);
        return result;
    }

    /**
     * post提交json给接口,回传信息
     *
     * @param url  接口地址
     * @param json json数据
     * @return 接口返回的字符串
     * @author liumingzhi 2016/06/10
     */
    public String postJsonForStr(String url, String json) {
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
        headers.setContentType(type);

        HttpEntity<String> entity = new HttpEntity<>(json, headers);
        String str = restTemplate.postForObject(url, entity, String.class);

        return str;
    }

    /**
     * post提交json给接口,回传信息
     *
     * @param url  接口地址
     * @param map  接口参数
     * @param json json数据
     * @return 接口返回的字符串
     * @author liumingzhi 2016/06/10
     */
    public String postJsonForStr(String url, Map<String, Object> map, String json) {
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
        headers.setContentType(type);

        //接口参数
        for (String key : map.keySet()) {
            headers.add(key, map.get(key).toString());
        }

        //json参数
        HttpEntity<String> entity = new HttpEntity<>(json, headers);

        String str = restTemplate.postForObject(url, entity, String.class);

        return str;
    }
}