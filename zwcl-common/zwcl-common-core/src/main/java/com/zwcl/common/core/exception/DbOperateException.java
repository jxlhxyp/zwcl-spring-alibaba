package com.zwcl.common.core.exception;

import com.zwcl.common.core.enums.ExceptionCode;

/**
 * @Description 业务异常包装类
 * @Date 2020/5/17 15:10:00
 * @Created by xyp
 */
public class DbOperateException extends BaseException{

    private static final long serialVersionUID = -2303357122330162359L;

    public DbOperateException(String message) {
        super(message);
    }

    public DbOperateException(Integer errorCode, String message) {
        super(errorCode, message);
    }

    public DbOperateException(ExceptionCode exceptionCode) {
        super(exceptionCode);
    }

    public DbOperateException(Throwable cause) {
        super(cause);
    }
}