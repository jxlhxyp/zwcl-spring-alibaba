package com.zwcl.common.core.excel;

import com.alibaba.excel.EasyExcel;

import java.io.InputStream;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author: Jame
 * @Date: 2019/11/18 20:48
 * @Description:导入excel文件抽象类
 */
public abstract class AbstractImportService<E1, E2> {

    private final int PAGE_SIZE = 5000;

    /**
     * 分批导入excel文件数据
     * @param inputStream excel文件流
     * @param head 表头
     * @param head 额外参数
     */
    protected void readExcel(InputStream inputStream, Class head, E2 param) {
        ExcelListener excelListener = new ExcelListener(saveData(param), PAGE_SIZE);
        EasyExcel.read(inputStream, head, excelListener).sheet().doRead();
    }

    /**
     * 保存数据抽象方法，需要业务方实现保存数据的具体逻辑
     * @param param
     * @return
     */
    protected abstract Consumer<List<E1>> saveData(E2 param);

}