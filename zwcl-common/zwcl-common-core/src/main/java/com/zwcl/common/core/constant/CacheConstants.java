package com.zwcl.common.core.constant;

import org.apache.commons.lang3.StringUtils;

/**
 * 缓存的key 常量
 * 暂时不用到
 * @author xyp
 */
public class CacheConstants
{
    /**
     * 令牌自定义标识
     */
    public static final String HEADER = "Authorization";

    /**
     * 令牌前缀
     */
    public static final String TOKEN_PREFIX = "Bearer ";

    /**
     * 用户ID字段
     */
    public static final String DETAILS_USER_ID = "user_id";

    /**
     * 用户名字段
     */
    public static final String DETAILS_USERNAME = "username";

    /**
     * 默认用户的来源，对应枚举RequestEndpointEnum中的值
     */
    public static final String DETAILS_CLIENIT = "0";

    /**
     * 默认用户的来源系统
     */
    public static final String DETAILS_FROM_APP="system";

    /**
     * 请求是外部系统还是内部系统
     */
    public static final String FROM = "from";

    /**
     * 权限缓存前缀
     */
    public final static String LOGIN_TOKEN_KEY = "Zwcl:Upms:LoginTokens:";

    public final static String REQUEST_TRACE = "TraceId";
}
