package com.zwcl.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum  RequestEndpointEnum implements BaseEnum {
    /**
     * B端用户
     **/
    B("0", "B端用户"),
    /**
     * C端小程序用户
     **/
    C1("11", "C端小程序用户"),

    /**
     * C端小程序用户
     **/
    C2("12", "C端App用户"),

    ;

    private String code;
    private String desc;
}
