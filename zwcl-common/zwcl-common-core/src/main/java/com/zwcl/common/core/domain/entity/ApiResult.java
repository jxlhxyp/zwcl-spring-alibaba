
package com.zwcl.common.core.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.zwcl.common.core.enums.ExceptionCode;
import com.zwcl.common.core.utils.StringUtilsEx;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * REST API 返回结果
 * </p>
 *
 * @author geekidea
 * @since 2018-11-08
 */
@Data
@Accessors(chain = true)
@Builder
@AllArgsConstructor
public class ApiResult<T> implements Serializable {
	private static final long serialVersionUID = 8004487252556526569L;

	/**
     * 响应码
     */
    private int code;

    /**
     * 是否成功
     */
    private boolean success;

    /**
     * 响应消息
     */
    private String message;

    /**
     * 响应数据
     */
    private T data;

    /**
     * 响应时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date time;

    public ApiResult() {
        time  = new Date();
    }

    public static ApiResult<Boolean> result(boolean flag){
        if (flag){
            return ok();
        }
        return fail();
    }

    public static ApiResult<Boolean> result(ExceptionCode apiCode){
        return result(apiCode,null);
    }

    public static <T> ApiResult<T> result(ExceptionCode apiCode,T data){
        return result(apiCode,null,data);
    }

    public static <T> ApiResult<T> result(ExceptionCode apiCode,String message,T data){
        boolean success = false;
        if (apiCode.getCode() == ExceptionCode.SUCCESS.getCode()){
            success = true;
        }
        if (StringUtilsEx.isBlank(message)){
            message = apiCode.getMessage();
        }
        ApiResult result= (ApiResult<T>) ApiResult.builder()
                .code(apiCode.getCode())
                .message(message)
                .data(data)
                .success(success)
                .time(new Date())
                .build();
        return result;
    }

    public static ApiResult<Boolean> ok(){
        return ok(null);
    }

    public static <T> ApiResult<T> ok(T data){
        return result(ExceptionCode.SUCCESS,data);
    }

    public static <T> ApiResult<T> ok(T data,String message){
        return result(ExceptionCode.SUCCESS,message,data);
    }

    public static ApiResult<Map<String,Object>> okMap(String key,Object value){
        Map<String,Object> map = new HashMap<>(1);
        map.put(key,value);
        return ok(map);
    }

    public static ApiResult<Boolean> fail(ExceptionCode apiCode){
        return result(apiCode,null);
    }

    public static ApiResult<String> fail(String message){
        return result(ExceptionCode.FAIL,message,null);
    }

    public static <T> ApiResult<T> fail(ExceptionCode apiCode,T data){
        if (ExceptionCode.SUCCESS == apiCode){
            throw new RuntimeException("失败结果状态码不能为" + ExceptionCode.SUCCESS.getCode());
        }
        return result(apiCode,data);
    }

    public static  ApiResult<String> fail(Integer errorCode,String message){
        return new ApiResult<String>()
                .setSuccess(false)
                .setCode(errorCode)
                .setMessage(message);
    }

    public static ApiResult<Map<String,Object>> fail(String key,Object value){
        Map<String,Object> map = new HashMap<>(1);
        map.put(key,value);
        return result(ExceptionCode.FAIL,map);
    }

    public static ApiResult<Boolean> fail() {
        return fail(ExceptionCode.FAIL);
    }
}