package com.zwcl.common.core.utils.thread;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * @ClassName: HandleCallable.java
 * @Description: 线程调用
 */
@SuppressWarnings("rawtypes")
public class HandleCallable<T,E> implements Callable<ResultBean> {
    private static Logger logger = LoggerFactory.getLogger(HandleCallable.class);
    // 线程名称
    private String threadName = "";
    // 需要处理的数据
    private List<E> data;
    // 辅助参数
    private Map<String, Object> params;
    // 具体执行任务
    private ITask<ResultBean<T>, E> task;
    //是否切分的数据批量执行
    private Boolean batchFlag;

    public HandleCallable(String threadName, List<E> data, Map<String, Object> params,
                          ITask<ResultBean<T>, E> task,Boolean batchFlag) {
        this.threadName = threadName;
        this.data = data;
        this.params = params;
        this.task = task;
        this.batchFlag = batchFlag;
    }

    //将该函数从单个数据执行改成批量
    @Override
    public ResultBean<List<ResultBean<T>>> call() throws Exception {
        // 该线程中所有数据处理返回结果
        ResultBean<List<ResultBean<T>>> resultBean = ResultBean.newInstance();
        if (data != null && data.size() > 0) {
            logger.info("线程：{},共处理:{}个数据，开始处理......", threadName, data.size());
            // 返回结果集
            List<ResultBean<T>> resultList = new ArrayList<>();
            if(batchFlag){
                resultList.add(task.batchExecute(data,params));   //一个线程批次数据，得到一个结果，便于批量插入
            }else {
                // 循环处理每个数据
                for (int i = 0; i < data.size(); i++) {
                    // 需要执行的数据
                    E e = data.get(i);
                    // 将数据执行结果加入到结果集中
                    resultList.add(task.execute(e, params));     //一条一个结果
                    logger.info("线程：{},第{}个数据，处理完成", threadName, (i + 1));
                }
            }
            logger.info("线程：{},共处理:{}个数据，处理完成......", threadName, data.size());
            resultBean.setData(resultList);
        }
        return resultBean;
    }

}
