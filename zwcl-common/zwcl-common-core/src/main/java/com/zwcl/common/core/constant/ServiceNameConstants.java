package com.zwcl.common.core.constant;

/**
 * 服务名称
 * 暂时不用到
 * @author xyp
 */
public class ServiceNameConstants
{
    /**
     * 认证服务的serviceid
     */
    public static final String AUTH_SERVICE = "gdw-auth";

    /**
     * 系统模块的serviceid
     */
    public static final String SYSTEM_SERVICE = "gdw-system";
}
