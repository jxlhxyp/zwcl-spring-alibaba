package com.zwcl.common.core.domain.dto;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * @author xyp
 * @className: BaseDTO
 * @description:
 * @date 2019/8/115:42
 */
@Data
public class BaseDto {
    public static String[] orderTypeArr={"asc","desc","ASC","DESC"};
    public static String suffix = "Str";
    private int current=1;
    private int size=10;
    private String order;//排序字段
    private String orderType;//升序asc/降序desc

    //防止注入
    public String getOrder(){
        if(StringUtils.isNotBlank(order) && Pattern.matches("^\\w+",order)){
            if(order.lastIndexOf(suffix)>0){
                return order.substring(0,order.length()-3);
            }
            return order;
        }

        return "";
    }
    //防止随意传参导致报错
    public String getOrderType(){
        if(StringUtils.isBlank(orderType)||!Arrays.asList(orderTypeArr).contains(orderType)){
            return "desc";
        }else return orderType;
    }
}
