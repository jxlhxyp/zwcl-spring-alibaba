package com.zwcl.common.core.utils.thread;

import java.util.List;
import java.util.Map;

/**
 * 任务处理接口
 * 具体业务逻辑可实现该接口
 *  T 返回值类型
 *  E 传入值类型
 */
public interface ITask<T, E> {

    /**
     * 一条数据，执行一次。比如，5万数据插入数据库，10个线程；一个线程需要插入5000条，执行5000次，得到5000个结果
     * 任务执行方法接口<BR>
     * 方法名：execute<BR>
     * @param e 传入对象
     * @param params 其他辅助参数
     * @return T<BR> 返回值类型
     */
    T execute(E singleData, Map<String, Object> params);

    /**
     * 一个线程批次数据，执行一次。比如，5万数据插入数据库，10个线程；一次插入5000条，得到一个结果
     * @param data
     * @param params
     * @return
     */
    T batchExecute(List<E> batchData, Map<String, Object> params);
}
