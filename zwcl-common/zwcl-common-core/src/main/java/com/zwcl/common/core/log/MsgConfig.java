package com.zwcl.common.core.log;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author: xyp
 * @Description: 日志配置
 * @Date: 2019/9/7 11:33
 * @Version: 1.0
 */
@Slf4j
public class MsgConfig extends ClassicConverter {
    @Override
    public String convert(ILoggingEvent event) {
        return event.getFormattedMessage();
    }
}
