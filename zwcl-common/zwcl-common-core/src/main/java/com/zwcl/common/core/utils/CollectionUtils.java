/**
 *
 */
package com.zwcl.common.core.utils;

import java.util.*;

/**
 * 集合相关工具类
 *
 * User: kunye
 * Date: 2019/7/22
 */
public final class CollectionUtils {

    public static <T> List<T> toList(T... elements) {
        List<T> list = new ArrayList<T>();
        Collections.addAll(list, elements);
        return list;
    }

    public static <T> Set<T> toSet(T... elements) {
        Set<T> set = new HashSet<T>();
        Collections.addAll(set, elements);
        return set;
    }

    public static <K, V> Map<K, V> toMap(K[] keys, V[] values) {

        if (keys.length != values.length) {
            throw new IllegalArgumentException("The length of keys and values are not equal.");
        }

        Map<K, V> map = new HashMap<K, V>();
        for (int i = 0; i < keys.length; i++) {
            map.put(keys[i], values[i]);
        }
        return map;
    }

    public static <T> Map<T, T> toMap(T... elements) {

        if (elements.length % 2 != 0) {
            throw new IllegalArgumentException("Length is illegal.");
        }

        Map<T, T> map = new HashMap<T, T>();
        for (int i = 0; i < elements.length; i++) {
            map.put(elements[i], elements[++i]);
        }
        return map;
    }


    /**
     * 判断是否为null 或者 list.size() == 0
     *
     * @param list 集合
     * @return 返回是否为null 或者 list.size() == 0
     * @Title: isEmpty
     * @Description: 判断是否为null 或者 list.size() == 0
     */
    public static boolean isEmpty(List<?> list) {
        if (null == list || list.isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * 判断是否不为null 或者 list.size() == 0
     *
     * @param list 集合
     * @return 返回是否不为null 或者 list.size() == 0
     * @Title: isNotEmpty
     * @Description: 判断是否不为null 或者 list.size() == 0
     */
    public static boolean isNotEmpty(List<?> list) {
        return !isEmpty(list);
    }

    /**
     * 判断Set集合是否为null 或者 set.size() == 0
     *
     * @param set Set集合
     * @return 返回Set集合是否为null 或者 set.size() == 0
     * @Title: isEmpty
     * @Description: 判断Set集合是否为null 或者 set.size() == 0
     */
    public static boolean isEmpty(Set<?> set) {
        if (null == set || set.isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * 判断Set集合是否不为 null 或者 set.size() == 0
     *
     * @param set Set集合
     * @return 返回Set集合是否不为 null 或者 set.size() == 0
     * @Title: isNotEmpty
     * @Description: 判断Set集合是否不为 null 或者 set.size() == 0
     */
    public static boolean isNotEmpty(Set<?> set) {
        return !isEmpty(set);
    }

    /**
     * @param collection 集合
     * @param <T>        类型
     * @param size       大小
     * @return 多个集合
     * @Title: split
     * @Description: 分割集合
     */
    public static <T> List<List<T>> split(List<T> collection, int size) {
        if (isEmpty(collection)) {
            return (List<List<T>>) collection;
        }
        int remainder = collection.size() % size;
        int full = collection.size() / size;
        List<List<T>> result = new ArrayList(full);
        int temp = 0;
        for (int i = 0; i < full; i++) {
            temp = (i + 1) * size;
            result.add(collection.subList(i * size, temp));
        }
        if (remainder > 0) {
            result.add(collection.subList(temp, temp + remainder));
        }
        return result;
    }

}
