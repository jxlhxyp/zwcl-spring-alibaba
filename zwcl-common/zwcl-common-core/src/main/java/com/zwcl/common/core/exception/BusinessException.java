package com.zwcl.common.core.exception;

import com.zwcl.common.core.enums.ExceptionCode;

/**
 * @Description 业务异常包装类
 * @Date 2020/5/17 15:10:00
 * @Created by xyp
 */
public class BusinessException extends BaseException{

    private static final long serialVersionUID = -2303357122330162359L;

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(Integer errorCode, String message) {
        super(errorCode, message);
    }

    public BusinessException(ExceptionCode exceptionCode) {
        super(exceptionCode);
    }

    public BusinessException(Throwable cause) {
        super(cause);
    }
}