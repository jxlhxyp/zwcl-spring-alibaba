package com.zwcl.common.core.domain.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 抽象的用户信息缓存
 */
@Data
@NoArgsConstructor
public class CacheUser implements Serializable {
    /**
     * 用户Id
     */
    private Long userId;

    /**
     * 用户名称，对于C端小程序用户，有可能没有
     */
    private String userName;

    /**
     * 用户来源，C端还是B端
     */
    private String from;

    /**
     * C端小程序用户，缓存该值，如果是B端，写死system
     */
    private String appCode;
}
