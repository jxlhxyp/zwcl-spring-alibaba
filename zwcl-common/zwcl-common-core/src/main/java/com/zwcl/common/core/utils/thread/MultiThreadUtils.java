package com.zwcl.common.core.utils.thread;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;


/**
 * @ClassName: MultiThreadUtils.java
 * @Description: 多线程工具类
 *  * TODO: 多线程例子  CompletableFuture.allOf(categoryCompletableFuture, brandCompletableFuture, spuCompletableFuture,
 *  *                 skuImagesCompletableFuture, salesCompletableFuture, storeCompletableFuture, saleAttrsCompletableFuture,
 *  *                 saleAttrCompletableFuture, skusJsonCompletableFuture, spuImagesCompletableFuture, groupCompletableFuture).join();
 *  例子：https://www.yuque.com/gavincoder/sryby9/onk6r1
 *  注意：使用这个类要注意内存消耗情况
 *  原则上100万数据，不允许一次传递List就是100万，而应该在外面分批次，比如5万一个批次的数据，这个批次的数据采用多线程进行处理。
 */
@SuppressWarnings({ "all" })
public class MultiThreadUtils<T,E> {

    private static Logger logger = LoggerFactory.getLogger(MultiThreadUtils.class);

    // 线程个数，如不赋值，默认为5
    private int threadCount = 10;
    //线程中的这个批次的数据，是一条条分散执行还是一次执行
    private boolean batchFlag=false;
    // 具体业务任务
    private ITask<ResultBean<T>, E> task;
    // 线程池管理器
    private CompletionService<ResultBean> pool = null;

    /**
     * 初始化线程池和线程个数<BR>
     */
    public static MultiThreadUtils newInstance(int threadCount,boolean batchFlag) {
        MultiThreadUtils instance = new MultiThreadUtils();
        //threadCount = threadCount;
        //batchFlag = batchFlag;
        instance.setThreadCount(threadCount);
        instance.setBatchFlag(batchFlag);
        return instance;
    }

    /**
     * 初始化线程池和线程个数<BR>
     */
    public static MultiThreadUtils newInstance(int threadCount) {
        MultiThreadUtils instance = new MultiThreadUtils();
        //threadCount = threadCount;
        //batchFlag = batchFlag;
        instance.setThreadCount(threadCount);
        return instance;
    }

    /**
     *
     * 多线程分批执行list中的任务<BR>
     * 方法名：execute<BR>
     */
    public ResultBean execute(List<E> data, Map<String, Object> params, ITask<ResultBean<T>, E> task) {
        // 创建线程池
        ExecutorService threadpool = Executors.newFixedThreadPool(threadCount);
        // 根据线程池初始化线程池管理器
        pool = new ExecutorCompletionService<ResultBean>(threadpool);
        // 开始时间（ms）
        long l = System.currentTimeMillis();
        // 数据量大小
        int length = data.size();
        // 每个线程处理的数据个数
        int taskCount = length / threadCount;
        // 划分每个线程调用的数据
        for (int i = 0; i < threadCount; i++) {
            // 每个线程任务数据list
            List<E> subData = null;
            if (i == (threadCount - 1)) {
                subData = data.subList(i * taskCount, length);
            } else {
                subData = data.subList(i * taskCount, (i + 1) * taskCount);
            }
            // 将数据分配给各个线程
            HandleCallable execute = new HandleCallable<T,E>(String.valueOf(i), subData, params, task,batchFlag);
            // 将线程加入到线程池
            pool.submit(execute);
        }

        // 总的返回结果集
        List<ResultBean<T>> result = new ArrayList<>();
        for (int i = 0; i < threadCount; i++) {
            // 每个线程处理结果集
            ResultBean<List<ResultBean<T>>> threadResult;
            try {
                threadResult = pool.take().get();
                result.addAll(threadResult.getData());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

        }
        // 关闭线程池
        threadpool.shutdownNow();
        // 执行结束时间
        long end_l = System.currentTimeMillis();
        logger.info("总耗时:{}ms", (end_l - l));
        return ResultBean.newInstance().setData(result);
    }

    public int getThreadCount() {
        return threadCount;
    }

    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }

    public void setBatchFlag(boolean batchFlag){
        this.batchFlag = batchFlag;
    }
}
