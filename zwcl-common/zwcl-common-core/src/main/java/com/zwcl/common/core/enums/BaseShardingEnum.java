package com.zwcl.common.core.enums;

import lombok.Data;

@Data
public class BaseShardingEnum {
    /**分片表名*/
    private String tableName;
    /**分片键*/
    private String shardingKey;
    /**系统标识*/
    private String bizType;
    /**主键规则版本*/
    private String idVersion;
    /**表名字母前缀*/
    private String charsPrefix;
    /**分片键值中纯数字起始下标索引，第一位是0,第二位是1，依次类推*/
    private int numberStartIndex;
    /**数据库索引位开始下标索引*/
    private int dbIndexBegin;
    /**表索引位开始下标索引*/
    private int tbIndexBegin;
    /**分布所在库数量*/
    private int dbCount;
    /**分布所在表数量-所有库中表数量总计*/
    private int tbCount;
    /**描述*/
    private String desc;
}
