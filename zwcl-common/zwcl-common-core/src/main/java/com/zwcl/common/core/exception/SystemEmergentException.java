package com.zwcl.common.core.exception;

import com.zwcl.common.core.enums.ExceptionCode;

/**
 * @Description 紧急升级的提示，给网关处用
 * @Date 2020/5/17 15:10:00
 * @Created by xyp
 */
public class SystemEmergentException extends BaseException{

    private static final long serialVersionUID = -2303357122330162359L;

    public SystemEmergentException(String message) {
        super(message);
    }

    public SystemEmergentException(Integer errorCode, String message) {
        super(errorCode, message);
    }

    public SystemEmergentException(ExceptionCode exceptionCode) {
        super(exceptionCode);
    }

    public SystemEmergentException(Throwable cause) {
        super(cause);
    }
}