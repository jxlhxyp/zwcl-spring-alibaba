package com.zwcl.common.core.utils;

@FunctionalInterface
public interface FieldStuffer<E> {

	void fill(E record);

}
