/*
 * Copyright 2019-2029 geekidea(https://github.com/geekidea)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zwcl.common.mybatis.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zwcl.common.core.exception.BusinessException;
import com.zwcl.common.core.exception.DbOperateException;

/**
 * 公共Service接口
 *
 * @author geekidea
 * @date 2018-11-08
 */
public interface BaseService<T> extends IService<T> {
//    /**
//     * 保存
//     *
//     * @param entity
//     * @return
//     * @throws Exception
//     */
//    boolean saveT(T entity) throws DbOperateException;
//
//    /**
//     * 修改
//     *
//     * @param entity
//     * @return
//     * @throws Exception
//     */
//    boolean updateT(T entity) throws DbOperateException;
//
//    /**
//     * 删除
//     *
//     * @param id
//     * @return
//     * @throws Exception
//     */
//    boolean deleteT(Long id) throws DbOperateException;
//
//    T selectT(Long id) throws DbOperateException;

//    /**
//     * 获取分页对象
//     *
//     * @param glassPaperQueryParam
//     * @return
//     * @throws Exception
//     */
//    Page<T> getPaperPageList(PaperPageParam paperPageParam) throws Exception;

}
