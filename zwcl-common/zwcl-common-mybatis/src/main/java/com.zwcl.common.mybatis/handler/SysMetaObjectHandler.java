package com.zwcl.common.mybatis.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;

//mybatisPlus填充审计字段
//TODO:mybatis自带19位的雪花算法的id生成器
//TODO:差填充当前用户信息
@Slf4j
@Component
public class SysMetaObjectHandler implements MetaObjectHandler {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void insertFill(MetaObject metaObject) {
        logger.info("正在调用该insert填充字段方法");
        if(metaObject.hasGetter("createTime")) {
            Object createTime = getFieldValByName("createTime", metaObject);
            if (null == createTime) {
                this.strictInsertFill(metaObject, "createTime", LocalDateTime.class, LocalDateTime.now());
            }
        }
        if(metaObject.hasGetter("updateTime")) {
            Object updateTime = getFieldValByName("updateTime", metaObject);
            if (null == updateTime) {
                this.strictInsertFill(metaObject, "updateTime", LocalDateTime.class, LocalDateTime.now());
            }
        }
//        if(metaObject.hasGetter("createTime")) {
//            Object createTime = getFieldValByName("createTime", metaObject);
//            Object createBy = getFieldValByName("createBy", metaObject);
//            Object updateTime = getFieldValByName("updateTime", metaObject);
//            Object updateBy = getFieldValByName("updateBy", metaObject);
//            //Object id = getFieldValByName("id",metaObject);
//
//            if (null == createTime) {
//                setFieldValByName("createTime", LocalDateTime.now(), metaObject);
//            }
//            if (null == updateTime) {
//                setFieldValByName("updateTime", LocalDateTime.now(), metaObject);
//            }
////            if (LoginUserContext.getLoginUser() != null) {
////                if (null == createBy) {
////                    setFieldValByName("createBy", LoginUserContext.getLoginUser().getId(), metaObject);
////                }
////                if (null == updateBy) {
////                    setFieldValByName("updateBy", LoginUserContext.getLoginUser().getId(), metaObject);
////                }
////            }
//        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        logger.info("正在调用该update填充字段方法");
        if(metaObject.hasGetter("updateTime")) {
            Object updateTime = getFieldValByName("updateTime", metaObject);
            if (null == updateTime) {
                this.strictInsertFill(metaObject, "updateTime", LocalDateTime.class, LocalDateTime.now()); // 起始版本 3.3.0(推荐)
            }
        }
//        if(metaObject.hasGetter("updateTime")) {
//            Object updateTime = getFieldValByName("updateTime", metaObject);
//            if (null == updateTime) {
//                setFieldValByName("updateTime", LocalDateTime.now(), metaObject);
//            }
//            Object updateBy = getFieldValByName("updateBy", metaObject);
////            if (null == updateBy) {
////                if (LoginUserContext.getLoginUser() != null) {
////                    setFieldValByName("updateBy", LoginUserContext.getLoginUser().getId(), metaObject);
////                }
////            }
//        }
    }
}
