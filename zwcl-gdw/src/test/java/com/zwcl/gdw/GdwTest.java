package com.zwcl.gdw;

import com.zwcl.common.core.utils.JsonUtils;
import com.zwcl.common.core.utils.yzm.QRCodeUtil;
import com.zwcl.gdw.vo.GdwMatchVo;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class GdwTest {

    @Test
    public void JsonTest(){
        String str= "{\"info\":\"成功\",\"receiveTime\":\"2021-03-17 13:35:26\",\"result_code\":\"1\"}";
        GdwMatchVo vo = JsonUtils.jsonToPojo(str, GdwMatchVo.class);
        log.info("实体:{}",vo);
    }

    /**
     * 生成图片
     * @throws Exception
     */
    @Test
    public void QrBuild() throws Exception {
        String imgBase64="";
        try {
            //生成二维码
            BufferedImage img = QRCodeUtil.encode("https://baidu.com", true);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();//io流
            ImageIO.write(img, "png", baos);//写入流中
            byte[] bytes = baos.toByteArray();//转换成字节
            BASE64Encoder encoder = new BASE64Encoder();
            String png_base64 = encoder.encodeBuffer(bytes).trim();//转换成base64串
            png_base64 = png_base64.replaceAll("\n", "").replaceAll("\r", "");//删除 \r\n
            imgBase64 = "data:image/png;base64," + png_base64;
            log.info(imgBase64);
        } catch (Exception e) {
            log.error("获取二维码图片失败" + e);
        }
    }
}
