package com.zwcl.gdw.test;

import com.zwcl.common.core.utils.AesUtil;
import com.zwcl.common.core.utils.JsonUtils;
import com.zwcl.common.core.utils.MD5Utils;
import com.zwcl.common.core.utils.ObjectMapUtil;
import com.zwcl.common.web.domain.SignRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import java.nio.charset.StandardCharsets;
import java.util.*;

@Slf4j
public class SignRequestTest {

    private static String appId="weibao";
    private static String appKey="59bc6455e8ba4c25bf2a07622caf023b";

    public static void main(String[] args) {
        Map<String,Object > reqParameter = new HashMap<>();
        reqParameter.put("plateNo","赣A0668L");
        reqParameter.put("idNum","360103198410065011");
        reqParameter.put("userName","谢永平");
        Map<String,Object > reqMap = commonParameter(reqParameter);
        StringBuilder data=getData(reqMap).append("?key=").append(appKey);
        log.info("即将进行md5的串：{}",data.toString());
        String sign= MD5Utils.md5Hex(data.toString());
        reqMap.put("sign", sign);
        log.info("人车匹配接口参数：{}",JsonUtils.objectToJson(reqMap));

        log.info("uuID:{}",UUID.randomUUID());
    }

    /***
     * 公共请求参数组装
     * @return
     */
    public static Map<String,Object > commonParameter(Map<String,Object > reqParameter){
        SignRequest signRequest=new SignRequest();
        signRequest.setAppId(appId);
        signRequest.setTimestamp(System.currentTimeMillis());
        //signRequest.setTimestamp(1616394568924L);
        // 组装请求参数
        try {
            log.info("参数Json串：{}",JsonUtils.objectToJson(reqParameter));
            String aesEnParam= AesUtil.encrypt(JsonUtils.objectToJson(reqParameter),appKey);
            log.info("Aes加密后的参数：{}",aesEnParam);
            String aesDnParam = AesUtil.decrypt(aesEnParam,appKey);
            log.info("Aes解密后的参数：{}",aesDnParam);
            //将base64编码，换成aes加密
            //signRequest.setRequestData(Base64.getEncoder().encodeToString(JsonUtils.objectToJson(reqParameter).getBytes(StandardCharsets.UTF_8)));
            signRequest.setRequestData(aesEnParam);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  ObjectMapUtil.bean2Map(signRequest);
    }

    public static StringBuilder getData(Map<String, Object> map) {
        map.remove("sign");
        List<Map.Entry<String, Object>> infoIds = new ArrayList<>(map.entrySet());
        // 对所有传入参数按照字段名的 ASCII 码从小到大排序（字典序）
        Collections.sort(infoIds, new Comparator<Map.Entry<String, Object>>() {
            @Override
            public int compare(Map.Entry<String, Object> o1, Map.Entry<String, Object> o2) {
                return (o1.getKey()).compareTo(o2.getKey());
            }
        });
        // 构造签名键值对的格式
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, Object> item : infoIds) {
            if (StringUtils.isNoneBlank(item.getKey())) {
                String key = item.getKey();
                Object val = item.getValue();
                if (ObjectUtils.isNotEmpty(val)) {
                    sb.append(key + "=" + val + "&");
                }
            }
        }
        log.info("排序后的串：{}",sb.toString());
        return sb;
    }


}
