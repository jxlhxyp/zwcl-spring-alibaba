package com.zwcl.gdw.mapper;

import com.zwcl.gdw.entity.RcMatchCall;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-16
 */
public interface RcMatchCallMapper extends BaseMapper<RcMatchCall> {

}
