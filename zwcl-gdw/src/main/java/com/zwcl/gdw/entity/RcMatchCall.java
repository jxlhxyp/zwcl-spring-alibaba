package com.zwcl.gdw.entity;

import cn.hutool.core.date.DateTime;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("rc_match_call")
public class RcMatchCall extends BaseEntity<RcMatchCall> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 调用方的标志
     */
    private String appId;

    /**
     * 调用地址，目前一个地址，可以不存
     */
    private String reqUrl;

    /**
     * 调用Ip，先不存
     */
    private String sourceIp;

    /**
     * 车牌号
     */
    private String plateNo;

    /**
     * 省份证号
     */
    private String idNum;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 响应信息
     */
    private String info;

    /**
     * 匹配结果编码
     */
    private Integer resultCode;

    /**
     * 接收时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime receiveTime;

    /**
     * 消耗毫秒数
     */
    private Long costTime;

    /**
     * 1:成功  0:失败
     */
    private Boolean succFlag;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
