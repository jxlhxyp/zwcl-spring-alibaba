package com.zwcl.gdw.entity;

import com.zwcl.common.web.domain.BaseOutEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UserVehicleInfo extends BaseOutEntity {

    @NotBlank(message = "车牌号不能为空")
    @ApiModelProperty(value = "车牌号")
    private String plateNo;

    @NotBlank(message = "身份证不能为空")
    @ApiModelProperty(value = "身份证号")
    private String idNum;

    @NotBlank(message = "姓名不能为空")
    @ApiModelProperty(value = "姓名")
    private String userName;
}
