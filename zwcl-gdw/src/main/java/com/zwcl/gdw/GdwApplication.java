package com.zwcl.gdw;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import springfox.documentation.oas.annotations.EnableOpenApi;

/**
 *  spring boot 加 swagger是另外一种方式
 *  swagger访问路径：http://localhost:7008/api/gdw/swagger-ui/index.html
 */
@SpringBootApplication
@MapperScan("com.zwcl.gdw.mapper")
//采用异步方法，要加上该注解
@EnableAsync
@EnableOpenApi
public class GdwApplication {
    public static void main(String[] args){
        SpringApplication.run(GdwApplication.class, args);
        System.out.println("启动成功");
    }
}
