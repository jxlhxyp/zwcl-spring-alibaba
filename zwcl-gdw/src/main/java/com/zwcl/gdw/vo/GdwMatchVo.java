package com.zwcl.gdw.vo;

import cn.hutool.core.date.DateTime;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

//TODO: 第三方返回的时间2021-03-17T13:35:26，这种类型在此处无法转换为LocalDateTime
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GdwMatchVo implements Serializable {

    private String info;

//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
//    private LocalDateTime receiveTime;

    private String receiveTime;

    private Integer resultCode;

}
