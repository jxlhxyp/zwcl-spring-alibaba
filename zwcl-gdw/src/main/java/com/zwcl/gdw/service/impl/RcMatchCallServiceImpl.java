package com.zwcl.gdw.service.impl;

import com.zwcl.common.core.utils.JsonUtils;
import com.zwcl.gdw.entity.RcMatchCall;
import com.zwcl.gdw.entity.UserVehicleInfo;
import com.zwcl.gdw.mapper.RcMatchCallMapper;
import com.zwcl.gdw.service.RcMatchCallService;
import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import com.zwcl.gdw.vo.GdwMatchVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-16
 */
@Service
public class RcMatchCallServiceImpl extends BaseServiceImpl<RcMatchCallMapper, RcMatchCall> implements RcMatchCallService {

    @Override
    @Async
    public void ayncSaveRcMatchCall(GdwMatchVo vo,String callResult, UserVehicleInfo info,Boolean succFlag,Long mills) {
        try {
            RcMatchCall rcMatchCall = new RcMatchCall();
            rcMatchCall.setPlateNo(info.getPlateNo());
            rcMatchCall.setIdNum(info.getIdNum());
            rcMatchCall.setUserName(info.getUserName());
            rcMatchCall.setSuccFlag(succFlag);
            rcMatchCall.setCostTime(mills);
            rcMatchCall.setAppId(info.getAppId());
            if(null!=vo) {
                //匹配成功
                rcMatchCall.setInfo(vo.getInfo());
                //字符串时间转化为localDateTime
                if(!StringUtils.isBlank(vo.getReceiveTime())){
                    DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    rcMatchCall.setReceiveTime(LocalDateTime.parse(vo.getReceiveTime().replace("T"," "),df));
                }
                //rcMatchCall.setReceiveTime(vo.getReceiveTime());
                rcMatchCall.setResultCode(vo.getResultCode());
            }else {
                rcMatchCall.setInfo(callResult);
            }
            this.save(rcMatchCall);
        }catch (Exception ex){
            log.error("调用记录保存失败：{}",ex);
        }
    }
}
