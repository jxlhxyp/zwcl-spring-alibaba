package com.zwcl.gdw.service;

import com.zwcl.gdw.entity.RcMatchCall;
import com.zwcl.common.mybatis.service.BaseService;
import com.zwcl.gdw.entity.UserVehicleInfo;
import com.zwcl.gdw.vo.GdwMatchVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xieyongping
 * @since 2021-03-16
 */
public interface RcMatchCallService extends BaseService<RcMatchCall> {

     void ayncSaveRcMatchCall(GdwMatchVo vo,String callResult, UserVehicleInfo info,Boolean succFlag,Long mills);
}
