package com.zwcl.codegen;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

//TODO:可能要带上swagger2文档
public class CodeGenerator {

    public static void main(String[] args) {
        // 1、创建代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 2、全局配置
        GlobalConfig gc = new GlobalConfig();
        //String projectPath = System.getProperty("user.dir");
        //gc.setOutputDir(projectPath + "/src/main/java");
        //TODO:修改成你本地的路径信息
        gc.setOutputDir("D:/data/codeGenerator");
        gc.setAuthor("xieyongping");
        gc.setOpen(true); //生成后是否打开资源管理器
        gc.setFileOverride(true); //重新生成时文件是否覆盖
        gc.setActiveRecord(true);// 不需要ActiveRecord特性的请改为false
        gc.setEnableCache(false);// XML 二级缓存
        gc.setBaseResultMap(true);// XML ResultMap
        gc.setBaseColumnList(false);// XML columList
        gc.setServiceName("%sService"); //去掉Service接口的首字母I
//        gc.setIdType(IdType.ID_WORKER_STR); //主键策略
//        gc.setDateType(DateType.ONLY_DATE);//定义生成的实体类中日期类型
//        gc.setSwagger2(false);//开启Swagger2模式
        mpg.setGlobalConfig(gc);


        // 3、数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        //TODO:根据需要修改数据地址
//        dsc.setUrl("jdbc:mysql://10.99.11.183:3306/known_glasses?useUnicode=true&characterEncoding=utf-8&useSSL=false&serverTimezone=GMT");
//        dsc.setUsername("root");
//        dsc.setPassword("zmRoot0918");
        dsc.setUrl("jdbc:mysql://106.53.146.10:3306/glass_shopping?useUnicode=true&characterEncoding=utf-8&useSSL=false&serverTimezone=GMT");
        dsc.setUsername("root");
        dsc.setPassword("Qd14(RE564!ghgejkGHJ");
        dsc.setDbType(DbType.MYSQL);
        mpg.setDataSource(dsc);

        // 4、包配置
        PackageConfig pc = new PackageConfig();
        //TODO:项目不同，包名路径需要修改
        pc.setParent("com.zwcl.glass.shopping.goods");
        pc.setModuleName(""); //模块名
        pc.setController("controller");
        //TODO:是否分层
        //因为分层，设置为如下,不分层则不需要如此划分
        //pc.setEntity("api.entity");
        //不分层的设置
        pc.setEntity("entity");
        pc.setService("service");
        pc.setMapper("mapper");
        mpg.setPackageInfo(pc);

        // 5、策略配置
        StrategyConfig strategy = new StrategyConfig();
        //TODO:生成哪张表，可以多张
        strategy.setInclude(new String[]{"goods_brand","goods_categories","goods_comment","goods_images","goods_sku","goods_sku_spec","goods_spec_name","goods_spec_value","goods_spu","goods_spu_spec"});//对那一张表生成代码
        strategy.setNaming(NamingStrategy.underline_to_camel);//数据库表映射到实体的命名策略
        //TODO: 是否要前缀 生成实体时去掉表前缀
        //strategy.setTablePrefix("glass_");
        strategy.setTablePrefix("");
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);//数据库表字段映射到实体的命名策略
        strategy.setEntityLombokModel(true); // lombok 模型 @Accessors(chain = true) setter链式操作

        strategy.setRestControllerStyle(true); //restful api风格控制器
        strategy.setControllerMappingHyphenStyle(false); //url中驼峰转连字符
        //TODO:设置是否要继承公共的BaseEntity，表中如果有公共字段，建议采用继承的方式
        strategy.setSuperEntityColumns(new String[]{"create_time","update_time","create_by","update_by","del_flag"});
        strategy.setSuperEntityClass("com.zwcl.common.core.domain.entity.BaseEntity");
        // 自定义 service 父类
        strategy.setSuperServiceClass("com.zwcl.common.mybatis.service.BaseService");
        // 自定义 service 实现类父类
        strategy.setSuperServiceImplClass("com.zwcl.common.mybatis.service.impl.BaseServiceImpl");
        mpg.setStrategy(strategy);

        // 6、执行
        mpg.execute();
    }
}