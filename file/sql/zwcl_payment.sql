/*
 Navicat Premium Data Transfer

 Source Server         : 知道眼镜
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : 106.53.146.10:3306
 Source Schema         : zwcl_payment

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 19/03/2021 13:54:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for payment_account
-- ----------------------------
DROP TABLE IF EXISTS `payment_account`;
CREATE TABLE `payment_account`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `bussiness_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '业务',
  `accout_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '账号名称',
  `mch_type` int(10) NOT NULL COMMENT '商户账号类型',
  `app_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `sub_app_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `mch_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '微信支付商户号 ',
  `sub_mch_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '微信支付子商户号 ',
  `secret_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '对应的mch_id商户号的api密钥',
  `sub_secret_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '对应的sub_mch_id商户号的api密钥',
  `del_flag` tinyint(4) NULL DEFAULT NULL COMMENT '删除状态 0：默认 1：删除',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建人',
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_mch_type`(`mch_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '微信商户账号' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of payment_account
-- ----------------------------
INSERT INTO `payment_account` VALUES (1, '眼镜业务', '安徽高灯', 12, 'wxf0e0f0bba2c1a0ae', 'wx1ed5f152443c917c', '1491962812', '1550137551', '6de8f3d3279583e1d59feeffabd9997c', '6de8f3d3279583e1d59feeffabd9997c', 0, '2020-04-16 17:20:41', '2020-04-16 17:20:44', '系统', '系统');

-- ----------------------------
-- Table structure for payment_channel
-- ----------------------------
DROP TABLE IF EXISTS `payment_channel`;
CREATE TABLE `payment_channel`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `channel_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '渠道名称',
  `channel_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '渠道ID',
  `merchant_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商户id',
  `sync_url` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '同步回调URL',
  `asyn_url` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '异步回调URL',
  `public_key` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '公钥',
  `private_key` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '私钥',
  `channel_state` int(11) NULL DEFAULT 0 COMMENT '渠道状态 0开启1关闭',
  `revision` int(11) NULL DEFAULT NULL COMMENT '乐观锁',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`, `channel_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '支付渠道 ' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for payment_log
-- ----------------------------
DROP TABLE IF EXISTS `payment_log`;
CREATE TABLE `payment_log`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `payment_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '支付流水号',
  `pay_status` tinyint(2) NOT NULL COMMENT '状态，1-待支付，2-已支付，3-支付失败，4-已取消',
  `operation_info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '操作信息',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_payment_no`(`payment_no`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '支付流水日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for payment_main
-- ----------------------------
DROP TABLE IF EXISTS `payment_main`;
CREATE TABLE `payment_main`  (
  `id` bigint(20) UNSIGNED NOT NULL,
  `payment_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '支付流水号',
  `user_id` int(12) UNSIGNED NOT NULL DEFAULT 0 COMMENT '下单人uid',
  `open_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'openId',
  `order_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '业务订单号',
  `order_type` tinyint(4) UNSIGNED NULL DEFAULT 0 COMMENT '订单类型',
  `transaction_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '微信支付订单号',
  `pay_status` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '状态，1-待支付，2-已支付，3-支付失败，4-支付中，5-已取消',
  `refund_status` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '状态，0-无退款，1-有退款',
  `origin_amount` decimal(10, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '订单原金额（元）',
  `pay_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '实际支付金额（元）',
  `discount_amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `service_amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '手续费',
  `refund_amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '退款金额',
  `payment_type` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '支付类型，1-手动支付，2-免密支付',
  `topay_time` datetime(0) NULL DEFAULT NULL COMMENT '发起支付时间',
  `pay_time` datetime(0) NULL DEFAULT NULL COMMENT '支付完成时间',
  `callback_time` datetime(0) NULL DEFAULT NULL COMMENT '回调时间',
  `mch_type` int(10) NULL DEFAULT 0 COMMENT '商户类型',
  `pay_way` tinyint(4) NOT NULL DEFAULT 0 COMMENT '支付渠道，1-微信支付  2-支付宝支付',
  `product_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '支付产品id',
  `product_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '支付产品名称',
  `bank_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '银行类型，微信回调时的字段',
  `err_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '失败码',
  `err_msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '失败信息',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unq_payment_no`(`payment_no`) USING BTREE,
  INDEX `nor_uid`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '支付流水表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for payment_refund
-- ----------------------------
DROP TABLE IF EXISTS `payment_refund`;
CREATE TABLE `payment_refund`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `refund_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '退款流水号',
  `payment_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '支付流水号',
  `user_id` int(12) NOT NULL COMMENT '用户ID',
  `order_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '订单号',
  `order_type` tinyint(2) NOT NULL DEFAULT 0 COMMENT '订单类型',
  `pay_amount` decimal(10, 2) NOT NULL COMMENT '原支付金额',
  `need_refund_amount` decimal(10, 2) NOT NULL COMMENT '需退款金额',
  `reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '退款原因',
  `refund_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态，1-待退款，2-退款中，3-已退款，4-退款失败',
  `manual` tinyint(4) NULL DEFAULT 0 COMMENT '是否手动退款，1-是，2-否',
  `pay_way` tinyint(4) NULL DEFAULT 0 COMMENT '支付渠道',
  `refund_time` datetime(0) NULL DEFAULT NULL COMMENT '退款完成时间',
  `callback_time` datetime(0) NULL DEFAULT NULL COMMENT '回调时间',
  `refund_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '实际退款金额（元）',
  `refund_recv_accout` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '退款入账账户',
  `out_refund_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '第三方退款单号',
  `out_trade_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '第三方支付单号',
  `payment_type` tinyint(1) NULL DEFAULT 0 COMMENT '支付类型',
  `mch_type` int(10) NULL DEFAULT NULL COMMENT '商户号类型',
  `err_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '失败码',
  `err_msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '失败信息',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `refund_no`(`refund_no`) USING BTREE,
  INDEX `payment_no`(`payment_no`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '退款流水表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for payment_refund_log
-- ----------------------------
DROP TABLE IF EXISTS `payment_refund_log`;
CREATE TABLE `payment_refund_log`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `refund_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '退款流水号',
  `refund_status` tinyint(2) NOT NULL COMMENT '状态，1-待退款，2-退款中，3-已退款，4-退款失败',
  `operation_info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '操作信息',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_refund_no`(`refund_no`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '退款流水日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for payment_request
-- ----------------------------
DROP TABLE IF EXISTS `payment_request`;
CREATE TABLE `payment_request`  (
  `id` bigint(20) NOT NULL,
  `request_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态，0-未处理，1-已处理',
  `call_type` tinyint(4) NULL DEFAULT NULL COMMENT '调用类型，1-http 2-Mq',
  `request_type` tinyint(4) NOT NULL COMMENT '请求类型,看代码枚举',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '请求参数',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建人',
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新人',
  `del_flag` tinyint(4) NULL DEFAULT 0 COMMENT '删除状态 0：默认 1：删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '请求数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of payment_request
-- ----------------------------
INSERT INTO `payment_request` VALUES (1370651631308206082, 1, 1, 1, '{\"openId\":\"zzzz\",\"orderNo\":\"10310004\",\"orderType\":1,\"payAmount\":100,\"payWay\":1,\"productName\":\"钛框\",\"userId\":6}', '2021-03-13 16:23:09', '2021-03-13 16:23:09', NULL, NULL, 0);
INSERT INTO `payment_request` VALUES (1370654264987152386, 1, 1, 1, '{\"openId\":\"zzzz\",\"orderNo\":\"10310004\",\"orderType\":1,\"payAmount\":100,\"payWay\":1,\"productName\":\"钛框\",\"userId\":6}', '2021-03-13 16:33:37', '2021-03-13 16:33:37', NULL, NULL, 0);
INSERT INTO `payment_request` VALUES (1370656663726424066, 1, 1, 1, '{\"openId\":\"zzzz\",\"orderNo\":\"10310004\",\"orderType\":1,\"payAmount\":100,\"payWay\":1,\"productName\":\"钛框\",\"userId\":6}', '2021-03-13 16:43:09', '2021-03-13 16:43:09', NULL, NULL, 0);
INSERT INTO `payment_request` VALUES (1370658346292432897, 1, 1, 1, '{\"openId\":\"zzzz\",\"orderNo\":\"10310004\",\"orderType\":1,\"payAmount\":100,\"payWay\":1,\"productName\":\"钛框\",\"userId\":6}', '2021-03-13 16:49:52', '2021-03-13 16:49:52', NULL, NULL, 0);
INSERT INTO `payment_request` VALUES (1370658971201757185, 1, 1, 1, '{\"openId\":\"zzzz\",\"orderNo\":\"10310004\",\"orderType\":1,\"payAmount\":100,\"payWay\":1,\"productName\":\"钛框\",\"userId\":6}', '2021-03-13 16:52:19', '2021-03-13 16:52:19', NULL, NULL, 0);
INSERT INTO `payment_request` VALUES (1370659286961590273, 1, 1, 1, '{\"openId\":\"zzzz\",\"orderNo\":\"10310004\",\"orderType\":1,\"payAmount\":100,\"payWay\":1,\"productName\":\"钛框\",\"userId\":6}', '2021-03-13 16:53:34', '2021-03-13 16:53:34', NULL, NULL, 0);

SET FOREIGN_KEY_CHECKS = 1;
