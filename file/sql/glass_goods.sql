/*
 Navicat Premium Data Transfer

 Source Server         : 知道眼镜
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : 106.53.146.10:3306
 Source Schema         : glass_goods

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 14/01/2021 16:05:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for glass_brand
-- ----------------------------
DROP TABLE IF EXISTS `glass_brand`;
CREATE TABLE `glass_brand`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '品牌描述',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 99 COMMENT '排序字段',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of glass_brand
-- ----------------------------
INSERT INTO `glass_brand` VALUES (1, '明月', '明月的品牌描述', 99, '2021-01-13 13:40:06', NULL, NULL, NULL, 0);
INSERT INTO `glass_brand` VALUES (2, '依视路', '依视路的介绍', 99, '2021-01-13 13:43:06', NULL, NULL, NULL, 0);
INSERT INTO `glass_brand` VALUES (3, '苹果', '苹果介绍', 99, '2021-01-13 15:06:58', NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for glass_categories
-- ----------------------------
DROP TABLE IF EXISTS `glass_categories`;
CREATE TABLE `glass_categories`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父级分类ID，0为顶级分类',
  `cate_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '分类名称',
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT 99 COMMENT '排序字段',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `categories_cate_name_unique`(`cate_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of glass_categories
-- ----------------------------
INSERT INTO `glass_categories` VALUES (1, 0, '镜片', 99, '2021-01-13 13:41:23', NULL, NULL, NULL, 0);
INSERT INTO `glass_categories` VALUES (2, 1, '国内镜片', 99, '2021-01-13 13:42:00', NULL, NULL, NULL, 0);
INSERT INTO `glass_categories` VALUES (3, 1, '进口镜片', 99, '2021-01-13 13:42:15', NULL, NULL, NULL, 0);
INSERT INTO `glass_categories` VALUES (4, 0, '智能手机', 99, '2021-01-13 15:07:28', NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for glass_goods_images
-- ----------------------------
DROP TABLE IF EXISTS `glass_goods_images`;
CREATE TABLE `glass_goods_images`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `spu_id` int(10) UNSIGNED NOT NULL COMMENT '商品ID',
  `link` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '图片URL地址',
  `position` smallint(5) UNSIGNED NULL DEFAULT NULL COMMENT '图片位置',
  `is_master` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否主图: 1是,0否',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `goods_images_goods_id_foreign`(`spu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of glass_goods_images
-- ----------------------------
INSERT INTO `glass_goods_images` VALUES (1, 1, 'https://marketing.golcer.cn/zwcl/glass/20210112/a81d354cf62546808046db4f1d665015.png', NULL, 1, '2021-01-13 15:56:39', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_images` VALUES (2, 1, 'https://marketing.golcer.cn/zwcl/glass/20210112/a81d354cf62546808046db4f1d665015.png', NULL, 0, '2021-01-13 15:56:57', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_images` VALUES (3, 1, 'https://marketing.golcer.cn/zwcl/glass/20210112/a81d354cf62546808046db4f1d665015.png', NULL, 0, '2021-01-13 15:57:03', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_images` VALUES (4, 1, 'https://marketing.golcer.cn/zwcl/glass/20210113/7ece9e1cd7a74b28bbb056525c2b0561.png', NULL, 0, '2021-01-13 15:58:32', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_images` VALUES (5, 2, 'https://marketing.golcer.cn/zwcl/glass/20210113/7ece9e1cd7a74b28bbb056525c2b0561.png', NULL, 1, '2021-01-13 15:58:43', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_images` VALUES (6, 2, 'https://marketing.golcer.cn/zwcl/glass/20210113/3f4c99f94c0d4fe18eff0b42361f639a.png', NULL, 0, '2021-01-13 15:59:01', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_images` VALUES (7, 2, 'https://marketing.golcer.cn/zwcl/glass/20210113/3f4c99f94c0d4fe18eff0b42361f639a.png', NULL, 0, '2021-01-13 15:59:08', NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for glass_goods_property
-- ----------------------------
DROP TABLE IF EXISTS `glass_goods_property`;
CREATE TABLE `glass_goods_property`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `spu_id` int(10) UNSIGNED NOT NULL COMMENT '商品ID',
  `prop_name_id` int(10) UNSIGNED NOT NULL COMMENT '属性名ID',
  `prop_value_id` int(10) UNSIGNED NOT NULL COMMENT '属性值ID',
  `is_sale` tinyint(1) NULL DEFAULT 1 COMMENT '1在售，0停售',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `goods_property_prop_name_id_foreign`(`prop_name_id`) USING BTREE,
  INDEX `goods_property_prop_value_id_foreign`(`prop_value_id`) USING BTREE,
  INDEX `goods_property_goods_id_foreign`(`spu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of glass_goods_property
-- ----------------------------
INSERT INTO `glass_goods_property` VALUES (1, 1, 1, 1, 1, '2021-01-13 15:27:41', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_property` VALUES (2, 1, 1, 2, 1, '2021-01-13 15:47:09', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_property` VALUES (3, 1, 1, 3, 1, '2021-01-13 15:51:49', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_property` VALUES (4, 1, 2, 4, 1, '2021-01-13 15:52:00', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_property` VALUES (5, 1, 2, 5, 1, '2021-01-13 15:52:08', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_property` VALUES (6, 2, 3, 6, 1, '2021-01-13 15:52:50', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_property` VALUES (7, 2, 3, 7, 1, '2021-01-13 15:52:59', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_property` VALUES (8, 2, 4, 8, 1, '2021-01-13 15:53:07', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_property` VALUES (9, 2, 4, 9, 1, '2021-01-13 15:53:21', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_property` VALUES (10, 2, 4, 10, 1, '2021-01-13 15:53:29', NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for glass_goods_sku
-- ----------------------------
DROP TABLE IF EXISTS `glass_goods_sku`;
CREATE TABLE `glass_goods_sku`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `spu_id` int(10) UNSIGNED NOT NULL COMMENT '商品ID',
  `sku_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'sku编码',
  `sku_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'sku名称',
  `stock` int(10) UNSIGNED NOT NULL COMMENT 'SKU库存',
  `price` decimal(10, 2) UNSIGNED NOT NULL COMMENT '商品售价',
  `properties` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '商品属性表ID，以逗号分隔，这个是不是要移动到spu',
  `outer_sku_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '外部商品的sku_Id',
  `jd_price` decimal(20, 2) NULL DEFAULT NULL COMMENT '京东价格',
  `tb_price` decimal(20, 2) NULL DEFAULT NULL COMMENT '淘宝价格',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '状态:1启用,0禁用',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `goods_sku_goods_id_foreign`(`spu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of glass_goods_sku
-- ----------------------------
INSERT INTO `glass_goods_sku` VALUES (1, 1, NULL, '苹果10 32G 白色', 100, 5000.00, ',1:1,2:4,', NULL, NULL, NULL, 1, '2021-01-13 16:16:57', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_sku` VALUES (2, 1, NULL, '苹果10 32G 黑色', 100, 5000.00, ',1:2,2:4,', NULL, NULL, NULL, 1, '2021-01-13 16:17:13', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_sku` VALUES (3, 1, NULL, '苹果10 32G 玫瑰红', 100, 5000.00, ',1:3,2:4,', NULL, NULL, NULL, 1, '2021-01-13 16:17:16', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_sku` VALUES (4, 1, NULL, '苹果10 64G 白色', 100, 6000.00, ',1:1,2:5,', NULL, NULL, NULL, 1, '2021-01-13 16:17:23', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_sku` VALUES (5, 1, NULL, '苹果10 64G 黑色', 100, 6000.00, ',1:2,2:5,', NULL, NULL, NULL, 1, '2021-01-13 16:17:27', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_sku` VALUES (6, 1, NULL, '苹果10 64G 玫瑰红', 100, 6000.00, ',1:3,2:5,', NULL, NULL, NULL, 1, '2021-01-13 16:17:31', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_sku` VALUES (7, 2, NULL, '明月 防蓝光 1.56', 100, 400.00, ',3:6,4:8,', NULL, NULL, NULL, 1, '2021-01-13 16:17:34', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_sku` VALUES (8, 2, NULL, '明月 防蓝光 1.71', 100, 500.00, ',3:6,4:9,', NULL, NULL, NULL, 1, '2021-01-13 16:17:38', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_sku` VALUES (9, 2, NULL, '明月 防蓝光 1.74', 100, 600.00, ',3:6,4:10,', NULL, NULL, NULL, 1, '2021-01-13 16:17:42', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_sku` VALUES (10, 2, NULL, '明月 普通 1.56', 100, 300.00, ',3:7,4:8,', NULL, NULL, NULL, 1, '2021-01-13 16:17:45', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_sku` VALUES (11, 2, NULL, '明月 普通 1.71', 100, 400.00, ',3:7,4:9,', NULL, NULL, NULL, 1, '2021-01-13 16:17:49', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_sku` VALUES (12, 2, NULL, '明月 普通 1.74', 100, 500.00, ',3:7,4:10,', NULL, NULL, NULL, 1, '2021-01-13 16:17:56', NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for glass_goods_spu
-- ----------------------------
DROP TABLE IF EXISTS `glass_goods_spu`;
CREATE TABLE `glass_goods_spu`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `spu_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '商品编号，唯一',
  `goods_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '商品名称',
  `brand_id` int(10) UNSIGNED NOT NULL COMMENT '品牌ID',
  `cate_id` int(10) UNSIGNED NOT NULL COMMENT '分类ID',
  `low_price` decimal(10, 2) UNSIGNED NULL DEFAULT 0.00 COMMENT '最低售价',
  `hight_price` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '最高售价',
  `original` decimal(10, 2) UNSIGNED NULL DEFAULT NULL COMMENT '商品原价',
  `tags` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '商品标签',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '商品内容',
  `summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '商品描述',
  `is_sale` tinyint(4) NOT NULL DEFAULT 1 COMMENT '上架状态: 1是0是',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `goods_brand_id_foreign`(`brand_id`) USING BTREE,
  INDEX `goods_cate_id_foreign`(`cate_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of glass_goods_spu
-- ----------------------------
INSERT INTO `glass_goods_spu` VALUES (1, '', '苹果10', 3, 4, 5000.00, 6000.00, NULL, '|手机|苹果|', NULL, NULL, 1, '2021-01-14 13:33:48', NULL, NULL, NULL, 0);
INSERT INTO `glass_goods_spu` VALUES (2, NULL, '明月玻璃1代', 1, 0, 300.00, 600.00, NULL, '|镜片|明月|国产|', NULL, NULL, 1, '2021-01-14 13:33:54', NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for glass_property_name
-- ----------------------------
DROP TABLE IF EXISTS `glass_property_name`;
CREATE TABLE `glass_property_name`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '属性名',
  `cate_id` int(10) UNSIGNED NOT NULL COMMENT '分类ID',
  `is_allow_alias` tinyint(4) NULL DEFAULT 0 COMMENT '是否允许别名: 1是0否',
  `is_color` tinyint(4) NULL DEFAULT 0 COMMENT '是否颜色属性: 1是0否',
  `is_enum` tinyint(4) NULL DEFAULT 0 COMMENT '是否枚举: 1是0否',
  `is_input` tinyint(4) NULL DEFAULT 0 COMMENT '是否输入属性: 1是0否',
  `is_key` tinyint(4) NULL DEFAULT 0 COMMENT '是否关键属性: 1是0否',
  `is_sale` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否销售属性:1是0否',
  `is_search` tinyint(4) NULL DEFAULT 0 COMMENT '是否搜索字段: 1是0否',
  `is_must` tinyint(4) NULL DEFAULT 0 COMMENT '是否必须属性: 1是0否',
  `is_multi` tinyint(4) NULL DEFAULT 0 COMMENT '是否多选: 1是0否',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '状态: 1启用,0禁用',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 99 COMMENT '排序字段',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `property_name_cate_id_foreign`(`cate_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of glass_property_name
-- ----------------------------
INSERT INTO `glass_property_name` VALUES (1, '颜色', 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 99, '2021-01-13 15:11:53', NULL, NULL, NULL, 0);
INSERT INTO `glass_property_name` VALUES (2, '内存', 4, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 99, '2021-01-13 15:12:07', NULL, NULL, NULL, 0);
INSERT INTO `glass_property_name` VALUES (3, '防蓝光', 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 99, '2021-01-13 15:14:49', NULL, NULL, NULL, 0);
INSERT INTO `glass_property_name` VALUES (4, '折射率', 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 99, '2021-01-13 15:15:15', NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for glass_property_value
-- ----------------------------
DROP TABLE IF EXISTS `glass_property_value`;
CREATE TABLE `glass_property_value`  (
  `id` int(10) NOT NULL,
  `prop_name_id` int(10) NULL DEFAULT NULL,
  `value` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of glass_property_value
-- ----------------------------
INSERT INTO `glass_property_value` VALUES (1, 1, '白色', '2021-01-13 15:15:44', NULL, NULL, NULL, 0);
INSERT INTO `glass_property_value` VALUES (2, 1, '黑色', '2021-01-13 15:15:55', NULL, NULL, NULL, 0);
INSERT INTO `glass_property_value` VALUES (3, 1, '玫瑰红', '2021-01-13 15:16:11', NULL, NULL, NULL, 0);
INSERT INTO `glass_property_value` VALUES (4, 2, '32G', '2021-01-13 15:16:23', NULL, NULL, NULL, 0);
INSERT INTO `glass_property_value` VALUES (5, 2, '64G', '2021-01-13 15:16:33', NULL, NULL, NULL, 0);
INSERT INTO `glass_property_value` VALUES (6, 3, '是', '2021-01-13 15:17:22', NULL, NULL, NULL, 0);
INSERT INTO `glass_property_value` VALUES (7, 3, '否', '2021-01-13 15:17:34', NULL, NULL, NULL, 0);
INSERT INTO `glass_property_value` VALUES (8, 4, '1.56', '2021-01-13 15:17:45', NULL, NULL, NULL, 0);
INSERT INTO `glass_property_value` VALUES (9, 4, '1.71', '2021-01-13 15:17:55', NULL, NULL, NULL, 0);
INSERT INTO `glass_property_value` VALUES (10, 4, '1.74', '2021-01-13 15:18:13', NULL, NULL, NULL, 0);

SET FOREIGN_KEY_CHECKS = 1;
