/*
 Navicat Premium Data Transfer

 Source Server         : 知道眼镜
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : 106.53.146.10:3306
 Source Schema         : glass_shopping

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 19/03/2021 13:50:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for goods_brand
-- ----------------------------
DROP TABLE IF EXISTS `goods_brand`;
CREATE TABLE `goods_brand`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '匹配名称',
  `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '品牌描述',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 99 COMMENT '排序字段',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 55 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_brand
-- ----------------------------
INSERT INTO `goods_brand` VALUES (1, '明月', '明月的品牌描述', 99, '2021-01-13 13:40:06', NULL, NULL, NULL, 0);
INSERT INTO `goods_brand` VALUES (2, '依视路', '依视路的介绍', 99, '2021-01-13 13:43:06', NULL, NULL, NULL, 0);
INSERT INTO `goods_brand` VALUES (3, '苹果', '苹果介绍', 99, '2021-01-13 15:06:58', NULL, NULL, NULL, 0);
INSERT INTO `goods_brand` VALUES (44, '华为', '', 99, '2021-01-25 15:20:32', '2021-01-25 15:20:32', NULL, NULL, 0);
INSERT INTO `goods_brand` VALUES (45, '小米', '', 99, '2021-01-25 15:20:32', '2021-01-25 15:20:32', NULL, NULL, 0);
INSERT INTO `goods_brand` VALUES (51, '知道严选', '', 99, '2021-02-05 15:14:55', '2021-02-05 15:14:55', NULL, NULL, 0);
INSERT INTO `goods_brand` VALUES (52, '蔡司', '', 99, '2021-02-05 15:36:30', '2021-02-05 15:36:30', NULL, NULL, 0);
INSERT INTO `goods_brand` VALUES (53, '凯米', '', 99, '2021-02-05 15:36:47', '2021-02-05 15:36:47', NULL, NULL, 0);
INSERT INTO `goods_brand` VALUES (54, '知道优选', '', 99, '2021-02-05 15:36:49', '2021-02-05 15:36:49', NULL, NULL, 0);

-- ----------------------------
-- Table structure for goods_categories
-- ----------------------------
DROP TABLE IF EXISTS `goods_categories`;
CREATE TABLE `goods_categories`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父级分类ID，0为顶级分类',
  `cate_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '分类名称',
  `cate_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '分类编码',
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT 99 COMMENT '排序字段',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `categories_cate_name_unique`(`cate_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_categories
-- ----------------------------
INSERT INTO `goods_categories` VALUES (1, 0, '镜片', 'JingPian', 99, '2021-02-04 18:34:52', NULL, NULL, NULL, 0);
INSERT INTO `goods_categories` VALUES (2, 0, '镜框', 'JingKuang', 99, '2021-02-05 15:12:56', NULL, NULL, NULL, 0);
INSERT INTO `goods_categories` VALUES (3, 1, '国内镜片', 'inner_glass', 99, '2021-01-23 18:20:18', NULL, NULL, NULL, 0);
INSERT INTO `goods_categories` VALUES (4, 1, '进口镜片', 'outer_glass', 99, '2021-01-23 18:20:08', NULL, NULL, NULL, 0);
INSERT INTO `goods_categories` VALUES (5, 0, '智能手机', 'ai_mobile', 99, '2021-01-23 18:20:23', NULL, NULL, NULL, 0);
INSERT INTO `goods_categories` VALUES (6, 0, '笔记本', 'compute_book', 99, '2021-01-25 14:47:42', NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for goods_comment
-- ----------------------------
DROP TABLE IF EXISTS `goods_comment`;
CREATE TABLE `goods_comment`  (
  `id` bigint(18) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NULL DEFAULT NULL COMMENT '用户id',
  `spu_id` int(10) NULL DEFAULT NULL COMMENT '针对的商品id',
  `comment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '评论内容',
  `images` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '评论中的图片',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '是否软删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for goods_images
-- ----------------------------
DROP TABLE IF EXISTS `goods_images`;
CREATE TABLE `goods_images`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `spu_id` int(10) UNSIGNED NOT NULL COMMENT '商品ID',
  `link` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '图片URL地址',
  `position` smallint(5) UNSIGNED NULL DEFAULT NULL COMMENT '图片位置',
  `is_master` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否主图: 1是,0否',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `goods_images_goods_id_foreign`(`spu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 316 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_images
-- ----------------------------
INSERT INTO `goods_images` VALUES (1, 1, 'https://marketing.golcer.cn/zwcl/glass/20210112/a81d354cf62546808046db4f1d665015.png', NULL, 1, '2021-01-13 15:56:39', NULL, NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (2, 1, 'https://marketing.golcer.cn/zwcl/glass/20210112/a81d354cf62546808046db4f1d665015.png', NULL, 0, '2021-01-13 15:56:57', NULL, NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (3, 1, 'https://marketing.golcer.cn/zwcl/glass/20210112/a81d354cf62546808046db4f1d665015.png', NULL, 0, '2021-01-13 15:57:03', NULL, NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (4, 1, 'https://marketing.golcer.cn/zwcl/glass/20210113/7ece9e1cd7a74b28bbb056525c2b0561.png', NULL, 0, '2021-01-13 15:58:32', NULL, NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (5, 2, 'https://marketing.golcer.cn/zwcl/glass/20210113/7ece9e1cd7a74b28bbb056525c2b0561.png', NULL, 1, '2021-01-13 15:58:43', NULL, NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (6, 2, 'https://marketing.golcer.cn/zwcl/glass/20210113/3f4c99f94c0d4fe18eff0b42361f639a.png', NULL, 0, '2021-01-13 15:59:01', NULL, NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (7, 2, 'https://marketing.golcer.cn/zwcl/glass/20210113/3f4c99f94c0d4fe18eff0b42361f639a.png', NULL, 0, '2021-01-13 15:59:08', NULL, NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (18, 44, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/3c3224078f5d4855bf890c541252afc5.jpg', NULL, 1, '2021-02-05 15:14:55', '2021-02-05 15:14:55', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (19, 45, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/d52f92ab400a4002b200d162511b586f.jpg', NULL, 1, '2021-02-05 15:14:55', '2021-02-05 15:14:55', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (20, 46, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/c0dc2654cd3742f2ab461749a7fcf0ba.jpg', NULL, 1, '2021-02-05 15:14:56', '2021-02-05 15:14:56', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (21, 47, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/5914a792d28c4c1693f222e2513c7490.jpg', NULL, 1, '2021-02-05 15:14:56', '2021-02-05 15:14:56', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (22, 48, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/29aeebce778f4b72b544a76ef4470739.jpg', NULL, 1, '2021-02-05 15:14:56', '2021-02-05 15:14:56', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (23, 49, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/be7bb1566e4340d4a8b3341f23581ca7.jpg', NULL, 1, '2021-02-05 15:14:56', '2021-02-05 15:14:56', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (24, 50, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/7c701bbd7d14487d91d1d2af83058e45.jpg', NULL, 1, '2021-02-05 15:14:57', '2021-02-05 15:14:57', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (25, 51, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/7aaa3f3a50474941a0447bf132343da6.jpg', NULL, 1, '2021-02-05 15:14:57', '2021-02-05 15:14:57', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (26, 52, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/3ab22d2d108247049460759f8e0bee5c.jpg', NULL, 1, '2021-02-05 15:14:57', '2021-02-05 15:14:57', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (27, 53, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/d6d7064fbe894e9f8a358d58501ae3b4.jpg', NULL, 1, '2021-02-05 15:14:57', '2021-02-05 15:14:57', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (28, 55, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/d32c2eeb6462447abcd17c610c69a121.jpg', NULL, 1, '2021-02-05 15:14:57', '2021-02-05 15:14:57', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (29, 56, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/06bb4a04fdc3465a908f7ebaa1c3d2c4.jpg', NULL, 1, '2021-02-05 15:14:57', '2021-02-05 15:14:57', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (30, 57, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/ae0e9b116dd949bc9cce1afee9f5e362.jpg', NULL, 1, '2021-02-05 15:14:57', '2021-02-05 15:14:57', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (31, 58, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/2bdaa0d2a37b40c39e2d05f05d1f247b.jpg', NULL, 1, '2021-02-05 15:14:58', '2021-02-05 15:14:58', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (32, 59, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/ebdf8ab1f37a4a91b93a7e451cc0d368.jpg', NULL, 1, '2021-02-05 15:14:58', '2021-02-05 15:14:58', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (33, 60, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/d92f07f5467d46a99932bf229da76266.jpg', NULL, 1, '2021-02-05 15:14:58', '2021-02-05 15:14:58', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (34, 61, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/44d0fd5e44db41e48a5156f40a59f45a.jpg', NULL, 1, '2021-02-05 15:14:58', '2021-02-05 15:14:58', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (35, 62, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/6ebd386016fb4590834b359921e674fb.jpg', NULL, 1, '2021-02-05 15:14:58', '2021-02-05 15:14:58', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (36, 63, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/ec05b88aaec94d01ac0df78a3188f632.jpg', NULL, 1, '2021-02-05 15:14:58', '2021-02-05 15:14:58', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (37, 64, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/84227f93684e494da647cad00fe440f9.jpg', NULL, 1, '2021-02-05 15:14:58', '2021-02-05 15:14:58', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (38, 65, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/004e9ae95ec14fe682bd126b11d20352.jpg', NULL, 1, '2021-02-05 15:14:58', '2021-02-05 15:14:58', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (39, 66, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/41d8e5776cf4407c8baf23f23f7facf1.jpg', NULL, 1, '2021-02-05 15:14:59', '2021-02-05 15:14:59', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (40, 67, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/6905e2d4ec3e4ee68c85a4740efa75a8.jpg', NULL, 1, '2021-02-05 15:14:59', '2021-02-05 15:14:59', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (41, 68, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/559fb55595724c0e85fce3078381acb5.jpg', NULL, 1, '2021-02-05 15:14:59', '2021-02-05 15:14:59', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (42, 69, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/b23da719d74e4c029e7de6d16dab4770.jpg', NULL, 1, '2021-02-05 15:14:59', '2021-02-05 15:14:59', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (43, 70, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/d6fe728a27684d619cc7ee9b1a4befe9.jpg', NULL, 1, '2021-02-05 15:14:59', '2021-02-05 15:14:59', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (44, 71, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/8f0efa37437f482994ac98cfa52af4c7.jpg', NULL, 1, '2021-02-05 15:14:59', '2021-02-05 15:14:59', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (45, 72, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/4c744454a5a24a1a84d407452f4e3bb3.jpg', NULL, 1, '2021-02-05 15:14:59', '2021-02-05 15:14:59', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (46, 73, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/f1f813d07aa54d18a6e8066c7aed92b7.jpg', NULL, 1, '2021-02-05 15:14:59', '2021-02-05 15:14:59', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (47, 74, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/2ba87dcb38cb4f0f9ca52abef5b7febd.jpg', NULL, 1, '2021-02-05 15:15:00', '2021-02-05 15:15:00', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (48, 75, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/c99dfd569cab4e39b60eddfaea037f2c.jpg', NULL, 1, '2021-02-05 15:15:00', '2021-02-05 15:15:00', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (49, 76, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/94ada1c5e9484105ba858a312cec4136.jpg', NULL, 1, '2021-02-05 15:15:00', '2021-02-05 15:15:00', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (50, 77, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/5992f604b05c485299b582f6d8f792fe.jpg', NULL, 1, '2021-02-05 15:15:00', '2021-02-05 15:15:00', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (51, 78, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/c9b801ef13d84296a5e17ce8a41b5c80.jpg', NULL, 1, '2021-02-05 15:15:00', '2021-02-05 15:15:00', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (52, 79, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/3d25e0bceff44372bed057e691328bef.jpg', NULL, 1, '2021-02-05 15:15:00', '2021-02-05 15:15:00', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (53, 80, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/5b626cc174f1452a94248cc2c11a086e.jpg', NULL, 1, '2021-02-05 15:15:00', '2021-02-05 15:15:00', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (54, 81, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/db9cf72d53e3440ab912ae8e84870191.jpg', NULL, 1, '2021-02-05 15:15:01', '2021-02-05 15:15:01', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (55, 83, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/575a6a2cf26048d0b25e6e2a0238d3f3.jpg', NULL, 1, '2021-02-05 15:15:01', '2021-02-05 15:15:01', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (56, 84, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/222e86f8d5c74b1cbd45be21207bd1b5.jpg', NULL, 1, '2021-02-05 15:15:01', '2021-02-05 15:15:01', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (57, 85, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/71d6f306573a44bc935a24be935ad5f2.jpg', NULL, 1, '2021-02-05 15:15:01', '2021-02-05 15:15:01', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (58, 87, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/8adc6a2d76704a7ebefe2837c6732829.jpg', NULL, 1, '2021-02-05 15:15:01', '2021-02-05 15:15:01', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (59, 88, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/78463f58e8694c53950f92732b3be15d.jpg', NULL, 1, '2021-02-05 15:15:01', '2021-02-05 15:15:01', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (60, 89, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/0f8ef955fc784b568c7b12b91e6332f4.jpg', NULL, 1, '2021-02-05 15:15:02', '2021-02-05 15:15:02', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (61, 90, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/a2023361c40645a3ad4345a725ea2835.jpg', NULL, 1, '2021-02-05 15:15:02', '2021-02-05 15:15:02', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (62, 91, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/36d68cee6d85448290c2dcfb6b06e6f3.jpg', NULL, 1, '2021-02-05 15:15:02', '2021-02-05 15:15:02', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (63, 92, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/34daca264a1f4a35a5a717ca472ad401.jpg', NULL, 1, '2021-02-05 15:15:02', '2021-02-05 15:15:02', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (64, 93, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/812dd76ac285435784b34848d455e81b.jpg', NULL, 1, '2021-02-05 15:15:02', '2021-02-05 15:15:02', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (65, 94, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/863b73bb83294421b1c071260463c88a.jpg', NULL, 1, '2021-02-05 15:15:03', '2021-02-05 15:15:03', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (66, 95, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/d09c4b181ba546c4ba51b91b8a018e03.jpg', NULL, 1, '2021-02-05 15:15:03', '2021-02-05 15:15:03', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (67, 96, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/deed540086d047c9830825529ce34a78.jpg', NULL, 1, '2021-02-05 15:15:03', '2021-02-05 15:15:03', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (68, 97, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/12df4077a5934735a1dd04a5e8517ce4.jpg', NULL, 1, '2021-02-05 15:15:03', '2021-02-05 15:15:03', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (69, 98, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/c63137b133024c168db1485b6e63750b.jpg', NULL, 1, '2021-02-05 15:15:03', '2021-02-05 15:15:03', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (70, 99, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/214bbaa0355245559a1b036e7c4e56a8.jpg', NULL, 1, '2021-02-05 15:15:03', '2021-02-05 15:15:03', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (71, 100, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/a9583f6a8e0243fb8da50fecf461a133.jpg', NULL, 1, '2021-02-05 15:15:03', '2021-02-05 15:15:03', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (72, 101, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/e9c056156a29459e91e9fdec55a9bbd6.jpg', NULL, 1, '2021-02-05 15:15:03', '2021-02-05 15:15:03', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (73, 102, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/d1ed8bf16f894e1e843dcffe50428574.jpg', NULL, 1, '2021-02-05 15:15:04', '2021-02-05 15:15:04', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (180, 240, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/3e4b1bc2d4b84191a540765c15c5f350.jpg', NULL, 1, '2021-02-05 15:49:01', '2021-02-05 15:49:01', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (181, 241, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/a9a5570d77fe4e0787721959770b590d.jpg', NULL, 1, '2021-02-05 15:49:02', '2021-02-05 15:49:02', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (182, 242, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/2d9214c05eb64398a9c0212c57028640.jpg', NULL, 1, '2021-02-05 15:49:02', '2021-02-05 15:49:02', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (183, 243, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/5e2d7886247c48339b2863b19f263c67.jpg', NULL, 1, '2021-02-05 15:49:02', '2021-02-05 15:49:02', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (184, 244, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/b382e5407cdc4e898bdcce8af3a24092.jpg', NULL, 1, '2021-02-05 15:49:02', '2021-02-05 15:49:02', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (185, 245, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/51b6335be45b435c8f414a5855e85a55.jpg', NULL, 1, '2021-02-05 15:49:02', '2021-02-05 15:49:02', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (186, 246, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/d9790441b1e349bfa1849c54f42cc1c9.jpg', NULL, 1, '2021-02-05 15:49:03', '2021-02-05 15:49:03', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (187, 247, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/74312b3e9c444dbca522cc0d134e00fe.jpg', NULL, 1, '2021-02-05 15:49:03', '2021-02-05 15:49:03', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (188, 248, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/f4ef5be010a64c05a53a7d003d52b4d1.jpg', NULL, 1, '2021-02-05 15:49:03', '2021-02-05 15:49:03', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (189, 249, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/6fbb015169ff49f2a98b2acef5a040d5.jpg', NULL, 1, '2021-02-05 15:49:03', '2021-02-05 15:49:03', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (190, 250, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/3e91e848ee114173a8ae23dbc37116fd.jpg', NULL, 1, '2021-02-05 15:49:03', '2021-02-05 15:49:03', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (191, 251, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/00c5565229b5437ebf8748a81536a199.jpg', NULL, 1, '2021-02-05 15:49:04', '2021-02-05 15:49:04', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (192, 252, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/fcbdeb75e5ef48dbb525dc5eff91ab8a.jpg', NULL, 1, '2021-02-05 15:49:04', '2021-02-05 15:49:04', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (193, 253, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/585bccf81fe74358bf9f94c43d5faa76.jpg', NULL, 1, '2021-02-05 15:49:04', '2021-02-05 15:49:04', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (194, 254, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/9ccf9a119ab14d448ba6b5ad6d4ab63c.jpg', NULL, 1, '2021-02-05 15:49:04', '2021-02-05 15:49:04', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (195, 255, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/fb9d8a5f7f3c4c9a9756a6891b03ce05.jpg', NULL, 1, '2021-02-05 15:49:04', '2021-02-05 15:49:04', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (196, 256, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/e943492e309d43dfad7a657ef3a6dcfe.jpg', NULL, 1, '2021-02-05 15:49:04', '2021-02-05 15:49:04', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (197, 257, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/b4adda171578495a9443b68a4cbf3877.jpg', NULL, 1, '2021-02-05 15:49:05', '2021-02-05 15:49:05', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (198, 258, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/42c4f74e21eb42dbb4b0a302d2dc2163.jpg', NULL, 1, '2021-02-05 15:49:05', '2021-02-05 15:49:05', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (199, 259, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/45a69293385e4b7eb363b0f07f7a0403.jpg', NULL, 1, '2021-02-05 15:49:05', '2021-02-05 15:49:05', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (200, 260, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/594ed1a4756b4648a0808508574042e4.jpg', NULL, 1, '2021-02-05 15:49:05', '2021-02-05 15:49:05', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (201, 261, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/6553c8806df146c693599bd32a838386.jpg', NULL, 1, '2021-02-05 15:49:05', '2021-02-05 15:49:05', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (202, 262, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/7b46b1d842d9456cbab0f66a3cadb0b7.jpg', NULL, 1, '2021-02-05 15:49:05', '2021-02-05 15:49:05', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (203, 263, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/dcd9c26d363545a9a12788bc76cfcdea.jpg', NULL, 1, '2021-02-05 15:49:06', '2021-02-05 15:49:06', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (204, 264, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/d7b81a1313b94a65a64a7fc7efbeb356.jpg', NULL, 1, '2021-02-05 15:49:06', '2021-02-05 15:49:06', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (205, 265, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/80fa0d84dd9e448085ee46cfde0c844c.jpg', NULL, 1, '2021-02-05 15:49:06', '2021-02-05 15:49:06', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (206, 266, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/3bd06bc448c84ede9847b1f15626c3cd.jpg', NULL, 1, '2021-02-05 15:49:06', '2021-02-05 15:49:06', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (207, 267, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/89fd7f477e444e42b4f6723c9dbc489a.jpg', NULL, 1, '2021-02-05 15:49:06', '2021-02-05 15:49:06', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (208, 268, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/b536fc5d6cb440d2ae5d6d35cbc5bf7b.jpg', NULL, 1, '2021-02-05 15:49:07', '2021-02-05 15:49:07', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (209, 269, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/2254735e6fe5428c9bf7d8fa1f0cee64.jpg', NULL, 1, '2021-02-05 15:49:07', '2021-02-05 15:49:07', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (210, 270, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/4b0536e0382e4cb5b882c052a097e184.jpg', NULL, 1, '2021-02-05 15:49:07', '2021-02-05 15:49:07', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (211, 271, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/b6f6dc5a753444fd8d0f7c73fac7bf84.jpg', NULL, 1, '2021-02-05 15:49:07', '2021-02-05 15:49:07', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (212, 272, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/bb819f4fecf3416eab2f1b7c7d6a079f.jpg', NULL, 1, '2021-02-05 15:49:08', '2021-02-05 15:49:08', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (213, 276, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/9c0ba0f79d894bbea28e4136687339dd.jpg', NULL, 1, '2021-02-05 15:49:08', '2021-02-05 15:49:08', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (214, 277, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/f770f25f07974d33911dc3d323656440.jpg', NULL, 1, '2021-02-05 15:49:08', '2021-02-05 15:49:08', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (215, 278, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/4ae2b128fbfc47ceb75b9e08251e3931.jpg', NULL, 1, '2021-02-05 15:49:09', '2021-02-05 15:49:09', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (216, 279, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/faa5e9a2583c429391589f3e5c19c08d.jpg', NULL, 1, '2021-02-05 15:49:09', '2021-02-05 15:49:09', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (217, 280, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/58e8cb750df741bba3512076bcefea19.jpg', NULL, 1, '2021-02-05 15:49:09', '2021-02-05 15:49:09', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (218, 289, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/41a8581230164677ab7694b9b33aa28b.jpg', NULL, 1, '2021-02-05 15:49:10', '2021-02-05 15:49:10', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (219, 290, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/6a8bf6f8866d40c793b46e05df2ad264.jpg', NULL, 1, '2021-02-05 15:49:10', '2021-02-05 15:49:10', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (220, 291, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/5b5f440b83e8450486bbfe8364b8ff50.jpg', NULL, 1, '2021-02-05 15:49:10', '2021-02-05 15:49:10', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (221, 292, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/e217d86ef713478fbd756d92ca0d3586.jpg', NULL, 1, '2021-02-05 15:49:11', '2021-02-05 15:49:11', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (222, 293, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/fc65d8e7937f44bdbb63bc2ec32a1957.jpg', NULL, 1, '2021-02-05 15:49:11', '2021-02-05 15:49:11', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (223, 294, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/59ecc6aecb8d4b7083c24daa9162a51b.jpg', NULL, 1, '2021-02-05 15:49:11', '2021-02-05 15:49:11', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (224, 295, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/4233ce309c66408d94dd102faffd4258.jpg', NULL, 1, '2021-02-05 15:49:12', '2021-02-05 15:49:12', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (225, 296, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/bce62f1268794badafd0c2630d31e9e3.jpg', NULL, 1, '2021-02-05 15:49:12', '2021-02-05 15:49:12', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (226, 297, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/b9322b6d519346a392365d3b54d40136.jpg', NULL, 1, '2021-02-05 15:49:12', '2021-02-05 15:49:12', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (227, 298, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/e5fabf19ac1640499623340352dc84aa.jpg', NULL, 1, '2021-02-05 15:49:12', '2021-02-05 15:49:12', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (228, 299, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/45ba3eae3e60448f861b1a73e29265cb.jpg', NULL, 1, '2021-02-05 15:49:13', '2021-02-05 15:49:13', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (229, 300, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/32c3b82fc81b46e7aa59cb1afc1484ab.jpg', NULL, 1, '2021-02-05 15:49:13', '2021-02-05 15:49:13', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (230, 301, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/f204ab8f9f3d48d3bec1aa7996bbeb63.jpg', NULL, 1, '2021-02-05 15:49:13', '2021-02-05 15:49:13', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (231, 302, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/9b4b35d222594da7ba130402926545ef.jpg', NULL, 1, '2021-02-05 15:49:14', '2021-02-05 15:49:14', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (232, 303, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/79eb993b8fca41649c47687d145019d2.jpg', NULL, 1, '2021-02-05 15:49:14', '2021-02-05 15:49:14', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (233, 304, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/4392085ec4e04e12b2b7c821d2cd3878.jpg', NULL, 1, '2021-02-05 15:49:14', '2021-02-05 15:49:14', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (234, 305, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/07ca0524301340718e279f037f26c7b8.jpg', NULL, 1, '2021-02-05 15:49:14', '2021-02-05 15:49:14', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (235, 306, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/c69e47bd8b2e4a9c80efb95b8f4c7cf8.jpg', NULL, 1, '2021-02-05 15:49:15', '2021-02-05 15:49:15', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (236, 307, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/251c18cc5966419e8b8a9d51247fc7ca.jpg', NULL, 1, '2021-02-05 15:49:15', '2021-02-05 15:49:15', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (237, 308, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/4f05ce2fc399488381024d9cf7fcca9b.jpg', NULL, 1, '2021-02-05 15:49:15', '2021-02-05 15:49:15', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (238, 309, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/1897de6d14db4fdbaf60a86712abf299.jpg', NULL, 1, '2021-02-05 15:49:15', '2021-02-05 15:49:15', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (239, 310, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/8f9864b780054d19bb31c8dd86ca15d4.jpg', NULL, 1, '2021-02-05 15:49:16', '2021-02-05 15:49:16', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (240, 322, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/559b7db27c5b47109c416b1784b0273d.jpg', NULL, 1, '2021-02-05 15:49:16', '2021-02-05 15:49:16', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (241, 323, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/5fa797b17c004fb287242ed7d991655b.jpg', NULL, 1, '2021-02-05 15:49:16', '2021-02-05 15:49:16', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (242, 324, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/c53c09ca03ad4e588dbf9fc354af0a1f.jpg', NULL, 1, '2021-02-05 15:49:17', '2021-02-05 15:49:17', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (243, 325, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/80e989d9c00a4ecabed8421e53c83a35.jpg', NULL, 1, '2021-02-05 15:49:17', '2021-02-05 15:49:17', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (244, 326, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/1d10753760d3400a972da15ee9197537.jpg', NULL, 1, '2021-02-05 15:49:17', '2021-02-05 15:49:17', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (245, 327, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/85eb0c15639542c8aae0e9e01c2986db.jpg', NULL, 1, '2021-02-05 15:49:17', '2021-02-05 15:49:17', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (246, 328, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/2710f1d34423403186c24966e6d6e7c7.jpg', NULL, 1, '2021-02-05 15:49:18', '2021-02-05 15:49:18', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (247, 329, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/c7b3c5597e81485f9c0d586152d6057e.jpg', NULL, 1, '2021-02-05 15:49:18', '2021-02-05 15:49:18', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (248, 330, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/88e6212d8dac46849cfe505686292859.jpg', NULL, 1, '2021-02-05 15:49:18', '2021-02-05 15:49:18', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (249, 331, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/bda6acea8afa43f9b1f115e64a20d03c.jpg', NULL, 1, '2021-02-05 15:49:18', '2021-02-05 15:49:18', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (250, 332, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/39918f00999940e8ba34b8db91fa5f6c.jpg', NULL, 1, '2021-02-05 15:49:19', '2021-02-05 15:49:19', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (251, 333, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/54401cb4cbf94f8ea30f45ca090f2d77.jpg', NULL, 1, '2021-02-05 15:49:19', '2021-02-05 15:49:19', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (252, 334, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/76a64331904b46eb97d70d9ca29a70d7.jpg', NULL, 1, '2021-02-05 15:49:19', '2021-02-05 15:49:19', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (253, 335, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/2f2762ba8fca4c7ca18f355b86db25ab.jpg', NULL, 1, '2021-02-05 15:49:19', '2021-02-05 15:49:19', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (254, 336, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/cddf9f8a4b48414989533018a05d6b6a.jpg', NULL, 1, '2021-02-05 15:49:19', '2021-02-05 15:49:19', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (255, 337, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/dff26b57745c4761b34c01b1d3733f6e.jpg', NULL, 1, '2021-02-05 15:49:19', '2021-02-05 15:49:19', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (256, 338, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/25e30bd8282b4899b825cfbb266e0dfb.jpg', NULL, 1, '2021-02-05 15:49:20', '2021-02-05 15:49:20', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (257, 339, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/0b87a2be99c04021b4875a5fca2a93f6.jpg', NULL, 1, '2021-02-05 15:49:20', '2021-02-05 15:49:20', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (258, 340, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/7d23af6fd8f04c42a96e3fd170d3e6e9.jpg', NULL, 1, '2021-02-05 15:49:20', '2021-02-05 15:49:20', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (259, 341, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/51de678471bf4d4ab9c5e22260b12f61.jpg', NULL, 1, '2021-02-05 15:49:20', '2021-02-05 15:49:20', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (260, 342, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/a81407efccd54626a917bf28ec3bb769.jpg', NULL, 1, '2021-02-05 15:49:20', '2021-02-05 15:49:20', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (261, 343, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/17a6f4a0678c45fc8699463449256a7b.jpg', NULL, 1, '2021-02-05 15:49:20', '2021-02-05 15:49:20', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (262, 344, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/19fed1c1e97e45cfb01596d92735106d.jpg', NULL, 1, '2021-02-05 15:49:20', '2021-02-05 15:49:20', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (263, 348, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/5e9fb43d905b4c2a96c8bf401816c3e0.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (264, 349, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/8544356ff514456a9fe7d42973034ca9.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (265, 350, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/f35d34a7384a4daebbe4c163eee44a92.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (266, 351, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/1039fcccd409409d812923c94160e6e0.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (267, 352, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/7ca9d1d71947458883ef8b42449c77bb.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (268, 353, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/3376a5b74692491d863a0deb5e92ed4a.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (269, 354, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/4a5311dc73394bbdabef720ba4c8c9ab.jpg', NULL, 1, '2021-02-05 15:49:22', '2021-02-05 15:49:22', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (270, 355, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/dd6137bf07f240ec819f9edabe1ee841.jpg', NULL, 1, '2021-02-05 15:49:22', '2021-02-05 15:49:22', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (271, 356, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/5f4c875a4ba34f8680cfa570d5d59f9f.jpg', NULL, 1, '2021-02-05 15:49:22', '2021-02-05 15:49:22', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (272, 357, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/8ee4b162053e48bd9a9fb4b4bfb1f9b0.jpg', NULL, 1, '2021-02-05 15:49:23', '2021-02-05 15:49:23', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (273, 358, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/a280ea181dec4332ab32f6a4d1f5a58c.jpg', NULL, 1, '2021-02-05 15:49:23', '2021-02-05 15:49:23', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (274, 359, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/ee0e4efab1904c688d3a23ddc76ba093.jpg', NULL, 1, '2021-02-05 15:49:23', '2021-02-05 15:49:23', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (275, 360, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/0ca719f5f8664466ae3782833f461f7d.jpg', NULL, 1, '2021-02-05 15:49:23', '2021-02-05 15:49:23', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (276, 361, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/5c410689ffbc40c58b535246f933c6ae.jpg', NULL, 1, '2021-02-05 15:49:23', '2021-02-05 15:49:23', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (277, 362, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/0c8e5f3799e347d089493f02c38159b7.jpg', NULL, 1, '2021-02-05 15:49:23', '2021-02-05 15:49:23', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (278, 363, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/3830da8a51be4a758c5aefe930e5c216.jpg', NULL, 1, '2021-02-05 15:49:23', '2021-02-05 15:49:23', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (279, 364, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/8eceb0e06b6442ee93f66487256eeaaf.jpg', NULL, 1, '2021-02-05 15:49:23', '2021-02-05 15:49:23', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (280, 365, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/71c13882f90e45b8acaa5f501486c1db.jpg', NULL, 1, '2021-02-05 15:49:24', '2021-02-05 15:49:24', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (281, 366, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/d020ae80ac314c7294c9e2e994b40b08.jpg', NULL, 1, '2021-02-05 15:49:24', '2021-02-05 15:49:24', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (282, 367, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/3441fdd915454836a69ad7f4346fedce.jpg', NULL, 1, '2021-02-05 15:49:24', '2021-02-05 15:49:24', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (283, 368, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/ad310d31d36c4523a0042d413dd9c55b.jpg', NULL, 1, '2021-02-05 15:49:24', '2021-02-05 15:49:24', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (284, 369, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210205/08dbb24a5d2e4ac2b79bd43455d81461.jpg', NULL, 1, '2021-02-05 15:49:24', '2021-02-05 15:49:24', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (285, 320, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/ffe24b22b8dd49c98acd27a44fd677a5.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (286, 321, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/ffe24b22b8dd49c98acd27a44fd677a5.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (287, 311, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/ffe24b22b8dd49c98acd27a44fd677a5.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (288, 375, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/ffe24b22b8dd49c98acd27a44fd677a5.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (289, 312, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/ffe24b22b8dd49c98acd27a44fd677a5.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (290, 313, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/ffe24b22b8dd49c98acd27a44fd677a5.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (291, 314, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/ffe24b22b8dd49c98acd27a44fd677a5.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (292, 315, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/ffe24b22b8dd49c98acd27a44fd677a5.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (293, 316, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/ffe24b22b8dd49c98acd27a44fd677a5.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (294, 317, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/ffe24b22b8dd49c98acd27a44fd677a5.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (295, 318, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/ffe24b22b8dd49c98acd27a44fd677a5.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (296, 319, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/ffe24b22b8dd49c98acd27a44fd677a5.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (297, 273, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/24baae1b3c02407eb6f5f36c7e926880.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (298, 274, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/24baae1b3c02407eb6f5f36c7e926880.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (299, 275, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/24baae1b3c02407eb6f5f36c7e926880.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (300, 281, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/c10d78f703ed4d6ca303bab01a83906c.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (301, 282, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/c10d78f703ed4d6ca303bab01a83906c.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (302, 283, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/c10d78f703ed4d6ca303bab01a83906c.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (303, 284, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/c10d78f703ed4d6ca303bab01a83906c.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (304, 285, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/c10d78f703ed4d6ca303bab01a83906c.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (305, 286, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/c10d78f703ed4d6ca303bab01a83906c.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (306, 287, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/c10d78f703ed4d6ca303bab01a83906c.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (307, 288, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/c10d78f703ed4d6ca303bab01a83906c.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (308, 345, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/5c84d674c297435f8041dcae996bf8a5.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (309, 346, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/5c84d674c297435f8041dcae996bf8a5.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (310, 347, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/5c84d674c297435f8041dcae996bf8a5.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (311, 370, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/ce543c9159ac4b89b49be5f1419e8c21.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (312, 371, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/ce543c9159ac4b89b49be5f1419e8c21.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (313, 372, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/ce543c9159ac4b89b49be5f1419e8c21.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (314, 373, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/ce543c9159ac4b89b49be5f1419e8c21.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_images` VALUES (315, 374, 'https://img-dev-1301057436.cos.ap-guangzhou.myqcloud.com/zwcl/glass/20210206/ce543c9159ac4b89b49be5f1419e8c21.jpg', NULL, 1, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);

-- ----------------------------
-- Table structure for goods_sku
-- ----------------------------
DROP TABLE IF EXISTS `goods_sku`;
CREATE TABLE `goods_sku`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `spu_id` int(10) UNSIGNED NOT NULL COMMENT '商品ID',
  `spu_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'spu编码',
  `sku_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'sku编码',
  `sku_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'sku名称',
  `stock` int(10) UNSIGNED NOT NULL COMMENT 'SKU库存',
  `price` decimal(10, 2) UNSIGNED NOT NULL COMMENT '商品售价',
  `origin_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '商品原价',
  `tags` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '商品的个性化标签',
  `jd_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '京东价格',
  `tb_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '淘宝价格',
  `is_gift` tinyint(1) NULL DEFAULT 0 COMMENT '是否为礼物赠送，暂时不用',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '状态:1启用,0禁用',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `goods_sku_goods_id_foreign`(`spu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_sku
-- ----------------------------
INSERT INTO `goods_sku` VALUES (1, 1, NULL, NULL, '苹果10 32G 白色', 100, 5000.00, NULL, ',32G,白色,', NULL, NULL, 0, 1, '2021-01-20 22:01:21', NULL, NULL, NULL, 0);
INSERT INTO `goods_sku` VALUES (2, 1, NULL, NULL, '苹果10 32G 黑色', 100, 5000.00, NULL, ',32G,黑色,', NULL, NULL, 0, 1, '2021-01-20 22:01:26', NULL, NULL, NULL, 0);
INSERT INTO `goods_sku` VALUES (3, 1, NULL, NULL, '苹果10 32G 玫瑰红', 100, 5000.00, NULL, ',32G,玫瑰红,', NULL, NULL, 0, 1, '2021-01-20 22:01:30', NULL, NULL, NULL, 0);
INSERT INTO `goods_sku` VALUES (4, 1, NULL, NULL, '苹果10 64G 白色', 100, 6000.00, NULL, ',64G,白色,', NULL, NULL, 0, 1, '2021-01-20 22:01:54', NULL, NULL, NULL, 0);
INSERT INTO `goods_sku` VALUES (5, 1, NULL, NULL, '苹果10 64G 黑色', 100, 6000.00, NULL, ',64G,黑色,', NULL, NULL, 0, 1, '2021-01-20 22:01:58', NULL, NULL, NULL, 0);
INSERT INTO `goods_sku` VALUES (6, 1, NULL, NULL, '苹果10 64G 玫瑰红', 100, 6000.00, NULL, ',64G,玫瑰红,', NULL, NULL, 0, 1, '2021-01-20 22:02:04', NULL, NULL, NULL, 0);
INSERT INTO `goods_sku` VALUES (7, 2, NULL, NULL, '明月 防蓝光 1.56', 100, 400.00, NULL, ',防蓝光,1.56,', NULL, NULL, 0, 1, '2021-01-20 22:02:37', NULL, NULL, NULL, 0);
INSERT INTO `goods_sku` VALUES (8, 2, NULL, NULL, '明月 防蓝光 1.71', 100, 500.00, NULL, ',防蓝光,1.71,', NULL, NULL, 0, 1, '2021-01-20 22:02:58', NULL, NULL, NULL, 0);
INSERT INTO `goods_sku` VALUES (9, 2, NULL, NULL, '明月 防蓝光 1.74', 100, 600.00, NULL, ',防蓝光,1.74,', NULL, NULL, 0, 1, '2021-01-20 22:03:04', NULL, NULL, NULL, 0);
INSERT INTO `goods_sku` VALUES (10, 2, NULL, NULL, '明月 普通 1.56', 100, 300.00, NULL, ',1.56,', NULL, NULL, 0, 1, '2021-01-20 22:03:18', NULL, NULL, NULL, 0);
INSERT INTO `goods_sku` VALUES (11, 2, NULL, NULL, '明月 普通 1.71', 100, 400.00, NULL, ',1.71,', NULL, NULL, 0, 1, '2021-01-20 22:03:23', NULL, NULL, NULL, 0);
INSERT INTO `goods_sku` VALUES (12, 2, NULL, NULL, '明月 普通 1.74', 100, 500.00, NULL, ',1.74,', NULL, NULL, 0, 1, '2021-01-20 22:03:31', NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for goods_sku_spec
-- ----------------------------
DROP TABLE IF EXISTS `goods_sku_spec`;
CREATE TABLE `goods_sku_spec`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sku_id` int(10) NOT NULL COMMENT 'sku的Id',
  `spec_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '属性名称',
  `spec_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '属性值',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for goods_spec_name
-- ----------------------------
DROP TABLE IF EXISTS `goods_spec_name`;
CREATE TABLE `goods_spec_name`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '规格名',
  `cate_id` int(10) UNSIGNED NOT NULL COMMENT '所属商品分类ID',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '状态: 1启用,0禁用',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 99 COMMENT '排序字段',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `property_name_cate_id_foreign`(`cate_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_spec_name
-- ----------------------------
INSERT INTO `goods_spec_name` VALUES (1, '颜色', 4, 1, 99, '2021-01-13 15:11:53', NULL, NULL, NULL, 0);
INSERT INTO `goods_spec_name` VALUES (2, '内存', 4, 1, 99, '2021-01-13 15:12:07', NULL, NULL, NULL, 0);
INSERT INTO `goods_spec_name` VALUES (3, '防蓝光', 1, 1, 99, '2021-01-13 15:14:49', NULL, NULL, NULL, 0);
INSERT INTO `goods_spec_name` VALUES (4, '折射率', 1, 1, 99, '2021-01-13 15:15:15', NULL, NULL, NULL, 0);
INSERT INTO `goods_spec_name` VALUES (5, '渐变', 1, 1, 99, '2021-01-20 18:20:24', NULL, NULL, NULL, 0);
INSERT INTO `goods_spec_name` VALUES (6, '防疲劳', 1, 1, 99, '2021-01-20 18:20:36', NULL, NULL, NULL, 0);
INSERT INTO `goods_spec_name` VALUES (7, '内存', 6, 1, 99, '2021-01-25 14:53:47', '2021-01-25 14:53:47', NULL, NULL, 0);
INSERT INTO `goods_spec_name` VALUES (8, 'cpu', 6, 1, 99, '2021-01-25 15:14:33', '2021-01-25 15:14:33', NULL, NULL, 0);

-- ----------------------------
-- Table structure for goods_spec_value
-- ----------------------------
DROP TABLE IF EXISTS `goods_spec_value`;
CREATE TABLE `goods_spec_value`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `spec_id` int(10) NULL DEFAULT NULL COMMENT '规格名id',
  `value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规格值',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_spec_value
-- ----------------------------
INSERT INTO `goods_spec_value` VALUES (1, 1, '白色', '2021-01-13 15:15:44', NULL, NULL, NULL, 0);
INSERT INTO `goods_spec_value` VALUES (2, 1, '黑色', '2021-01-13 15:15:55', NULL, NULL, NULL, 0);
INSERT INTO `goods_spec_value` VALUES (3, 1, '玫瑰红', '2021-01-13 15:16:11', NULL, NULL, NULL, 0);
INSERT INTO `goods_spec_value` VALUES (4, 2, '32G', '2021-01-13 15:16:23', NULL, NULL, NULL, 0);
INSERT INTO `goods_spec_value` VALUES (5, 2, '64G', '2021-01-13 15:16:33', NULL, NULL, NULL, 0);
INSERT INTO `goods_spec_value` VALUES (6, 3, '是', '2021-01-13 15:17:22', NULL, NULL, NULL, 0);
INSERT INTO `goods_spec_value` VALUES (7, 3, '否', '2021-01-13 15:17:34', NULL, NULL, NULL, 0);
INSERT INTO `goods_spec_value` VALUES (8, 4, '1.56', '2021-01-13 15:17:45', NULL, NULL, NULL, 0);
INSERT INTO `goods_spec_value` VALUES (9, 4, '1.71', '2021-01-13 15:17:55', NULL, NULL, NULL, 0);
INSERT INTO `goods_spec_value` VALUES (10, 4, '1.74', '2021-01-13 15:18:13', NULL, NULL, NULL, 0);
INSERT INTO `goods_spec_value` VALUES (11, 5, '是', '2021-01-20 18:21:00', NULL, NULL, NULL, 0);
INSERT INTO `goods_spec_value` VALUES (12, 5, '否', '2021-01-20 18:21:08', NULL, NULL, NULL, 0);
INSERT INTO `goods_spec_value` VALUES (13, 6, '是', '2021-01-20 18:21:23', NULL, NULL, NULL, 0);
INSERT INTO `goods_spec_value` VALUES (14, 6, '否', '2021-01-20 18:21:31', NULL, NULL, NULL, 0);
INSERT INTO `goods_spec_value` VALUES (15, 7, '8G', '2021-01-25 15:14:31', '2021-01-25 15:14:31', NULL, NULL, 0);
INSERT INTO `goods_spec_value` VALUES (16, 7, '16G', '2021-01-25 15:14:33', '2021-01-25 15:14:33', NULL, NULL, 0);
INSERT INTO `goods_spec_value` VALUES (17, 8, '4核8线程', '2021-01-25 15:14:33', '2021-01-25 15:14:33', NULL, NULL, 0);
INSERT INTO `goods_spec_value` VALUES (18, 8, '8核16线程', '2021-01-25 15:14:33', '2021-01-25 15:14:33', NULL, NULL, 0);

-- ----------------------------
-- Table structure for goods_spu
-- ----------------------------
DROP TABLE IF EXISTS `goods_spu`;
CREATE TABLE `goods_spu`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `spu_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '商品编号，唯一',
  `goods_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '商品名称',
  `brand_id` int(10) UNSIGNED NOT NULL COMMENT '品牌ID',
  `cate_id` int(10) UNSIGNED NOT NULL COMMENT '分类ID',
  `low_price` decimal(10, 2) UNSIGNED NULL DEFAULT 0.00 COMMENT '最低售价',
  `hight_price` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '最高售价',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '商品内容',
  `summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '商品描述',
  `total_sale_num` int(10) NULL DEFAULT 0 COMMENT '销售量，但是现在，需要拉取有赞的单才能统计',
  `recommend_rate` tinyint(2) NULL DEFAULT 5 COMMENT '推荐指数',
  `jd_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '京东价格',
  `tb_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '淘宝价格',
  `is_sale` tinyint(4) NOT NULL DEFAULT 1 COMMENT '上架状态: 1是0是',
  `is_gift` tinyint(1) NULL DEFAULT 0 COMMENT '是否为礼物赠送，暂时不用',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `goods_brand_id_foreign`(`brand_id`) USING BTREE,
  INDEX `goods_cate_id_foreign`(`cate_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 376 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_spu
-- ----------------------------
INSERT INTO `goods_spu` VALUES (1, '', '苹果10', 3, 5, 5000.00, 6000.00, NULL, NULL, 0, 5, 6500.00, 6500.00, 1, 0, '2021-01-23 18:20:55', NULL, NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (18, NULL, '华为 8G 4核8线程 笔记本', 44, 6, 0.00, 0.00, NULL, '8G|4核8线程', 0, 5, NULL, NULL, 1, 0, '2021-02-01 18:41:52', '2021-01-25 15:20:45', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (19, NULL, '小米 8G 4核8线程 笔记本', 45, 6, 0.00, 0.00, NULL, '8G|4核8线程', 0, 5, NULL, NULL, 1, 0, '2021-02-01 18:42:12', '2021-01-25 15:20:45', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (20, NULL, '华为 16G 4核8线程 笔记本', 44, 6, 0.00, 0.00, NULL, '16G|4核8线程', 0, 5, NULL, NULL, 1, 0, '2021-02-01 18:01:22', '2021-01-25 15:20:45', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (21, NULL, '小米 16G 4核8线程 笔记本', 45, 6, 0.00, 0.00, NULL, '16G|4核8线程', 0, 5, NULL, NULL, 1, 0, '2021-02-01 18:01:31', '2021-01-25 15:20:45', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (22, NULL, '华为 8G 8核16线程 笔记本', 44, 6, 0.00, 0.00, NULL, '8G|8核16线程', 0, 5, NULL, NULL, 1, 0, '2021-02-01 19:08:52', '2021-01-25 15:20:45', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (23, NULL, '小米 8G 8核16线程 笔记本', 45, 6, 0.00, 0.00, NULL, '8G|8核16线程', 0, 5, NULL, NULL, 1, 0, '2021-02-01 18:01:47', '2021-01-25 15:20:45', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (24, NULL, '华为 16G 8核16线程 笔记本', 44, 6, 0.00, 0.00, NULL, '16G|8核16线程', 0, 5, NULL, NULL, 1, 0, '2021-02-01 18:01:57', '2021-01-25 15:20:45', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (25, NULL, '小米 16G 8核16线程 笔记本', 45, 6, 0.00, 0.00, NULL, '16G|8核16线程', 0, 5, NULL, NULL, 1, 0, '2021-02-01 18:02:07', '2021-01-25 15:20:45', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (44, NULL, '合金、纯钛、半钛等金属材质，复古潮流圆形全框1971', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:55', '2021-02-05 15:14:55', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (45, NULL, '复古潮流圆形全框，合金、纯钛、半钛等金属材质，男女通用1792', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:55', '2021-02-05 15:14:55', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (46, NULL, '复古潮流圆形全框，合金、纯钛、半钛等金属材质，男女通用1793', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:56', '2021-02-05 15:14:56', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (47, NULL, '复古潮流圆形全框，合金、纯钛、半钛等金属材质，男女通用1794', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:56', '2021-02-05 15:14:56', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (48, NULL, '复古潮流椭圆形全框，合金、纯钛、半钛等金属材质，男女通用1795', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:56', '2021-02-05 15:14:56', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (49, NULL, '复古潮流椭圆形全框，合金、纯钛、半钛等金属材质，男女通用1796', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:56', '2021-02-05 15:14:56', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (50, NULL, '复古潮流多边圆形全框，合金、纯钛、半钛等金属材质，男女通用1797', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:56', '2021-02-05 15:14:56', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (51, NULL, '复古潮流椭圆形全框，，合金、纯钛、半钛等金属材质，男女通用1798', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:57', '2021-02-05 15:14:57', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (52, NULL, '合金、纯钛、半钛等金属材质，复古潮流圆形全框，男女通用1799', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:57', '2021-02-05 15:14:57', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (53, NULL, '复古潮流圆形全框，合金、纯钛、半钛等金属材质，男女通用1800', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:57', '2021-02-05 15:14:57', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (54, NULL, '复古潮流多边方形全框，合金、纯钛、半钛等金属材质，男女通用6143', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:57', '2021-02-05 15:14:57', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (55, NULL, '复古大框方形全框，合金、纯钛、半钛等金属材质，男女通用9908', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:57', '2021-02-05 15:14:57', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (56, NULL, '经典休闲方形半框，合金、纯钛、半钛等金属材质，男款2019003', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:57', '2021-02-05 15:14:57', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (57, NULL, '经典商务方形全框，合金、纯钛、半钛等金属材质，女款2019009', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:57', '2021-02-05 15:14:57', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (58, NULL, '经典休闲方形全框，合金、纯钛、半钛等金属材质，男款2019029', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:57', '2021-02-05 15:14:57', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (59, NULL, '经典休闲方形半框，合金、纯钛、半钛等金属材质，男款2019031', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:58', '2021-02-05 15:14:58', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (60, NULL, '经典休闲方形半框，合金、纯钛、半钛等金属材质，男款RS0001', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:58', '2021-02-05 15:14:58', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (61, NULL, '经典休闲方形半框，合金、纯钛、半钛等金属材质，男款RS0030', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:58', '2021-02-05 15:14:58', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (62, NULL, '经典商务方形全框，合金、纯钛、半钛等金属材质，女款RS0062', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:58', '2021-02-05 15:14:58', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (63, NULL, '复古潮流圆形全框，合金、纯钛、半钛等金属材质，男女通用RS0081', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:58', '2021-02-05 15:14:58', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (64, NULL, '复古大框方形全框，合金、纯钛、半钛等金属材质，男女通用RS0084', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:58', '2021-02-05 15:14:58', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (65, NULL, '经典休闲方形全框，合金、纯钛、半钛等金属材质，男女通用RS0085', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:58', '2021-02-05 15:14:58', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (66, NULL, '经典商务方形半框，合金、纯钛、半钛等金属材质，男款RS0095', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:59', '2021-02-05 15:14:59', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (67, NULL, '经典休闲方形全框，合金、纯钛、半钛等金属材质，男款RS0097', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:59', '2021-02-05 15:14:59', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (68, NULL, '经典休闲方形半框，合金、纯钛、半钛等金属材质，男款RS0118', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:59', '2021-02-05 15:14:59', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (69, NULL, '经典商务方形半框，合金、纯钛、半钛等金属材质，女款RS0122', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:59', '2021-02-05 15:14:59', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (70, NULL, '经典商务方形半框，合金、纯钛、半钛等金属材质，男款RS0126', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:59', '2021-02-05 15:14:59', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (71, NULL, '经典商务方形半框，合金、纯钛、半钛等金属材质，男款RS0127', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:59', '2021-02-05 15:14:59', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (72, NULL, '时尚大框方形半框，合金、纯钛、半钛等金属材质，男女通用RS0130', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:59', '2021-02-05 15:14:59', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (73, NULL, '经典商务方形全框，合金、纯钛、半钛等金属材质，男女通用RS0131', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:59', '2021-02-05 15:14:59', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (74, NULL, '经典休闲方形半框，合金、纯钛、半钛等金属材质，男款RS9001', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:14:59', '2021-02-05 15:14:59', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (75, NULL, '经典休闲方形半框，合金、纯钛、半钛等金属材质，男款RS9009', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:15:00', '2021-02-05 15:15:00', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (76, NULL, '经典休闲方形半框，合金、纯钛、半钛等金属材质，男款RS9076', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:15:00', '2021-02-05 15:15:00', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (77, NULL, '经典休闲方形半框，合金、纯钛、半钛等金属材质，男款RS9080', 51, 2, 149.00, 149.00, NULL, '深圳品质', 0, 5, 209.00, 179.00, 1, 0, '2021-02-05 15:15:00', '2021-02-05 15:15:00', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (78, NULL, '经典休闲大框方形全框，韩国TR、塑钢、橡胶、板材等塑胶类材质，男女通用1629', 51, 2, 89.00, 89.00, NULL, '深圳品质', 0, 5, 149.00, 119.00, 1, 0, '2021-02-05 15:15:00', '2021-02-05 15:15:00', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (79, NULL, '复古风格圆形全框，韩国TR、塑钢、橡胶、板材等塑胶类材质，男女通用1630', 51, 2, 89.00, 89.00, NULL, '深圳品质', 0, 5, 149.00, 119.00, 1, 0, '2021-02-05 15:15:00', '2021-02-05 15:15:00', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (80, NULL, '经典休闲方形全框，韩国TR、塑钢、橡胶、板材等塑胶类材质，男女通用1631', 51, 2, 89.00, 89.00, NULL, '深圳品质', 0, 5, 149.00, 119.00, 1, 0, '2021-02-05 15:15:00', '2021-02-05 15:15:00', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (81, NULL, '复古风格圆形全框，韩国TR、塑钢、橡胶、板材等塑胶类材质，女款1632', 51, 2, 89.00, 89.00, NULL, '深圳品质', 0, 5, 149.00, 119.00, 1, 0, '2021-02-05 15:15:00', '2021-02-05 15:15:00', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (82, NULL, '经典休闲大框方形全框，韩国TR、塑钢、橡胶、板材等塑胶类材质，男女通用1624', 51, 2, 89.00, 89.00, NULL, '深圳品质', 0, 5, 149.00, 119.00, 1, 0, '2021-02-05 15:15:01', '2021-02-05 15:15:01', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (83, NULL, '经典休闲大框方形全框，韩国TR、塑钢、橡胶、板材等塑胶类材质，男女通用1625', 51, 2, 89.00, 89.00, NULL, '深圳品质', 0, 5, 149.00, 119.00, 1, 0, '2021-02-05 15:15:01', '2021-02-05 15:15:01', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (84, NULL, '经典休闲方形全框，韩国TR、塑钢、橡胶、板材等塑胶类材质，男女通用1626', 51, 2, 89.00, 89.00, NULL, '深圳品质', 0, 5, 149.00, 119.00, 1, 0, '2021-02-05 15:15:01', '2021-02-05 15:15:01', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (85, NULL, '经典休闲方形全框，韩国TR、塑钢、橡胶、板材等塑胶类材质，男女通用1627', 51, 2, 89.00, 89.00, NULL, '深圳品质', 0, 5, 149.00, 119.00, 1, 0, '2021-02-05 15:15:01', '2021-02-05 15:15:01', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (86, NULL, '经典休闲方形全框，韩国TR、塑钢、橡胶、板材等塑胶类材质，男女通用1628', 51, 2, 89.00, 89.00, NULL, '深圳品质', 0, 5, 149.00, 119.00, 1, 0, '2021-02-05 15:15:01', '2021-02-05 15:15:01', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (87, NULL, '经典休闲方形全框，韩国TR、塑钢、橡胶、板材等塑胶类材质，男女通用1217', 51, 2, 89.00, 89.00, NULL, '深圳品质', 0, 5, 149.00, 119.00, 1, 0, '2021-02-05 15:15:01', '2021-02-05 15:15:01', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (88, NULL, '经典休闲方形全框，韩国TR、塑钢、橡胶、板材等塑胶类材质，男女通用1335', 51, 2, 89.00, 89.00, NULL, '深圳品质', 0, 5, 149.00, 119.00, 1, 0, '2021-02-05 15:15:01', '2021-02-05 15:15:01', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (89, NULL, '经典休闲方形全框，韩国TR、塑钢、橡胶、板材等塑胶类材质，男女通用1617', 51, 2, 89.00, 89.00, NULL, '深圳品质', 0, 5, 149.00, 119.00, 1, 0, '2021-02-05 15:15:01', '2021-02-05 15:15:01', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (90, NULL, '经典休闲方形全框，韩国TR、塑钢、橡胶、板材等塑胶类材质，男女通用1618', 51, 2, 89.00, 89.00, NULL, '深圳品质', 0, 5, 149.00, 119.00, 1, 0, '2021-02-05 15:15:02', '2021-02-05 15:15:02', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (91, NULL, '复古风格圆形全框，韩国TR、塑钢、橡胶、板材等塑胶类材质，男女通用1619', 51, 2, 89.00, 89.00, NULL, '深圳品质', 0, 5, 149.00, 119.00, 1, 0, '2021-02-05 15:15:02', '2021-02-05 15:15:02', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (92, NULL, '经典休闲方形全框，韩国TR、塑钢、橡胶、板材等塑胶类材质，男女通用1620', 51, 2, 89.00, 89.00, NULL, '深圳品质', 0, 5, 149.00, 119.00, 1, 0, '2021-02-05 15:15:02', '2021-02-05 15:15:02', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (93, NULL, '经典休闲方形全框，韩国TR、塑钢、橡胶、板材等塑胶类材质，男女通用1621', 51, 2, 89.00, 89.00, NULL, '深圳品质', 0, 5, 149.00, 119.00, 1, 0, '2021-02-05 15:15:02', '2021-02-05 15:15:02', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (94, NULL, '经典休闲方形全框，韩国TR、塑钢、橡胶、板材等塑胶类材质，男女通用1622', 51, 2, 89.00, 89.00, NULL, '深圳品质', 0, 5, 149.00, 119.00, 1, 0, '2021-02-05 15:15:02', '2021-02-05 15:15:02', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (95, NULL, '复古大框方形全框，合金、纯钛、半钛等金属材质，男女通用P6633', 51, 2, 249.00, 249.00, NULL, '深圳品质', 0, 5, 309.00, 279.00, 1, 0, '2021-02-05 15:15:03', '2021-02-05 15:15:03', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (96, NULL, '时尚潮流款方形眉形框，合金、钛金与塑胶复合材料，男女通用P6651', 51, 2, 249.00, 249.00, NULL, '深圳品质', 0, 5, 309.00, 279.00, 1, 0, '2021-02-05 15:15:03', '2021-02-05 15:15:03', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (97, NULL, '经典商务方形全框，合金、纯钛、半钛等金属材质，男女通用P99010', 51, 2, 249.00, 249.00, NULL, '深圳品质', 0, 5, 309.00, 279.00, 1, 0, '2021-02-05 15:15:03', '2021-02-05 15:15:03', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (98, NULL, '经典商务方形半框，合金、纯钛、半钛等金属材质，男女通用P99012', 51, 2, 249.00, 249.00, NULL, '深圳品质', 0, 5, 309.00, 279.00, 1, 0, '2021-02-05 15:15:03', '2021-02-05 15:15:03', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (99, NULL, '经典商务方形半框，合金、纯钛、半钛等金属材质，男女通用P99026', 51, 2, 249.00, 249.00, NULL, '深圳品质', 0, 5, 309.00, 279.00, 1, 0, '2021-02-05 15:15:03', '2021-02-05 15:15:03', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (100, NULL, '经典商务方形全框，合金、纯钛、半钛等金属材质，男女通用P99041', 51, 2, 249.00, 249.00, NULL, '深圳品质', 0, 5, 309.00, 279.00, 1, 0, '2021-02-05 15:15:03', '2021-02-05 15:15:03', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (101, NULL, '经典商务方形全框，合金、纯钛、半钛等金属材质，男女通用P99063', 51, 2, 249.00, 249.00, NULL, '深圳品质', 0, 5, 309.00, 279.00, 1, 0, '2021-02-05 15:15:03', '2021-02-05 15:15:03', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (102, NULL, '经典商务方形半框，合金、纯钛、半钛等金属材质，男女通用P99064', 51, 2, 249.00, 249.00, NULL, '深圳品质', 0, 5, 309.00, 279.00, 1, 0, '2021-02-05 15:15:04', '2021-02-05 15:15:04', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (240, NULL, '明月1.56非球面绿膜树脂镜片', 1, 1, 149.00, 149.00, NULL, NULL, 0, 5, 209.00, 179.00, 1, 0, '2021-02-06 21:46:43', '2021-02-05 15:49:01', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (241, NULL, '明月1.60非球面绿膜树脂镜片', 1, 1, 230.00, 230.00, NULL, NULL, 0, 5, 290.00, 260.00, 1, 0, '2021-02-06 21:46:45', '2021-02-05 15:49:02', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (242, NULL, '明月1.67非球面绿膜树脂镜片', 1, 1, 510.00, 510.00, NULL, NULL, 0, 5, 570.00, 540.00, 1, 0, '2021-02-05 15:49:02', '2021-02-05 15:49:02', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (243, NULL, '明月1.71非球面绿膜树脂镜片', 1, 1, 669.00, 669.00, NULL, NULL, 0, 5, 729.00, 699.00, 1, 0, '2021-02-05 15:49:02', '2021-02-05 15:49:02', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (244, NULL, '明月1.74非球面绿膜树脂镜片', 1, 1, 1199.00, 1199.00, NULL, NULL, 0, 5, 1259.00, 1229.00, 1, 0, '2021-02-05 15:49:02', '2021-02-05 15:49:02', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (245, NULL, '明月1.56超韧全能非球面绿膜树脂镜片', 1, 1, 290.00, 290.00, NULL, NULL, 0, 5, 350.00, 320.00, 1, 0, '2021-02-05 15:49:02', '2021-02-05 15:49:02', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (246, NULL, '明月1.60超韧非球面绿膜树脂镜片', 1, 1, 390.00, 390.00, NULL, NULL, 0, 5, 450.00, 420.00, 1, 0, '2021-02-05 15:49:02', '2021-02-05 15:49:02', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (247, NULL, '明月1.56PMC非球面超硬绿膜树脂镜片', 1, 1, 310.00, 310.00, NULL, NULL, 0, 5, 370.00, 340.00, 1, 0, '2021-02-05 15:49:03', '2021-02-05 15:49:03', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (248, NULL, '明月1.60PMC非球面绿膜树脂镜片', 1, 1, 410.00, 410.00, NULL, NULL, 0, 5, 470.00, 440.00, 1, 0, '2021-02-05 15:49:03', '2021-02-05 15:49:03', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (249, NULL, '明月1.71PMC超亮非球面超硬绿膜树脂镜片71KP', 1, 1, 799.00, 799.00, NULL, NULL, 0, 5, 859.00, 829.00, 1, 0, '2021-02-05 15:49:03', '2021-02-05 15:49:03', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (250, NULL, '明月1.55防蓝光非球面蓝膜树脂镜片', 1, 1, 210.00, 210.00, NULL, NULL, 0, 5, 270.00, 240.00, 1, 0, '2021-02-05 15:49:03', '2021-02-05 15:49:03', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (251, NULL, '明月1.60防蓝光非球面蓝膜树脂镜片', 1, 1, 290.00, 290.00, NULL, NULL, 0, 5, 350.00, 320.00, 1, 0, '2021-02-05 15:49:03', '2021-02-05 15:49:03', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (252, NULL, '明月1.71防蓝光非球面超硬蓝膜树脂镜片71L', 1, 1, 779.00, 779.00, NULL, NULL, 0, 5, 839.00, 809.00, 1, 0, '2021-02-05 15:49:04', '2021-02-05 15:49:04', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (253, NULL, '明月1.61深变灰非球面绿膜树脂镜片', 1, 1, 490.00, 490.00, NULL, NULL, 0, 5, 550.00, 520.00, 1, 0, '2021-02-05 15:49:04', '2021-02-05 15:49:04', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (254, NULL, '明月1.56全季膜变灰非球面超硬绿膜树脂镜片', 1, 1, 390.00, 390.00, NULL, NULL, 0, 5, 450.00, 420.00, 1, 0, '2021-02-05 15:49:04', '2021-02-05 15:49:04', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (255, NULL, '明月1.67膜变灰非球面超硬绿膜树脂镜片', 1, 1, 849.00, 849.00, NULL, NULL, 0, 5, 909.00, 879.00, 1, 0, '2021-02-05 15:49:04', '2021-02-05 15:49:04', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (256, NULL, '明月1.71膜变灰非球面超硬绿膜树脂镜片', 1, 1, 1079.00, 1079.00, NULL, NULL, 0, 5, 1139.00, 1109.00, 1, 0, '2021-02-05 15:49:04', '2021-02-05 15:49:04', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (257, NULL, '明月1.56学生读写专用树脂镜片', 1, 1, 399.00, 399.00, NULL, NULL, 0, 5, 459.00, 429.00, 1, 0, '2021-02-05 15:49:05', '2021-02-05 15:49:05', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (258, NULL, '明月1.60学生读写专用绿膜树脂镜片', 1, 1, 580.00, 580.00, NULL, NULL, 0, 5, 640.00, 610.00, 1, 0, '2021-02-05 15:49:05', '2021-02-05 15:49:05', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (259, NULL, '明月1.56抗疲劳渐进绿膜树脂镜片', 1, 1, 370.00, 370.00, NULL, NULL, 0, 5, 430.00, 400.00, 1, 0, '2021-02-05 15:49:05', '2021-02-05 15:49:05', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (260, NULL, '明月1.55防蓝光抗疲劳渐进蓝膜树脂镜片', 1, 1, 679.00, 679.00, NULL, NULL, 0, 5, 739.00, 709.00, 1, 0, '2021-02-05 15:49:05', '2021-02-05 15:49:05', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (261, NULL, '明月1.60 防蓝光变色超韧内渐进定制镜片非球面树脂', 1, 1, 859.00, 859.00, NULL, NULL, 0, 5, 919.00, 889.00, 1, 0, '2021-02-05 15:49:05', '2021-02-05 15:49:05', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (262, NULL, '明月1.56抗疲劳渐进绿膜树脂镜片', 1, 1, 949.00, 949.00, NULL, NULL, 0, 5, 1009.00, 979.00, 1, 0, '2021-02-05 15:49:05', '2021-02-05 15:49:05', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (263, NULL, '明月1.60抗疲劳渐进绿膜树脂镜片', 1, 1, 1080.00, 1080.00, NULL, NULL, 0, 5, 1140.00, 1110.00, 1, 0, '2021-02-05 15:49:05', '2021-02-05 15:49:05', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (264, NULL, '蔡司1.50非球面树脂防紫外线防辐射钻立方防蓝光镜片', 52, 1, 520.00, 520.00, NULL, NULL, 0, 5, 580.00, 550.00, 1, 0, '2021-02-05 15:49:06', '2021-02-05 15:49:06', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (265, NULL, '蔡司1.56非球面树脂防紫外线防辐射钻立方防蓝光镜片', 52, 1, 598.00, 598.00, NULL, NULL, 0, 5, 658.00, 628.00, 1, 0, '2021-02-05 15:49:06', '2021-02-05 15:49:06', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (266, NULL, '蔡司1.60非球面树脂防紫外线防辐射钻立方防蓝光镜片', 52, 1, 719.00, 719.00, NULL, NULL, 0, 5, 779.00, 749.00, 1, 0, '2021-02-05 15:49:06', '2021-02-05 15:49:06', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (267, NULL, '蔡司1.67非球面树脂防紫外线防辐射钻立方防蓝光镜片', 52, 1, 1090.00, 1090.00, NULL, NULL, 0, 5, 1150.00, 1120.00, 1, 0, '2021-02-05 15:49:06', '2021-02-05 15:49:06', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (268, NULL, '蔡司1.74非球面树脂防紫外线防辐射钻立方防蓝光镜片', 52, 1, 1550.00, 1550.00, NULL, NULL, 0, 5, 1610.00, 1580.00, 1, 0, '2021-02-05 15:49:06', '2021-02-05 15:49:06', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (269, NULL, '蔡司1.56非球面树脂防紫外线防辐射钻立方铂金膜镜片', 52, 1, 540.00, 540.00, NULL, NULL, 0, 5, 600.00, 570.00, 1, 0, '2021-02-05 15:49:07', '2021-02-05 15:49:07', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (270, NULL, '蔡司1.60非球面树脂防紫外线防辐射钻立方铂金膜镜片', 52, 1, 860.00, 860.00, NULL, NULL, 0, 5, 920.00, 890.00, 1, 0, '2021-02-05 15:49:07', '2021-02-05 15:49:07', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (271, NULL, '蔡司1.67非球面树脂防紫外线防辐射钻立方铂金膜镜片', 52, 1, 1399.00, 1399.00, NULL, NULL, 0, 5, 1459.00, 1429.00, 1, 0, '2021-02-05 15:49:07', '2021-02-05 15:49:07', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (272, NULL, '蔡司1.74非球面树脂防紫外线防辐射钻立方铂金膜镜片', 52, 1, 1699.00, 1699.00, NULL, NULL, 0, 5, 1759.00, 1729.00, 1, 0, '2021-02-05 15:49:07', '2021-02-05 15:49:07', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (273, NULL, '蔡司1.56非球面树脂防紫外线防辐射钻立方铂金膜变色镜片', 52, 1, 920.00, 920.00, NULL, NULL, 0, 5, 980.00, 950.00, 1, 0, '2021-02-05 15:49:08', '2021-02-05 15:49:08', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (274, NULL, '蔡司1.60非球面树脂防紫外线防辐射钻立方铂金膜变色镜片', 52, 1, 1299.00, 1299.00, NULL, NULL, 0, 5, 1359.00, 1329.00, 1, 0, '2021-02-05 15:49:08', '2021-02-05 15:49:08', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (275, NULL, '蔡司1.67非球面树脂防紫外线防辐射钻立方铂金膜变色镜片', 52, 1, 2299.00, 2299.00, NULL, NULL, 0, 5, 2359.00, 2329.00, 1, 0, '2021-02-05 15:49:08', '2021-02-05 15:49:08', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (276, NULL, '蔡司1.50非球面树脂防紫外线防辐射钻立方铂金膜成长乐镜片', 52, 1, 770.00, 770.00, NULL, NULL, 0, 5, 830.00, 800.00, 1, 0, '2021-02-05 15:49:08', '2021-02-05 15:49:08', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (277, NULL, '蔡司1.60非球面树脂防紫外线防辐射钻立方铂金膜成长乐镜片', 52, 1, 910.00, 910.00, NULL, NULL, 0, 5, 970.00, 940.00, 1, 0, '2021-02-05 15:49:08', '2021-02-05 15:49:08', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (278, NULL, '蔡司1.67非球面树脂防紫外线防辐射钻立方铂金膜成长乐镜片', 52, 1, 1699.00, 1699.00, NULL, NULL, 0, 5, 1759.00, 1729.00, 1, 0, '2021-02-05 15:49:09', '2021-02-05 15:49:09', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (279, NULL, '蔡司1.50非球面树脂防紫外线防辐射莲花膜成长乐镜片', 52, 1, 670.00, 670.00, NULL, NULL, 0, 5, 730.00, 700.00, 1, 0, '2021-02-05 15:49:09', '2021-02-05 15:49:09', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (280, NULL, '蔡司1.60非球面树脂防紫外线防辐射莲花膜成长乐镜片', 52, 1, 970.00, 970.00, NULL, NULL, 0, 5, 1030.00, 1000.00, 1, 0, '2021-02-05 15:49:09', '2021-02-05 15:49:09', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (281, NULL, '蔡司1.50非球面树脂防紫外线防辐射莲花膜', 52, 1, 207.00, 207.00, NULL, NULL, 0, 5, 267.00, 237.00, 1, 0, '2021-02-06 21:20:08', '2021-02-05 15:49:09', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (282, NULL, '蔡司1.56非球面树脂防紫外线防辐射莲花膜', 52, 1, 350.00, 350.00, NULL, NULL, 0, 5, 410.00, 380.00, 1, 0, '2021-02-06 21:20:10', '2021-02-05 15:49:09', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (283, NULL, '蔡司1.60非球面树脂防紫外线防辐射莲花膜', 52, 1, 565.00, 565.00, NULL, NULL, 0, 5, 625.00, 595.00, 1, 0, '2021-02-05 15:53:59', '2021-02-05 15:49:09', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (284, NULL, '蔡司1.67非球面树脂防紫外线防辐射莲花膜', 52, 1, 565.00, 565.00, NULL, NULL, 0, 5, 625.00, 595.00, 1, 0, '2021-02-06 21:20:14', '2021-02-05 15:49:10', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (285, NULL, '蔡司1.74非球面树脂防紫外线防辐射莲花膜', 52, 1, 1399.00, 1399.00, NULL, NULL, 0, 5, 1459.00, 1429.00, 1, 0, '2021-02-05 15:49:10', '2021-02-05 15:49:10', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (286, NULL, '蔡司1.56非球面树脂防紫外线防辐射莲花膜变色镜片', 52, 1, 650.00, 650.00, NULL, NULL, 0, 5, 710.00, 680.00, 1, 0, '2021-02-05 15:49:10', '2021-02-05 15:49:10', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (287, NULL, '蔡司1.60非球面树脂防紫外线防辐射莲花膜变色镜片', 52, 1, 920.00, 920.00, NULL, NULL, 0, 5, 980.00, 950.00, 1, 0, '2021-02-05 15:49:10', '2021-02-05 15:49:10', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (288, NULL, '蔡司1.67非球面树脂防紫外线防辐射莲花膜变色镜片', 52, 1, 1550.00, 1550.00, NULL, NULL, 0, 5, 1610.00, 1580.00, 1, 0, '2021-02-05 15:49:10', '2021-02-05 15:49:10', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (289, NULL, '蔡司1.50非球面树脂防紫外线防辐射A系列莲花膜渐进镜片', 52, 1, 530.00, 530.00, NULL, NULL, 0, 5, 590.00, 560.00, 1, 0, '2021-02-05 15:49:10', '2021-02-05 15:49:10', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (290, NULL, '蔡司1.60非球面树脂防紫外线防辐射A系列莲花膜渐进镜片', 52, 1, 770.00, 770.00, NULL, NULL, 0, 5, 830.00, 800.00, 1, 0, '2021-02-05 15:49:10', '2021-02-05 15:49:10', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (291, NULL, '蔡司1.67非球面树脂防紫外线防辐射A系列莲花膜渐进镜片', 52, 1, 1159.00, 1159.00, NULL, NULL, 0, 5, 1219.00, 1189.00, 1, 0, '2021-02-05 15:49:10', '2021-02-05 15:49:10', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (292, NULL, '蔡司1.50新典锐非球面树脂防紫外线防辐射钻立方铂金膜渐进镜片', 52, 1, 1049.00, 1049.00, NULL, NULL, 0, 5, 1109.00, 1079.00, 1, 0, '2021-02-05 15:49:11', '2021-02-05 15:49:11', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (293, NULL, '蔡司1.60新典锐非球面树脂防紫外线防辐射钻立方铂金膜渐进镜片', 52, 1, 1499.00, 1499.00, NULL, NULL, 0, 5, 1559.00, 1529.00, 1, 0, '2021-02-05 15:49:11', '2021-02-05 15:49:11', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (294, NULL, '蔡司1.67新典锐非球面树脂防紫外线防辐射钻立方铂金膜渐进镜片', 52, 1, 2209.00, 2209.00, NULL, NULL, 0, 5, 2269.00, 2239.00, 1, 0, '2021-02-05 15:49:11', '2021-02-05 15:49:11', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (295, NULL, '蔡司1.50非球面树脂防紫外线防辐射钻立方铂金膜渐进变色镜片', 52, 1, 849.00, 849.00, NULL, NULL, 0, 5, 909.00, 879.00, 1, 0, '2021-02-05 15:49:11', '2021-02-05 15:49:11', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (296, NULL, '蔡司1.60非球面树脂防紫外线防辐射钻立方铂金膜渐进变色镜片', 52, 1, 1150.00, 1150.00, NULL, NULL, 0, 5, 1210.00, 1180.00, 1, 0, '2021-02-05 15:49:12', '2021-02-05 15:49:12', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (297, NULL, '蔡司1.67非球面树脂防紫外线防辐射钻立方铂金膜渐进变色镜片', 52, 1, 1749.00, 1749.00, NULL, NULL, 0, 5, 1809.00, 1779.00, 1, 0, '2021-02-05 15:49:12', '2021-02-05 15:49:12', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (298, NULL, '蔡司1.50非球面树脂防紫外线防辐射驾驶型极光膜镜片', 52, 1, 599.00, 599.00, NULL, NULL, 0, 5, 659.00, 629.00, 1, 0, '2021-02-05 15:49:12', '2021-02-05 15:49:12', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (299, NULL, '蔡司1.60非球面树脂防紫外线防辐射驾驶型极光膜镜片', 52, 1, 949.00, 949.00, NULL, NULL, 0, 5, 1009.00, 979.00, 1, 0, '2021-02-05 15:49:12', '2021-02-05 15:49:12', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (300, NULL, '蔡司1.67非球面树脂防紫外线防辐射驾驶型极光膜镜片', 52, 1, 1190.00, 1190.00, NULL, NULL, 0, 5, 1250.00, 1220.00, 1, 0, '2021-02-05 15:49:13', '2021-02-05 15:49:13', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (301, NULL, '蔡司1.74非球面树脂防紫外线防辐射驾驶型极光膜镜片', 52, 1, 2780.00, 2780.00, NULL, NULL, 0, 5, 2840.00, 2810.00, 1, 0, '2021-02-05 15:49:13', '2021-02-05 15:49:13', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (302, NULL, '蔡司1.50非球面树脂防紫外线防辐射驾驶型变色极光膜镜片', 52, 1, 1598.00, 1598.00, NULL, NULL, 0, 5, 1658.00, 1628.00, 1, 0, '2021-02-05 15:49:13', '2021-02-05 15:49:13', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (303, NULL, '蔡司1.60非球面树脂防紫外线防辐射驾驶型变色极光膜镜片', 52, 1, 2399.00, 2399.00, NULL, NULL, 0, 5, 2459.00, 2429.00, 1, 0, '2021-02-05 15:49:14', '2021-02-05 15:49:14', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (304, NULL, '蔡司1.67非球面树脂防紫外线防辐射驾驶型变色极光膜镜片', 52, 1, 3130.00, 3130.00, NULL, NULL, 0, 5, 3190.00, 3160.00, 1, 0, '2021-02-05 15:49:14', '2021-02-05 15:49:14', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (305, NULL, '蔡司1.50非球面树脂防紫外线防辐射驾驶型渐进变色极光膜镜片', 52, 1, 2999.00, 2999.00, NULL, NULL, 0, 5, 3059.00, 3029.00, 1, 0, '2021-02-05 15:49:14', '2021-02-05 15:49:14', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (306, NULL, '蔡司1.60非球面树脂防紫外线防辐射驾驶型渐进变色极光膜镜片', 52, 1, 3779.00, 3779.00, NULL, NULL, 0, 5, 3839.00, 3809.00, 1, 0, '2021-02-05 15:49:14', '2021-02-05 15:49:14', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (307, NULL, '蔡司1.67非球面树脂防紫外线防辐射驾驶型渐进变色极光膜镜片', 52, 1, 4090.00, 4090.00, NULL, NULL, 0, 5, 4150.00, 4120.00, 1, 0, '2021-02-05 15:49:15', '2021-02-05 15:49:15', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (308, NULL, '蔡司1.50非球面树脂防紫外线防辐射驾驶型渐进极光膜镜片', 52, 1, 1999.00, 1999.00, NULL, NULL, 0, 5, 2059.00, 2029.00, 1, 0, '2021-02-05 15:49:15', '2021-02-05 15:49:15', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (309, NULL, '蔡司1.60非球面树脂防紫外线防辐射驾驶型渐进极光膜镜片', 52, 1, 2499.00, 2499.00, NULL, NULL, 0, 5, 2559.00, 2529.00, 1, 0, '2021-02-05 15:49:15', '2021-02-05 15:49:15', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (310, NULL, '蔡司1.67非球面树脂防紫外线防辐射驾驶型渐进极光膜镜片', 52, 1, 2779.00, 2779.00, NULL, NULL, 0, 5, 2839.00, 2809.00, 1, 0, '2021-02-05 15:49:15', '2021-02-05 15:49:15', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (311, NULL, '蔡司1.50非球面树脂防紫外线防辐射钻立方防蓝光数码型镜片', 52, 1, 710.00, 710.00, NULL, NULL, 0, 5, 770.00, 740.00, 1, 0, '2021-02-05 15:49:16', '2021-02-05 15:49:16', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (312, NULL, '蔡司1.60非球面树脂防紫外线防辐射钻立方防蓝光数码型镜片', 52, 1, 1079.00, 1079.00, NULL, NULL, 0, 5, 1139.00, 1109.00, 1, 0, '2021-02-05 15:49:16', '2021-02-05 15:49:16', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (313, NULL, '蔡司1.67非球面树脂防紫外线防辐射钻立方防蓝光数码型镜片', 52, 1, 1599.00, 1599.00, NULL, NULL, 0, 5, 1659.00, 1629.00, 1, 0, '2021-02-05 15:49:16', '2021-02-05 15:49:16', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (314, NULL, '蔡司1.74非球面树脂防紫外线防辐射钻立方防蓝光数码型镜片', 52, 1, 2199.00, 2199.00, NULL, NULL, 0, 5, 2259.00, 2229.00, 1, 0, '2021-02-05 15:49:16', '2021-02-05 15:49:16', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (315, NULL, '蔡司1.50非球面树脂防紫外线防辐射钻立方铂金膜数码型镜片', 52, 1, 680.00, 680.00, NULL, NULL, 0, 5, 740.00, 710.00, 1, 0, '2021-02-05 15:49:16', '2021-02-05 15:49:16', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (316, NULL, '蔡司1.60非球面树脂防紫外线防辐射钻立方铂金膜数码型镜片', 52, 1, 1049.00, 1049.00, NULL, NULL, 0, 5, 1109.00, 1079.00, 1, 0, '2021-02-05 15:49:16', '2021-02-05 15:49:16', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (317, NULL, '蔡司1.67非球面树脂防紫外线防辐射钻立方铂金膜数码型镜片', 52, 1, 1459.00, 1459.00, NULL, NULL, 0, 5, 1519.00, 1489.00, 1, 0, '2021-02-05 15:49:16', '2021-02-05 15:49:16', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (318, NULL, '蔡司1.74非球面树脂防紫外线防辐射钻立方铂金膜数码型镜片', 52, 1, 2199.00, 2199.00, NULL, NULL, 0, 5, 2259.00, 2229.00, 1, 0, '2021-02-05 15:49:16', '2021-02-05 15:49:16', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (319, NULL, '蔡司1.50非球面树脂防紫外线防辐射钻立方铂金膜数码型变色镜片', 52, 1, 1269.00, 1269.00, NULL, NULL, 0, 5, 1329.00, 1299.00, 1, 0, '2021-02-05 15:49:16', '2021-02-05 15:49:16', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (320, NULL, '蔡司1.60非球面树脂防紫外线防辐射钻立方铂金膜数码型变色镜片', 52, 1, 1599.00, 1599.00, NULL, NULL, 0, 5, 1659.00, 1629.00, 1, 0, '2021-02-05 15:49:16', '2021-02-05 15:49:16', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (321, NULL, '蔡司1.67非球面树脂防紫外线防辐射钻立方铂金膜数码型变色镜片', 52, 1, 2199.00, 2199.00, NULL, NULL, 0, 5, 2259.00, 2229.00, 1, 0, '2021-02-05 15:49:16', '2021-02-05 15:49:16', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (322, NULL, '依视路1.56非球面钻晶A+', 2, 1, 230.00, 230.00, NULL, NULL, 0, 5, 290.00, 260.00, 1, 0, '2021-02-05 15:49:16', '2021-02-05 15:49:16', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (323, NULL, '依视路1.67非球面依视美依视路钻晶A+', 2, 1, 590.00, 590.00, NULL, NULL, 0, 5, 650.00, 620.00, 1, 0, '2021-02-05 15:49:16', '2021-02-05 15:49:16', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (324, NULL, '依视路1.56非球面全视线第七代变色钻晶A+', 2, 1, 390.00, 390.00, NULL, NULL, 0, 5, 450.00, 420.00, 1, 0, '2021-02-05 15:49:16', '2021-02-05 15:49:16', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (325, NULL, '依视路1.56非球面钻晶A4', 2, 1, 399.00, 399.00, NULL, NULL, 0, 5, 459.00, 429.00, 1, 0, '2021-02-05 15:49:17', '2021-02-05 15:49:17', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (326, NULL, '依视路1.67非球面依视美钻晶A4', 2, 1, 820.00, 820.00, NULL, NULL, 0, 5, 880.00, 850.00, 1, 0, '2021-02-05 15:49:17', '2021-02-05 15:49:17', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (327, NULL, '依视路1.60非球面钻晶A4', 2, 1, 540.00, 540.00, NULL, NULL, 0, 5, 600.00, 570.00, 1, 0, '2021-02-05 15:49:17', '2021-02-05 15:49:17', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (328, NULL, '依视路1.56非球面超薄钻晶A3', 2, 1, 330.00, 330.00, NULL, NULL, 0, 5, 390.00, 360.00, 1, 0, '2021-02-05 15:49:17', '2021-02-05 15:49:17', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (329, NULL, '依视路1.60非球面特薄钻晶A3', 2, 1, 430.00, 430.00, NULL, NULL, 0, 5, 490.00, 460.00, 1, 0, '2021-02-05 15:49:18', '2021-02-05 15:49:18', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (330, NULL, '依视路1.67非球面依视美钻晶A3', 2, 1, 720.00, 720.00, NULL, NULL, 0, 5, 780.00, 750.00, 1, 0, '2021-02-05 15:49:18', '2021-02-05 15:49:18', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (331, NULL, '依视路1.74非球面丽耐钻晶A3', 2, 1, 1899.00, 1899.00, NULL, NULL, 0, 5, 1959.00, 1929.00, 1, 0, '2021-02-05 15:49:18', '2021-02-05 15:49:18', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (332, NULL, '依视路1.74双面非球丽耐钻晶A4', 2, 1, 2699.00, 2699.00, NULL, NULL, 0, 5, 2759.00, 2729.00, 1, 0, '2021-02-05 15:49:18', '2021-02-05 15:49:18', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (333, NULL, '依视路1.56非球面超薄全视线第七代变色(灰）钻晶A3', 2, 1, 460.00, 460.00, NULL, NULL, 0, 5, 520.00, 490.00, 1, 0, '2021-02-05 15:49:19', '2021-02-05 15:49:19', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (334, NULL, '依视路1.591非球面宇宙全视线第七代变色(灰/绿）钻晶A3', 2, 1, 699.00, 699.00, NULL, NULL, 0, 5, 759.00, 729.00, 1, 0, '2021-02-06 22:26:18', '2021-02-05 15:49:19', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (335, NULL, '依视路1.67非球面依视美全视线第七代变色(灰）钻晶A3', 2, 1, 1050.00, 1050.00, NULL, NULL, 0, 5, 1110.00, 1080.00, 1, 0, '2021-02-05 15:49:19', '2021-02-05 15:49:19', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (336, NULL, '依视路1.56非球面超薄全视线第七代变色(灰）钻晶A4', 2, 1, 950.00, 950.00, NULL, NULL, 0, 5, 1010.00, 980.00, 1, 0, '2021-02-05 15:49:19', '2021-02-05 15:49:19', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (337, NULL, '依视路1.591非球面宇宙全视线第七代变色(灰/绿）钻晶A4', 2, 1, 1190.00, 1190.00, NULL, NULL, 0, 5, 1250.00, 1220.00, 1, 0, '2021-02-06 22:26:14', '2021-02-05 15:49:19', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (338, NULL, '依视路1.67非球面依视美全视线第七代变色(灰）钻晶A4', 2, 1, 1990.00, 1990.00, NULL, NULL, 0, 5, 2050.00, 2020.00, 1, 0, '2021-02-05 15:49:19', '2021-02-05 15:49:19', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (339, NULL, '依视路1.50万里路自由视3.0 DFB 宇宙钻晶A+', 2, 1, 749.00, 749.00, NULL, NULL, 0, 5, 809.00, 779.00, 1, 0, '2021-02-05 15:49:20', '2021-02-05 15:49:20', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (340, NULL, '依视路1.56万里路自由视3.0 DFB 超薄全视线第七代变色（灰）钻晶A+', 2, 1, 1560.00, 1560.00, NULL, NULL, 0, 5, 1620.00, 1590.00, 1, 0, '2021-02-06 21:47:50', '2021-02-05 15:49:20', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (341, NULL, '依视路1.56万里路舒适型DFB超薄钻晶A4', 2, 1, 1200.00, 1200.00, NULL, NULL, 0, 5, 1260.00, 1230.00, 1, 0, '2021-02-05 15:49:20', '2021-02-05 15:49:20', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (342, NULL, '依视路1.59万里路舒适型DFB宇宙钻晶A+', 2, 1, 1140.00, 1140.00, NULL, NULL, 0, 5, 1200.00, 1170.00, 1, 0, '2021-02-05 15:49:20', '2021-02-05 15:49:20', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (343, NULL, '依视路1.59万里路舒适型DFB特薄钻晶A4', 2, 1, 1499.00, 1499.00, NULL, NULL, 0, 5, 1559.00, 1529.00, 1, 0, '2021-02-05 15:49:20', '2021-02-05 15:49:20', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (344, NULL, '依视路1.56万里路舒适型DFB超薄变色钻晶A4', 2, 1, 1900.00, 1900.00, NULL, NULL, 0, 5, 1960.00, 1930.00, 1, 0, '2021-02-05 15:49:20', '2021-02-05 15:49:20', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (345, NULL, '依视路1.56钻晶锐驰驾驶型镜片', 2, 1, 770.00, 770.00, NULL, NULL, 0, 5, 830.00, 800.00, 1, 0, '2021-02-05 15:49:20', '2021-02-05 15:49:20', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (346, NULL, '依视路1.60钻晶锐驰驾驶型镜片', 2, 1, 970.00, 970.00, NULL, NULL, 0, 5, 1030.00, 1000.00, 1, 0, '2021-02-05 15:49:20', '2021-02-05 15:49:20', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (347, NULL, '依视路1.591钻晶锐驰驾驶型变色镜片', 2, 1, 1699.00, 1699.00, NULL, NULL, 0, 5, 1759.00, 1729.00, 1, 0, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (348, NULL, '依视路1.56好学生标准型DFB   钻晶A+', 2, 1, 500.00, 500.00, NULL, NULL, 0, 5, 560.00, 530.00, 1, 0, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (349, NULL, '依视路1.56好学生标准型DFB PC  钻晶A4', 2, 1, 780.00, 780.00, NULL, NULL, 0, 5, 840.00, 810.00, 1, 0, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (350, NULL, '依视路1.591好学生标准型DFB 钻晶A+', 2, 1, 750.00, 750.00, NULL, NULL, 0, 5, 810.00, 780.00, 1, 0, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (351, NULL, '依视路1.591好学生标准型DFB 钻晶A4', 2, 1, 970.00, 970.00, NULL, NULL, 0, 5, 1030.00, 1000.00, 1, 0, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (352, NULL, '依视路爱赞1.56防护型/舒压型/增强型  钻晶A4', 2, 1, 490.00, 490.00, NULL, NULL, 0, 5, 550.00, 520.00, 1, 0, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (353, NULL, '依视路爱赞PC防护型/舒压型/增强型  钻晶A4', 2, 1, 735.00, 735.00, NULL, NULL, 0, 5, 795.00, 765.00, 1, 0, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (354, NULL, '依视路爱赞1.67防护型/舒压型/增强型  钻晶A4', 2, 1, 1100.00, 1100.00, NULL, NULL, 0, 5, 1160.00, 1130.00, 1, 0, '2021-02-05 15:49:21', '2021-02-05 15:49:21', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (355, NULL, '依视路爱赞1.56防护型/舒压型/增强型T7灰变  钻晶A4', 2, 1, 1150.00, 1150.00, NULL, NULL, 0, 5, 1210.00, 1180.00, 1, 0, '2021-02-05 15:49:22', '2021-02-05 15:49:22', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (356, NULL, '依视路爱赞A360防护型/舒压型/增强型T7灰变/绿变PC  钻晶A4', 2, 1, 1450.00, 1450.00, NULL, NULL, 0, 5, 1510.00, 1480.00, 1, 0, '2021-02-05 15:49:22', '2021-02-05 15:49:22', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (357, NULL, '依视路爱赞A360防护型/舒压型/增强型T7灰变1.67  钻晶A4', 2, 1, 2250.00, 2250.00, NULL, NULL, 0, 5, 2310.00, 2280.00, 1, 0, '2021-02-05 15:49:22', '2021-02-05 15:49:22', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (358, NULL, '凯米1.56高性能U2非球面树脂镜片', 53, 1, 240.00, 240.00, NULL, NULL, 0, 5, 300.00, 270.00, 1, 0, '2021-02-05 15:49:23', '2021-02-05 15:49:23', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (359, NULL, '凯米1.61高性能U2非球面树脂镜片', 53, 1, 340.00, 340.00, NULL, NULL, 0, 5, 400.00, 370.00, 1, 0, '2021-02-05 15:49:23', '2021-02-05 15:49:23', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (360, NULL, '凯米1.67高性能U2非球面树脂镜片', 53, 1, 490.00, 490.00, NULL, NULL, 0, 5, 550.00, 520.00, 1, 0, '2021-02-05 15:49:23', '2021-02-05 15:49:23', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (361, NULL, '凯米1.74高性能U2非球面树脂镜片', 53, 1, 660.00, 660.00, NULL, NULL, 0, 5, 720.00, 690.00, 1, 0, '2021-02-05 15:49:23', '2021-02-05 15:49:23', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (362, NULL, '凯米1.56防蓝光U6非球面树脂镜片', 53, 1, 290.00, 290.00, NULL, NULL, 0, 5, 350.00, 320.00, 1, 0, '2021-02-05 15:49:23', '2021-02-05 15:49:23', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (363, NULL, '凯米1.61防蓝光U6非球面树脂镜片', 53, 1, 390.00, 390.00, NULL, NULL, 0, 5, 450.00, 420.00, 1, 0, '2021-02-05 15:49:23', '2021-02-05 15:49:23', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (364, NULL, '凯米1.67防蓝光U6非球面树脂镜片', 53, 1, 550.00, 550.00, NULL, NULL, 0, 5, 610.00, 580.00, 1, 0, '2021-02-05 15:49:23', '2021-02-05 15:49:23', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (365, NULL, '凯米1.74高性能U6非球面树脂镜片', 53, 1, 710.00, 710.00, NULL, NULL, 0, 5, 770.00, 740.00, 1, 0, '2021-02-05 15:49:23', '2021-02-05 15:49:23', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (366, NULL, '凯米1.56标准型HMC非球面树脂镜片', 53, 1, 190.00, 190.00, NULL, NULL, 0, 5, 250.00, 220.00, 1, 0, '2021-02-05 15:49:24', '2021-02-05 15:49:24', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (367, NULL, '凯米1.61标准型HMC非球面树脂镜片', 53, 1, 290.00, 290.00, NULL, NULL, 0, 5, 350.00, 320.00, 1, 0, '2021-02-05 15:49:24', '2021-02-05 15:49:24', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (368, NULL, '凯米1.67标准型HMC非球面树脂镜片', 53, 1, 390.00, 390.00, NULL, NULL, 0, 5, 450.00, 420.00, 1, 0, '2021-02-05 15:49:24', '2021-02-05 15:49:24', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (369, NULL, '凯米1.74标准型HMC非球面树脂镜片', 53, 1, 590.00, 590.00, NULL, NULL, 0, 5, 650.00, 620.00, 1, 0, '2021-02-05 15:49:24', '2021-02-05 15:49:24', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (370, NULL, '知道眼镜优选1.61MR-8非球面树脂镜片', 54, 1, 160.00, 160.00, NULL, NULL, 0, 5, 220.00, 190.00, 1, 0, '2021-02-06 22:26:35', '2021-02-05 15:49:24', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (371, NULL, '知道眼镜优选1.67MR-7非球面树脂镜片', 54, 1, 190.00, 190.00, NULL, NULL, 0, 5, 250.00, 220.00, 1, 0, '2021-02-05 15:49:24', '2021-02-05 15:49:24', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (372, NULL, '知道眼镜优选1.74MR-174非球面树脂镜片', 54, 1, 330.00, 330.00, NULL, NULL, 0, 5, 390.00, 360.00, 1, 0, '2021-02-05 15:49:24', '2021-02-05 15:49:24', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (373, NULL, '知道眼镜优选1.61MR-8非球面树脂防蓝光镜片', 54, 1, 180.00, 180.00, NULL, NULL, 0, 5, 240.00, 210.00, 1, 0, '2021-02-05 15:49:24', '2021-02-05 15:49:24', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (374, NULL, '知道眼镜优选1.67MR-7非球面树脂防蓝光镜片', 54, 1, 220.00, 220.00, NULL, NULL, 0, 5, 280.00, 250.00, 1, 0, '2021-02-05 15:49:24', '2021-02-05 15:49:24', NULL, NULL, 0);
INSERT INTO `goods_spu` VALUES (375, NULL, '知道眼镜优选1.74MR-174非球面树脂防蓝光镜片', 54, 1, 350.00, 350.00, NULL, NULL, 0, 5, 410.00, 380.00, 1, 0, '2021-02-05 15:49:24', '2021-02-05 15:49:24', NULL, NULL, 0);

-- ----------------------------
-- Table structure for goods_spu_spec
-- ----------------------------
DROP TABLE IF EXISTS `goods_spu_spec`;
CREATE TABLE `goods_spu_spec`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `spu_id` int(10) UNSIGNED NOT NULL COMMENT '商品ID',
  `spec_name_id` int(10) UNSIGNED NOT NULL COMMENT '规格名ID',
  `spec_value_id` int(10) UNSIGNED NOT NULL COMMENT '规格值ID',
  `is_sale` tinyint(1) NULL DEFAULT 1 COMMENT '1在售，0停售',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `goods_property_prop_name_id_foreign`(`spec_name_id`) USING BTREE,
  INDEX `goods_property_prop_value_id_foreign`(`spec_value_id`) USING BTREE,
  INDEX `goods_property_goods_id_foreign`(`spu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of goods_spu_spec
-- ----------------------------
INSERT INTO `goods_spu_spec` VALUES (1, 1, 1, 1, 1, '2021-01-13 15:27:41', NULL, NULL, NULL, 0);
INSERT INTO `goods_spu_spec` VALUES (2, 1, 1, 2, 1, '2021-01-13 15:47:09', NULL, NULL, NULL, 0);
INSERT INTO `goods_spu_spec` VALUES (3, 1, 1, 3, 1, '2021-01-13 15:51:49', NULL, NULL, NULL, 0);
INSERT INTO `goods_spu_spec` VALUES (4, 1, 2, 4, 1, '2021-01-13 15:52:00', NULL, NULL, NULL, 0);
INSERT INTO `goods_spu_spec` VALUES (5, 1, 2, 5, 1, '2021-01-13 15:52:08', NULL, NULL, NULL, 0);
INSERT INTO `goods_spu_spec` VALUES (6, 2, 3, 6, 1, '2021-01-13 15:52:50', NULL, NULL, NULL, 0);
INSERT INTO `goods_spu_spec` VALUES (7, 2, 3, 7, 1, '2021-01-13 15:52:59', NULL, NULL, NULL, 0);
INSERT INTO `goods_spu_spec` VALUES (8, 2, 4, 8, 1, '2021-01-13 15:53:07', NULL, NULL, NULL, 0);
INSERT INTO `goods_spu_spec` VALUES (9, 2, 4, 9, 1, '2021-01-13 15:53:21', NULL, NULL, NULL, 0);
INSERT INTO `goods_spu_spec` VALUES (10, 2, 4, 10, 1, '2021-01-13 15:53:29', NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for marketing_activity
-- ----------------------------
DROP TABLE IF EXISTS `marketing_activity`;
CREATE TABLE `marketing_activity`  (
  `id` int(10) NOT NULL,
  `activity_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '活动编码,如：ProcessCost,Freight,FullMinus1,FullMinus2',
  `activity_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '活动名称',
  `activity_type` tinyint(4) NOT NULL COMMENT '活动形式10-全场促销活动 11-免费用  20-发券活动 30-拼团活动 40-秒杀活动',
  `target_users` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '目标人群包编码串 ，逗号分隔，All，代表所有用户',
  `target_goods` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '目标商品包编码串，逗号分隔，All，代表所有商品',
  `begin_time` datetime(0) NULL DEFAULT NULL COMMENT '活动开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '活动结束时间',
  `limit_num` int(10) NULL DEFAULT -1 COMMENT '活动限制次数，无限制：-1',
  `join_num` int(10) NULL DEFAULT 0 COMMENT '参与活动的单数',
  `priority` int(4) NULL DEFAULT 0 COMMENT '优先级，0为最高',
  `is_valid` tinyint(1) NULL DEFAULT 1 COMMENT '是否生效',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for marketing_activity_rule
-- ----------------------------
DROP TABLE IF EXISTS `marketing_activity_rule`;
CREATE TABLE `marketing_activity_rule`  (
  `id` int(10) NOT NULL,
  `activity_id` int(10) NOT NULL COMMENT '活动id',
  `activity_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '活动编码,如：ProcessCost,Freight,FullMinus1,FullMinus2',
  `rule_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规则名称',
  `rule_type` tinyint(4) NOT NULL COMMENT '规则类型100-礼品赠送  200-免除费用  300-满减 400-满折 ...',
  `activity_rule` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '活动规则,Json格式，如阶梯满1000减100，满400减10，如：有下单镜片 免加工费；有下单镜片，有赠送',
  `show_in_order` tinyint(1) NULL DEFAULT 0 COMMENT '是否在订单项中显示1显示，0不显示',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for marketing_join_record
-- ----------------------------
DROP TABLE IF EXISTS `marketing_join_record`;
CREATE TABLE `marketing_join_record`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '这张表暂时可以不要',
  `activity_id` int(10) NOT NULL COMMENT '活动id',
  `activity_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '活动编码',
  `user_id` int(12) NOT NULL COMMENT '用户Id',
  `order_no` bigint(20) NULL DEFAULT NULL COMMENT '有订单则保存订单id',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for marketing_user_coupon
-- ----------------------------
DROP TABLE IF EXISTS `marketing_user_coupon`;
CREATE TABLE `marketing_user_coupon`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL COMMENT '用户Id',
  `activity_id` int(10) NULL DEFAULT NULL COMMENT '活动id',
  `activity_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '活动编码',
  `receive_time` datetime(0) NULL DEFAULT NULL COMMENT '领用时间',
  `coupon_no` varchar(0) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '券码',
  `begin_time` datetime(0) NULL DEFAULT NULL COMMENT '券开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '券结束时间',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for order_address
-- ----------------------------
DROP TABLE IF EXISTS `order_address`;
CREATE TABLE `order_address`  (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL COMMENT '用户id（多条记录）',
  `user_phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '下单用户手机号',
  `consignee_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '收货人名称',
  `consignee_phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '收货人手机号',
  `address_area` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所在地区',
  `address_detail` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '详细地址',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for order_cancel
-- ----------------------------
DROP TABLE IF EXISTS `order_cancel`;
CREATE TABLE `order_cancel`  (
  `id` int(12) NOT NULL,
  `order_no` bigint(20) NOT NULL COMMENT '订单号',
  `user_id` int(12) NOT NULL COMMENT '用户id',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户姓名',
  `user_phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '客户联系电话',
  `audit_status` tinyint(4) NULL DEFAULT NULL COMMENT '申请审核状态，50-发起申请 51-审核中 52-通过申请 53-拒绝申请',
  `send_back_status` tinyint(4) NULL DEFAULT NULL COMMENT '寄回状态，60-商品寄回中 61-商品寄回成功 62-商品寄回失败',
  `cancel_status` tinyint(4) NULL DEFAULT 80 COMMENT '取消状态 90-申请审核中 91-商品寄回中 92-退款处理中 93-处理完成',
  `cancel_reason` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '取消或者退款订单原因',
  `send_back_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '寄回的邮寄单号',
  `remark` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注说明，如订单处理什么状态，可以直接退款等，系统自动记录说明',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for order_car
-- ----------------------------
DROP TABLE IF EXISTS `order_car`;
CREATE TABLE `order_car`  (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL COMMENT '用户id',
  `spu_id` int(10) NOT NULL COMMENT '商品spuId',
  `sku_id` int(10) NOT NULL COMMENT '商品sku id',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '单价',
  `buy_num` int(4) NULL DEFAULT 1 COMMENT '购买数量',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for order_detail
-- ----------------------------
DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE `order_detail`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_no` bigint(20) NOT NULL COMMENT '主订单号',
  `spu_id` int(10) NOT NULL COMMENT '商品spuID',
  `sku_id` int(10) NOT NULL COMMENT '商品skuID',
  `sku_desc` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '预留，商品sku的快照，json格式，包含spu信息，规格等',
  `price` decimal(10, 2) NOT NULL COMMENT '单价',
  `discount_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '优惠价，如果没有活动，则和单价相等；有活动，则计算好价格，退款依照该价格',
  `amount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '最终实际支付金额，基本要等于discount_price*buynum',
  `buy_num` int(4) NULL DEFAULT 1 COMMENT '购买数量',
  `is_gift` tinyint(1) NULL DEFAULT 0 COMMENT '暂时可能不会用到，是否为赠送礼物，可以将加工费和运费也作为礼物一种',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for order_document
-- ----------------------------
DROP TABLE IF EXISTS `order_document`;
CREATE TABLE `order_document`  (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL COMMENT '用户id（多条记录）',
  `user_phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '下单用户手机号',
  `left_degree` int(4) NOT NULL DEFAULT 0 COMMENT '左眼度数',
  `right_degree` int(4) NOT NULL DEFAULT 0 COMMENT '右眼度数',
  `left_astigmia` int(4) NULL DEFAULT 0 COMMENT '左眼散光，0表示无散光',
  `right_astigmia` int(4) NULL DEFAULT 0 COMMENT '右眼散光，0表示无散光',
  `ipd` int(4) NOT NULL DEFAULT 55 COMMENT '双眼瞳距',
  `left_axial` int(4) NULL DEFAULT 0 COMMENT '左眼轴位',
  `right_axial` int(4) NULL DEFAULT 0 COMMENT '右眼轴位',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for order_log
-- ----------------------------
DROP TABLE IF EXISTS `order_log`;
CREATE TABLE `order_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '这张表看是否有必要',
  `order_no` bigint(20) NULL DEFAULT NULL COMMENT '订单号',
  `order_status` tinyint(4) NULL DEFAULT NULL COMMENT '订单状态（一个订单有一系列的流程中转记录）',
  `remark` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '当前流程的备注',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for order_main
-- ----------------------------
DROP TABLE IF EXISTS `order_main`;
CREATE TABLE `order_main`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_no` bigint(20) NOT NULL COMMENT '订单号',
  `user_id` int(12) NOT NULL COMMENT '用户id',
  `user_phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户手机号',
  `address_id` int(12) NULL DEFAULT NULL COMMENT '地址信息的Id',
  `document_id` int(12) NULL DEFAULT NULL COMMENT '下单档案的id',
  `amount` decimal(10, 2) NOT NULL COMMENT '订单原总金额',
  `discount_amout` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `actual_amount` decimal(10, 2) NOT NULL COMMENT '实际总金额',
  `pay_status` tinyint(4) NOT NULL DEFAULT 10 COMMENT '订单支付状态',
  `pay_transaction_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '订单交易流水号',
  `payment_time` timestamp(0) NULL DEFAULT NULL COMMENT '支付时间',
  `has_refund` tinyint(1) NULL DEFAULT 0 COMMENT '0-无退款，1-有退款',
  `settled` tinyint(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '是否已结算 0未结算  1结算',
  `discount_desc` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'json格式，汇总用户最终的优惠结果，也算是订单的备注信息。',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for order_make
-- ----------------------------
DROP TABLE IF EXISTS `order_make`;
CREATE TABLE `order_make`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_no` bigint(20) NULL DEFAULT NULL COMMENT '订单号',
  `make_status` tinyint(4) NULL DEFAULT 30 COMMENT '制作状态，  30-订单待加工  31-订单加工中  32-订单加工完成 ',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for order_marketing
-- ----------------------------
DROP TABLE IF EXISTS `order_marketing`;
CREATE TABLE `order_marketing`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_no` bigint(20) NOT NULL COMMENT '订单号',
  `activity_id` int(10) NOT NULL COMMENT '营销活动id',
  `activity_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '营销活动编码',
  `result_type` tinyint(4) NULL DEFAULT NULL COMMENT '活动结果类型，1-商品减免金额 2-赠送东西 3-减免其他费用',
  `minus_amout` decimal(10, 2) NULL DEFAULT NULL COMMENT '减免金额',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for order_post
-- ----------------------------
DROP TABLE IF EXISTS `order_post`;
CREATE TABLE `order_post`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_no` bigint(20) NULL DEFAULT NULL COMMENT '订单号',
  `post_status` tinyint(4) NULL DEFAULT 40 COMMENT '发货状态，40-订单待发货 41-订单发货中  42-订单发货成功 43-用户拒收  44-订单发货失败',
  `post_info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '收货人信息的json',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for order_refund
-- ----------------------------
DROP TABLE IF EXISTS `order_refund`;
CREATE TABLE `order_refund`  (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `refund_no` bigint(20) NOT NULL COMMENT '退款单号',
  `order_no` bigint(20) NOT NULL COMMENT '订单号',
  `user_id` int(12) NOT NULL COMMENT '用户id',
  `open_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '退款用户的openId',
  `refund_amout` decimal(10, 2) NOT NULL COMMENT '订单需要退款的金额，按 折扣比例计算',
  `refund_status` tinyint(4) NULL DEFAULT 20 COMMENT '退款状态，20-待退款 21-退款中 22-退款成功 23-退款失败',
  `refund_time` timestamp(0) NULL DEFAULT NULL COMMENT '退款时间',
  `refund_transaction_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '退款交易流水号',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for order_turning
-- ----------------------------
DROP TABLE IF EXISTS `order_turning`;
CREATE TABLE `order_turning`  (
  `id` bigint(20) NOT NULL,
  `order_no` bigint(20) NOT NULL COMMENT '订单号',
  `main_status` tinyint(4) NULL DEFAULT NULL COMMENT '订单主状态，和order_main保持一致，1-下单中 2-加工中 3-发货中 4-售后处理中 5-订单完成',
  `sub_status` tinyint(4) NULL DEFAULT NULL COMMENT '订单子状态，各个子流程的状态值，10-待支付 11-支付成功 12-支付失败 13-取消支付 及以后流程',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
