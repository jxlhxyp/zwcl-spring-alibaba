/*
 Navicat Premium Data Transfer

 Source Server         : 知道眼镜
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : 106.53.146.10:3306
 Source Schema         : glass_tools

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 13/01/2021 16:55:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for glass_article
-- ----------------------------
DROP TABLE IF EXISTS `glass_article`;
CREATE TABLE `glass_article`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文章标题',
  `category_id` int(4) NOT NULL COMMENT '所属类别',
  `content_type` tinyint(2) NOT NULL COMMENT '1，H5链接   2. 自定义内容',
  `article_url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文章链接',
  `article_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '文章内容',
  `article_tags` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文章标签，|分割的格式，根据这个搜索',
  `state` tinyint(2) NULL DEFAULT NULL COMMENT '投放状态(0:待发布,1已发布，2已下架)',
  `sort` int(10) NOT NULL DEFAULT 1 COMMENT '排序号',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '是否伪删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of glass_article
-- ----------------------------
INSERT INTO `glass_article` VALUES (1, '验光', 1, 1, 'https://mp.weixin.com/sadlasd/sdaf.html', NULL, '验光|正式', 1, 1, '2021-01-11 16:30:03', '2021-01-11 16:30:03', NULL, NULL, 0);
INSERT INTO `glass_article` VALUES (2, '镜框选型', 1, 1, 'https://mp.weixin.com/sadlasd/sdaf.html', NULL, '镜框|正式', 1, 2, '2021-01-11 16:30:27', '2021-01-11 16:30:27', NULL, NULL, 0);
INSERT INTO `glass_article` VALUES (3, '镜框选型2', 2, 1, 'https://mp.weixin.com/sadlasd/sdaf.html', NULL, '镜框|正式', 1, 1, '2021-01-11 16:31:32', '2021-01-11 16:30:35', NULL, NULL, 0);
INSERT INTO `glass_article` VALUES (4, '镜片选择', 1, 1, 'https://mp.weixin.com/sadlasd/sdaf.html', NULL, '镜片|正式', 1, 3, '2021-01-11 16:31:16', '2021-01-11 16:31:16', NULL, NULL, 0);

-- ----------------------------
-- Table structure for glass_article_category
-- ----------------------------
DROP TABLE IF EXISTS `glass_article_category`;
CREATE TABLE `glass_article_category`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类别编码',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类别名称',
  `sort` int(4) NULL DEFAULT NULL COMMENT '排序号',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '是否伪删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of glass_article_category
-- ----------------------------
INSERT INTO `glass_article_category` VALUES (1, NULL, '测试类别', 2, '2021-01-11 12:00:18', '2021-01-11 12:00:19', NULL, NULL, 0);
INSERT INTO `glass_article_category` VALUES (2, NULL, '正式类别', 1, '2021-01-11 13:49:30', '2021-01-11 13:49:30', NULL, NULL, 0);

-- ----------------------------
-- Table structure for glass_article_comment
-- ----------------------------
DROP TABLE IF EXISTS `glass_article_comment`;
CREATE TABLE `glass_article_comment`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `article_id` int(20) NOT NULL COMMENT '文章id',
  `user_id` int(20) NOT NULL COMMENT '用户id',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名称',
  `parent_id` int(20) NULL DEFAULT NULL COMMENT '针对哪条评论的id',
  `comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '评论内容',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '是否伪删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `article_id_Index`(`article_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of glass_article_comment
-- ----------------------------
INSERT INTO `glass_article_comment` VALUES (1, 1, 1, 'jxlhxyp', 0, 'sb', '2021-01-11 19:07:16', '2021-01-11 18:34:50', NULL, NULL, 0);
INSERT INTO `glass_article_comment` VALUES (2, 1, 2, 'jxlhxyp1', 0, 'sb', '2021-01-11 19:07:18', '2021-01-11 18:35:07', NULL, NULL, 0);
INSERT INTO `glass_article_comment` VALUES (3, 1, 3, 'jxlhxyp2', 0, 'sb', '2021-01-11 19:07:21', '2021-01-11 18:35:19', NULL, NULL, 0);
INSERT INTO `glass_article_comment` VALUES (4, 1, 4, 'jxlhxyp3', 1, 'sb', '2021-01-11 18:35:34', '2021-01-11 18:35:34', NULL, NULL, 0);
INSERT INTO `glass_article_comment` VALUES (5, 1, 4, 'jxlhxyp3', 2, 'sb', '2021-01-11 18:35:37', '2021-01-11 18:35:37', NULL, NULL, 0);
INSERT INTO `glass_article_comment` VALUES (6, 1, 5, 'jxlhxyp4', 4, 'sb', '2021-01-11 19:19:24', '2021-01-11 19:19:24', NULL, NULL, 0);
INSERT INTO `glass_article_comment` VALUES (7, 1, 6, 'jxlhxyp54', 1, 'sb', '2021-01-11 19:22:59', '2021-01-11 19:22:59', NULL, NULL, 0);
INSERT INTO `glass_article_comment` VALUES (8, 1, 7, 'jxlhxyp6', 1, 'sb', '2021-01-11 19:21:35', '2021-01-11 19:23:05', NULL, NULL, 0);

-- ----------------------------
-- Table structure for glass_article_read
-- ----------------------------
DROP TABLE IF EXISTS `glass_article_read`;
CREATE TABLE `glass_article_read`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `article_id` int(20) NOT NULL COMMENT '文章id',
  `read_num` int(12) NULL DEFAULT 0 COMMENT '阅读数',
  `like_num` int(12) NULL DEFAULT 0 COMMENT '点赞数',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '是否伪删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `article_id_Index`(`article_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of glass_article_read
-- ----------------------------
INSERT INTO `glass_article_read` VALUES (1, 1, 7, 3, '2021-01-11 17:51:42', '2021-01-11 17:49:38', NULL, NULL, 0);

-- ----------------------------
-- Table structure for glass_paper
-- ----------------------------
DROP TABLE IF EXISTS `glass_paper`;
CREATE TABLE `glass_paper`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '试卷名称',
  `description` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '试卷描述',
  `unify_notice` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '试卷得出结论后的统一性提示或者链接跳转',
  `paper_order` int(4) NULL DEFAULT NULL COMMENT '试卷顺序',
  `test_num` int(10) NULL DEFAULT 0 COMMENT '测试人次',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '版本',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of glass_paper
-- ----------------------------
INSERT INTO `glass_paper` VALUES (1, '验光选择', '简介：配眼镜去哪里验光比较好？\r\n有人说医院验光好，医院的医生验光更专业，也有人又说医院的医院是学眼病的不是学验光的，还是眼镜店的验光师验光更好，还有人说，甚至不需要验光直接测旧眼镜的数据就可以去配眼镜，我非常的迷茫不知道自己或者家人的情况最好去哪里验光。', NULL, 1, NULL, NULL, 0, '2021-01-13 11:19:32', NULL, NULL, 0, NULL);
INSERT INTO `glass_paper` VALUES (2, '镜片选择', '为什么在一家眼镜店配的国产镜片1000多，但是到另一家却说只是普通的镜片最多200多元，眼镜片的水分真的那么大吗？眼镜片的功能，性能有什么特殊的吗？为什么同样的镜片医院的贵很多？普通的国产镜片和进口镜片区别有多大，哪种才适合我？', NULL, 2, 0, NULL, 0, '2020-10-30 17:27:04', NULL, NULL, 0, NULL);
INSERT INTO `glass_paper` VALUES (3, 'test', 'test', '', 3, 0, NULL, 0, '2020-12-15 16:42:02', NULL, NULL, 1, NULL);
INSERT INTO `glass_paper` VALUES (4, 'test', 'test', '', 3, 0, NULL, 0, '2020-12-15 16:42:00', NULL, NULL, 1, NULL);
INSERT INTO `glass_paper` VALUES (5, 'test', 'test', '', 3, 0, NULL, 0, '2020-12-15 16:41:59', NULL, NULL, 1, NULL);
INSERT INTO `glass_paper` VALUES (6, 'test', 'test', '', 3, 0, NULL, 0, '2020-12-15 16:41:57', '2020-11-15 00:42:28', NULL, 1, NULL);
INSERT INTO `glass_paper` VALUES (7, 'test', 'test', '', 3, 0, NULL, 0, '2020-12-15 16:41:56', '2020-11-15 00:44:29', NULL, 1, NULL);
INSERT INTO `glass_paper` VALUES (8, 'test', 'test8888', '', 3, 0, NULL, 0, '2020-12-15 16:41:53', NULL, NULL, 1, NULL);
INSERT INTO `glass_paper` VALUES (9, 'test', 'test', '', 3, 0, NULL, 0, '2020-12-15 16:41:52', NULL, NULL, 1, NULL);
INSERT INTO `glass_paper` VALUES (10, 'test', 'test', '', 3, 0, NULL, 0, '2020-12-15 16:41:51', NULL, NULL, 1, NULL);
INSERT INTO `glass_paper` VALUES (11, 'test', 'test', '', 3, 0, NULL, 0, '2020-12-15 16:41:50', NULL, NULL, 1, NULL);

-- ----------------------------
-- Table structure for glass_paper_conclusion
-- ----------------------------
DROP TABLE IF EXISTS `glass_paper_conclusion`;
CREATE TABLE `glass_paper_conclusion`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paper_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '所属试卷',
  `type_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'single, complex，',
  `conclusion_content` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '结论内容',
  `conclusion_pic` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '结论的图片展示，逗号分隔',
  `conclusion_tags` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '结论代表的商品标签，逗号分隔。如：GlassFrame:钛架,国外|GlassLens:玻璃,加厚',
  `rule_content` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '得出某个结论的规则',
  `is_valid` tinyint(1) NULL DEFAULT 1,
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '版本',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '规则得出结论，先默认为相与' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of glass_paper_conclusion
-- ----------------------------
INSERT INTO `glass_paper_conclusion` VALUES (1, '1', '1_1', '在视力够用，度数变化不大的情况下，可以选择直接查询现在佩戴的眼镜数据，查询后直接配镜。没有记录数据的情况下，可以使用查片仪直接查询。', NULL, NULL, '[{\"problemId\":3,\"options\":[\"D\",\"E\",\"F\"]},{\"problemId\":4,\"options\":[\"B\"]},{\"problemId\":8,\"options\":[\"A\",\"C\"]},{\"problemId\":9,\"options\":[\"A\",\"B\"]}]', 1, NULL, 0, '2020-10-27 20:56:04', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (2, '1', '1_2', '选择眼科医院或者三甲医院检查眼睛的视功能异常或者眼病的情况后，再进行综合验光。注：一些专业的眼镜店也可以检查眼睛的视功能。但是一般不能检查眼病。', NULL, NULL, '[{\"problemId\":6,\"options\":[\"B\",\"C\",\"D\",\"E\",\"F\",\"G\"]}]', 1, NULL, 0, '2020-10-27 20:56:04', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (3, '1', '1_2', '选择眼科医院或者三甲医院检查眼睛的视功能异常或者眼病的情况后，再进行综合验光。注：一些专业的眼镜店也可以检查眼睛的视功能。但是一般不能检查眼病。', NULL, NULL, '[{\"problemId\":7,\"options\":[\"B\",\"C\",\"D\",\"E\"]}]', 1, NULL, 0, '2020-10-27 20:56:04', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (4, '1', '1_3', '视力下降很多的情况下，建议先去医院进行散瞳验光，然后隔天去验光专业的眼镜店进行综合验光，视力下降不多，可以在眼镜店通过物理散瞳方式，然后进行综合验光。注：对于处于发育阶段的孩童散瞳可以一定程度上排除假性近视。', NULL, NULL, '[{\"problemId\":3,\"options\":[\"A\",\"B\",\"C\"]},{\"problemId\":9,\"options\":[\"C\",\"D\",\"E\"]}]', 1, NULL, 0, '2020-10-27 20:56:04', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (5, '1', '1_4', '可以选择提供去综合验光的眼镜店进行验光。', NULL, NULL, '[{\"problemId\":3,\"options\":[\"D\",\"E\",\"F\"]},{\"problemId\":9,\"options\":[\"C\",\"D\",\"E\"]}]', 1, NULL, 0, '2020-10-27 20:56:04', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (6, '1', '1_4', '可以选择提供去综合验光的眼镜店进行验光。', NULL, NULL, '[{\"problemId\":3,\"options\":[\"A\",\"B\",\"C\"]},{\"problemId\":9,\"options\":[\"A\",\"B\"]}]', 1, NULL, 0, '2020-10-27 20:56:04', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (7, '2', '2_1', '推荐选择1.61折射率或者1.67折射率', NULL, NULL, '[{\"problemId\":12,\"options\":[\"A\"]},{\"problemId\":19,\"options\":[\"1.61\"]}]', 1, NULL, 0, '2020-11-02 16:38:13', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (8, '2', '2_2', '推荐选择1.67折射率或者1.74折射率', NULL, NULL, '[{\"problemId\":12,\"options\":[\"A\"]},{\"problemId\":19,\"options\":[\"1.67\"]}]', 1, NULL, 0, '2020-11-02 16:38:13', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (9, '2', '2_1', '推荐选择1.61折射率或者1.67折射率', NULL, NULL, '[{\"problemId\":17,\"options\":[\"A\",\"B\"]},{\"problemId\":19,\"options\":[\"1.61\"]}]', 1, NULL, 0, '2020-11-02 16:38:13', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (10, '2', '2_2', '推荐选择1.67折射率或者1.74折射率', NULL, NULL, '[{\"problemId\":17,\"options\":[\"A\",\"B\"]},{\"problemId\":19,\"options\":[\"1.67\"]}]', 1, NULL, 0, '2020-11-02 16:38:13', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (11, '2', '2_1', '推荐选择1.61折射率或者1.67折射率', NULL, NULL, '[{\"problemId\":18,\"options\":[\"D\",\"E\"]},{\"problemId\":19,\"options\":[\"1.61\"]}]', 1, NULL, 0, '2020-11-02 16:38:13', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (12, '2', '2_2', '推荐选择1.67折射率或者1.74折射率', NULL, NULL, '[{\"problemId\":18,\"options\":[\"D\",\"E\"]},{\"problemId\":19,\"options\":[\"1.67\"]}]', 1, NULL, 0, '2020-11-02 16:38:13', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (13, '2', '2_3', '防蓝光', NULL, NULL, '[{\"problemId\":14,\"options\":[\"A\"]}]', 1, NULL, 0, '2020-11-02 16:41:57', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (14, '2', '2_4', '通透性好，硬度高的玻璃材料', NULL, NULL, '[{\"problemId\":16,\"options\":[\"B\"]}]', 1, NULL, 0, '2020-11-02 16:43:15', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (15, '2', '2_5', '变色膜层', NULL, NULL, '[{\"problemId\":14,\"options\":[\"B\"]}]', 1, NULL, 0, '2020-11-02 16:44:14', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (16, '2', '2_6', '渐进', NULL, NULL, '[{\"problemId\":14,\"options\":[\"C\"]}]', 1, NULL, 0, '2020-11-02 16:45:05', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (17, '2', '2_7', '抗疲劳', NULL, NULL, '[{\"problemId\":14,\"options\":[\"D\"]}]', 1, NULL, 0, '2020-11-02 16:45:05', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (18, '2', '2_8', '法国、德国、日本进口品牌镜片', NULL, NULL, '[{\"problemId\":12,\"options\":[\"A\"]},{\"problemId\":13,\"options\":[\"A\"]},{\"problemId\":13,\"options\":[\"B\"]},{\"problemId\":13,\"options\":[\"C\"]}]', 1, NULL, 0, '2020-11-02 17:17:07', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (19, '2', '2_9', '国产树脂材料', NULL, NULL, '[{\"problemId\":12,\"options\":[\"B\"]},{\"problemId\":13,\"options\":[\"E\"]}]', 1, NULL, 0, '2020-11-02 17:20:06', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (20, '2', '2_10', 'MR-8材料', NULL, NULL, '[{\"problemId\":12,\"options\":[\"D\"]},{\"problemId\":13,\"options\":[\"D\"]},{\"problemId\":19,\"options\":[\"1.61\"]}]', 1, NULL, 0, '2020-11-02 17:20:40', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (21, '2', '2_11', 'MR-7材料', NULL, NULL, '[{\"problemId\":12,\"options\":[\"D\"]},{\"problemId\":13,\"options\":[\"D\"]},{\"problemId\":19,\"options\":[\"1.67\"]}]', 1, NULL, 0, '2020-11-02 17:45:46', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (22, '2', '2_12', 'MR-174材料', NULL, NULL, '[{\"problemId\":12,\"options\":[\"D\"]},{\"problemId\":13,\"options\":[\"D\"]},{\"problemId\":19,\"options\":[\"1.74\"]}]', 1, NULL, 0, '2020-11-02 17:46:18', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_conclusion` VALUES (23, '2', '2_13', '1.591PC材料', NULL, NULL, '[{\"problemId\":16,\"options\":[\"C\"]}]', 1, NULL, 0, '2020-11-02 18:02:14', NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for glass_paper_conflict_rule
-- ----------------------------
DROP TABLE IF EXISTS `glass_paper_conflict_rule`;
CREATE TABLE `glass_paper_conflict_rule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paper_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `rule_type` tinyint(2) NULL DEFAULT NULL COMMENT '00：默认为选择题冲突规则',
  `rule_content` varchar(3000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `is_valid` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '冲突规则，回头加工好存储在缓存' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for glass_paper_problem
-- ----------------------------
DROP TABLE IF EXISTS `glass_paper_problem`;
CREATE TABLE `glass_paper_problem`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paper_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '试卷Id',
  `problem_type` tinyint(2) NULL DEFAULT NULL COMMENT '11 单选题   12 多选题   21 滑块设值  31 填空  41 问答',
  `subject` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '题干',
  `definition` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '题目定义的json',
  `pic_urls` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '相关联的图片',
  `h5_urls` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '相关联的H5文章',
  `order_index` int(4) NULL DEFAULT NULL COMMENT '题目在试卷中的顺序',
  `is_valid` tinyint(1) NULL DEFAULT 1 COMMENT '题目是否有效',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '版本',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of glass_paper_problem
-- ----------------------------
INSERT INTO `glass_paper_problem` VALUES (3, '1', 11, '我的年龄', '[{\"option\":\"A\",\"content\":\"6岁以下\"},{\"option\":\"B\",\"content\":\"6至12岁\"},{\"option\":\"C\",\"content\":\"13至18岁\"},{\"option\":\"D\",\"content\":\"18至40岁\"},{\"option\":\"E\",\"content\":\"40至49岁\"},{\"option\":\"F\",\"content\":\"50以上\"}]', NULL, NULL, 1, 1, NULL, 0, '2020-10-27 20:56:29', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_problem` VALUES (4, '1', 11, '是否是第一次验光配镜', '[{\"option\":\"A\",\"content\":\"是的\"},{\"option\":\"B\",\"content\":\"不是\"},{\"option\":\"C\",\"content\":\"不是，但多年未戴眼镜了\"}]', NULL, NULL, 2, 1, NULL, 0, '2020-10-27 20:56:29', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_problem` VALUES (5, '1', 12, '我的眼睛屈光情况', '[{\"option\":\"A\",\"content\":\"近视\"},{\"option\":\"B\",\"content\":\"散光\"},{\"option\":\"C\",\"content\":\"远视\"},{\"option\":\"D\",\"content\":\"老花\"},{\"option\":\"E\",\"content\":\"不清楚、不确定\"}]', NULL, NULL, 3, 1, NULL, 0, '2020-10-27 20:56:29', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_problem` VALUES (6, '1', 12, '我的眼睛其他视功能状态', '[{\"option\":\"A\",\"content\":\"都在正常范围内\"},{\"option\":\"B\",\"content\":\"非常容易视疲劳\"},{\"option\":\"C\",\"content\":\"经常容易复视或重影\"},{\"option\":\"D\",\"content\":\"有斜视或有隐性斜视\"},{\"option\":\"E\",\"content\":\"有色弱或色盲\"},{\"option\":\"F\",\"content\":\"有弱视\"},{\"option\":\"G\",\"content\":\"有其他视功能问题\"},{\"option\":\"H\",\"content\":\"不确定\"}]', NULL, NULL, 4, 1, NULL, 0, '2020-10-27 20:56:29', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_problem` VALUES (7, '1', 12, '我的眼睛是否有其他眼病问题', '[{\"option\":\"A\",\"content\":\"没有任何眼病问题\"},{\"option\":\"B\",\"content\":\"高眼压、甚至青光眼\"},{\"option\":\"C\",\"content\":\"各类急慢性眼部炎症\"},{\"option\":\"D\",\"content\":\"白内障\"},{\"option\":\"E\",\"content\":\"其他眼病\"},{\"option\":\"F\",\"content\":\"不确定\"}]', NULL, NULL, 5, 1, NULL, 0, '2020-10-27 20:56:29', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_problem` VALUES (8, '1', 11, '我现在所佩戴眼镜的使用情况', '[{\"option\":\"A\",\"content\":\"多年未变，感觉眼镜度数还够用\"},{\"option\":\"B\",\"content\":\"感觉现在的眼镜度数不够用了\"},{\"option\":\"C\",\"content\":\"感觉是镜片旧了、花了导致模糊\"},{\"option\":\"D\",\"content\":\"其他\"}]', NULL, NULL, 6, 1, NULL, 0, '2020-10-27 20:56:29', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_problem` VALUES (9, '1', 12, '我的现在视力情况', '[{\"option\":\"A\",\"content\":\"视力有4.9及4.9以上\"},{\"option\":\"B\",\"content\":\"不清楚视力情况、但能满足正常的生活、工作、学习需要\"},{\"option\":\"C\",\"content\":\"感觉度数增加了，视力情况不能够满足正常生活、工作、学习需要\"},{\"option\":\"D\",\"content\":\"特别是晚上视力差了，感觉最近有散光了或者散光增加了\"},{\"option\":\"E\",\"content\":\"眼睛度数一直快速增加\"},{\"option\":\"F\",\"content\":\"其他\"}]', NULL, NULL, 7, 1, NULL, 0, '2020-10-27 20:56:29', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_problem` VALUES (10, '1', 12, '我的主要用眼距离状态', '[{\"option\":\"A\",\"content\":\"看近用的需求比较多（眼睛距离所视物50厘米以内）\"},{\"option\":\"B\",\"content\":\"看远用的需求比较多（眼睛距离所视物5米开外）\"},{\"option\":\"C\",\"content\":\"远近需求都差不多\"}]', NULL, NULL, 8, 0, NULL, 0, '2020-10-27 20:56:29', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_problem` VALUES (11, '1', 11, '我对现在自己戴的眼镜的数据的了解情况', '[{\"option\":\"A\",\"content\":\"没记录，不了解\"},{\"option\":\"B\",\"content\":\"记录了，非常详细\"},{\"option\":\"C\",\"content\":\"记录了，但是并不准确\"},{\"option\":\"D\",\"content\":\"只是大概记得\"}]', NULL, NULL, 9, 0, NULL, 0, '2020-10-27 20:56:29', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_problem` VALUES (12, '2', 11, '我希望镜片的重量', '[{\"option\":\"A\",\"content\":\"能够更轻一点，越轻越好\"},{\"option\":\"B\",\"content\":\"相对于轻，我更在乎镜片的价格\"},{\"option\":\"C\",\"content\":\"相对于轻，我更在乎镜片的其他性能\"},{\"option\":\"D\",\"content\":\"各方面适中就好\"}]', NULL, NULL, 1, 1, NULL, 0, '2020-10-30 17:32:02', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_problem` VALUES (13, '2', 12, '我希望镜片的更具备以下性能', '[{\"option\":\"A\",\"content\":\"韧性好、更耐摔\"},{\"option\":\"B\",\"content\":\"硬度高、更耐花\"},{\"option\":\"C\",\"content\":\"通透性更好，成像质量更好\"},{\"option\":\"D\",\"content\":\"各方面适中就好\"},{\"option\":\"E\",\"content\":\"相对于性能，我更在乎镜片的价格\"}]', NULL, NULL, 2, 1, NULL, 0, '2020-10-30 17:34:11', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_problem` VALUES (14, '2', 11, '我希望镜片的功能要求', '[{\"option\":\"A\",\"content\":\"有防疲劳防蓝光功能的，因为看手机电脑等电子产品多\"},{\"option\":\"B\",\"content\":\"能在太阳光下变色，外出游玩或者开车方便\"},{\"option\":\"C\",\"content\":\"年纪超过50岁了，希望镜片能够看远看近方便使用，不用来回摘戴\"},{\"option\":\"D\",\"content\":\"年级在35-50岁之间，看近用眼时非常容易疲劳\"},{\"option\":\"E\",\"content\":\"有基础功能，防紫外线防辐射的就行\"},{\"option\":\"F\",\"content\":\"没有这方面的概念\"}]', NULL, '关于防蓝光、变色、渐进镜片、防紫外线等镜片功能的文章', 3, 1, NULL, 0, '2020-10-30 17:35:59', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_problem` VALUES (15, '2', 11, '我换眼镜的频率', '[{\"option\":\"A\",\"content\":\"1年内\"},{\"option\":\"B\",\"content\":\"1年至2年\"},{\"option\":\"C\",\"content\":\"2至4年\"},{\"option\":\"D\",\"content\":\"4年以上\"}]', NULL, NULL, 4, 1, NULL, 0, '2020-10-30 17:43:45', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_problem` VALUES (16, '2', 11, '我常用镜片材料', '\r\n[{\"option\":\"A\",\"content\":\"树脂\"},{\"option\":\"B\",\"content\":\"玻璃\"},{\"option\":\"C\",\"content\":\"pc\"},{\"option\":\"D\",\"content\":\"其他\"},{\"option\":\"E\",\"content\":\"不确定\"}]', NULL, NULL, 5, 1, NULL, 0, '2020-10-30 17:45:01', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_problem` VALUES (17, '2', 11, '我的眼睛瞳距', '[{\"option\":\"A\",\"content\":\"52-55mm\"},{\"option\":\"B\",\"content\":\"56-59mm\"},{\"option\":\"C\",\"content\":\"60-62mm\"},{\"option\":\"D\",\"content\":\"63-65mm\"},{\"option\":\"E\",\"content\":\"66mm及以上\"},{\"option\":\"F\",\"content\":\"不确定\"}]', '有图', '如何自测眼镜瞳距', 6, 1, NULL, 0, '2020-10-30 17:47:00', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_problem` VALUES (18, '2', 11, '我戴的镜框一般大小', '[{\"option\":\"A\",\"content\":\"片宽45-48\"},{\"option\":\"B\",\"content\":\"片宽49-52\"},{\"option\":\"C\",\"content\":\"片宽 53-55\"},{\"option\":\"D\",\"content\":\"片宽56-60\"},{\"option\":\"E\",\"content\":\"片宽60以上\"},{\"option\":\"F\",\"content\":\"不确定\"}]', '有图', NULL, 7, 1, NULL, 0, '2020-10-30 17:49:33', NULL, NULL, NULL, 0);
INSERT INTO `glass_paper_problem` VALUES (19, '2', 21, '我的屈光数据大概（近视或者远视老花等数据），注：-3.00表示近视300度，+3.00表示远视或老花300度。', '[{\"subject\":\"左眼近视/远视\",\"key\":\"leftShortSighted\",\"content\":{\"min\":-20.00,\"max\":20.00,\"step\":0.75},\"valueShow\":\"block\"},{\"subject\":\"左眼散光\",\"key\":\"leftFlood\",\"content\":{\"min\":-8.00,\"max\":0,\"step\":0.25},\"valueShow\":\"precise\"},{\"subject\":\"右眼近视/远视\",\"key\":\"rightShortSighted\",\"content\":{\"min\":-20.00,\"max\":20.00,\"step\":0.75},\"valueShow\":\"block\"},{\"subject\":\"右眼散光\",\"key\":\"rightFlood\",\"content\":{\"min\":-8.00,\"max\":0,\"step\":0.25},\"valueShow\":\"precise\"}]', NULL, NULL, 8, 1, NULL, 0, '2020-11-01 13:40:39', NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for glass_test_record
-- ----------------------------
DROP TABLE IF EXISTS `glass_test_record`;
CREATE TABLE `glass_test_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paper_id` int(11) NOT NULL COMMENT '试卷id',
  `user_id` int(11) NOT NULL COMMENT '用户Id',
  `test_result` varchar(3000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '测试结果',
  `auto_conclusion` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '自动结论，存储一些id分割即可',
  `auto_tags` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '自动结论得出的商品标签',
  `manual_conclusion` varchar(1500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '人工结论',
  `test_time` datetime(0) NULL DEFAULT NULL COMMENT '测试时间',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户手机号',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `version` int(11) NOT NULL DEFAULT 0 COMMENT '版本',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `Index_User_Paper`(`paper_id`, `user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of glass_test_record
-- ----------------------------
INSERT INTO `glass_test_record` VALUES (1, 1, 4, '[{\"problemId\":3,\"problemType\":11,\"result\":[\"C\"]},{\"problemId\":4,\"problemType\":11,\"result\":[\"B\"]},{\"problemId\":5,\"problemType\":12,\"result\":[\"A\"]},{\"problemId\":6,\"problemType\":12,\"result\":[\"A\"]},{\"problemId\":7,\"problemType\":12,\"result\":[\"A\"]},{\"problemId\":8,\"problemType\":11,\"result\":[\"B\"]},{\"problemId\":9,\"problemType\":12,\"result\":[\"A\"]}]', '可以选择提供去综合验光的眼镜店进行验光。', NULL, NULL, '2021-01-13 11:08:20', NULL, NULL, 0, '2021-01-13 11:08:20', '2021-01-13 11:08:20', NULL, NULL, 0);
INSERT INTO `glass_test_record` VALUES (3, 1, 4, '[{\"problemId\":3,\"problemType\":11,\"result\":[\"C\"]},{\"problemId\":4,\"problemType\":11,\"result\":[\"B\"]},{\"problemId\":5,\"problemType\":12,\"result\":[\"A\"]},{\"problemId\":6,\"problemType\":12,\"result\":[\"A\"]},{\"problemId\":7,\"problemType\":12,\"result\":[\"A\"]},{\"problemId\":8,\"problemType\":11,\"result\":[\"B\"]},{\"problemId\":9,\"problemType\":12,\"result\":[\"A\"]}]', '可以选择提供去综合验光的眼镜店进行验光。', NULL, NULL, '2021-01-13 11:09:56', NULL, NULL, 0, '2021-01-13 11:09:56', '2021-01-13 11:09:56', NULL, NULL, 0);
INSERT INTO `glass_test_record` VALUES (4, 1, 4, '[{\"problemId\":3,\"problemType\":11,\"result\":[\"C\"]},{\"problemId\":4,\"problemType\":11,\"result\":[\"B\"]},{\"problemId\":5,\"problemType\":12,\"result\":[\"A\"]},{\"problemId\":6,\"problemType\":12,\"result\":[\"A\"]},{\"problemId\":7,\"problemType\":12,\"result\":[\"A\"]},{\"problemId\":8,\"problemType\":11,\"result\":[\"B\"]},{\"problemId\":9,\"problemType\":12,\"result\":[\"A\"]}]', '可以选择提供去综合验光的眼镜店进行验光。', NULL, NULL, '2021-01-13 11:11:42', NULL, NULL, 0, '2021-01-13 11:11:42', '2021-01-13 11:11:42', NULL, NULL, 0);
INSERT INTO `glass_test_record` VALUES (5, 1, 4, '[{\"problemId\":3,\"problemType\":11,\"result\":[\"C\"]},{\"problemId\":4,\"problemType\":11,\"result\":[\"B\"]},{\"problemId\":5,\"problemType\":12,\"result\":[\"A\"]},{\"problemId\":6,\"problemType\":12,\"result\":[\"A\"]},{\"problemId\":7,\"problemType\":12,\"result\":[\"A\"]},{\"problemId\":8,\"problemType\":11,\"result\":[\"B\"]},{\"problemId\":9,\"problemType\":12,\"result\":[\"A\"]}]', '可以选择提供去综合验光的眼镜店进行验光。', NULL, NULL, '2021-01-13 11:19:04', NULL, NULL, 0, '2021-01-13 11:19:04', '2021-01-13 11:19:04', NULL, NULL, 0);
INSERT INTO `glass_test_record` VALUES (6, 1, 4, '[{\"problemId\":3,\"problemType\":11,\"result\":[\"C\"]},{\"problemId\":4,\"problemType\":11,\"result\":[\"B\"]},{\"problemId\":5,\"problemType\":12,\"result\":[\"A\"]},{\"problemId\":6,\"problemType\":12,\"result\":[\"A\"]},{\"problemId\":7,\"problemType\":12,\"result\":[\"A\"]},{\"problemId\":8,\"problemType\":11,\"result\":[\"B\"]},{\"problemId\":9,\"problemType\":12,\"result\":[\"A\"]}]', '可以选择提供去综合验光的眼镜店进行验光。', NULL, NULL, '2021-01-13 11:19:09', NULL, NULL, 0, '2021-01-13 11:19:09', '2021-01-13 11:19:09', NULL, NULL, 0);

-- ----------------------------
-- Table structure for glass_user_info
-- ----------------------------
DROP TABLE IF EXISTS `glass_user_info`;
CREATE TABLE `glass_user_info`  (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '20',
  `user_id` int(20) NOT NULL,
  `eye_type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0正常，1近视，2散光',
  `left_short_sight` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '左眼近视度数',
  `right_short_sight` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '右眼近视度数',
  `left_flood` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '左眼散光度数',
  `right_flood` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '右眼散光度数',
  `ipd` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '瞳距',
  `pic_url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '上传的图片',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '是否伪删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of glass_user_info
-- ----------------------------
INSERT INTO `glass_user_info` VALUES (1, 1, 1, '-300', '-300', NULL, NULL, '52mm', 'http://cos.com/1.jpg', '2021-01-12 10:00:01', '2021-01-12 10:00:01', NULL, NULL, 0);
INSERT INTO `glass_user_info` VALUES (2, 1, 1, '-310', '-310', NULL, NULL, '52mm', 'http://cos.com/1.jpg', '2021-01-12 10:01:28', '2021-01-12 10:01:28', NULL, NULL, 0);
INSERT INTO `glass_user_info` VALUES (3, 1, 1, '-320', '-320', NULL, NULL, '52mm', 'http://cos.com/1.jpg', '2021-01-12 10:01:43', '2021-01-12 10:01:28', NULL, NULL, 0);
INSERT INTO `glass_user_info` VALUES (4, 1, 1, '-310', '-310', NULL, NULL, '52mm', 'http://cos.com/1.jpg', '2021-01-12 10:02:42', '2021-01-12 10:02:42', NULL, NULL, 0);

SET FOREIGN_KEY_CHECKS = 1;
