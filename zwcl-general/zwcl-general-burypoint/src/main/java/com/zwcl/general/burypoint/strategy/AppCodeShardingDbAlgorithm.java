package com.zwcl.general.burypoint.strategy;

import com.zwcl.common.core.enums.AppCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Slf4j
//@Component
public class AppCodeShardingDbAlgorithm implements PreciseShardingAlgorithm<String> {

    //找寻库
    @Override
    public String doSharding(Collection<String> collection, PreciseShardingValue<String> preciseShardingValue) {
        Integer no = AppCodeEnum.of(preciseShardingValue.getValue()).getNo()-1;
//        StringBuilder dateBaseName=new StringBuilder();
//        //拼接数据库
//        dateBaseName.append(preciseShardingValue.getLogicTableName())
//                .append("_").append(no);
//        log.info("分库的库名==========："+dateBaseName);
//        return dateBaseName.toString();
        return "ds"+no.toString();
    }
}
