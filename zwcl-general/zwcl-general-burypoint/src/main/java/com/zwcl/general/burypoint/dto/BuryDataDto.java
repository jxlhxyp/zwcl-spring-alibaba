package com.zwcl.general.burypoint.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * xyp
 * 前端的埋点实体
 * 认为其中的部分字段不需要
 * 还有很多字段不太明白含义
 * 参见：数据埋点样例的Json
 */
@Data
public class BuryDataDto {

    /**
     * 增加应用业务编码
     */
    @NotBlank(message = "应用编码不能为空")
    private String appCode;

    private String userToken;

    private String userId;

    //private String openId;

    //private String sessionId;

    /**
     * 页面地址
     */
    private String pages;

    /**
     * 访问区域
     */
    private String region;


    private String objName;

    /**
     * 当前页的配置
     */
    //private String pathQuery;

    private Integer mPositionId;

    private Integer mPlanId;

    private Integer actionId;

    private Integer position;

    private Integer eventTypeId;

    private String minaVersion;

    private String pagesTimestamp;

    //private String brand;

    private String phoneModel;

    private String weixinVersion;

    private String system;

    //private String platform;

    private String sdkVersion;

    //private String networkType;

    private Integer notifyId;

    private Integer fromId;

    private String entryId;

    private String pl;

    private Integer channel;

    private String location;

    /**
     * 可以不传
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

}

