package com.zwcl.general.burypoint.service.impl;

import com.zwcl.general.burypoint.entity.BuryData;
import com.zwcl.general.burypoint.service.SendBuryDataService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RabbitSendBuryDataServiceImpl implements SendBuryDataService {

    /**
     * 调用rabbit生产者，发送消息数据
     * @param dataList
     * @return
     */
    @Override
    public Boolean sendBuryDataMsg(List<BuryData> dataList) {
        return null;
    }
}
