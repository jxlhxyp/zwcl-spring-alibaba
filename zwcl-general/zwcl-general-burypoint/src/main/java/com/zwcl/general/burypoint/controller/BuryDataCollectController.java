package com.zwcl.general.burypoint.controller;

import com.zwcl.common.core.utils.BeanUtils;
import com.zwcl.common.core.utils.BeanUtilsEx;
import com.zwcl.general.burypoint.dto.BuryDataDto;
import com.zwcl.general.burypoint.entity.BuryData;
import com.zwcl.general.burypoint.service.SaveBuryDataService;
import com.zwcl.general.burypoint.service.SendBuryDataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RequestMapping("/collect")
@RestController
public class BuryDataCollectController {

    @Autowired
    private SendBuryDataService sendBuryDataService;

    @Autowired
    private SaveBuryDataService saveBuryDataService;

    /**
     * 可以批量收集埋点的数据
     * 策略：
     * 1. 前端点击按钮进行操作，实时传递埋点数据
     * 2. 非点击操作，停留页面，10条传递一次
     * 3. 用户离开页面或小程序，此时都积累了多少条就传递多少条
     * @param buryList
     * @return
     */
    @PostMapping("/async")
    public Boolean asyncCollectBuryData(@RequestBody List<BuryDataDto> buryList){
        //1. 根据业务编码，或者业务配置的topic等

        //2. 消息投递，最好可以兼容很多的操作
        //将消息发送到消息中间件，可以是rabbitMq，rokectMq
        //但是最好是kafka
        return true;
    }

    /**
     * 同步保存数据
     * @param buryList
     * @return
     */
    @PostMapping("/sync")
    public Boolean syncCollectBuryData(@RequestBody List<BuryDataDto> buryList){
        //TODO: 注意 ，使用BeanUtilsEx中的copyList会报错
        try {
            List<BuryData> dataList= BeanUtils.convertList(buryList,BuryData.class);
            //saveBuryDataService.saveBuryDataList(dataList);
            //TODO: 发现分库分表不支持上述一条语句的批量插入方法。
            saveBuryDataService.saveBuryDataBatch(dataList);
        }catch (Exception ex){
            log.info("批量插入日志失败："+ex.getMessage());
            return false;
        }
        return true;
    }

    @PostMapping("/batch")
    public Boolean batchCollect(@RequestBody List<BuryDataDto> buryList){
        List<BuryData> dataList= BeanUtils.convertList(buryList,BuryData.class);
        saveBuryDataService.saveBuryDataBatch(dataList);
        return true;
    }
}
