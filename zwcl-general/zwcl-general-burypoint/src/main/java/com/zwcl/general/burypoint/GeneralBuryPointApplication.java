package com.zwcl.general.burypoint;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author xyp
 * 通用数据埋点项目
 * 该项目只被前端调用，无需加feign api接口层
 * TODO:暂时无法启动，还差数据库没有配置
 * 该项目要注意以下几点：
 * 1. 不加限流
 * 2. 埋点接口写入通过kafka，异步解耦。先期可以用rabbitMq
 * 3. 日志消息消费后，持久化需要分库分表，或者先期采用MongoDB
 * 4. 针对业务进行配置，不同的业务，分不同的消息topic，分不同的库，里面再分表
 * 5. 为扩展方面，针对持久化层，最好采用工厂方法，应对将来的变化
 */
@SpringCloudApplication
@MapperScan("com.zwcl.general.burypoint.mapper")
@EnableDiscoveryClient
//采用异步方法，要加上该注解
@EnableAsync
public class GeneralBuryPointApplication {
    public static void main(String[] args){
        SpringApplication.run(GeneralBuryPointApplication.class, args);
        System.out.println("启动成功");
    }
}
