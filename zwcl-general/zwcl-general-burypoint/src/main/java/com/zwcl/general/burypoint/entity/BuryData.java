package com.zwcl.general.burypoint.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zwcl.common.core.domain.entity.BaseEntity;
import com.zwcl.common.core.validator.groups.Update;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 表还没有创建
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName("bury_data")
public class BuryData extends Model<BuryData> {
    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 增加应用业务编码
     */
    private String appCode;

    private String userToken;

    private String userId;

    //private String openId;

    //private String sessionId;

    /**
     * 页面地址
     */
    private String pages;

    /**
     * 访问区域
     */
    private String region;


    private String objName;

    /**
     * 当前页的配置
     */
    //private String pathQuery;

    private Integer mPositionId;

    private Integer mPlanId;

    private Integer actionId;

    private Integer position;

    private Integer eventTypeId;

    private String minaVersion;

    private String pagesTimestamp;

    //private String brand;

    private String phoneModel;

    private String weixinVersion;

    private String system;

    //private String platform;

    private String sdkVersion;

    //private String networkType;

    private Integer notifyId;

    private Integer fromId;

    private String entryId;

    private String pl;

    private Integer channel;

    private String location;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
