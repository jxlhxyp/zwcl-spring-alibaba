package com.zwcl.general.burypoint.dto;

import lombok.Data;

/**
 * 现公司的埋点日志格式
 */
@Data
public class GdActionLog {
    /**
     * 页面地址
     */
    private String pages;

    /**
     * 访问区域
     */
    private String region;


    private String objName;

    /**
     * 当前页的配置
     */
    private String pathQuery;

    private Integer mPositionId;

    private Integer mPlanId;

    private Integer actionId;

    private Integer position;

    private Integer eventTypeId;

    private String minaVersion;

    private String pagesTimestamp;

    private String brand;

    private String phoneModel;

    private String weixinVersion;

    private String system;

    private String platform;

    private String sdkVersion;

    private String networkType;

    private Integer notifyId;

    private Integer fromId;

    private String entryId;

    private String pl;

    private Integer channel;

    private String location;

    private String userToken;

    private String userId;

    private String openId;

    private String sessionId;
}
