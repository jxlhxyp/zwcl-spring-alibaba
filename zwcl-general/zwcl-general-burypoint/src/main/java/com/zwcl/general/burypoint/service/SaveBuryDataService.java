package com.zwcl.general.burypoint.service;

import com.zwcl.common.mybatis.service.BaseService;
import com.zwcl.general.burypoint.entity.BuryData;

import java.util.List;

/**
 * 保存埋点日志的消息
 */
public interface SaveBuryDataService extends BaseService<BuryData> {

    Boolean saveBuryDataList(List<BuryData> dataList);

    Boolean saveBuryDataBatch(List<BuryData> dataList);
}
