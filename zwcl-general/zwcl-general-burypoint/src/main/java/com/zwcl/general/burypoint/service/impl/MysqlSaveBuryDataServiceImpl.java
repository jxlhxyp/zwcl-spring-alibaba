package com.zwcl.general.burypoint.service.impl;

import com.zwcl.common.mybatis.service.impl.BaseServiceImpl;
import com.zwcl.general.burypoint.entity.BuryData;
import com.zwcl.general.burypoint.mapper.BuryDataMapper;
import com.zwcl.general.burypoint.service.SaveBuryDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 异步批量保存埋点数据，@Aysnc
 * 根据appCode进行分库，根据时间的月份进行分表
 * TODO: @Async没有实现完成
 */
@Service
public class MysqlSaveBuryDataServiceImpl extends BaseServiceImpl<BuryDataMapper, BuryData> implements SaveBuryDataService {

    @Autowired
    private BuryDataMapper buryDataMapper;
    /**
     * 异步，批量保存，不加事务
     * @param dataList
     * @return
     */
    @Override
    @Async
    public Boolean saveBuryDataList(List<BuryData> dataList) {
        return buryDataMapper.saveBatch(dataList);
    }


    @Override
    @Async
    public Boolean saveBuryDataBatch(List<BuryData> dataList) {
        return this.saveBatch(dataList);
    }
}
