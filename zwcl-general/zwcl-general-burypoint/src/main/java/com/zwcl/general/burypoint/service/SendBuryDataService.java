package com.zwcl.general.burypoint.service;

import com.zwcl.general.burypoint.entity.BuryData;

import java.util.List;

/**
 * 发送埋点日志的消息
 */
public interface SendBuryDataService {

    Boolean sendBuryDataMsg(List<BuryData> dataList);
}
