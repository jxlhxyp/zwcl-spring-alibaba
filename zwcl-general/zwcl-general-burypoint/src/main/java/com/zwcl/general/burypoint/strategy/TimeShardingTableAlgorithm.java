package com.zwcl.general.burypoint.strategy;

import lombok.extern.slf4j.Slf4j;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Date;

/**
 * @Description 月份分表查询策略
 * @Author tanljs
 * @Date 2020/1/6
 * @Email Alex.tan@wetax.com.cn
 */
@Slf4j
//@Component
public class TimeShardingTableAlgorithm implements PreciseShardingAlgorithm<LocalDateTime> {
    @Override
    public String doSharding(Collection<String> collection, PreciseShardingValue<LocalDateTime> preciseShardingValue) {
        StringBuffer tableName = new StringBuffer();
        //DateFormat sdf = new SimpleDateFormat("yyyyMM");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMM");
        String createMonth = dateTimeFormatter.format(preciseShardingValue.getValue());
        tableName.append(preciseShardingValue.getLogicTableName())
                .append("_").append(createMonth);
        log.info("分库的数据表名=============" + tableName.toString());
        return tableName.toString();
    }
}
