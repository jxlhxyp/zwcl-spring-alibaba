package com.zwcl.general.burypoint.mapper;

import com.zwcl.general.burypoint.entity.BuryData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xieyongping
 * @since 2021-01-08
 */
public interface BuryDataMapper extends BaseMapper<BuryData> {

    Boolean saveBatch(List<BuryData> list);
}
