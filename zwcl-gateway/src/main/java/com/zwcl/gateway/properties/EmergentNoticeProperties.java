package com.zwcl.gateway.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 紧急异常提示
 *
 * @author xyp
 */
@Data
@Configuration
@RefreshScope // 从配置中心动态获取并刷新配置
@ConfigurationProperties(prefix = "emergent")
public class EmergentNoticeProperties {

    private Boolean emergentFlag;

    private String msg;

    private Integer minute;
}

