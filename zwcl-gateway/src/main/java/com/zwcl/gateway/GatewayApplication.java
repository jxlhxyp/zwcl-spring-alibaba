package com.zwcl.gateway;

import com.zwcl.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * 网关启动程序
 * @author xyp
 * spring cloud 加swagger引入公共包，使用@EnableCustomSwagger2
 * swagger访问路径：http://localhost:8880/swagger-ui/index.html
 * 在上述路径的右上角进行切换，切换时，也要确保访问的微服务是启动的，否则也无法访问
 * 并且，gateway的nacos配置中，要忽略/v2/api-docs 路径
 */
@EnableDiscoveryClient
//TODO:看看是使用SpringCloudApplication
@SpringCloudApplication
@EnableCustomSwagger2
@RefreshScope
public class GatewayApplication {
    public static void main(String[] args){
        SpringApplication.run(GatewayApplication.class, args);
        System.out.println("启动成功");
    }
}

