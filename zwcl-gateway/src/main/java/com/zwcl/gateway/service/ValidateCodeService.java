package com.zwcl.gateway.service;

import com.zwcl.common.core.domain.entity.ApiResult;
import com.zwcl.common.core.exception.BusinessException;

import java.io.IOException;

/**
 * 验证码处理
 * 
 * @author xyp
 */
public interface ValidateCodeService
{
    /**
     * 生成验证码
     */
    ApiResult createCapcha() throws IOException, BusinessException;

    /**
     * 校验验证码
     */
    void checkCapcha(String key, String value) throws BusinessException;
}
